/**
 * BibSonomy-BibTeX-Parser - BibTeX Parser from http://www-plan.cs.colorado.edu/henkel/stuff/javabib/
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.bibtex.parser;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.PersonNameUtils;
import org.bibsonomy.util.StringUtils;
import org.junit.Test;

import bibtex.expansions.CrossReferenceExpansionException;
import bibtex.expansions.ExpansionException;
import bibtex.expansions.PersonListParserException;

/**
 * @author rja
 */
public class SimpleBibTeXParserTest {

	private static final String entry1 = "@book{behrendt2007,\n" +
	"title = {Web 2.0 },\n" + 
	"author = {Jens Behrendt and Klaus Zeppenfeld},\n" + 
	"publisher = {Springer},\n" + 
	"year = 2007,\n" + 
	"url = {http://ftubhan.tugraz.at/han/ZDB-2-STI/www.springerlink.com/content/wk5317/},\n" + 
	"biburl = {http://www.bibsonomy.org/bibtex/22407a08751c316c63686d37228a25b3d/diam_eter},\n" + 
	"keywords = {ISR_07}\n" +
	"}";

	/*
	 * FIXME:
	 * 
	 * author = {{Boss and Friends, Inc.}}
	 * currently fails, since PersonNameUtils uses "," as author delimiter ...
	 */
	private static final String entry2 = "@article{foo,\n" +
	"title = {Foo Barness},\n" +
	"author = {M. Mustermann and Navarro Bullock, Beate and von der Schmidt, Alex and John Chris Smith and {Long Company Name, Inc.}}}";

	private static final String entry3 = "@phdthesis{david2007domain,\n" + 
	"address={Saarbrücken},\n" + 
	"author={Sánchez, David and {Universitat Polit{226}ecnica de Catalunya}},\n" + 
	"isbn={9783836470698 3836470691},\n" + 
	"pages={--},\n" + 
	"publisher={VDM Verlag Dr. Müller}, refid={426144281},\n" + 
	"title={Domain ontology learning from the web an unsupervised, automatic and domain independent approach},\n" + 
	"year={2007},\n" + 
	"url = {http://www.worldcat.org/title/domain-ontology-learning-from-the-web-an-unsupervised-automatic-and-domain-independent-approach/oclc/426144281&referer=brief_results}\n" + 
	"}";

	@Test
	public void testParseBibTeX1() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();
		final BibTex bibtex = parser.parseBibTeX(entry1);

		assertEquals("Web 2.0 ", bibtex.getTitle());
		assertEquals("Behrendt, Jens", bibtex.getAuthor().get(0).toString());
		assertEquals("Zeppenfeld, Klaus", bibtex.getAuthor().get(1).toString()); 
		assertEquals("book", bibtex.getEntrytype());
		assertEquals("behrendt2007", bibtex.getBibtexKey());
		assertEquals("Springer", bibtex.getPublisher());
		assertEquals("2007", bibtex.getYear());
		assertEquals("http://ftubhan.tugraz.at/han/ZDB-2-STI/www.springerlink.com/content/wk5317/", bibtex.getUrl());
		assertEquals("ISR_07", bibtex.getMiscField("keywords"));
		assertEquals("http://www.bibsonomy.org/bibtex/22407a08751c316c63686d37228a25b3d/diam_eter", bibtex.getMiscField("biburl"));
	}

	@Test
	public void testParseBibTeX2() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();
		final BibTex bibtex = parser.parseBibTeX(entry2);

		assertEquals("Foo Barness", bibtex.getTitle());
		assertEquals("Mustermann, M. and Navarro Bullock, Beate and von der Schmidt, Alex and Smith, John Chris and {Long Company Name, Inc.}", PersonNameUtils.serializePersonNames(bibtex.getAuthor()));
		final List<PersonName> authors = bibtex.getAuthor();
		assertEquals("Mustermann", authors.get(0).getLastName());
		assertEquals("Navarro Bullock", authors.get(1).getLastName());
		assertEquals("von der Schmidt", authors.get(2).getLastName());
		assertEquals("Smith", authors.get(3).getLastName());
		assertEquals("{Long Company Name, Inc.}", authors.get(4).getLastName());
		assertEquals("article", bibtex.getEntrytype());
		assertEquals("foo", bibtex.getBibtexKey());
	}

	@Test
	public void testParse3() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();
		final String foo = 
			"@article{foo,\n" +
			"author = {{Hartmann}, L. and {Burkert}, A.},\n" +
			"title = {Hallo}\n}";
		final BibTex bibtex = parser.parseBibTeX(foo);

		assertEquals("Hallo", bibtex.getTitle());
		//		assertEquals("L. {Hartmann} and A. {Burkert}", bibtex.getAuthor()); TODO: correct?!?
		assertEquals("article", bibtex.getEntrytype());
		assertEquals("foo", bibtex.getBibtexKey());
	}

	@Test
	public void testParseBibTe32() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();
		final BibTex bibtex = parser.parseBibTeX(entry3);

		assertEquals("Domain ontology learning from the web an unsupervised, automatic and domain independent approach", bibtex.getTitle());
		assertEquals("phdthesis", bibtex.getEntrytype());
		assertEquals("david2007domain", bibtex.getBibtexKey());
		assertEquals("Sánchez, David and {Universitat Polit{226}ecnica de Catalunya}", PersonNameUtils.serializePersonNames(bibtex.getAuthor()));
	}

	/**
	 * 
	 * Note: this has changed in July 2010!
	 * 
	 * Currently, we normalize author names, i.e., 
	 * 
	 * Knuth, D.E.
	 * 
	 * becomes
	 * 
	 * D.E. Knuth. 
	 * 
	 * In principle, this is bad, because it breaks names like
	 * 
	 * Vander Wal, Martin
	 * 
	 * (BibTeX then thinks "Vander" is a second surname).
	 * 
	 * Nevertheless, we document this "feature" here because we can't just 
	 * change it without changing other methods (like author handling).
	 * @throws Exception 
	 */
	@Test
	public void testAuthorNormalization() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();

		final String author = "Knuth, D.E. and Max {Well, Jr.} and {Foo, Sr.}, Bar and Balby Marinho, Leandro";
		
		final BibTex parsedBibTeX = parser.parseBibTeX(
				"@article{foo,\n" +
				"  author = {" + author + "}\n" + 
				"}"
		);

		assertEquals("Knuth, D.E.", parsedBibTeX.getAuthor().get(0).toString());
		assertEquals("Max", parsedBibTeX.getAuthor().get(1).getFirstName());
		assertEquals("{Well, Jr.}", parsedBibTeX.getAuthor().get(1).getLastName());
		assertEquals("Bar", parsedBibTeX.getAuthor().get(2).getFirstName());
		assertEquals("{Foo, Sr.}", parsedBibTeX.getAuthor().get(2).getLastName());
		assertEquals("Leandro", parsedBibTeX.getAuthor().get(3).getFirstName());
		assertEquals("Balby Marinho", parsedBibTeX.getAuthor().get(3).getLastName());

//		System.out.println(BibTexUtils.toBibtexString(parsedBibTeX));
//		System.out.println(PersonNameUtils.discoverPersonNames(author));
	}

	@Test
	public void testAuthorNormalization2() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();

		final String author = "Foo, Bar,";
		
		final BibTex parsedBibTeX = parser.parseBibTeX(
				"@article{foo,\n" +
				"  author = {" + author + "}\n" + 
				"}"
		);
		
		final ExpansionException warning = parser.getWarnings().get(0);
		assertEquals(PersonListParserException.class, warning.getClass());
		assertEquals("Name ends with comma: 'Foo, Bar,' - in 'foo'", warning.getMessage());
		

	}
	
	/**
	 * We disabled month normalization, this is documented here.
	 * 
	 * Why did we disable it? Otherwise, a month like "jun" would be normalized
	 * to "June" and using this with BibTeX destroys I18N! (there are some 
	 * BibTeX styles, which can substitute "jun" by the correct word, depending
	 * on the language you have set for your document. This works only with the
	 * abbreviations!).
	 */
	@Test
	public void testMonthNormalization() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();

		final BibTex parsedBibTeX = parser.parseBibTeX(
				"@article{foo,\n" +
				"  month = jun\n" + 
				"}"
		);

		assertEquals("jun", parsedBibTeX.getMonth());
	}

	@Test
	public void testMacroExpansion() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();

		final BibTex parsedBibTeX = parser.parseBibTeX(
				"@string{AW = \"Addison--Wesley Publishing Company\"}\n" +
				"@article{foo,\n" +
				"  month = jun,\n" + 
				"  publisher = AW,\n" +
				"  journal = jacm,\n" +
				"  confmonth = {30~} # jan # {~-- 2~} # feb,\n" + 
				"  nonstandardfield = AW # \" foo\"\n" + 
				"}"
		);
		/*
		 * defined at the beginning
		 */
		assertEquals("Addison--Wesley Publishing Company", parsedBibTeX.getPublisher());
		/*
		 * predefined in BibTeX styles
		 */
		assertEquals("Journal of the ACM", parsedBibTeX.getJournal());
	}

	/**
	 * Test if crossref expansion works
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCrossrefExpansion() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();

		final List<BibTex> parsedBibTeX = parser.parseBibTeXs(
				"@proceedings{iswc,\n" +
				"   booktitle = \"ISWC\",\n" +
				"   editor = \"John Doe\",\n" +
				"   year = 2222,\n" +
				" }\n" +
				"@inproceedings{foo,\n" +
				"  month = jun,\n" + 
				"  crossref = iswc,\n" +
				"  title = \"Cool paper\",\n" +
				"  author = \"Bit Bucket\"\n" + 
				"}"
		);
		assertEquals(Arrays.asList(new PersonName("John", "Doe")), parsedBibTeX.get(0).getEditor());
		assertEquals("2222", parsedBibTeX.get(0).getYear());
		assertEquals(Arrays.asList(new PersonName("Bit", "Bucket")), parsedBibTeX.get(1).getAuthor());
	}

	/**
	 * Test if one entry is parsed even when crossref entry is missing. 
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCrossrefExpansionMissingCrossref() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();

		final BibTex parsedBibTeX = parser.parseBibTeX(
				"@inproceedings{foo,\n" +
				"  month = jun,\n" + 
				"  crossref = iswc,\n" +
				"  title = \"Cool paper\",\n" +
				"  author = \"Bit Bucket\",\n" + 
				"}"
		);
		assertEquals(Arrays.asList(new PersonName("Bit", "Bucket")), parsedBibTeX.getAuthor());
	}
	
	/**
	 * Test if several entries are parsed and warnings are added.  
	 * 
	 * @throws Exception
	 */
	@Test
	public void testCrossrefExpansionMissingCrossref2() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();

		final List<BibTex> parsedBibTeX = parser.parseBibTeXs(getTestFile("test2.bib"));

		assertEquals(2, parsedBibTeX.size());

		assertEquals("2014", parsedBibTeX.get(0).getYear());
		assertEquals("1145--1154", parsedBibTeX.get(0).getPages());

		assertEquals("2014", parsedBibTeX.get(1).getYear());
		assertEquals("2065--2074", parsedBibTeX.get(1).getPages());

		// We have two entries, both with missing crossrefs.
		assertEquals(2, parser.getWarnings().size());

		assertEquals(CrossReferenceExpansionException.class, parser.getWarnings().get(0).getClass());

		final CrossReferenceExpansionException firstWarning = (CrossReferenceExpansionException) parser.getWarnings().get(0);

		assertEquals("crossref key not found", firstWarning.getMessage());
		assertEquals("DBLP:conf/acl/PickhardtGKWSS14", firstWarning.getEntryKey());
		assertEquals("dblp:conf/acl/2014-1", firstWarning.getCrossrefKey());
	}
	
	@Test
	public void testFile1() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();
		final BibTex parsedBibTeX = parser.parseBibTeX(getTestFile("test1.bib"));
		/*
		 * defined at the beginning
		 */
		assertEquals("Australian Comput. Soc.", parsedBibTeX.getPublisher());
		/*
		 * concatenated bibtex field
		 */
		assertEquals("{30~}#jan#{~-- 2~}#feb", parsedBibTeX.getMiscField("confmonth"));
		/*
		 * After 
		 *   serialize -> parse -> serialize
		 * we must get the same BibTeX string
		 * 
		 */

		final String s = BibTexUtils.toBibtexString(parsedBibTeX, BibTexUtils.SERIALIZE_BIBTEX_OPTION_PLAIN_MISCFIELD);
		final BibTex sp = parser.parseBibTeX(s);
		final String sp2 = BibTexUtils.toBibtexString(sp, BibTexUtils.SERIALIZE_BIBTEX_OPTION_PLAIN_MISCFIELD);
		assertEquals(s, sp2);
	}

	@Test
	public void testHiggsPaper() throws Exception {
		final SimpleBibTeXParser parser = new SimpleBibTeXParser();
		final BibTex parsedBibteX = parser.parseBibTeX(getTestFile("higgs.bib"));
		assertEquals(3025, parsedBibteX.getAuthor().size());
	}

	private static String getTestFile(final String filename) throws IOException {
		final BufferedReader stream = new BufferedReader(new InputStreamReader(SimpleBibTeXParserTest.class.getClassLoader().getResourceAsStream(filename), StringUtils.CHARSET_UTF_8));
		final StringBuilder buf = new StringBuilder();
		String line;
		while ((line = stream.readLine()) != null) {
			buf.append(line + "\n");
		}
		stream.close();
		return buf.toString();
	}

}
