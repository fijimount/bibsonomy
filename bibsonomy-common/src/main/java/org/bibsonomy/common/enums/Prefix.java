package org.bibsonomy.common.enums;

/**
 * all alpha numeric prefixes possible for selecting items
 *
 * @author dzo
 */
public enum Prefix {
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	NUMBER,
	OTHER
}
