/**
 * BibSonomy-Common - Common things (e.g., exceptions, enums, utils, etc.)
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.common.enums;

/**
 * Defines sorting orders - to be used in conjunction with a sort key
 * 
 * @author Dominik Benz
 * @see org.bibsonomy.common.enums.SortKey
 */
public enum SortOrder {
	/** ascending order */
	ASC,
	/** descending order*/
	DESC;

	/**
	 * Retrieve SortDirection by name
	 *
	 * @param name	"asc" or "desc"
	 *
	 * @return the corresponding SortDirection enum, returns DESC if no match found
	 */
	public static SortOrder getByName(String name) {
		try {
			return SortOrder.valueOf(name.toUpperCase());
		} catch (NullPointerException | IllegalArgumentException np) {
			return DESC;
		}
	}
}