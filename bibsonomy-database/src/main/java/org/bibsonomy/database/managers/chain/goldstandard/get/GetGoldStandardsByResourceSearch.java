/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.managers.chain.goldstandard.get;

import java.util.List;

import org.bibsonomy.database.common.DBSession;
import org.bibsonomy.database.managers.chain.goldstandard.GoldStandardChainElement;
import org.bibsonomy.database.managers.chain.util.QueryAdapter;
import org.bibsonomy.services.searcher.PostSearchQuery;
import org.bibsonomy.model.GoldStandard;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.query.PostQuery;

/**
 * @author dzo
 * @param <RR> 
 * @param <R>
 */
public class GetGoldStandardsByResourceSearch<RR extends Resource, R extends Resource & GoldStandard<RR>> extends GoldStandardChainElement<RR, R> {

	@Override
	protected List<Post<R>> handle(final QueryAdapter<PostQuery<R>> param, final DBSession session) {
		final PostSearchQuery<?> searchQuery;
        // TODO is there a better way to not lose PostSearchQuery fields like year or systags?
        if (param.getQuery() instanceof PostSearchQuery<?>) {
            searchQuery = (PostSearchQuery<?>) param.getQuery();
        } else {
            searchQuery = new PostSearchQuery<>(param.getQuery());
        }
		final User loggedinUser = param.getLoggedinUser();
		return this.databaseManager.getSearch().getPosts(loggedinUser, searchQuery);
	}

	@Override
	protected boolean canHandle(QueryAdapter<PostQuery<R>> param) {
		return true;
	}
}
