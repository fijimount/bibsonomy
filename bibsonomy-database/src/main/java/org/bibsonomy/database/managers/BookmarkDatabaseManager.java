/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.managers;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.List;
import java.util.Set;

import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.FilterEntity;
import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.common.information.JobInformation;
import org.bibsonomy.database.common.DBSession;
import org.bibsonomy.database.params.BookmarkParam;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.User;

/**
 * Used to CRUD bookmarks from the database.
 * 
 * @author Miranda Grahl
 * @author Jens Illig
 * @author Christian Schenk
 * @author Daniel Zoller
 */
public class BookmarkDatabaseManager extends PostDatabaseManager<Bookmark, BookmarkParam> {
	private static final BookmarkDatabaseManager singleton = new BookmarkDatabaseManager();

	private static final HashID[] hashRange = { HashID.SIM_HASH0 };

	/**
	 * @return BookmarkDatabaseManager
	 */
	public static BookmarkDatabaseManager getInstance() {
		return singleton;
	}

	private BookmarkDatabaseManager() {
	}

	@Override
	protected List<Post<Bookmark>> getPostsForHomepage(final BookmarkParam param, final DBSession session) {
		final Set<Filter> filters = param.getFilters();
		if (present(filters)) {
			if (filters.contains(FilterEntity.UNFILTERED)) {
				return this.postList("getBookmarkForHomepageUnfiltered", param, session);
			}
		}

		return super.getPostsForHomepage(param, session);
	}

	@Override
	public List<Post<Bookmark>> getPostsFromClipboardForUser(final String loginUser, final int limit, final int offset, final DBSession session) {
		throw new UnsupportedOperationException("not available for bookmarks");
	}

	@Override
	protected void checkPost(final Post<Bookmark> post, final DBSession session) {
		// nop
	}

	@Override
	protected List<JobInformation> onPostInsert(final Post<Bookmark> post, final User loggedinUser, final DBSession session) {
		return this.plugins.onBookmarkInsert(post, loggedinUser, session);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bibsonomy.database.managers.PostDatabaseManager#onPostUpdate(java
	 * .lang.Integer, java.lang.Integer, org.bibsonomy.database.util.DBSession)
	 */
	@Override
	protected void onPostUpdate(final int oldContentId, final int newContentId, final DBSession session) {
		this.plugins.onBookmarkUpdate(oldContentId, newContentId, session);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.database.managers.PostDatabaseManager#onPostDelete(java
	 * .lang.Integer, org.bibsonomy.database.util.DBSession)
	 */
	@Override
	protected void onPostDelete(final int contentId, final DBSession session) {
		this.plugins.onBookmarkDelete(contentId, session);
	}
	
	/* (non-Javadoc)
	 * @see org.bibsonomy.database.managers.PostDatabaseManager#onMassUpdate(java.lang.String, int)
	 */
	@Override
	protected void onPostMassUpdate(String userName, int groupId, DBSession session) {
		this.plugins.onBookmarkMassUpdate(userName, groupId, session);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bibsonomy.database.managers.PostDatabaseManager#getHashRange()
	 */
	@Override
	protected HashID[] getHashRange() {
		return hashRange;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.bibsonomy.database.managers.PostDatabaseManager#createInsertParam(org
	 * .bibsonomy.model.Post, org.bibsonomy.database.util.DBSession)
	 */
	@Override
	protected BookmarkParam createInsertParam(final Post<? extends Bookmark> post) {
		final BookmarkParam insert = this.getNewParam();

		insert.setResource(post.getResource());
		insert.setDate(post.getDate());
		insert.setChangeDate(post.getChangeDate());
		insert.setRequestedContentId(post.getContentId());
		insert.setHash(post.getResource().getIntraHash());
		insert.setDescription(post.getDescription());
		insert.setUserName(post.getUser().getName());
		insert.setUrl(post.getResource().getUrl());

		// in field group in table bookmark, insert the id for PUBLIC, PRIVATE
		// or the id of the FIRST group in list
		final int groupId = post.getGroups().iterator().next().getGroupId();
		insert.setGroupId(groupId);
		
		return insert;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.bibsonomy.database.managers.PostDatabaseManager#getNewParam()
	 */
	@Override
	protected BookmarkParam getNewParam() {
		return new BookmarkParam();
	}
}
