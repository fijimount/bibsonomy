/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database;

import static org.bibsonomy.util.ValidationUtils.present;

import org.bibsonomy.common.exceptions.AccessDeniedException;
import org.bibsonomy.database.common.DBSession;
import org.bibsonomy.database.managers.GroupDatabaseManager;
import org.bibsonomy.database.managers.UserDatabaseManager;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.LogicInterface;

/**
 * This class produces DBLogic instances with user authentication
 *
 * @author Jens Illig
 */
public abstract class DBLogicUserInterfaceFactory extends AbstractDBLogicInterfaceFactory {

	protected final UserDatabaseManager userDBManager = UserDatabaseManager.getInstance();
	protected final GroupDatabaseManager groupDb = GroupDatabaseManager.getInstance();

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterfaceFactory#getLogicAccess(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public LogicInterface getLogicAccess(final String loginName, final String password) {
		final DBLogic logic = this.buildLogic();
		if (loginName != null) {
			final User loggedInUser = this.getLoggedInUser(loginName, password);
			if (loggedInUser.getName() != null) {
				logic.setLoginUser(loggedInUser);
				return logic;
			}
			throw new AccessDeniedException("Wrong Authentication ('" + loginName + "'/'" + password + "')");
		}
		// guest access
		logic.setLoginUser(new User());
		return logic;
	}

	/**
	 * Returns a user object containing the details of the user, if he is logged
	 * in correctly. If not, the returned user object is empty and it's user
	 * name NULL.
	 *
	 * @param loginName
	 * @param password
	 * @return user object with details of the logged in user
	 */
	protected User getLoggedInUser(final String loginName, final String password) {
		final DBSession session = this.openSession();
		try {
			final User loggedInUser = this.getLoggedInUserAccess(loginName, password, session);
			if (present(loggedInUser.getName())) {
				loggedInUser.setGroups(this.groupDb.getGroupsForUser(loggedInUser.getName(), true, session));
			}
			return loggedInUser;
		} finally {
			session.close();
		}
	}

	/**
	 * Calls the correct validation method on the {@link UserDatabaseManager}.
	 * @param loginName
	 * @param password
	 * @param session
	 * @return the logged in user (success) or an user with name NULL (failure)
	 */
	protected User getLoggedInUserAccess(final String loginName, final String password, final DBSession session) {
		return this.userDBManager.validateUserAccessByPassword(loginName, password, session);
	}
}