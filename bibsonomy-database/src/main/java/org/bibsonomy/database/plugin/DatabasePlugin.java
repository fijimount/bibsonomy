/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.plugin;

import org.bibsonomy.common.information.JobInformation;
import org.bibsonomy.database.common.DBSession;
import org.bibsonomy.database.params.ClipboardParam;
import org.bibsonomy.database.params.BibTexExtraParam;
import org.bibsonomy.database.params.DocumentParam;
import org.bibsonomy.database.params.InboxParam;
import org.bibsonomy.database.params.UserParam;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.DiscussionItem;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.GroupMembership;
import org.bibsonomy.model.Person;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.ResourcePersonRelation;
import org.bibsonomy.model.User;
import org.bibsonomy.model.cris.CRISLink;
import org.bibsonomy.model.cris.Project;
import org.bibsonomy.model.enums.GoldStandardRelation;

import java.util.Collections;
import java.util.List;

/**
 * This interface supplies hooks which can be implemented by plugins. This way
 * the code for basic operations, like updating a bookmark or publication, can
 * be kept concise and is easier to maintain.<br/>
 * 
 * If a method returns <code>null</code> its execution will be skipped.
 * 
 * @author Jens Illig
 * @author Christian Schenk
 * @author Stefan Stützer
 * @author Anton Wilhelm
 */
public interface DatabasePlugin {

	/**
	 * Called when a publication is inserted.
	 *
	 * @param post
	 * @param loggedinUser
	 * @param session
	 */
	default List<JobInformation> onPublicationInsert(Post<? extends BibTex> post, User loggedinUser, DBSession session) {
		return Collections.emptyList();
	}

	/**
	 * Called when a publication is deleted.
	 * 
	 * @param contentId
	 * @param session
	 */
	default void onPublicationDelete(int contentId, DBSession session) {
		// noop
	}

	/**
	 * Called when a publication is updated.
	 * 
	 * @param newContentId
	 * @param contentId
	 * @param session
	 */
	default void onPublicationUpdate(int newContentId, int contentId, DBSession session) {
		// noop
	}
	
	/**
	 * @param username
	 * @param groupId
	 * @param session
	 */
	public void onPublicationMassUpdate(String username, int groupId, DBSession session);
	
	/**
	 * Called when a gold standard publication is created.
	 * 
	 * @param interhash
	 * @param session
	 */
	public void onGoldStandardCreate(String interhash, DBSession session);

	/**
	 * Called when a gold standard publication will be updated.
	 * 
	 * @param newContentId
	 * @param contentId
	 * @param newInterhash
	 * @param interhash
	 * @param session
	 */
	public void onGoldStandardUpdate(int newContentId, int contentId, String newInterhash, String interhash, DBSession session);
	
	/**
	 * Called when a reference of a gold standard publication will be created
	 * @param userName
	 * @param interHash_publication
	 * @param interHash_reference
	 * @param interHash_relation
	 */
	public void onGoldStandardPublicationReferenceCreate(String userName, String interHash_publication, String interHash_reference, String interHash_relation);
	
	/**
	 * Called when a reference of a gold standard publication will be deleted
	 * 
	 * @param userName
	 * @param interHash_publication
	 * @param interHash_reference
	 * @param interHashRelation
	 * @param session
	 */
	public void onGoldStandardRelationDelete(String userName, String interHash_publication, String interHash_reference, GoldStandardRelation interHashRelation, DBSession session);
	
	/**
	 * Called when a gold standard publication is deleted.
	 *  @param interhash
	 * @param loggedinUser
	 * @param session
	 */
	default void onGoldStandardDelete(String interhash, User loggedinUser, DBSession session) {
		// noop
	}
	
	/**
	 * Called when a Bookmark is inserted.
	 * @param post
	 * @param logginUser
	 * @param session
	 */
	default List<JobInformation> onBookmarkInsert(Post<? extends Resource> post, User logginUser, DBSession session) {
		return Collections.emptyList();
	}

	/**
	 * Called when a Bookmark is deleted.
	 * 
	 * @param contentId
	 * @param session
	 */
	public void onBookmarkDelete(int contentId, DBSession session);

	/**
	 * Called when a Bookmark is updated.
	 * 
	 * @param newContentId
	 * @param contentId
	 * @param session
	 */
	public void onBookmarkUpdate(int newContentId, int contentId, DBSession session);
	
	/**
	 * @param userName the username
	 * @param groupId the groupid
	 * @param session TODO
	 */
	public void onBookmarkMassUpdate(final String userName, final int groupId, DBSession session);
	
	/**
	 * Called when a TagRelation is deleted.
	 * 
	 * @param upperTagName
	 * @param lowerTagName
	 * @param userName
	 * @param session
	 */
	public void onTagRelationDelete(String upperTagName, String lowerTagName, String userName, DBSession session);
	
	/**
	 * Called when a Concept is deleted.
	 * 
	 * @param conceptName
	 * @param userName
	 * @param session
	 */
	public void onConceptDelete(String conceptName, String userName, DBSession session);
	
	/**
	 * Called when a Tag is deleted.
	 * 
	 * @param contentId
	 * @param session
	 */
	public void onTagDelete(int contentId, DBSession session);
	
	/**
	 * Called when a User is inserted.
	 * 
	 * @param userName
	 * @param session
	 */
	public void onUserInsert(String userName, DBSession session);

	/**
	 * Called when a User is deleted.
	 * 
	 * @param userName
	 * @param session
	 */
	public void onUserDelete(String userName, DBSession session);

	/**
	 * Called when a User is updated.
	 *  @param userName
	 * @param loggedinUser
	 * @param session
	 */
	default void onUserUpdate(String userName, User loggedinUser, DBSession session) {
		// noop
	}

	/**
	 * Called when a user is removed from a group.
	 *  @param group
	 * @param userName
	 * @param loggedinUser
	 * @param session
	 */
	default void onChangeUserMembershipInGroup(Group group, String userName, User loggedinUser, DBSession session) {
		// noop
	}
	
	/**
	 * Called when a fellowship will be deleted
	 * 
	 * @param param
	 * @param session
	 */
	public void onDeleteFellowship(final UserParam param, final DBSession session);
	
	/**
	 * Called when a friendship will be deleted
	 * 
	 * @param param
	 * @param session
	 */
	public void onDeleteFriendship(final UserParam param, final DBSession session);
	
	/**
	 * Called when a clipboard item will be deleted
	 * 
	 * @param param
	 * @param session
	 */
	public void onDeleteClipboardItem(final ClipboardParam param, final DBSession session);
	
	/**
	 * Called when all clipboard items will be deleted
	 * 
	 * @param userName 
	 * @param session 
	 * 
	 */
	public void onDeleteAllClipboardItems(final String userName, final DBSession session);
	
	/**
	 * called when a comment was updated
	 * 
	 * @param interHash
	 * @param comment
	 * @param oldComment
	 * @param session
	 */
	public void onDiscussionUpdate(final String interHash, DiscussionItem comment, DiscussionItem oldComment, DBSession session);	
	
	/**
	 * called when several comments had to be updated.
	 * 
	 * @param username
	 * @param groupId
	 * @param session 
	 */
	public void onDiscussionMassUpdate(final String username, final int groupId, DBSession session);
	
	/**
	 * called when a comment will be deleted
	 * 
	 * @param interHash
	 * @param deletedComment
	 * @param session
	 */
	public void onDiscussionItemDelete(final String interHash, final DiscussionItem deletedComment, final DBSession session);
	
	/**
	 * called when a document will be deleted
	 * 
	 * @param deletedDocumentParam
	 * @param session
	 */
	public void onDocumentDelete(final DocumentParam deletedDocumentParam, final DBSession session);
	
	/**
	 * called when a will be updated
	 * 
	 * @param updatedDocumentParam
	 * @param session
	 */
	public void onDocumentUpdate(final DocumentParam updatedDocumentParam, final DBSession session);
	
	/**
	 * TODO document me
	 * 
	 * @param deletedInboxMessageParam 
	 * @param session 
	 */
	public void onInboxMailDelete(final InboxParam deletedInboxMessageParam, final DBSession session);
	
	/**
	 * called when a BibTexExtraURL will be deleted
	 * 
	 * @param deletedBibTexExtraParam
	 * @param session
	 * @author MarcelM
	 */
	public void onBibTexExtraDelete(final BibTexExtraParam deletedBibTexExtraParam, final DBSession session);
	
	/**
	 * called when a personName will be deleted
	 * @param personName should be set to the old personNameChangeId and the new modifiedBy and modifiedBy values
	 * @param loggedInUser the user that deleted the person name
	 * @param session
	 */
	default void onPersonNameDelete(final PersonName personName, User loggedInUser, final DBSession session) {
		// noop
	}
	
	/**
	 * called when a person will be updated
	 * @param oldPerson
	 * @param newPerson
	 * @param session
	 */
	public void onPersonUpdate(final Person oldPerson, Person newPerson, final DBSession session);
	
	/**
	 * called when a person will be updated by username change
	 * @param userName
	 * @param session
	 */
	public void onPersonUpdateByUserName(final String userName, final DBSession session);
	
	/**
	 * called when a person will be deleted
	 * @param person should be set to the old personId and the new modifiedBy and modifiedBy values
	 * @param user
	 * @param session
	 */
	public void onPersonDelete(final Person person, User user, final DBSession session);
	
	/**
	 * called when a pubPerson will be deleted
	 * @param rel the relation to be deleted updated with the deleting user and the date of the deletion
	 * @param loginUser
	 * @param session
	 */
	public void onPubPersonDelete(final ResourcePersonRelation rel, User loginUser, final DBSession session);

	/**
	 * @param oldPerson
	 * @param loggedinUser
	 * @param session
	 */
	default void onPersonNameUpdate(PersonName oldPerson, User loggedinUser, DBSession session) {
		// noop
	}

	/**
	 * called before a relation is updated (currently the oldRelation is deleted the new relation is inserted)
	 * @param oldRelation
	 * @param newRelation
	 * @param loggedinUser
	 * @param session
	 */
	default void onPersonResourceRelationUpdate(ResourcePersonRelation oldRelation, ResourcePersonRelation newRelation, User loggedinUser, DBSession session) {
		// noop
	}
	/**
	 * called before a project is inserted into the database
	 * @param project
	 * @param session
	 */
	default void onProjectInsert(final Project project, DBSession session) {
		// noop
	}

	/**
	 * called before a project is updated
	 * @param oldProject
	 * @param newProject
	 * @param loggedinUser
	 * @param session
	 */
	default void onProjectUpdate(final Project oldProject, final Project newProject, User loggedinUser, final DBSession session) {
		// noop
	}

	/**
	 * called before a project is deleted
	 * @param project
	 * @param loggedinUser
	 * @param session
	 */
	default void onProjectDelete(Project project, User loggedinUser, DBSession session) {
		// noop
	}

	/**
	 * called before a cris link is updated
	 * @param oldCRISLink
	 * @param link
	 * @param loginUser
	 * @param session
	 */
	default void onCRISLinkUpdate(CRISLink oldCRISLink, CRISLink link, User loginUser, DBSession session) {
		// noop
	}

	/**
	 * called after a user is added to a group
	 * @param group
	 * @param membership
	 * @param loggedinUser
	 * @param session
	 */
	default void onAddedGroupMembership(final Group group, final GroupMembership membership, User loggedinUser, final DBSession session) {
		// noop
	}

	/**
	 * called after a user was removed from a group
	 * @param group
	 * @param username
	 * @param loggedinUser
	 * @param session
	 */
	default void onRemovedGroupMembership(Group group, String username, User loggedinUser, DBSession session) {
		// noop
	}


	/**
	 * Called before a group is removed.
	 *
	 * @param group the group that will be removed.
	 * @param loggedInUser the user that is currently logged in.
	 * @param session a database session.
	 */
	default void beforeRemoveGroup(Group group, User loggedInUser, DBSession session) {
		// noop
	}


	/**
	 * Called before a user is removed from a group.
	 *
	 * @param group the group.
	 * @param username the name of the user that will be removed from the group.
	 * @param loggedInUser the user that is currently logged in.
	 * @param session a database session.
	 */
	default void beforeRemoveGroupMembership(Group group, String username, User loggedInUser, DBSession session) {
		// noop
	}


	default void onCRISLinkDelete(CRISLink crisLink, User loginUser, DBSession session) {
		// noop
	}
}