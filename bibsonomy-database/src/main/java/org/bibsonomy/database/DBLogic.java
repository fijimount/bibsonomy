/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database;

import static org.bibsonomy.util.ValidationUtils.assertNotNull;
import static org.bibsonomy.util.ValidationUtils.present;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.auth.util.SimpleAuthUtils;
import org.bibsonomy.common.JobResult;
import org.bibsonomy.common.SortCriteria;
import org.bibsonomy.common.enums.Classifier;
import org.bibsonomy.common.enums.ClassifierSettings;
import org.bibsonomy.common.enums.ConceptStatus;
import org.bibsonomy.common.enums.ConceptUpdateOperation;
import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.FilterEntity;
import org.bibsonomy.common.enums.GroupID;
import org.bibsonomy.common.enums.GroupRole;
import org.bibsonomy.common.enums.GroupUpdateOperation;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.common.enums.InetAddressStatus;
import org.bibsonomy.common.enums.PersonUpdateOperation;
import org.bibsonomy.common.enums.PostAccess;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.common.enums.QueryScope;
import org.bibsonomy.common.enums.Role;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SpamStatus;
import org.bibsonomy.common.enums.SyncSettingsUpdateOperation;
import org.bibsonomy.common.enums.TagRelation;
import org.bibsonomy.common.enums.TagSimilarity;
import org.bibsonomy.common.enums.UserRelation;
import org.bibsonomy.common.enums.UserUpdateOperation;
import org.bibsonomy.common.errors.UnspecifiedErrorMessage;
import org.bibsonomy.common.exceptions.AccessDeniedException;
import org.bibsonomy.common.exceptions.DatabaseException;
import org.bibsonomy.common.exceptions.ObjectMovedException;
import org.bibsonomy.common.exceptions.ObjectNotFoundException;
import org.bibsonomy.common.exceptions.QueryTimeoutException;
import org.bibsonomy.common.exceptions.SynchronizationRunningException;
import org.bibsonomy.common.exceptions.UnsupportedResourceTypeException;
import org.bibsonomy.common.exceptions.ValidationException;
import org.bibsonomy.database.common.DBSession;
import org.bibsonomy.database.common.DBSessionFactory;
import org.bibsonomy.database.managers.AdminDatabaseManager;
import org.bibsonomy.database.managers.AuthorDatabaseManager;
import org.bibsonomy.database.managers.BibTexDatabaseManager;
import org.bibsonomy.database.managers.BibTexExtraDatabaseManager;
import org.bibsonomy.database.managers.BookmarkDatabaseManager;
import org.bibsonomy.database.managers.CRISLinkDatabaseManager;
import org.bibsonomy.database.managers.ClipboardDatabaseManager;
import org.bibsonomy.database.managers.CrudableContent;
import org.bibsonomy.database.managers.DocumentDatabaseManager;
import org.bibsonomy.database.managers.GoldStandardBookmarkDatabaseManager;
import org.bibsonomy.database.managers.GoldStandardPublicationDatabaseManager;
import org.bibsonomy.database.managers.GroupDatabaseManager;
import org.bibsonomy.database.managers.InboxDatabaseManager;
import org.bibsonomy.database.managers.PermissionDatabaseManager;
import org.bibsonomy.database.managers.PersonDatabaseManager;
import org.bibsonomy.database.managers.PersonResourceRelationDatabaseManager;
import org.bibsonomy.database.managers.PostDatabaseManager;
import org.bibsonomy.database.managers.ProjectDatabaseManager;
import org.bibsonomy.database.managers.StatisticsDatabaseManager;
import org.bibsonomy.database.managers.StatisticsProvider;
import org.bibsonomy.database.managers.TagDatabaseManager;
import org.bibsonomy.database.managers.TagRelationDatabaseManager;
import org.bibsonomy.database.managers.UserDatabaseManager;
import org.bibsonomy.database.managers.WikiDatabaseManager;
import org.bibsonomy.database.managers.chain.util.QueryAdapter;
import org.bibsonomy.database.managers.discussion.CommentDatabaseManager;
import org.bibsonomy.database.managers.discussion.DiscussionDatabaseManager;
import org.bibsonomy.database.managers.discussion.DiscussionItemDatabaseManager;
import org.bibsonomy.database.managers.discussion.ReviewDatabaseManager;
import org.bibsonomy.database.managers.metadata.MetaDataProvider;
import org.bibsonomy.database.managers.util.PostStatisticsProviderDelegator;
import org.bibsonomy.database.params.BibTexParam;
import org.bibsonomy.database.params.BookmarkParam;
import org.bibsonomy.database.params.GenericParam;
import org.bibsonomy.database.params.StatisticsParam;
import org.bibsonomy.database.params.TagParam;
import org.bibsonomy.database.params.TagRelationParam;
import org.bibsonomy.database.params.UserParam;
import org.bibsonomy.database.systemstags.SystemTagsExtractor;
import org.bibsonomy.database.systemstags.SystemTagsUtil;
import org.bibsonomy.database.systemstags.search.NetworkRelationSystemTag;
import org.bibsonomy.database.systemstags.search.SearchSystemTag;
import org.bibsonomy.database.util.LogicInterfaceHelper;
import org.bibsonomy.model.Author;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Comment;
import org.bibsonomy.model.DiscussionItem;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.GoldStandardBookmark;
import org.bibsonomy.model.GoldStandardPublication;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.GroupMembership;
import org.bibsonomy.model.GroupRequest;
import org.bibsonomy.model.ImportResource;
import org.bibsonomy.model.Person;
import org.bibsonomy.model.PersonMatch;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.PhDRecommendation;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.ResourcePersonRelation;
import org.bibsonomy.model.Review;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.model.Wiki;
import org.bibsonomy.model.cris.CRISLink;
import org.bibsonomy.model.cris.Linkable;
import org.bibsonomy.model.cris.Project;
import org.bibsonomy.model.enums.GoldStandardRelation;
import org.bibsonomy.model.enums.PersonIdType;
import org.bibsonomy.model.enums.PersonResourceRelationOrder;
import org.bibsonomy.model.enums.PersonResourceRelationType;
import org.bibsonomy.model.extra.BibTexExtra;
import org.bibsonomy.model.logic.GoldStandardPostLogicInterface;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.logic.exception.ResourcePersonAlreadyAssignedException;
import org.bibsonomy.model.logic.query.GroupQuery;
import org.bibsonomy.model.logic.query.PersonQuery;
import org.bibsonomy.model.logic.query.PostQuery;
import org.bibsonomy.model.logic.query.ProjectQuery;
import org.bibsonomy.model.logic.query.Query;
import org.bibsonomy.model.logic.query.ResourcePersonRelationQuery;
import org.bibsonomy.model.logic.query.statistics.meta.MetaDataQuery;
import org.bibsonomy.model.logic.querybuilder.ResourcePersonRelationQueryBuilder;
import org.bibsonomy.model.metadata.PostMetaData;
import org.bibsonomy.model.statistics.Statistics;
import org.bibsonomy.model.sync.ConflictResolutionStrategy;
import org.bibsonomy.model.sync.SyncService;
import org.bibsonomy.model.sync.SynchronizationData;
import org.bibsonomy.model.sync.SynchronizationDirection;
import org.bibsonomy.model.sync.SynchronizationPost;
import org.bibsonomy.model.sync.SynchronizationStatus;
import org.bibsonomy.model.user.remote.RemoteUserId;
import org.bibsonomy.model.util.BibTexReader;
import org.bibsonomy.model.util.GroupUtils;
import org.bibsonomy.model.util.PostUtils;
import org.bibsonomy.model.util.UserUtils;
import org.bibsonomy.sync.SynchronizationDatabaseManager;
import org.bibsonomy.util.ExceptionUtils;
import org.bibsonomy.util.Sets;
import org.bibsonomy.util.ValidationUtils;

import java.net.InetAddress;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * Database Implementation of the LogicInterface
 *
 * @author Jens Illig
 * @author Christian Kramer
 * @author Christian Claus
 * @author Dominik Benz
 * @author Robert Jäschke
 *
 */
public class DBLogic implements LogicInterface {
	private static final Log log = LogFactory.getLog(DBLogic.class);
	/*
	 * help maps for post managers, statistics and discussion managers
	 */
	private final Map<Class<? extends Resource>, CrudableContent<? extends Resource, ? extends GenericParam>> allDatabaseManagers = new HashMap<>();
	private final Map<Class<? extends DiscussionItem>, DiscussionItemDatabaseManager<? extends DiscussionItem>> allDiscussionManagers = new HashMap<>();

	private final Map<Class<? extends Query>, StatisticsProvider<? extends Query>> allStatisticDatabaseMangers = new HashMap<>();

	private final AuthorDatabaseManager authorDBManager;
	private final DocumentDatabaseManager docDBManager;
	private final PermissionDatabaseManager permissionDBManager;

	private final PostDatabaseManager<Bookmark, BookmarkParam> bookmarkDBManager;
	private final BibTexDatabaseManager publicationDBManager;
	private final GoldStandardPublicationDatabaseManager goldStandardPublicationDBManager;
	private final GoldStandardBookmarkDatabaseManager goldStandardBookmarkDBManager;
	private final BibTexExtraDatabaseManager bibTexExtraDBManager;

	private final DiscussionDatabaseManager discussionDatabaseManager;
	private final ReviewDatabaseManager reviewDBManager;
	private final CommentDatabaseManager commentDBManager;

	private final UserDatabaseManager userDBManager;
	private final GroupDatabaseManager groupDBManager;
	private final TagDatabaseManager tagDBManager;

	private final AdminDatabaseManager adminDBManager;

	private final PersonDatabaseManager personDBManager;
	private PersonResourceRelationDatabaseManager personResourceRelationManager;

	private final StatisticsDatabaseManager statisticsDBManager;
	private final TagRelationDatabaseManager tagRelationsDBManager;
	private final ClipboardDatabaseManager clipboardDBManager;
	private final InboxDatabaseManager inboxDBManager;
	private final WikiDatabaseManager wikiDBManager;

	private ProjectDatabaseManager projectDatabaseManager;
	private CRISLinkDatabaseManager crisLinkDatabaseManager;

	private final SynchronizationDatabaseManager syncDBManager;

	private DBSessionFactory dbSessionFactory;
	private BibTexReader publicationReader;
	private User loginUser;
	private Map<Class<?>, MetaDataProvider<?>> metaDataProvidersMap;

	/**
	 * Returns an implementation of the DBLogic.
	 */
	protected DBLogic() {
		// publication db manager
		this.publicationDBManager = BibTexDatabaseManager.getInstance();

		// bookmark db manager
		this.bookmarkDBManager = BookmarkDatabaseManager.getInstance();

		// gold standard publication db manager
		this.goldStandardPublicationDBManager = GoldStandardPublicationDatabaseManager.getInstance();
		this.goldStandardBookmarkDBManager = GoldStandardBookmarkDatabaseManager.getInstance();

		// discussion and discussion item db manager
		this.commentDBManager = CommentDatabaseManager.getInstance();
		this.reviewDBManager = ReviewDatabaseManager.getInstance();
		this.discussionDatabaseManager = DiscussionDatabaseManager.getInstance();
		this.authorDBManager = AuthorDatabaseManager.getInstance();
		this.docDBManager = DocumentDatabaseManager.getInstance();
		this.userDBManager = UserDatabaseManager.getInstance();
		this.groupDBManager = GroupDatabaseManager.getInstance();
		this.tagDBManager = TagDatabaseManager.getInstance();
		this.adminDBManager = AdminDatabaseManager.getInstance();
		this.permissionDBManager = PermissionDatabaseManager.getInstance();
		this.statisticsDBManager = StatisticsDatabaseManager.getInstance();
		this.tagRelationsDBManager = TagRelationDatabaseManager.getInstance();
		this.personDBManager = PersonDatabaseManager.getInstance();

		this.clipboardDBManager = ClipboardDatabaseManager.getInstance();
		this.inboxDBManager = InboxDatabaseManager.getInstance();

		this.wikiDBManager = WikiDatabaseManager.getInstance();

		this.syncDBManager = SynchronizationDatabaseManager.getInstance();

		this.bibTexExtraDBManager = BibTexExtraDatabaseManager.getInstance();
	}

	protected void initializeMaps() {
		// register all discussion subtype managers
		this.allDiscussionManagers.put(Comment.class, this.commentDBManager);
		this.allDiscussionManagers.put(Review.class, this.reviewDBManager);

		// register all resource managers
		this.allDatabaseManagers.put(BibTex.class, this.publicationDBManager);
		this.allDatabaseManagers.put(Bookmark.class, this.bookmarkDBManager);
		this.allDatabaseManagers.put(GoldStandardPublication.class, this.goldStandardPublicationDBManager);
		this.allDatabaseManagers.put(GoldStandardBookmark.class, this.goldStandardBookmarkDBManager);

		// register the statistic database managers
		this.allStatisticDatabaseMangers.put(ProjectQuery.class, this.projectDatabaseManager);
		this.allStatisticDatabaseMangers.put(PersonQuery.class, this.personDBManager);
		this.allStatisticDatabaseMangers.put(ResourcePersonRelationQuery.class, this.personResourceRelationManager);
		// XXX: merge with setting up resource managers
		// TODO: add also standard resource managers
		final Map<Class<? extends Resource>, StatisticsProvider<?>> resourceManagers = new HashMap<>();
		resourceManagers.put(GoldStandardPublication.class, this.goldStandardPublicationDBManager);
		resourceManagers.put(GoldStandardBookmark.class, this.goldStandardBookmarkDBManager);

		this.allStatisticDatabaseMangers.put(PostQuery.class, new PostStatisticsProviderDelegator(resourceManagers));
	}


	/**
	 * Returns a new database session. If a user is logged in, he gets the
	 * master connection, if not logged in, the secondary connection
	 */
	private DBSession openSession() {
		return this.dbSessionFactory.getDatabaseSession();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getUserDetails(java.lang.String)
	 */
	@Override
	public User getUserDetails(final String userName) {
		try (final DBSession session = this.openSession()) {
			/*
			 * We don't use userName but user.getName() in the remaining part of
			 * this method, since the name gets normalized in getUserDetails().
			 */
			final User user = this.userDBManager.getUserDetails(userName, session);

			/*
			 * get the claimed person for the user only if not a dummy user was requested
			 */
			final String foundUserName = user.getName();
			if (present(foundUserName)) {
				final Person claimedPerson = this.personDBManager.getPersonByUser(foundUserName, session);
				user.setClaimedPerson(claimedPerson);
			}

			/*
			 * only admin and myself may see which group I'm a member of
			 * group admins may see the details of the group's dummy user (in
			 * that case, the group's name is user.getName()
			 */
			if (this.permissionDBManager.isAdminOrSelf(this.loginUser, foundUserName)
							|| this.permissionDBManager.isAdminOrHasGroupRoleOrHigher(this.loginUser, foundUserName, GroupRole.ADMINISTRATOR)) {
				user.setGroups(this.groupDBManager.getGroupsForUser(foundUserName, true, true, session));
				user.setPendingGroups(this.groupDBManager.getPendingMembershipsForUser(userName, session));
				// inject the reported spammers.
				final List<User> reportedSpammersList = this.userDBManager.getUserRelation(foundUserName, UserRelation.SPAMMER, NetworkRelationSystemTag.BibSonomySpammerSystemTag, session);
				user.setReportedSpammers(new HashSet<>(reportedSpammersList));
				// fill user's spam information
				this.adminDBManager.getClassifierUserDetails(user, session);
				return user;
			}

			/*
			 * return a complete empty user, in case of a deleted user
			 */
			if (user.getRole() == Role.DELETED) {
				return new User();
			}

			/*
			 * respect user privacy settings
			 * clear all profile attributes if current login user isn't allowed
			 * to see the profile
			 */
			if (!this.permissionDBManager.isAllowedToAccessUsersProfile(user, this.loginUser, session)) {
				/*
				 * TODO: this practically clears /all/ user information
				 */
				/*
				 * FIXME: This is necessary to avoid null pointer Exceptions
				 * when the user's picture is not visible.
				 * The fileLogic should do this instead by setting the default
				 * pic in such cases.
				 */
				final User dummyUser = this.userDBManager.createEmptyUser();
				dummyUser.setName(foundUserName);

				return dummyUser;
			}

			/*
			 * clear the private stuff
			 */
			user.setEmail(null);

			user.setApiKey(null);
			user.setPassword(null);

			user.setReminderPassword(null);
			user.setReminderPasswordRequestDate(null);

			user.setSettings(null);

			/*
			 * FIXME: other things set in userDBManager.getUserDetails() maybe
			 * not cleared!
			 */

			return user;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#getSynchronization(java.lang
	 * .String, java.lang.Class, java.util.List,
	 * org.bibsonomy.model.sync.ConflictResolutionStrategy, java.lang.String)
	 */
	@Override
	public List<SynchronizationPost> getSyncPlan(final String userName, final URI service, final Class<? extends Resource> resourceType, final List<SynchronizationPost> clientPosts, final ConflictResolutionStrategy strategy, final SynchronizationDirection direction) {
		// handle resourceType = null
		if (!present(resourceType)) {
			throw new IllegalArgumentException("no resourceType was given - abort getSyncPlan()");
		}

		this.permissionDBManager.ensureWriteAccess(this.loginUser, userName);

		if (!present(strategy)) {
			log.error("no conflict resolution strategy received in getSyncPlan method! Use LAST WINS");
		}

		Date lastSuccessfulSyncDate = null;

		final List<SynchronizationPost> posts;

		try (final DBSession session = this.openSession()) {
			final SynchronizationData data = this.syncDBManager.getLastSyncData(userName, service, resourceType, null, session);

			/*
			 * check for a running synchronization
			 */
			if (present(data) && SynchronizationStatus.RUNNING.equals(data.getStatus())) {
				// running synchronization
				// FIXME: if synchronization fails, we can't recover
				throw new SynchronizationRunningException();
			}
			/*
			 * check for last successful synchronization
			 */
			final SynchronizationData lsd = this.syncDBManager.getLastSyncData(userName, service, resourceType, SynchronizationStatus.DONE, session);
			if (present(lsd)) {
				lastSuccessfulSyncDate = lsd.getLastSyncDate();
			} else if (!SynchronizationDirection.BOTH.equals(direction)) {
				// be sure that both systems are in sync before only syncing only in one direction
				throw new IllegalStateException("sync request rejected! The client hasn't performed an initial sync in both directions!");
			}
			/*
			 * flag synchronization as planned
			 * FIXME: if the client is not in the sync_services table, this
			 * statements silently fails. :-(
			 */
			log.debug("try to set syncdata as planned");

			final SyncService syncService = this.syncDBManager.getSyncServiceDetails(service, session);
			if (present(syncService)) {
				this.syncDBManager.insertSynchronizationData(userName, service, resourceType, new Date(), SynchronizationStatus.PLANNED, session);
			} else {
				log.error("no SyncService found with URI: " + service.toString());
				throw new IllegalArgumentException("no SyncService found with URI: " + service.toString());
			}

			/*
			 * get posts from server (=this machine)
			 */
			final Map<String, SynchronizationPost> serverPosts;
			if (BibTex.class.equals(resourceType)) {
				serverPosts = this.publicationDBManager.getSyncPostsMapForUser(userName, session);
			} else if (Bookmark.class.equals(resourceType)) {
				serverPosts = this.bookmarkDBManager.getSyncPostsMapForUser(userName, session);
			} else {
				throw new UnsupportedResourceTypeException();
			}

			/*
			 * if necessary, set the synchronization date to some distant old
			 * value
			 */
			if (!present(lastSuccessfulSyncDate)) {
				lastSuccessfulSyncDate = new Date(0);
			}
			/*
			 * calculate synchronization plan
			 */
			posts = this.syncDBManager.getSyncPlan(serverPosts, clientPosts, lastSuccessfulSyncDate, strategy, direction);

			/*
			 * attach "real" posts to the synchronization posts, which will be
			 * updated (or created) on the client
			 */
			final CrudableContent<? extends Resource, ? extends GenericParam> resourceTypeDatabaseManager = this.allDatabaseManagers.get(resourceType);
			final List<Integer> listOfGroupIDs = UserUtils.getListOfGroupIDs(this.loginUser);
			final String loginUserName = this.loginUser.getName();
			for (final SynchronizationPost post : posts) {
				switch (post.getAction()) {
					case CREATE_CLIENT:
						// $FALL-THROUGH$
					case UPDATE_CLIENT:
						// FIXME: this is horribly expensive!
						post.setPost(resourceTypeDatabaseManager.getPostDetails(loginUserName, post.getIntraHash(), userName, listOfGroupIDs, session));
						break;
					default:
						break;
				}
			}

		}

		return posts;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.sync.SyncLogicInterface#createSyncService()
	 */
	@Override
	public void createSyncService(final SyncService service, final boolean server) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			this.syncDBManager.createSyncService(service, server, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#deleteSyncService(java.net
	 * .URI, boolean)
	 */
	@Override
	public void deleteSyncService(final URI service, final boolean server) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			this.syncDBManager.deleteSyncService(service, server, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#createSyncServer(java.lang
	 * .String, org.bibsonomy.model.sync.SyncService)
	 */
	@Override
	public void createSyncServer(final String userName, final SyncService server) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			this.syncDBManager.createSyncServerForUser(userName, server, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#updateSyncServer(java.lang
	 * .String, java.net.URI, java.util.Properties)
	 */
	@Override
	public void updateSyncServer(final String userName, final SyncService service, final SyncSettingsUpdateOperation operation) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			this.syncDBManager.updateSyncServerForUser(userName, service, operation, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#deleteSyncServer(java.lang
	 * .String, java.net.URI)
	 */
	@Override
	public void deleteSyncServer(final String userName, final URI service) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			this.syncDBManager.deleteSyncServerForUser(userName, service, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#getSyncServiceSettings()
	 */
	@Override
	public List<SyncService> getSyncServiceSettings(final String userName, final URI service, final boolean server) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			return this.syncDBManager.getSyncServiceSettings(userName, service, server, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.sync.SyncLogicInterface#getSyncServiceDetails()
	 */
	@Override
	public SyncService getSyncServiceDetails(final URI serviceURI) {
		try (final DBSession session = this.openSession()) {
			return this.syncDBManager.getSyncServiceDetails(serviceURI, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#getLastSynchronizationData
	 * (java.lang.String, int, int)
	 */
	@Override
	public SynchronizationData getLastSyncData(final String userName, final URI service, final Class<? extends Resource> resourceType) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			final SynchronizationData lastSyncData = this.syncDBManager.getLastSyncData(userName, service, resourceType, null, session);
			if (present(lastSyncData)) {
				return lastSyncData;
			}
			/*
			 * no sync found -> return very "old" date to bypass NPE later on
			 * FIXME: is this correct or does it break something?
			 */
			final SynchronizationData synchronizationData = new SynchronizationData();
			// fill: ss.uri, sd.user_name, sd.content_type, sd.last_sync_date,
			// sd.status, sd.info
			synchronizationData.setService(service);
			synchronizationData.setResourceType(resourceType);
			synchronizationData.setLastSyncDate(new Date(0));
			synchronizationData.setStatus(SynchronizationStatus.UNDONE);
			return synchronizationData;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#setCurrentSyncDone(org.bibsonomy
	 * .model.sync.SynchronizationData)
	 */
	@Override
	public void updateSyncData(final String userName, final URI service, final Class<? extends Resource> resourceType, final Date syncDate, final SynchronizationStatus status, final String info, final Date newDate) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			this.syncDBManager.updateSyncData(userName, service, resourceType, syncDate, status, info, newDate, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#setCurrentSyncDone(org.bibsonomy
	 * .model.sync.SynchronizationData)
	 */
	@Override
	public void deleteSyncData(final String userName, final URI service, final Class<? extends Resource> resourceType, final Date syncDate) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			if (Resource.class.equals(resourceType)) {
				// XXX: more generic
				this.syncDBManager.deleteSyncData(userName, service, Bookmark.class, syncDate, session);
				this.syncDBManager.deleteSyncData(userName, service, BibTex.class, syncDate, session);
			} else {
				this.syncDBManager.deleteSyncData(userName, service, resourceType, syncDate, session);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.sync.SyncLogicInterface#getPostsForSync(java.lang
	 * .Class, java.lang.String)
	 */
	@Override
	public List<SynchronizationPost> getSyncPosts(final String userName, final Class<? extends Resource> resourceType) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
		try (final DBSession session = this.openSession()) {
			if (resourceType == BibTex.class) {
				return this.publicationDBManager.getSyncPostsListForUser(userName, session);
			} else if (resourceType == Bookmark.class) {
				return this.bookmarkDBManager.getSyncPostsListForUser(userName, session);
			} else {
				throw new UnsupportedResourceTypeException();
			}
		}
	}

	/**
	 * TODO: rename method doesn't validate anything
	 * Method to handle privacy settings of posts for synchronization
	 *
	 * @param post
	 */
	private static void validateGroupsForSynchronization(final Post<? extends Resource> post) {
		/*
		 * if post has group make it private
		 */
		if (!GroupUtils.containsExclusiveGroup(post.getGroups())) {
			post.setGroups(Collections.singleton(GroupUtils.buildPrivateGroup()));
		}
	}

	@Override
	public <R extends Resource> List<Post<R>> getPosts(final PostQuery<R> query) {
		final Class<R> resourceType = query.getResourceClass();
		final QueryScope queryScope = query.getScope();
		final String hash = query.getHash();
		final Date startDate = query.getStartDate();
		final Date endDate = query.getEndDate();
		final String search = query.getSearch();
		final GroupingEntity grouping = query.getGrouping();
		final String groupingName = query.getGroupingName();
		final Set<Filter> filters = query.getFilters();
		final List<SortCriteria> sortCriteria = query.getSortCriteria();
		final List<String> tags = query.getTags();
		final int start = query.getStart();
		final int end = query.getEnd();
		// check allowed start-/end-values
		this.permissionDBManager.checkStartEnd(this.loginUser, grouping, start, end, "posts");

		this.handleAdminFilters(filters);

		// check for systemTags disabling this resourceType
		if (!systemTagsAllowResourceType(tags, resourceType)) {
			return new ArrayList<>();
		}
		try (final DBSession session = this.openSession()) {
			/*
			 * if (resourceType == Resource.class) { yes, this IS unsave and
			 * indeed it BREAKS restrictions on generic-constraints. it is the
			 * result of two designs: 1. @ibatis: database-results should be
			 * accessible as a stream or should at least be saved using the
			 * visitor pattern (collection<? super X> arguments would do fine)
			 * 2. @bibsonomy: this method needs runtime-type-checking which is
			 * not supported by generics so what: copy each and every entry
			 * manually or split this method to become type-safe WITHOUT falling
			 * back to <? extends Resource> (which means read-only) in the whole
			 * project result = bibtexDBManager.getPosts(authUser, grouping,
			 * groupingName, tags, hash, popular, added, start, end, false); //
			 * TODO: solve problem with limit+offset:
			 * result.addAll(bookmarkDBManager.getPosts(authUser, grouping,
			 * groupingName, tags, hash, popular, added, start, end, false));
			 */
			if (ValidationUtils.safeContains(filters, FilterEntity.HISTORY) && !(resourceType == GoldStandardPublication.class || resourceType == GoldStandardBookmark.class)) {
				this.permissionDBManager.ensureIsAdminOrSelfOrHasGroupRoleOrHigher(this.loginUser, groupingName, GroupRole.USER);
			}
			// TODO maybe clean up, firstSortKey only there to not change the buildParam signature
			SortKey firstSortKey = null;
			if (present(sortCriteria)) {
				firstSortKey = sortCriteria.get(0).getSortKey();
			}
			if (resourceType == BibTex.class) {
				final BibTexParam param = LogicInterfaceHelper.buildParam(BibTexParam.class, resourceType, queryScope, grouping, groupingName, tags, hash, firstSortKey, start, end, startDate, endDate, search, filters, this.loginUser);
				// sets the sort order
				param.setSortCriteria(sortCriteria);

				// check permissions for displaying links to documents
				final boolean allowedToAccessUsersOrGroupDocuments = this.permissionDBManager.isAllowedToAccessUsersOrGroupDocuments(this.loginUser, grouping, groupingName, session);
				if (!allowedToAccessUsersOrGroupDocuments) {
					if (ValidationUtils.safeContains(filters, FilterEntity.JUST_PDF)) {
						throw new AccessDeniedException("error.pdf_only_not_authorized_for_" + grouping.toString().toLowerCase());
					}
					param.setPostAccess(PostAccess.POST_ONLY);
				} else {
					// user can access all post details (including docs)
					param.setPostAccess(PostAccess.FULL);
				}

				param.setQuery((PostQuery<BibTex>) query);

				// this is save because of RTTI-check of resourceType argument
				// which is of class T
				final List<Post<R>> publications = (List) this.publicationDBManager.getPosts(param, session);
				SystemTagsExtractor.handleHiddenSystemTags(publications, this.loginUser.getName());
				return publications;
			}

			if (resourceType == Bookmark.class) {
				final BookmarkParam param = LogicInterfaceHelper.buildParam(BookmarkParam.class, resourceType, queryScope, grouping, groupingName, tags, hash, firstSortKey, start, end, startDate, endDate, search, filters, this.loginUser);
				param.setQuery((PostQuery<Bookmark>) query);
				// sets the sort order to search index
				param.setSortCriteria(sortCriteria);
				final List<Post<R>> bookmarks = (List) this.bookmarkDBManager.getPosts(param, session);
				SystemTagsExtractor.handleHiddenSystemTags(bookmarks, this.loginUser.getName());
				return bookmarks;
			}

			if (resourceType == GoldStandardPublication.class) {
				return (List) this.goldStandardPublicationDBManager.getPosts((PostQuery<GoldStandardPublication>) query, this.loginUser, session);
			}

			if (resourceType == GoldStandardBookmark.class) {
				return (List) this.goldStandardBookmarkDBManager.getPosts((PostQuery<GoldStandardBookmark>) query, this.loginUser, session);
			}

			throw new UnsupportedResourceTypeException();
		} catch (final QueryTimeoutException ex) {
			// if a query times out, we return an empty list
			return new ArrayList<>();
		}
	}

	private static boolean systemTagsAllowResourceType(final Collection<String> tags, final Class<? extends Resource> resourceType) {
		if (present(tags)) {
			for (final String tagName : tags) {
				final SearchSystemTag sysTag = SystemTagsUtil.createSearchSystemTag(tagName);
				if (present(sysTag)) {
					if (!sysTag.allowsResource(resourceType)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.PostLogicInterface#getPostDetails(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public Post<? extends Resource> getPostDetails(final String resourceHash, final String userName) throws ObjectMovedException, ObjectNotFoundException {
		try (DBSession session = this.openSession()) {
			return this.getPostDetails(resourceHash, userName, session);
		}
	}

	private Post<? extends Resource> getPostDetails(final String resourceHash, final String userName, final DBSession session) {
		for (final CrudableContent<? extends Resource, ? extends GenericParam> manager : this.allDatabaseManagers.values()) {
			final Post<? extends Resource> post = manager.getPostDetails(this.loginUser.getName(), resourceHash, userName, UserUtils.getListOfGroupIDs(this.loginUser), session);
			/*
			 * if a manager found a post, return it
			 */
			if (present(post)) {
				/*
				 * XXX: can't be added to the postDatabaseManager; calls
				 * getPostDetails with an empty list of visible groups
				 */
				final Resource resource = post.getResource();
				final List<DiscussionItem> discussionSpace = this.discussionDatabaseManager.getDiscussionSpace(this.loginUser, resource.getInterHash(), session);
				resource.setDiscussionItems(discussionSpace);
				SystemTagsExtractor.handleHiddenSystemTags(post, this.loginUser.getName());
				return post;
			}
			/*
			 * check next manager
			 */
		}

		return null;
	}

	@Override
	public List<Group> getGroups(final GroupQuery query) {
		try (final DBSession session = this.openSession()) {
			/*
			 * (AD)
			 * Perform security checks for the different cases. This is pretty strange, since there is no easy way
			 * to find out which security check applies to a certain chain element. IMHO this should be
			 * implemented as some kind of facade around the chain elements.
			 */
			if (query.isPending()) {
				if (present(query.getUserName())) {
					this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, query.getUserName());
				} else {
					this.permissionDBManager.ensureAdminAccess(this.loginUser);
				}
			}

			return this.groupDBManager.queryGroups(query, this.loginUser, session);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.bibsonomy.model.logic.LogicInterface#getDeletedGroupUsers(int, int)
	 */
	@Override
	public List<User> getDeletedGroupUsers(int start, int end) {
		try (final DBSession session = this.openSession()) {
			this.permissionDBManager.ensureAdminAccess(this.loginUser);
			return this.userDBManager.getDeletedGroupUsers(start, end, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.sync.SyncLogicInterface#getSyncServices(final boolean server)
	 */
	@Override
	public List<SyncService> getAutoSyncServer() {
		try (final DBSession session = this.openSession()) {
			return this.syncDBManager.getAutoSyncServer(session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.sync.SyncLogicInterface#getAutoSyncServer()
	 */
	@Override
	public List<SyncService> getSyncServices(final boolean server, final String sslDn) {
		try (final DBSession session = this.openSession()) {
			return this.syncDBManager.getSyncServices(server, sslDn, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getGroupDetails(java.lang.String
	 * )
	 */
	@Override
	public Group getGroupDetails(final String groupName, final boolean pending) {
		try (final DBSession session = this.openSession()) {
			if (pending) {
				final String requestingUser;
				if (this.permissionDBManager.isAdmin(this.loginUser)) {
					requestingUser = null;
				} else {
					requestingUser = this.loginUser.getName();
				}
				return this.groupDBManager.getPendingGroup(groupName, requestingUser, session);
			}

			final Group myGroup = this.groupDBManager.getGroup(this.loginUser.getName(), groupName, true, this.permissionDBManager.isAdmin(this.loginUser), session);
			if (!GroupUtils.isValidGroup(myGroup)) {
				return null;
			}
			myGroup.setTagSets(this.groupDBManager.getGroupTagSets(groupName, session));
			if (this.permissionDBManager.isAdminOrHasGroupRoleOrHigher(this.loginUser, groupName, GroupRole.MODERATOR)) {
				final Group pendingMembershipsGroup = this.groupDBManager.getGroupWithPendingMemberships(groupName, session);
				if (present(pendingMembershipsGroup)) {
					myGroup.setPendingMemberships(pendingMembershipsGroup.getMemberships());
				}
			}
			return myGroup;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#getTags(java.lang.Class,
	 * org.bibsonomy.common.enums.GroupingEntity, java.lang.String,
	 * java.lang.String, java.util.List, java.lang.String,
	 * org.bibsonomy.common.enums.SortKey, int, int, java.lang.String,
	 * org.bibsonomy.common.enums.TagSimilarity)
	 */
	@Override
	@Deprecated
	public List<Tag> getTags(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final String search, final String regex, final TagSimilarity relation, final SortKey sortKey, final Date startDate, final Date endDate, final int start, final int end) {
		return this.getTags(resourceType, grouping, groupingName, tags, hash, search, QueryScope.LOCAL, regex, relation, sortKey, startDate, endDate, start, end);
	}

	@Override
	public List<Tag> getTags(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final String search, final QueryScope queryScope, final String regex, final TagSimilarity relation, final SortKey sortKey, final Date startDate, final Date endDate, final int start, final int end) {
		if (GroupingEntity.ALL.equals(grouping)) {
			this.permissionDBManager.checkStartEnd(this.loginUser, grouping, start, end, "tags");
		}

		try (final DBSession session = this.openSession()) {
			final TagParam param = LogicInterfaceHelper.buildParam(TagParam.class, resourceType, queryScope, grouping, groupingName, tags, hash, sortKey, start, end, startDate, endDate, search, null, this.loginUser);
			param.setTagRelationType(relation);
			param.setQueryScope(queryScope);

			if (resourceType == BibTex.class || resourceType == Bookmark.class || resourceType == Resource.class) {
				// this is save because of RTTI-check of resourceType argument
				// which is of class T
				param.setRegex(regex);
				// need to switch from class to string to ensure legibility of
				// Tags.xml
				param.setContentTypeByClass(resourceType);
				param.setResourceType(resourceType);
				return this.tagDBManager.getTags(param, session);
			}

			throw new UnsupportedResourceTypeException("The requested resourcetype (" + resourceType.getClass().getName() + ") is not supported.");
		} catch (final QueryTimeoutException ex) {
			// if a query times out, we return an empty list
			return new ArrayList<>();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getTagDetails(java.lang.String)
	 */
	@Override
	public Tag getTagDetails(final String tagName) {
		try (final DBSession session = this.openSession()) {
			return this.tagDBManager.getTagDetails(this.loginUser, tagName, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#deleteUser(java.lang.String)
	 */
	@Override
	public void deleteUser(final String userName) {
		try (final DBSession session = this.openSession()) {
			// TODO: take care of toLowerCase()!
			this.ensureLoggedIn();
			/*
			 * only an admin or the user himself may delete the account
			 */
			this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);
			this.userDBManager.deleteUser(userName, this.loginUser, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#deleteGroup(java.lang.String)
	 */
	@Override
	public void deleteGroup(final String groupName, final boolean pending, final boolean quickDelete) {
		// needs login.
		this.ensureLoggedIn();

		final DBSession session = this.openSession();

		if (pending) {
			try {
				session.beginTransaction();
				this.permissionDBManager.ensureAdminAccess(this.loginUser);
				final Group pendingGroup = this.groupDBManager.getPendingGroup(groupName, null, session);
				if (!present(pendingGroup)) {
					throw new IllegalStateException("pending group '" + groupName + "' does not exist");
				}
				this.groupDBManager.deletePendingGroup(groupName, session);
				session.commitTransaction();
				return;
			} finally {
				session.endTransaction();
				session.close();
			}
		}

		// only group and system admins are allowed to delete the group
		this.permissionDBManager.ensureIsAdminOrHasGroupRoleOrHigher(this.loginUser, groupName, GroupRole.ADMINISTRATOR);

		try {
			session.beginTransaction();
			// make sure that the group exists
			final Group group = this.groupDBManager.getGroup(this.loginUser.getName(), groupName, true, true, session);

			if (!present(group)) {
				ExceptionUtils.logErrorAndThrowRuntimeException(log, null, "Group ('" + groupName + "') doesn't exist");
			}

			/*
			 * only administrators are allowed to delete an organization
			 */
			if (group.isOrganization()) {
				this.permissionDBManager.ensureAdminAccess(this.loginUser);
			}

			if (present(group.getSubgroups())) {
				ExceptionUtils.logErrorAndThrowRuntimeException(log, null, "Group ('" + groupName + "') is part of a hierarchy and can't be deleted.");
			}

			if (!quickDelete) {
				// ensure that the group has no members except the admin (please not the group user of older groups has role ADMIN)
				final List<GroupMembership> groupMemberships = GroupUtils.getGroupMemberShipsWithoutDummyUser(group.getMemberships());
				if (groupMemberships.size() > 1) {
					ExceptionUtils.logErrorAndThrowRuntimeException(log, null, "Group ('" + group.getName() + "') has at least one member beside the administrator.");
				}
			}

			// all the posts/discussions of the group members (one admin and the dummy user) need to be edited as well before deleting the group
			for (final GroupMembership membership : group.getMemberships()) {
				this.updateUserItemsForLeavingGroup(group, membership.getUser().getName(), session);
			}

			this.groupDBManager.deleteGroup(groupName, quickDelete, this.loginUser, session);
			session.commitTransaction();
		} finally {
			session.endTransaction();
			session.close();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.PostLogicInterface#deletePosts(java.lang.String
	 * , java.util.List)
	 */
	@Override
	public void deletePosts(final String userName, final List<String> resourceHashes) {
		/*
		 * check permissions
		 */
		this.ensureLoggedIn();

		this.permissionDBManager.ensureIsAdminOrSelfOrHasGroupRoleOrHigher(this.loginUser, userName, GroupRole.MODERATOR);

		/*
		 * to store hashes of missing resources
		 */
		final List<String> missingResources = new LinkedList<>();

		try (final DBSession session = this.openSession()) {
			final String lowerCaseUserName = present(userName) ? userName.toLowerCase() : null;
			for (final String resourceHash : resourceHashes) {
				/*
				 * delete one resource
				 */
				boolean resourceFound = false;
				// TODO would be nice to know about the resourcetype or the
				// instance behind this resourceHash
				for (final CrudableContent<? extends Resource, ? extends GenericParam> man : this.allDatabaseManagers.values()) {
					if (man.deletePost(lowerCaseUserName, resourceHash, this.loginUser, session)) {
						resourceFound = true;
						break;
					}
				}
				/*
				 * remember missing resources
				 */
				if (!resourceFound) {
					missingResources.add(resourceHash);
				}
			}
		}
		/*
		 * throw exception for missing resources
		 */
		if (missingResources.size() > 0) {
			throw new IllegalStateException("The resource(s) with ID(s) " + missingResources + " do(es) not exist and could hence not be deleted.");
		}
	}

	/**
	 * Check for each group actually exist and if the
	 * posting user is allowed to post. If yes, insert the correct group ID into
	 * the given post's groups.
	 * @param user
	 * @param groups the groups to validate
	 * @param session
	 */
	protected void validateGroups(final User user, final Set<Group> groups, final DBSession session) {
		/*
		 * First check for "public" and "private". Those two groups are special,
		 * they can't be assigned with another group.
		 */
		if (GroupUtils.containsExclusiveGroup(groups)) {
			if (groups.size() > 1) {
				/*
				 * Those two groups are exclusive - they can not appear together
				 * or with any other group.
				 */
				throw new ValidationException("Group 'public' (or 'private') can not be combined with other groups.");
			}
			/*
			 * only one group and it is "public" or "private" -> set group id
			 * and return post
			 */
			final Group group = groups.iterator().next();
			if (group.equals(GroupUtils.buildPrivateGroup())) {
				group.setGroupId(GroupUtils.buildPrivateGroup().getGroupId());
			} else {
				group.setGroupId(GroupUtils.buildPublicGroup().getGroupId());
			}
		} else {
			/*
			 * only non-special groups remain (including "friends") - check
			 * those
			 */
			/*
			 * retrieve the user's groups
			 */
			final Set<Integer> groupIds = new HashSet<>(this.groupDBManager.getGroupIdsForUser(user.getName(), true, session));
			/*
			 * add "friends" group
			 */
			groupIds.add(GroupID.FRIENDS.getId());
			/*
			 * check that there are only groups the user is allowed to post to.
			 */
			for (final Group group : groups) {
				final Group testGroup = this.groupDBManager.getGroupByName(group.getName().toLowerCase(), session);
				if (testGroup == null) {
					// group does not exist
					throw new ValidationException("Group " + group.getName() + " does not exist");
				}
				if (!groupIds.contains(testGroup.getGroupId())) {
					// the posting user is not a member of this group
					throw new ValidationException("User " + user.getName() + " is not a member of group " + group.getName());
				}
				group.setGroupId(testGroup.getGroupId());
			}
		}

		// no group specified -> make it public
		if (groups.isEmpty()) {
			groups.add(GroupUtils.buildPublicGroup());
		}
	}

	/**
	 * Helper method to retrieve an appropriate database manager
	 *
	 * @param <T>
	 *        extends Resource - the resource type
	 * @param post
	 *        - a post of type T
	 * @return an appropriate database manager
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <T extends Resource> CrudableContent<T, GenericParam> getFittingDatabaseManager(final Post<T> post) {
		final Class<?> resourceClass = post.getResource().getClass();
		CrudableContent<? extends Resource, ? extends GenericParam> man = this.allDatabaseManagers.get(resourceClass);
		if (man == null) {
			for (final Map.Entry<Class<? extends Resource>, CrudableContent<? extends Resource, ? extends GenericParam>> entry : this.allDatabaseManagers.entrySet()) {
				if (entry.getKey().isAssignableFrom(resourceClass)) {
					man = entry.getValue();
					break;
				}
			}
			if (man == null) {
				throw new UnsupportedResourceTypeException();
			}
		}
		return (CrudableContent) man;
	}

	/**
	 * helper method to check if a user is currently logged in
	 */
	private void ensureLoggedIn() {
		if (this.loginUser.getName() == null) {
			throw new AccessDeniedException("Please log in!");
		}
	}

	private void ensureLoggedInAndNoSpammer() {
		this.ensureLoggedIn();
		if (this.loginUser.isSpammer()) {
			throw new AccessDeniedException("You are not allowed to use this function!");
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#createGroup(org.bibsonomy.model
	 * .Group)
	 */
	@Override
	public String createGroup(final Group group) {
		this.ensureLoggedIn();
		/*
		 * check permissions
		 */
		if (this.loginUser.isSpammer()) {
			ExceptionUtils.logErrorAndThrowRuntimeException(log, null, "The user is flagged as spammer - cannot create a group with this name");
		}

		final GroupRequest groupRequest = group.getGroupRequest();
		if (!present(groupRequest)) {
			ExceptionUtils.logErrorAndThrowRuntimeException(log, null, "No group request given");
		} else {
			this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, groupRequest.getUserName());
			// this should be done by some validator
			if (!this.permissionDBManager.isAdmin(this.loginUser) && !present(groupRequest.getReason())) {
				ExceptionUtils.logErrorAndThrowRuntimeException(log, null, "no reason provided");
			}
		}

		/*
		 * only allow administrators to create new organizations
		 */
		final boolean isOrganization = group.isOrganization();
		final String internalGroupId = group.getInternalId();
		if (present(internalGroupId) || isOrganization) {
			this.permissionDBManager.ensureAdminAccess(this.loginUser);
		}

		try (final DBSession session = this.openSession()) {
			this.groupDBManager.createPendingGroup(group, session);

			/*
			 * activate the group immediately if it's an organization
			 */
			if (isOrganization)  {
				this.groupDBManager.activateGroup(group.getName(), this.loginUser, session);
			}

			return group.getName();
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.bibsonomy.model.logic.LogicInterface#restoreGroup(org.bibsonomy.model.Group)
	 */
	@Override
	public String restoreGroup(final Group group) {
		// check admin permissions
		this.permissionDBManager.ensureAdminAccess(this.loginUser);

		try (final DBSession session = this.openSession()) {
			this.groupDBManager.restoreGroup(group, this.loginUser, session);
			return group.getName();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#updateGroup(org.bibsonomy.model
	 * .Group, org.bibsonomy.common.enums.GroupUpdateOperation,
	 * org.bibsonomy.model.GroupMembership)
	 */
	@Override
	public String updateGroup(final Group paramGroup, final GroupUpdateOperation operation, final GroupMembership membership) {
		final String groupName = paramGroup.getName();
		if (!present(paramGroup) || !present(groupName)) {
			throw new ValidationException("No group name given.");
		}

		final String requestedUserName = present(membership) && present(membership.getUser()) && present(membership.getUser().getName()) ? membership.getUser().getName() : null;
		final boolean userSharedDocuments = present(membership) ? membership.isUserSharedDocuments() : false;

		/*
		 * for every operation the user must at least be logged in
		 */
		this.ensureLoggedIn();

		/*
		 * perform operations
		 */
		try (final DBSession session = this.openSession()) {
			session.beginTransaction();

			boolean hasAdminPrivileges = this.permissionDBManager.isAdmin(this.loginUser);

			// check the groups existence and retrieve the current group
			final Group group = this.groupDBManager.getGroup(this.loginUser.getName(), groupName, false, hasAdminPrivileges, session);
			if (!GroupUtils.isValidGroup(group) && !(GroupUpdateOperation.ACTIVATE.equals(operation) || GroupUpdateOperation.DELETE_GROUP_REQUEST.equals(operation))) {
				throw new IllegalArgumentException("Group does not exist");
			}
			final GroupMembership currentGroupMembership = group.getGroupMembershipForUser(requestedUserName);

			// organization related access roles
			if (group.isOrganization()) {
				if(!hasAdminPrivileges) {
					if (GroupUpdateOperation.ADD_INVITED.equals(operation)) {
						assertNotNull(membership);

						// only members of a group may be able to modify something
						GroupMembership loggedInUserMembership = group.getGroupMembershipForUser(this.loginUser.getName());

						if (!present(loggedInUserMembership)) {
							throw new AccessDeniedException("Only members and system admins can invite users to an organization.");
						}

						GroupRole loggedInUserRole = loggedInUserMembership.getGroupRole();

						if (!(GroupRole.MODERATOR.equals(loggedInUserRole) || GroupRole.ADMINISTRATOR.equals(loggedInUserRole))) {
							throw new AccessDeniedException("Only group members with roles: MODERATOR and ADMINISTRATOR can invite users.");
						}
					} else if (GroupUpdateOperation.ADD_MEMBER.equals(operation)) {
						// ADD_MEMBER contains further logic for checking. Here we just make sure that its callable.
					} else {
						throw new AccessDeniedException("Insufficient privileges for modifying an organization.");
					}
					// 2) system admin can perform any op
				} else if (!GroupUpdateOperation.UPDATE_USER_SHARED_DOCUMENTS.equals(operation)) {
					this.permissionDBManager.ensureAdminAccess(this.loginUser);
				}
			}

			// perform actual operation
			switch (operation) {
			case UPDATE_ALL:
				throw new UnsupportedOperationException("The method " + GroupUpdateOperation.UPDATE_ALL + " is not yet implemented.");
			case UPDATE_SETTINGS:
				this.permissionDBManager.ensureGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.ADMINISTRATOR);
				this.groupDBManager.updateGroupSettings(paramGroup, session);
				break;
			case UPDATE_GROUPROLE:

				if (!present(currentGroupMembership)) {
					throw new IllegalArgumentException("The requested user " + requestedUserName + " is not a member of group " + group.getName());
				}

				this.permissionDBManager.ensureGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.MODERATOR);

				// extra check if role change concerns an administrator
				final GroupRole requestedGroupRole = membership.getGroupRole();
				final GroupRole currentGroupRole = currentGroupMembership.getGroupRole();
				if (GroupRole.ADMINISTRATOR.equals(requestedGroupRole) || GroupRole.ADMINISTRATOR.equals(currentGroupRole)) {
					this.permissionDBManager.ensureGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.ADMINISTRATOR);
					// make sure that we keep at least one admin
					if (!GroupRole.ADMINISTRATOR.equals(requestedGroupRole) && this.groupDBManager.hasExactlyOneAdmin(group, session)) {
						throw new IllegalArgumentException("Group has only this admin left, cannot remove this user.");
					}
				}

				this.groupDBManager.updateGroupRole(group.getName(), requestedUserName, requestedGroupRole, this.loginUser, session);
				break;
			case ADD_MEMBER:
				// we need to query the groupMembership, since the group object
				// might not contain the memberships if the loginUser is not
				// allowed to see them
				GroupMembership groupMembership = this.groupDBManager.getPendingMembershipForUserAndGroup(requestedUserName, group.getName(), session);

				// We need to be careful with the exception, since it reveals
				// information about pending memberships
				if (!present(groupMembership)) {
					if (this.permissionDBManager.isAdmin(this.loginUser)) {
						// create a pending membership to activate it later
						this.groupDBManager.addPendingMembership(group.getName(), requestedUserName, userSharedDocuments, GroupRole.REQUESTED, this.loginUser, session);
						groupMembership = this.groupDBManager.getPendingMembershipForUserAndGroup(requestedUserName, groupName, session);
					} else {
						throw new AccessDeniedException("You have not been invited to this group");
					}
				}

				switch (groupMembership.getGroupRole()) {
				case INVITED:
					// only the user themselves can accept an invitation
					this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, requestedUserName);
					this.groupDBManager.addUserToGroup(group.getName(), requestedUserName, userSharedDocuments, GroupRole.USER, this.loginUser, session);
					break;
				case REQUESTED:
					// only mods or admins can accept requests
					this.permissionDBManager.ensureIsAdminOrHasGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.MODERATOR);
					this.groupDBManager.addUserToGroup(group.getName(), requestedUserName, groupMembership.isUserSharedDocuments(), GroupRole.USER, this.loginUser, session);
					break;
				default:
					throw new AccessDeniedException("Can't add this member to the group");
				}
				break;
			case REMOVE_MEMBER:
				// Check for correct role that can remove the user
				if (!present(currentGroupMembership)) {
					throw new IllegalArgumentException("User cannot be removed from group");
				}
				final GroupRole roleOfUserToRemove = currentGroupMembership.getGroupRole();
				if (GroupRole.USER.equals(roleOfUserToRemove)) {
					if (!this.permissionDBManager.isAdminOrSelf(this.loginUser, requestedUserName)) {
						this.permissionDBManager.ensureGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.MODERATOR);
					}
				} else {
					this.permissionDBManager.ensureIsAdminOrHasGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.ADMINISTRATOR);
					// we need at least one admin in the group at all times.
					if (GroupRole.ADMINISTRATOR.equals(roleOfUserToRemove) && this.groupDBManager.hasExactlyOneAdmin(group, session)) {
						throw new IllegalArgumentException("Group has only this admin left, cannot remove this user.");
					}
				}

				this.groupDBManager.removeUserFromGroup(group.getName(), requestedUserName, false, this.loginUser, session);
				this.updateUserItemsForLeavingGroup(group, requestedUserName, session);
				break;
			case UPDATE_USER_SHARED_DOCUMENTS:
				this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, requestedUserName);
				this.groupDBManager.updateUserSharedDocuments(paramGroup, membership, this.loginUser, session);
				break;
			case UPDATE_GROUP_REPORTING_SETTINGS:
				this.permissionDBManager.ensureIsAdminOrHasGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.ADMINISTRATOR);
				this.groupDBManager.updateGroupPublicationReportingSettings(paramGroup, session);
				break;
			case ACTIVATE:
				this.permissionDBManager.ensureAdminAccess(this.loginUser);
				// Use paramGroup since group is unretrievable from the database.
				this.groupDBManager.activateGroup(groupName, this.loginUser, session);
				break;
			case DELETE_GROUP_REQUEST:
				final Group requestedGroup = this.groupDBManager.getPendingGroup(groupName, this.loginUser.getName(), session);
				if (!present(requestedGroup)) {
					throw new AccessDeniedException("You can only delete group requests of groups you have requested.");
				}

				this.groupDBManager.deletePendingGroup(groupName, session);
				break;
			case ADD_INVITED:
				this.permissionDBManager.ensureIsAdminOrHasGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.MODERATOR);
				this.groupDBManager.addPendingMembership(group.getName(), requestedUserName, userSharedDocuments, GroupRole.INVITED, this.loginUser, session);
				break;
			case ADD_REQUESTED:
				// TODO: check for banned users in this group
				// check if the group allows join requests
				if (!group.isAllowJoin()) {
					throw new AccessDeniedException("The group does not allow join group requests.");
				}
				this.groupDBManager.addPendingMembership(group.getName(), requestedUserName, userSharedDocuments, GroupRole.REQUESTED, this.loginUser, session);
				break;
				// TODO: Refactor to one GroupUpdateOperation
			case REMOVE_INVITED:
			case DECLINE_JOIN_REQUEST:
				final GroupMembership currentMembership = this.groupDBManager.getPendingMembershipForUserAndGroup(requestedUserName, group.getName(), session);

				if (!present(currentMembership) || !GroupRole.PENDING_GROUP_ROLES.contains(currentMembership.getGroupRole())) {
					throw new AccessDeniedException("You are not allowed to decline this request/invitation");
				}
				if (GroupRole.INVITED.equals(currentMembership.getGroupRole()) || GroupRole.REQUESTED.equals(currentMembership.getGroupRole())) {
					if (this.permissionDBManager.isAdminOrSelf(this.loginUser, requestedUserName) || this.permissionDBManager.isAdminOrHasGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.ADMINISTRATOR)) {
						this.groupDBManager.removePendingMembership(group.getName(), requestedUserName, session);
					}
				}
				break;
			case UPDATE_PERMISSIONS:
				this.permissionDBManager.ensureAdminAccess(this.loginUser);
				this.groupDBManager.updateGroupLevelPermissions(this.loginUser.getName(), paramGroup, session);
				break;
			default:
				throw new UnsupportedOperationException("The requested method is not yet implemented.");
			}
			session.commitTransaction();
			session.endTransaction();
		}
		return groupName;
	}

	/**
	 * @param group
	 * @param userName
	 * @param session
	 */
	private void updateUserItemsForLeavingGroup(final Group group, final String userName, final DBSession session) {
		// get the id of the group
		final int groupId = group.getGroupId();

		// set all tas shared with the group to private (groupID 1)
		this.tagDBManager.updateTasInGroupFromLeavingUser(userName, groupId, session);

		// FIXME: handle group tas?

		/*
		 * update the visibility of the post that are "assigned" to the group
		 * XXX: a loop over all resource database managers that allow groups
		 */
		this.publicationDBManager.updatePostsInGroupFromLeavingUser(userName, groupId, session);
		this.bookmarkDBManager.updatePostsInGroupFromLeavingUser(userName, groupId, session);

		// set all discussions in the group to private (groupID 1)
		this.discussionDatabaseManager.updateDiscussionsInGroupFromLeavingUser(userName, groupId, session);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.PostLogicInterface#createPosts(java.util.List)
	 */
	@Override
	public List<JobResult> createPosts(List<Post<?>> posts) {
		// TODO: Which of these checks should result in a DatabaseException,
		this.ensureLoggedIn();
		/*
		 * check permissions
		 */
		for (final Post<?> post : posts) {
			PostUtils.populatePost(post, this.loginUser);
			this.permissionDBManager.ensureWriteAccess(post, this.loginUser);
		}

		// XXX: find other solution which does not use BibTex subclasses
		posts = this.replaceImportResources(posts);

		/*
		 * insert posts TODO: more efficient implementation (transactions,
		 * deadlock handling, asynchronous, etc.)
		 */
		final List<JobResult> jobResults = new LinkedList<>();

		final DatabaseException collectedException = new DatabaseException();
		/*
		 * open session to store all the posts
		 */
		try (final DBSession session = this.openSession()) {
			for (final Post<?> post : posts) {
				try {
					jobResults.add(this.createPost(post, session));
					// FIXME: return these exceptions as jobresults
				} catch (final DatabaseException dbex) {
					collectedException.addErrors(dbex);
					log.warn("error message due to exception", dbex);
				} catch (final Exception ex) {
					// some exception other than those covered in the
					// DatabaseException was thrown
					collectedException.addToErrorMessages(PostUtils.getKeyForPost(post), new UnspecifiedErrorMessage(ex));
					log.warn("'unspecified' error message due to exception", ex);
				}
			}
		}

		if (collectedException.hasErrorMessages()) {
			throw collectedException;
		}

		return jobResults;
	}

	private List<Post<?>> replaceImportResources(final List<? extends Post<? extends Resource>> posts) {
		final List<Post<?>> replacedPosts = new LinkedList<>();
		for (final Post<? extends Resource> post : posts) {
			replacedPosts.add(this.replaceImportResource(post));
		}

		return replacedPosts;
	}

	private Post<?> replaceImportResource(final Post<?> post) {
		final Resource resource = post.getResource();
		if (resource instanceof ImportResource) {
			final BibTex parsedResource = this.parsePublicationImportResource((ImportResource) resource);

			final Post<BibTex> replacedPost = new Post<>(post, true);
			replacedPost.setResource(parsedResource);
			return replacedPost;
		}

		return post;
	}

	private BibTex parsePublicationImportResource(final ImportResource resource) {
		final Collection<BibTex> publications = this.publicationReader.read(resource);
		if (!present(publications)) {
			throw new IllegalStateException("bibtexReader did not throw exception and returned empty result");
		}
		return publications.iterator().next();
	}

	/**
	 * Adds a post in the database.
	 */
	private <T extends Resource> JobResult createPost(final Post<T> post, final DBSession session) {
		final CrudableContent<T, GenericParam> manager = this.getFittingDatabaseManager(post);
		post.getResource().recalculateHashes();

		/*
		 * check and set post visibility for synchronization
		 */
		if (Role.SYNC.equals(this.loginUser.getRole())) {
			validateGroupsForSynchronization(post);
		}

		this.validateGroups(post.getUser(), post.getGroups(), session);

		PostUtils.limitedUserModification(post, this.loginUser);
		/*
		 * change group IDs to spam group IDs
		 */
		PostUtils.setGroupIds(post, this.loginUser);

		// if we don't get an exception here, we assume the resource has
		// been successfully created
		return manager.createPost(post, this.loginUser, session);
	}

	@Override
	public List<JobResult> updatePosts(final List<Post<?>> posts, final PostUpdateOperation operation) {
		/*
		 * TODO: Which of these checks should result in a DatabaseException,
		 * which do we want to handle otherwise (=status quo)
		 */
		this.ensureLoggedIn();
		/*
		 * check permissions
		 */
		for (final Post<?> post : posts) {
			PostUtils.populatePost(post, this.loginUser);
			this.permissionDBManager.ensureWriteAccess(post, this.loginUser);
			this.permissionDBManager.ensureApprovalStatusAllowed(post, this.loginUser);
		}

		final List<JobResult> jobResults = new LinkedList<>();
		final DatabaseException collectedException = new DatabaseException();
		try (final DBSession session = this.openSession()) {
			for (final Post<?> post : posts) {
				try {
					jobResults.add(this.updatePost(post, operation, session));
				} catch (final DatabaseException dbex) {
					collectedException.addErrors(dbex);
				} catch (final Exception ex) {
					// some exception other than those covered in the
					// DatabaseException was thrown
					log.error("updating post " + post.getResource().getIntraHash() + "/" + this.loginUser.getName() + " failed", ex);
					collectedException.addToErrorMessages(PostUtils.getKeyForPost(post), new UnspecifiedErrorMessage(ex));
				}
			}
		}

		if (collectedException.hasErrorMessages()) {
			throw collectedException;
		}

		return jobResults;
	}

	/**
	 * Updates a post in the database.
	 */
	private <T extends Resource> JobResult updatePost(final Post<T> post, final PostUpdateOperation operation, final DBSession session) {
		final CrudableContent<T, GenericParam> manager = this.getFittingDatabaseManager(post);
		final String oldIntraHash = post.getResource().getIntraHash();

		if (Role.SYNC.equals(this.loginUser.getRole())) {
			validateGroupsForSynchronization(post);
		}
		this.validateGroups(post.getUser(), post.getGroups(), session);

		PostUtils.limitedUserModification(post, this.loginUser);

		/*
		 * change group IDs to spam group IDs
		 */
		if (post.getUser().equals(this.loginUser)) {
			PostUtils.setGroupIds(post, this.loginUser);
		} else {
			final String postUserName = post.getUser().getName();
			final User groupUserDetails = this.userDBManager.getUserDetails(postUserName, session);
			PostUtils.setGroupIds(post, groupUserDetails);
		}

		/*
		 * XXX: this is a "hack" and will be replaced any time If the operation
		 * is UPDATE_URLS then create/delete the url right here and return the
		 * intra hash.
		 */
		if (PostUpdateOperation.UPDATE_URLS_ADD.equals(operation)) {
			log.debug("Adding URL in updatePost()/DBLogic.java");
			final BibTexExtra resourceExtra = ((BibTex) post.getResource()).getExtraUrls().get(0);

			/*
			 * TODO: here we extract the bibtex extra attributes to build a new
			 * bibtexextra object in the manager/param
			 */
			this.bibTexExtraDBManager.createURL(oldIntraHash, this.loginUser.getName(), resourceExtra.getUrl().toExternalForm(), resourceExtra.getText(), session);
			return JobResult.buildSuccess(oldIntraHash);
		} else if (PostUpdateOperation.UPDATE_URLS_DELETE.equals(operation)) {
			log.debug("Deleting URL in updatePost()/DBLogic.java");
			final BibTexExtra resourceExtra = ((BibTex) post.getResource()).getExtraUrls().get(0);
			this.bibTexExtraDBManager.deleteURL(oldIntraHash, this.loginUser.getName(), resourceExtra.getUrl(), session);

			return JobResult.buildSuccess(oldIntraHash);
		}

		/*
		 * update post
		 *
		 * if we don't get an exception here, we assume the resource has been
		 * successfully updated
		 */
		return manager.updatePost(post, oldIntraHash, this.loginUser, operation, session);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#updateTags(org.bibsonomy.model
	 * .User, java.util.List, java.util.List) <p>TODO: possible options which
	 * one might want to add:</p> <ul> <li>ignore case</li> </ul>
	 */
	@Override
	public int updateTags(final User user, final List<Tag> tagsToReplace, final List<Tag> replacementTags, final boolean updateRelations) {
		this.ensureLoggedIn();
		this.permissionDBManager.ensureWriteAccess(this.loginUser, user.getName());

		try (final DBSession session = this.openSession()) {
			if (updateRelations) {
				if (tagsToReplace.size() != 1 || replacementTags.size() != 1) {
					throw new ValidationException("tag relations can only be updated, when exactly one tag is exchanged by exactly one other tag.");
				}

				this.tagRelationsDBManager.updateTagRelations(user, tagsToReplace.get(0), replacementTags.get(0), session);

			}

			/*
			 * finally delegate to tagDBManager
			 */
			return this.tagDBManager.updateTags(user, tagsToReplace, replacementTags, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#createUser(org.bibsonomy.model
	 * .User)
	 */
	@Override
	public String createUser(final User user) {
		/*
		 * We ensure, that the user is logged in and has admin privileges. This
		 * seems to be a contradiction, because if a user wants to register, he
		 * is not logged in.
		 *
		 * The current solution to this paradox is, that registration is done
		 * using an instance of the DBLogic which contains a user with role
		 * "admin".
		 */
		this.ensureLoggedIn();
		this.permissionDBManager.ensureAdminAccess(this.loginUser);

		return this.storeUser(user, false);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#updateUser(org.bibsonomy.model
	 * .User)
	 */
	@Override
	public String updateUser(final User user, final UserUpdateOperation operation) {
		/*
		 * only logged in users can update user settings.
		 */
		final String username = user.getName();
		if (!UserUpdateOperation.ACTIVATE.equals(operation)) {
			this.ensureLoggedIn();

			/*
			 * group admins can change settings of their group
			 */
			final Group group = this.getGroupDetails(username, false);
			if (GroupUtils.isValidGroup(group)) {
				this.permissionDBManager.ensureIsAdminOrHasGroupRoleOrHigher(this.loginUser, group.getName(), GroupRole.ADMINISTRATOR);
			} else {

				/*
				 * only admins can change settings of /other/ users
				 */
				this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, username);
			}
		}

		try (final DBSession session = this.openSession()) {
			switch (operation) {
				case UPDATE_PASSWORD:
					return this.userDBManager.updatePasswordForUser(user, this.loginUser, session);
				case DELETE_OPENID:
					this.userDBManager.deleteOpenIDUser(username, session);
					return username;
				case UPDATE_SETTINGS:
					return this.userDBManager.updateUserSettingsForUser(user, this.loginUser, session);
				case UPDATE_API:
					this.userDBManager.updateApiKeyForUser(user, this.loginUser, session);
					break;
				case UPDATE_CORE:
					return this.userDBManager.updateUserProfile(user, this.loginUser, session);
				case UPDATE_LIMITED_USER:
					return this.userDBManager.updateLimitedUser(user, this.loginUser, session);
				case ACTIVATE:
					return this.userDBManager.activateUser(user, session);
				case UPDATE_SPAMMER_STATUS:
					/*
					 * only admins are allowed to change spammer settings
					 */
					log.debug("Start update this framework");
					this.permissionDBManager.ensureAdminAccess(this.loginUser);
					/*
					 * open session and update spammer settings
					 */
					final String mode = this.adminDBManager.getClassifierSettings(ClassifierSettings.TESTING, session);
					log.debug("User prediction: " + user.getPrediction());
					return this.adminDBManager.flagSpammer(user, this.getAuthenticatedUser().getName(), mode, session);
				case UPDATE_ALL:
					return this.storeUser(user, true);
				default:
					throw new UnsupportedOperationException(operation + " not supported.");
			}
		}
		return null;
	}

	/**
	 * TODO: extract the method to create and update user
	 *
	 * Adds/updates a user in the database.
	 */
	private String storeUser(final User user, final boolean update) {
		try (final DBSession session = this.openSession()) {
			final User existingUser = this.userDBManager.getUserDetails(user.getName(), session);
			if (update) {
				/*
				 * update the user
				 */
				if (!present(existingUser.getName())) {
					/*
					 * error: user name does not exist
					 */
					throw new ValidationException("user " + user.getName() + " does not exist");
				}

				return this.userDBManager.updateUser(user, this.loginUser, session);
			}

			final List<User> pendingUserList = this.userDBManager.getPendingUserByUsername(user.getName(), 0, Integer.MAX_VALUE, session);
			/*
			 * create a new user
			 */
			if (present(existingUser.getName()) || present(pendingUserList)) {
				/*
				 * error: user name already exists
				 */
				throw new ValidationException("user " + user.getName() + " already exists");
			}
			return this.userDBManager.createUser(user, session);
		}
		/*
		 * TODO: check, if rollback is handled correctly!
		 */
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#getAuthenticatedUser()
	 */
	@Override
	public User getAuthenticatedUser() {
		return this.loginUser;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getAuthors(org.bibsonomy.common
	 * .enums.GroupingEntity, java.lang.String, java.util.List,
	 * java.lang.String, org.bibsonomy.common.enums.SortKey,
	 * org.bibsonomy.common.enums.FilterEntity, int, int, java.lang.String)
	 */
	@Override
	public List<Author> getAuthors(final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final SortKey sortKey, final FilterEntity filter, final int start, final int end, final String search) {
		/*
		 * FIXME: implement a chain or something similar
		 */

		try (final DBSession session = this.openSession()) {
			if (GroupingEntity.ALL.equals(grouping)) {
				return this.authorDBManager.getAuthors(session);
			}

			throw new UnsupportedOperationException("Currently only ALL authors can be listed.");
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#addDocument(org.bibsonomy.model
	 * .Document, java.lang.String)
	 */
	@Override
	public String createDocument(final Document document, final String resourceHash) {
		this.ensureLoggedIn();
		final String userName = document.getUserName();

		/*
		 * users can only modify their own documents
		 */
		this.permissionDBManager.ensureWriteAccess(this.loginUser, userName);

		try (final DBSession session = this.openSession()) {
			if (resourceHash != null) {
				/*
				 * document shall be attached to a post
				 */
				Post<BibTex> post = null;
				try {
					post = this.publicationDBManager.getPostDetails(this.loginUser.getName(), resourceHash, userName, UserUtils.getListOfGroupIDs(this.loginUser), session);
				} catch (final ObjectMovedException | ObjectNotFoundException ex) {
					// ignore
				}
				if (post != null) {
					/*
					 * post really exists!
					 */
					final boolean existingDoc = this.docDBManager.checkForExistingDocuments(userName, resourceHash, document.getFileName(), session);
					if (existingDoc) {
						/*
						 * the post has already a file with that name attached
						 * ...
						 * FIXME: is this really required?
						 */
						this.docDBManager.updateDocument(post.getContentId(), document.getFileHash(), document.getFileName(), document.getDate(), userName, document.getMd5hash(), session);

					} else {
						// add document
						this.docDBManager.addDocument(userName, post.getContentId(), document.getFileHash(), document.getFileName(), document.getMd5hash(), this.loginUser, session);
					}

				} else {
					throw new ValidationException("Could not find a post with hash '" + resourceHash + "'.");
				}

			} else {
				// checks whether a layout definition is already uploaded
				// if not the new one will be stored in the database
				if (this.docDBManager.getDocument(userName, document.getFileHash(), session) == null) {
					this.docDBManager.addDocument(userName, DocumentDatabaseManager.DEFAULT_CONTENT_ID, document.getFileHash(), document.getFileName(), document.getMd5hash(), this.loginUser, session);
				}
			}
		}

		log.info("created new file " + document.getFileName() + " for user " + userName);
		return document.getFileHash();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getDocument(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public Document getDocument(final String userName, final String fileHash) {
		this.ensureLoggedIn();

		final String lowerCaseUserName = userName.toLowerCase();
		this.permissionDBManager.ensureWriteAccess(this.loginUser, lowerCaseUserName);

		try (final DBSession session = this.openSession()) {
			return this.docDBManager.getDocument(lowerCaseUserName, fileHash, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getDocument(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public Document getDocument(final String userName, final String resourceHash, final String fileName) {
		this.ensureLoggedIn();

		final String lowerCaseUserName = userName.toLowerCase();

		try (final DBSession session = this.openSession()) {
			if (present(resourceHash)) {
				/*
				 * we just forward this task to getPostDetails from the
				 * BibTeXDatabaseManager and extract the documents.
				 */
				Post<BibTex> post = null;
				try {
					// FIXME: remove strange getpostdetails method
					post = this.publicationDBManager.getPostDetails(this.loginUser.getName(), resourceHash, lowerCaseUserName, UserUtils.getListOfGroupIDs(this.loginUser), true, session);
				} catch (final ObjectMovedException | ObjectNotFoundException ex) {
					// ignore
				}

				if (post != null && post.getResource().getDocuments() != null) {
					/*
					 * post found and post contains documents (bibtexdbmanager
					 * checks, if user might access documents and only then
					 * inserts them)
					 */
					for (final Document document : post.getResource().getDocuments()) {
						if (document.getFileName().equals(fileName)) {
							return document;
						}
					}
				}
			} else {
				/*
				 * users can only access their own documents
				 */
				this.permissionDBManager.ensureWriteAccess(this.loginUser, lowerCaseUserName);
				/*
				 * TODO: implement access to non post-connected documents
				 */
			}
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.LogicInterface#getDocuments(java.lang.String)
	 */
	@Override
	public List<Document> getDocuments(String userName) {
		this.ensureLoggedIn();

		final String lowerCaseUserName = userName.toLowerCase();
		this.permissionDBManager.ensureWriteAccess(this.loginUser, lowerCaseUserName);

		try (final DBSession session = this.openSession()) {
			return this.docDBManager.getLayoutDocuments(lowerCaseUserName, session);
		}
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.LogicInterface#getDocumentStatistics(org.bibsonomy.common.enums.GroupingEntity, java.lang.String, org.bibsonomy.common.enums.FilterEntity, java.util.Set, java.util.Date, java.util.Date)
	 */
	@Override
	public Statistics getDocumentStatistics(final GroupingEntity groupingEntity, final String grouping, final Set<Filter> filters, final Date startDate, final Date endDate) {
		this.ensureLoggedIn();
		this.permissionDBManager.ensureAdminAccess(this.loginUser); // TOOD: currently only for admins

		try (final DBSession session = this.openSession()) {
			this.handleAdminFilters(filters);

			final StatisticsParam param = LogicInterfaceHelper.buildParam(StatisticsParam.class, null, null, groupingEntity, grouping, null, null, null, -1, -1, startDate, endDate, null, filters, this.loginUser);
			return this.statisticsDBManager.getDocumentStatistics(param, session);
		} catch (final QueryTimeoutException ex) {
			// if a query times out, we return 0 (cause we also return empty
			// list when a query timeout exception is thrown)
			return new Statistics(0);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#renameDocument(org.bibsonomy
	 * .model.Document, java.lang.String, java.lang.String)
	 */
	@Override
	public void updateDocument(final String userName, final String resourceHash, final String documentName, final Document document) {
		/*
		 * users can only modify their own documents
		 */
		this.ensureLoggedIn();
		this.permissionDBManager.ensureWriteAccess(this.loginUser, userName);

		try (final DBSession session = this.openSession()) {
			final String newName = document.getFileName();
			if (resourceHash != null) {
				/*
				 * the document belongs to a post --> check if the user owns the
				 * post
				 */
				Post<BibTex> post = null;
				try {
					post = this.publicationDBManager.getPostDetails(this.loginUser.getName(), resourceHash, userName, UserUtils.getListOfGroupIDs(this.loginUser), session);
				} catch (final ObjectMovedException | ObjectNotFoundException ex) {
					// ignore
				}
				if (post != null) {
					/*
					 * the given resource hash belongs to a post of the user ->
					 * rename the corresponding document to the new name
					 */
					final Document existingDocument = this.docDBManager.getDocumentForPost(userName, resourceHash, documentName, session);
					if (present(existingDocument)) {
						this.docDBManager.updateDocument(post.getContentId().intValue(), existingDocument.getFileHash(), newName, existingDocument.getDate(),
										userName, existingDocument.getMd5hash(), session);
					}
				} else {
					throw new ValidationException("Could not find a post with hash '" + resourceHash + "'.");
				}
			} else {
				throw new ValidationException("update document without resourceHash is not possible");
			}
			log.debug("renamed document " + documentName + " from user " + userName + "to " + newName);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#deleteDocument(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteDocument(final Document document, final String resourceHash) {
		this.ensureLoggedIn();

		final String userName = document.getUserName();
		/*
		 * users can only modify their own documents
		 */
		this.permissionDBManager.ensureWriteAccess(this.loginUser, userName);

		try (final DBSession session = this.openSession()) {
			if (resourceHash != null) {
				/*
				 * the document belongs to a post --> check if the user owns the
				 * post
				 */
				Post<BibTex> post = null;
				try {
					post = this.publicationDBManager.getPostDetails(this.loginUser.getName(), resourceHash, userName, UserUtils.getListOfGroupIDs(this.loginUser), session);
				} catch (final ObjectMovedException | ObjectNotFoundException ex) {
					// ignore
				}
				if (post != null) {
					/*
					 * the given resource hash belongs to a post of the user ->
					 * delete the corresponding document
					 */
					final Document existingDocument = this.docDBManager.getDocumentForPost(userName, resourceHash, document.getFileName(), session);
					if (present(existingDocument)) {
						this.docDBManager.deleteDocument(post.getContentId(), existingDocument, session);
					}
				} else {
					throw new ValidationException("Could not find a post with hash '" + resourceHash + "'.");
				}
			} else {
				/*
				 * the document does not belong to a post
				 */
				this.docDBManager.deleteDocumentWithNoPost(DocumentDatabaseManager.DEFAULT_CONTENT_ID, userName, document.getFileHash(), this.loginUser, session);
			}
		}
		log.debug("deleted document " + document.getFileName() + " from user " + userName);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#addInetAddressStatus(java.net
	 * .InetAddress, org.bibsonomy.common.enums.InetAddressStatus)
	 */
	@Override
	public void createInetAddressStatus(final InetAddress address, final InetAddressStatus status) {
		this.ensureLoggedIn();
		// only admins are allowed to change the status of an address
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			this.adminDBManager.addInetAddressStatus(address, status, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#deleteInetAdressStatus(java.
	 * net.InetAddress)
	 */
	@Override
	public void deleteInetAdressStatus(final InetAddress address) {
		this.ensureLoggedIn();
		// only admins are allowed to change the status of an address
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			this.adminDBManager.deleteInetAdressStatus(address, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getInetAddressStatus(java.net
	 * .InetAddress)
	 */
	@Override
	public InetAddressStatus getInetAddressStatus(final InetAddress address) {
		this.ensureLoggedIn();
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.adminDBManager.getInetAddressStatus(address, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.PostLogicInterface#getPostStatistics(java.lang
	 * .Class, org.bibsonomy.common.enums.GroupingEntity, java.lang.String,
	 * java.util.List, java.lang.String, org.bibsonomy.common.enums.SortKey,
	 * org.bibsonomy.common.enums.FilterEntity, int, int, java.lang.String,
	 * org.bibsonomy.common.enums.StatisticsConstraint)
	 */
	@Override
	public Statistics getPostStatistics(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final String search, final Set<Filter> filters, final SortKey sortKey, final Date startDate, final Date endDate, final int start, final int end) {

		try (final DBSession session = this.openSession()) {
			this.handleAdminFilters(filters);

			final StatisticsParam param = LogicInterfaceHelper.buildParam(StatisticsParam.class, resourceType, null, grouping, groupingName, tags, hash, sortKey, start, end, startDate, endDate, search, filters, this.loginUser);
			if (resourceType == GoldStandardPublication.class || resourceType == BibTex.class || resourceType == Bookmark.class || resourceType == Resource.class) {
				param.setContentTypeByClass(resourceType);
				return this.statisticsDBManager.getPostStatistics(param, session);
			}

			throw new UnsupportedResourceTypeException("The requested resourcetype (" + resourceType.getClass().getName() + ") is not supported.");
		} catch (final QueryTimeoutException ex) {
			// if a query times out, we return 0 (cause we also return empty
			// list when a query timeout exception is thrown)
			return new Statistics(0);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getConcepts(java.lang.Class,
	 * org.bibsonomy.common.enums.GroupingEntity, java.lang.String,
	 * java.lang.String, java.util.List,
	 * org.bibsonomy.common.enums.ConceptStatus, int, int)
	 */
	@Override
	public List<Tag> getConcepts(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final String regex, final List<String> tags, final ConceptStatus status, final int start, final int end) {
		try (final DBSession session = this.openSession()) {
			final TagRelationParam param = LogicInterfaceHelper.buildParam(TagRelationParam.class, resourceType, null, grouping, groupingName, tags, null, null, start, end, null, null, null, null, this.loginUser);
			param.setConceptStatus(status);
			return this.tagRelationsDBManager.getConcepts(param, session);
		}
	}

	@Override
	public Tag getConceptDetails(final String conceptName, final GroupingEntity grouping, final String groupingName) {
		try (final DBSession session = this.openSession()) {
			if (GroupingEntity.USER.equals(grouping) || GroupingEntity.GROUP.equals(grouping) && present(groupingName)) {
				return this.tagRelationsDBManager.getConceptForUser(conceptName, groupingName, session);
			} else if (GroupingEntity.ALL.equals(grouping)) {
				return this.tagRelationsDBManager.getGlobalConceptByName(conceptName, session);
			}

			throw new RuntimeException("Can't handle request");
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#createConcept(org.bibsonomy.
	 * model.Tag, org.bibsonomy.common.enums.GroupingEntity, java.lang.String)
	 */
	@Override
	public String createConcept(final Tag concept, final GroupingEntity grouping, final String groupingName) {
		if (GroupingEntity.USER.equals(grouping)) {
			this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, groupingName);
			return this.storeConcept(concept, grouping, groupingName, false);
		}
		throw new UnsupportedOperationException("Currently, tag relations can only be created for users.");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#deleteConcept(java.lang.String,
	 * org.bibsonomy.common.enums.GroupingEntity, java.lang.String)
	 */
	@Override
	public void deleteConcept(final String concept, final GroupingEntity grouping, final String groupingName) {
		if (GroupingEntity.USER.equals(grouping)) {
			this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, groupingName);

			try (final DBSession session = this.openSession()) {
				this.tagRelationsDBManager.deleteConcept(concept, groupingName, session);
			}
			return;
		}
		throw new UnsupportedOperationException("Currently, tag relations can only be deleted for users.");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#deleteRelation(java.lang.String,
	 * java.lang.String, org.bibsonomy.common.enums.GroupingEntity,
	 * java.lang.String)
	 */
	@Override
	public void deleteRelation(final String upper, final String lower, final GroupingEntity grouping, final String groupingName) {
		if (GroupingEntity.USER.equals(grouping)) {
			this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, groupingName);

			try (final DBSession session = this.openSession()) {
				this.tagRelationsDBManager.deleteRelation(upper, lower, groupingName, session);
				return;
			}
		}
		throw new UnsupportedOperationException("Currently, tag relations can only be created for users.");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#updateConcept(org.bibsonomy.
	 * model.Tag, org.bibsonomy.common.enums.GroupingEntity, java.lang.String)
	 */
	@Override
	public String updateConcept(final Tag concept, final GroupingEntity grouping, final String groupingName, final ConceptUpdateOperation operation) {
		if (!GroupingEntity.USER.equals(grouping)) {
			throw new UnsupportedOperationException("Currently only user's can have concepts.");
		}

		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, groupingName);

		// now switch the operation and call the right method in the
		// taglRelationsDBManager or DBLogic
		try (final DBSession session = this.openSession()) {
			switch (operation) {
				case UPDATE:
					return this.storeConcept(concept, grouping, groupingName, true);
				case PICK:
					this.tagRelationsDBManager.pickConcept(concept, groupingName, session);
					break;
				case UNPICK:
					this.tagRelationsDBManager.unpickConcept(concept, groupingName, session);
					break;
				case UNPICK_ALL:
					this.tagRelationsDBManager.unpickAllConcepts(groupingName, session);
					return null;
				case PICK_ALL:
					this.tagRelationsDBManager.pickAllConcepts(groupingName, session);
					return null;
			}

			return concept.getName();
		}

	}

	/**
	 * Helper method to store a concept
	 *
	 * @param concept
	 * @param grouping
	 * @param groupingName
	 * @param update
	 * @return
	 */
	private String storeConcept(final Tag concept, final GroupingEntity grouping, final String groupingName, final boolean update) {
		try (final DBSession session = this.openSession()) {
			if (!update) {
				this.deleteConcept(concept.getName(), grouping, groupingName);
			}
			this.tagRelationsDBManager.insertRelations(concept, groupingName, session);
			return concept.getName();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#getUsers(java.lang.Class,
	 * org.bibsonomy.common.enums.GroupingEntity, java.lang.String,
	 * java.util.List, java.lang.String, org.bibsonomy.common.enums.SortKey,
	 * org.bibsonomy.common.enums.UserRelation, java.lang.String, int, int)
	 */
	@Override
	public List<User> getUsers(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final SortKey sortKey, final UserRelation relation, final String search, final int start, final int end) {
		// assemble param object
		final UserParam param = LogicInterfaceHelper.buildParam(UserParam.class, resourceType, null, grouping, groupingName, tags, hash, sortKey, start, end, null, null, search, null, this.loginUser);
		param.setUserRelation(relation);

		// check start/end values
		if (GroupingEntity.ALL.equals(grouping)) {
			this.permissionDBManager.checkStartEnd(this.loginUser, grouping, start, end, "users");
		}

		try (final DBSession session = this.openSession()) {
			// start chain
			return this.userDBManager.getUsers(param, session);
		}
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.LogicInterface#getUserStatistics()
	 */
	@Override
	public Statistics getUserStatistics(final GroupingEntity grouping, final Set<Filter> filters, final Classifier classifier, final SpamStatus status, final Date startDate, final Date endDate) {
		try (final DBSession session = this.openSession()) {
			return this.statisticsDBManager.getUserStatistics(grouping, startDate, filters, classifier, status, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getClassifiedUsers(org.bibsonomy
	 * .common.enums.Classifier, org.bibsonomy.common.enums.SpamStatus, int)
	 */
	@Override
	public List<User> getClassifiedUsers(final Classifier classifier, final SpamStatus status, final int limit) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.adminDBManager.getClassifiedUsers(classifier, status, limit, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getClassifierSettings(org.bibsonomy
	 * .common.enums.ClassifierSettings)
	 */
	@Override
	public String getClassifierSettings(final ClassifierSettings key) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.adminDBManager.getClassifierSettings(key, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#updateClassifierSettings(org
	 * .bibsonomy.common.enums.ClassifierSettings, java.lang.String)
	 */
	@Override
	public void updateClassifierSettings(final ClassifierSettings key, final String value) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			this.adminDBManager.updateClassifierSettings(key, value, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getClassifierHistory(java.lang
	 * .String)
	 */
	@Override
	public List<User> getClassifierHistory(final String userName) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.adminDBManager.getClassifierHistory(userName, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getClassifierComparison(int)
	 */
	@Override
	public List<User> getClassifierComparison(final int interval, final int limit) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.adminDBManager.getClassifierComparison(interval, limit, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getOpenIDUser(java.lang.String)
	 */
	@Override
	public String getOpenIDUser(final String openID) {
		try (final DBSession session = this.openSession()) {
			return this.userDBManager.getOpenIDUser(openID, session);
		}
	}

	/*
	 * FIXME: implement this method as chain element of getUsers()
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#getUsernameByLdapUserId()
	 */
	@Override
	public String getUsernameByLdapUserId(final String userId) {
		try (final DBSession session = this.openSession()) {
			return this.userDBManager.getUsernameByLdapUser(userId, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getUsernameByRemoteUserId(org
	 * .bibsonomy.model.user.remote.RemoteUserId)
	 */
	@Override
	public String getUsernameByRemoteUserId(final RemoteUserId remoteUserId) {
		try (final DBSession session = this.openSession()) {
			return this.userDBManager.getUsernameByRemoteUser(remoteUserId, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getTagStatistics(java.lang.Class
	 * , org.bibsonomy.common.enums.GroupingEntity, java.lang.String,
	 * java.lang.String, java.util.List,
	 * org.bibsonomy.common.enums.ConceptStatus, int, int)
	 */
	@Override
	public int getTagStatistics(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String regex, final ConceptStatus status, final Set<Filter> filters, final Date startDate, final Date endDate, final int start, final int end) {
		try (final DBSession session = this.openSession()) {
			final StatisticsParam param = LogicInterfaceHelper.buildParam(StatisticsParam.class, resourceType, null, grouping, groupingName, tags, null, null, start, end, startDate, endDate, null, filters, this.loginUser);
			if (present(resourceType)) {
				param.setContentTypeByClass(resourceType);
			}

			param.setConceptStatus(status);
			return this.statisticsDBManager.getTagStatistics(param, session);
		}
	}

	/*
	 * We create a UserRelation of the form (sourceUser, targetUser)\in relation
	 * This Method only works for the FOLLOWER_OF and the OF_FRIEND relation
	 * Other relation will result in an UnsupportedRelationException
	 *
	 * TODO: the "tag" parameter is currently ignored by this function. As soon
	 * as tagged relationships are needed, please implement the handling of
	 * the "tag" parameter from here on (mainly in the UserDBManager)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#insertUserRelationship()
	 */
	@Override
	public void createUserRelationship(final String sourceUser, final String targetUser, final UserRelation relation, final String tag) {
		this.ensureLoggedIn();
		/*
		 * relationships can only be created by the logged-in user or admins
		 */
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, sourceUser);

		/*
		 * finally try to create relationship
		 */
		try (final DBSession session = this.openSession()) {
			/*
			 * check if relationship may be created (e.g. some special users
			 * like 'dblp' are disallowed)
			 */
			this.permissionDBManager.checkUserRelationship(this.loginUser, this.userDBManager.getUserDetails(targetUser, session), relation, tag);
			this.userDBManager.createUserRelation(sourceUser, targetUser, relation, tag, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#getUserRelationship(java.lang
	 * .String, org.bibsonomy.common.enums.UserRelation)
	 */
	@Override
	public List<User> getUserRelationship(final String sourceUser, final UserRelation relation, final String tag) {
		this.ensureLoggedIn();
		// TODO: ask Robert about this method
		// this.permissionDBManager.checkUserRelationship(sourceUser,
		// targetUser, relation);
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, sourceUser);

		try (final DBSession session = this.openSession()) {
			// get all users that are in relation with sourceUser
			return this.userDBManager.getUserRelation(sourceUser, relation, tag, session);
		}
		// unsupported relations will cause an UnsupportedRelationException
	}

	/*
	 * We delete a UserRelation of the form (sourceUser, targetUser)\in relation
	 * This Method only works for the FOLLOWER_OF and the OF_FRIEND relation
	 * Other relation will result in an UnsupportedRelationException FIXME: use
	 * Strings (usernames) instead of users
	 *
	 * TODO: the "tag" parameter is currently ignored by this function. As soon
	 * as tagged relationships are needed, please implement the handling of
	 * the "tag" parameter from here on (mainly in the UserDBManager)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#deleteUserRelationship()
	 */
	@Override
	public void deleteUserRelationship(final String sourceUser, final String targetUser, final UserRelation relation, final String tag) {
		this.ensureLoggedIn();
		// ask Robert about this method
		// this.permissionDBManager.checkUserRelationship(sourceUser,
		// targetUser, relation);
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, sourceUser);

		try (final DBSession session = this.openSession()) {
			this.userDBManager.deleteUserRelation(sourceUser, targetUser, relation, tag, session);
		}
		// unsupported Relations will cause an UnsupportedRelationException
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#createClipboardItems()
	 */
	@Override
	public int createClipboardItems(final List<Post<? extends Resource>> posts) {
		this.ensureLoggedIn();

		try (final DBSession session = this.openSession()) {
			for (final Post<? extends Resource> post : posts) {
				if (post.getResource() instanceof Bookmark) {
					throw new UnsupportedResourceTypeException("Bookmarks can't be stored in the clipboard");
				}
				/*
				 * get the complete post from the database
				 */
				final String intraHash = post.getResource().getIntraHash();
				final String postUserName = post.getUser().getName();
				final Post<BibTex> copy = this.publicationDBManager.getPostDetails(this.loginUser.getName(), intraHash, postUserName, UserUtils.getListOfGroupIDs(this.loginUser), session);

				/*
				 * post might be null, because a) it does not exist b) user may
				 * not access it
				 */
				if (copy == null) {
					/*
					 * TODO: exception handling?!
					 */
					throw new ValidationException("Post with hash " + intraHash + " of user " + postUserName + " not found!");
				}

				/*
				 * insert the post from the user's clipboard
				 */
				this.clipboardDBManager.createItem(this.loginUser.getName(), copy.getContentId(), session);
			}

			// get actual clipboard size
			return this.clipboardDBManager.getNumberOfClipboardEntries(this.loginUser.getName(), session);
		} catch (final Exception ex) {
			log.error(ex);
			throw new RuntimeException(ex);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#deleteClipboardItems()
	 */
	@Override
	public int deleteClipboardItems(final List<Post<? extends Resource>> posts, final boolean clearClipboard) {
		this.ensureLoggedIn();

		try (final DBSession session = this.openSession()) {
			// decide which delete function will be called
			if (clearClipboard) {
				// clear all in clipboard
				this.clipboardDBManager.deleteAllItems(this.loginUser.getName(), session);
			} else {
				// delete specific post
				for (final Post<? extends Resource> post : posts) {
					if (post.getResource() instanceof Bookmark) {
						throw new UnsupportedResourceTypeException("Bookmarks can't be stored in the clipboard");
					}
					/*
					 * get the content_id from the database
					 */
					final Integer contentIdOfPost = this.publicationDBManager.getContentIdForPost(post.getResource().getIntraHash(), post.getUser().getName(), session);
					if (!present(contentIdOfPost)) {
						throw new ValidationException("Post not found. Can't remove post from clipboard.");
					}
					/*
					 * delete the post from the user's clipboard
					 */
					this.clipboardDBManager.deleteItem(this.loginUser.getName(), contentIdOfPost, session);
				}
			}

			// get actual clipboardsize
			return this.clipboardDBManager.getNumberOfClipboardEntries(this.loginUser.getName(), session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.LogicInterface#deleteInboxMessages(java.util
	 * .List, boolean)
	 */
	@Override
	public int deleteInboxMessages(final List<Post<? extends Resource>> posts, final boolean clearInbox) {
		/*
		 * check permissions
		 */
		this.ensureLoggedIn();
		/*
		 * delete one message from the inbox
		 */
		try (final DBSession session = this.openSession()) {
			if (clearInbox) {
				this.inboxDBManager.deleteAllInboxMessages(this.loginUser.getName(), session);
			} else {
				for (final Post<? extends Resource> post : posts) {
					final String sender = post.getUser().getName();
					final String receiver = this.loginUser.getName();
					final String resourceHash = post.getResource().getIntraHash();
					if (!present(receiver) || !present(resourceHash)) {
						/*
						 * FIXME: proper exception message!
						 */
						throw new ValidationException("You are not authorized to perform the requested operation");
					}
					this.inboxDBManager.deleteInboxMessage(sender, receiver, resourceHash, session);
				}
			}
			return this.inboxDBManager.getNumInboxMessages(this.loginUser.getName(), session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.GoldStandardPostLogicInterface#createRelation
	 * (java.lang.String, java.util.Set)
	 */
	@Override
	public void createResourceRelations(final String postHash, final Set<String> references, final GoldStandardRelation relation) {
		// only admins can create references
		this.permissionDBManager.ensureAdminAccess(this.loginUser);

		try (final DBSession session = this.openSession()) {
			this.goldStandardPublicationDBManager.addRelationsToPost(this.loginUser.getName(), postHash, references, relation, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.GoldStandardPostLogicInterface#deleteReferences
	 * (java.lang.String, java.util.Set)
	 */
	@Override
	public void deleteResourceRelations(final String postHash, final Set<String> references, final GoldStandardRelation relation) {
		// only admins can delete references
		this.permissionDBManager.ensureAdminAccess(this.loginUser);

		try (final DBSession session = this.openSession()) {
			this.goldStandardPublicationDBManager.removeRelationsFromPost(this.loginUser.getName(), postHash, references, relation, session);
		}
	}

	/**
	 * This method creates a new wiki for a user given by its username.
	 *
	 * @param userName the user for whom this wiki is to be created.
	 * @param wiki the wiki for userName.
	 */
	@Override
	public void createWiki(final String userName, final Wiki wiki) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);

		try (final DBSession session = this.openSession()) {
			this.wikiDBManager.createWiki(userName, wiki, session);
		}
	}

	/**
	 * Retrieves a wiki from the database.
	 *
	 * @see org.bibsonomy.model.logic.LogicInterface#getWiki(java.lang.String, java.util.Date)
	 * @param userName the user for whom the wiki is to be retrieved.
	 * @param date - if <code>null</code>, the latest version of the wiki is
	 *        returned. Otherwise, the latest version before <code>date</code>.
	 * @return the current wiki for userName, latest before date or an empty
	 *         wiki if the
	 *         logged in user isn't allowed to access userName's wiki.
	 */
	@Override
	public Wiki getWiki(final String userName, final Date date) {
		try (final DBSession session = this.openSession()) {
			final User requUser = this.getUserDetails(userName);
			/*
			 * We return an empty wiki for users who are not allowed to access
			 * this wiki.
			 */
			if (!this.permissionDBManager.isAllowedToAccessUsersProfile(requUser, this.loginUser, session)) {
				return new Wiki();
			}

			if (!present(date)) {
				return this.wikiDBManager.getCurrentWiki(userName, session);
			}

			/*
			 * TODO: remove this comment when the time is right!
			 * this will never happen to get called because right now
			 * (29.04.2013)
			 * this method is only called with date = null.
			 */
			return this.wikiDBManager.getPreviousWiki(userName, date, session);
		}
	}

	/**
	 * This method will not be used yet, still it has to come here because of
	 * inheritance issues. It isn't called from anywhere anyway, yet.
	 *
	 * This method will retrieve old versions of a user's wiki for reversing
	 * actions or changes in the wiki.
	 *
	 * @param userName the name of the requesting user
	 * @return a list of dates where the wiki of userName has been changed.
	 */
	@Override
	public List<Date> getWikiVersions(final String userName) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, userName);

		try (final DBSession session = this.openSession()) {
			return this.wikiDBManager.getWikiVersions(userName, session);
		}
	}

	/**
	 * updates the current wiki with the new one.
	 */
	@Override
	public void updateWiki(final String userName, final Wiki wiki) {
		if (!this.permissionDBManager.isAdminOrSelf(this.loginUser, userName)) {
			// if we are here then the user is not the logged in one which means it is a group user
			if (!this.permissionDBManager.isAdminOrHasGroupRoleOrHigher(this.loginUser, userName, GroupRole.MODERATOR)) {
				throw new AccessDeniedException();
			}
		}

		try (final DBSession session = this.openSession()) {
			final Wiki currentWiki = this.wikiDBManager.getCurrentWiki(userName, session);

			/*
			 * Check if the wiki exists
			 */
			if (currentWiki != null) {

				/*
				 * Check if the text has changed compared to the
				 * current version in the database.
				 *
				 * If currentWikiText is null, we just interpret this
				 * as a missing wiki (shouldn't happen that much anymore)
				 * and set the contents to an empty string.
				 */
				String currentWikiText = currentWiki.getWikiText();
				if (currentWikiText == null) {
					currentWikiText = "";
				}

				/*
				 * If we find differences, update the database.
				 */
				if (!currentWikiText.equals(wiki.getWikiText())) {
					this.wikiDBManager.updateWiki(userName, wiki, session);
					this.wikiDBManager.logWiki(userName, currentWiki, session);
				}

				/*
				 * a wiki does not exist, at least there is nothing in the
				 * database.
				 * This should never happen after the 2.0.35 release, because
				 * we will create a wiki for each new user. All the old users
				 * should
				 * have been updated as well then.
				 */
			} else {
				this.createWiki(userName, wiki);
			}
		}
	}

	@Override
	public void createExtendedField(final Class<? extends Resource> resourceType, final String userName, final String intraHash, final String key, final String value) {

		try (final DBSession session = this.openSession()) {
			if (BibTex.class == resourceType) {
				this.publicationDBManager.createExtendedField(userName, intraHash, key, value, session);
			} else {
				throw new UnsupportedResourceTypeException("The requested resourcetype (" + resourceType.getClass().getName() + ") is not supported.");
			}
		}
	}

	@Override
	public void deleteExtendedField(final Class<? extends Resource> resourceType, final String userName, final String intraHash, final String key, final String value) {

		try (final DBSession session = this.openSession()) {
			if (BibTex.class == resourceType) {
				if (!present(key)) {
					this.publicationDBManager.deleteAllExtendedFieldsData(userName, intraHash, session);
				} else {
					if (!present(value)) {
						this.publicationDBManager.deleteExtendedFieldsByKey(userName, intraHash, key, session);
					} else {
						this.publicationDBManager.deleteExtendedFieldByKeyValue(userName, intraHash, key, value, session);
					}
				}
			} else {
				throw new UnsupportedResourceTypeException("The requested resourcetype (" + resourceType.getClass().getName() + ") is not supported.");
			}
		}
	}

	@Override
	public Map<String, List<String>> getExtendedFields(final Class<? extends Resource> resourceType, final String userName, final String intraHash, final String key) {

		try (final DBSession session = this.openSession()) {
			if (BibTex.class == resourceType) {
				return this.publicationDBManager.getExtendedFields(userName, intraHash, key, session);
			}

			throw new UnsupportedResourceTypeException("The requested resourcetype (" + resourceType.getClass().getName() + ") is not supported.");
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.ReviewLogicInterface#getReviews(java.lang.String
	 * )
	 */
	@Override
	public List<DiscussionItem> getDiscussionSpace(final String interHash) {
		try (final DBSession session = this.openSession()) {
			return this.discussionDatabaseManager.getDiscussionSpace(this.loginUser, interHash, session);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.DiscussionLogicInterface#createDiscussionItem
	 * (java.lang.String, java.lang.String, org.bibsonomy.model.DiscussionItem)
	 */
	@Override
	public void createDiscussionItem(final String interHash, final String username, final DiscussionItem discussionItem) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, username);

		final DBSession session = this.openSession();
		session.beginTransaction();
		/*
		 * TODO: Only checking should be done, GoldstandardCreation is the job
		 * of the calling Controller
		 * FIXME: dzo:  move gold standard creation to logic else we have to implement the logic in
		 * the discussion ajax controller also for the api; to be discussed
		 */
		try {
			// verify that there exists a gold standard
			final Post<? extends Resource> goldStandardPost = this.getPostDetails(interHash, GoldStandardPostLogicInterface.GOLD_STANDARD_USER_NAME);
			if (!present(goldStandardPost)) {
				throw new ObjectNotFoundException(interHash);
			}
			/*
			 * create the discussion item
			 */
			discussionItem.setResourceType(goldStandardPost.getResource().getClass());
			final User commentUser = this.userDBManager.getUserDetails(username, session);
			discussionItem.setUser(commentUser);
			this.createDiscussionItem(interHash, discussionItem, session);
			session.commitTransaction();
		} finally {
			session.endTransaction();
			session.close();
		}
	}

	private void prepareDiscussionItem(final User commentUser, final Set<Group> groups, final DBSession session) {
		this.validateGroups(commentUser, groups, session);

		// transfer to spammer group id's if neccessary
		GroupUtils.prepareGroups(groups, commentUser.isSpammer());
	}

	private <D extends DiscussionItem> void createDiscussionItem(final String interHash, final D discussionItem, final DBSession session) {
		this.prepareDiscussionItem(discussionItem.getUser(), discussionItem.getGroups(), session);
		this.getCommentDatabaseManager(discussionItem).createDiscussionItemForResource(interHash, discussionItem, session);
	}

	@SuppressWarnings("unchecked")
	private <D extends DiscussionItem> DiscussionItemDatabaseManager<D> getCommentDatabaseManager(final DiscussionItem discussionItem) {
		return (DiscussionItemDatabaseManager<D>) this.allDiscussionManagers.get(discussionItem.getClass());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.DiscussionLogicInterface#updateDiscussionItem
	 * (java.lang.String, java.lang.String, org.bibsonomy.model.DiscussionItem)
	 */
	@Override
	public void updateDiscussionItem(final String username, final String interHash, final DiscussionItem discussionItem) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, username);

		try (final DBSession session = this.openSession()) {
			final User commentUser = this.userDBManager.getUserDetails(username, session);
			discussionItem.setUser(commentUser);

			this.updateDiscussionItemForUser(interHash, discussionItem, session);
		}
	}

	private <D extends DiscussionItem> void updateDiscussionItemForUser(final String interHash, final D discussionItem, final DBSession session) {
		this.prepareDiscussionItem(discussionItem.getUser(), discussionItem.getGroups(), session);
		this.getCommentDatabaseManager(discussionItem).updateDiscussionItemForResource(interHash, discussionItem.getHash(), discussionItem, session);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.bibsonomy.model.logic.DiscussionLogicInterface#deleteDiscussionItem
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteDiscussionItem(final String username, final String interHash, final String commentHash) {
		this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, username);

		try (final DBSession session = this.openSession()) {
			final User user = this.userDBManager.getUserDetails(username, session);
			for (final DiscussionItemDatabaseManager<? extends DiscussionItem> discussionItemManager : this.allDiscussionManagers.values()) {
				if (discussionItemManager.deleteDiscussionItemForResource(interHash, user, commentHash, session)) {
					return;
				}
			}
		}
	}

	@Override
	public List<PostMetaData> getPostMetaData(final HashID hashType, final String resourceHash, final String userName, final String metaDataPluginKey) {
		try (final DBSession session = this.openSession()) {
			return this.publicationDBManager.getPostMetaData(hashType, resourceHash, userName, metaDataPluginKey, session);
		}
	}

	@Deprecated
	@Override
	public List<Tag> getTagRelation(final int start, final int end, final TagRelation relation, final List<String> tagNames) {
		// TODO Auto-generated method stub
		return null;
	}

	private void handleAdminFilters(final Set<Filter> filters) {
		/*
		 * if filter is set to spam posts admins can see public spam!
		 */
		if (ValidationUtils.safeContains(filters, FilterEntity.ADMIN_SPAM_POSTS)) {
			this.permissionDBManager.ensureAdminAccess(this.loginUser);
			// add public spam group to the groups of the loggedin users
			this.loginUser.addGroup(new Group(GroupID.PUBLIC_SPAM));
		}
	}

	@Override
	public List<Person> getPersons(final PersonQuery query) {
		try (final DBSession session = this.openSession()) {
			return this.personDBManager.getPersons(query, this.loginUser, session);
		}
	}

	@Override
	public void createResourceRelation(final ResourcePersonRelation resourcePersonRelation) throws ResourcePersonAlreadyAssignedException {
		this.ensureLoggedInAndNoSpammer();

		ValidationUtils.assertNotNull(resourcePersonRelation.getPerson());
		ValidationUtils.assertNotNull(resourcePersonRelation.getPerson().getPersonId());
		ValidationUtils.assertNotNull(resourcePersonRelation.getRelationType());

		// this calls query getResourcePersonRelationByResourcePersonRelation in Person.xml
		final ResourcePersonRelationQueryBuilder builder = new ResourcePersonRelationQueryBuilder()
						.byInterhash(resourcePersonRelation.getPost().getResource().getInterHash())
						.byRelationType(resourcePersonRelation.getRelationType())
						.byAuthorIndex(Integer.valueOf(resourcePersonRelation.getPersonIndex()));
		final List<ResourcePersonRelation> existingRelations = this.getResourceRelations(builder.build());
		if (existingRelations.size() > 0) {
			final ResourcePersonRelation existingRelation = existingRelations.get(0);
			throw new ResourcePersonAlreadyAssignedException(existingRelation);
		}

		resourcePersonRelation.setChangedBy(this.loginUser.getName());
		resourcePersonRelation.setChangedAt(new Date());

		try (final DBSession session = this.openSession()) {
			this.personDBManager.addResourceRelation(resourcePersonRelation, this.loginUser, session);
		}
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.PersonLogicInterface#removePersonRelation(java.lang.String, java.lang.String, org.bibsonomy.model.Person, org.bibsonomy.model.enums.PersonResourceRelation)
	 */
	@Override
	public void removeResourceRelation(String personId, final String interHash, int index, PersonResourceRelationType type) {
		this.ensureLoggedInAndNoSpammer();
		try (final DBSession session = this.openSession()) {
			this.personDBManager.removeResourceRelation(personId, interHash, index, type, this.loginUser, session);
		}
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.PersonLogicInterface#createOrUpdatePerson(org.bibsonomy.model.Person)
	 */
	@Override
	public String createPerson(final Person person) {
		this.ensureLoggedInAndNoSpammer();
		try (final DBSession session = this.openSession()) {
			return this.createPerson(person, session);
		}
	}

	private String createPerson(final Person person, final DBSession session) {
		final String personUser = person.getUser();
		final String personId = person.getPersonId();
		if (personUser != null) {
			// ensure that only the logged in user or a admin can claim a person for his/herself
			this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, personUser);
			if (this.permissionDBManager.isAdmin(this.loginUser)) {
				// also ensure that the user exists that should be linked
				final User userDetailsOfClaimingUser = this.getUserDetails(personUser);
				if (!present(userDetailsOfClaimingUser.getName())) {
					throw new IllegalStateException("user '" + personUser + "' not in the system");
				}
			}

			if (present(personId)) {
				final Person personOld = this.personDBManager.getPersonById(personId, session);
				if (personOld == null) {
					throw new NoSuchElementException("person " + personId);
				}
				if (personOld.getUser() != null && !personOld.getUser().equals(this.loginUser.getName())) {
					throw new AccessDeniedException();
				}
			}
		}
		person.setChangeDate(new Date());
		person.setChangedBy(this.loginUser.getName());

		this.personDBManager.createPerson(person, session);
		this.updatePersonNames(person, session);

		return person.getPersonId();
	}

	/**
	 * Updates the given person
	 * @param person		person object containing the new values
	 * @param operation		the desired update operation
	 */
	@Override
	public void updatePerson(final Person person, final PersonUpdateOperation operation) {
		this.ensureLoggedInAndNoSpammer();

		// at least the person id must be set (to know which person should be updated)
		final String personId = person.getPersonId();
		if (!present(personId)) {
			throw new ValidationException("Invalid person ID given.");
		}

		try (final DBSession session = this.openSession()) {
			// check that the old person exists
			final Person personOld = this.personDBManager.getPersonById(personId, session);
			if (personOld == null) {
				throw new NoSuchElementException("person " + personId);
			}

			/*
			 * check if the user can edit the person
			 * (when it is already claimed by an user only that user (and admins) can edit the person)
			 */
			final String oldClaimedUser = personOld.getUser();
			if (present(oldClaimedUser)) {
				this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, oldClaimedUser);
			}

			/*
			 * a person can only be claimed by the loggedin user or the admin can do it for everyone
			 * FIXME: check that the user exisits!
			 */
			final String claimedUser = person.getUser();
			final boolean personClaimed = present(claimedUser);
			if (personClaimed) {
				this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, claimedUser);
			}

			/*
			 * check for email, homepage - can only be edited if the loggedin user claimed
			 * the person (but admins can edit infos anyway)
			 */
			if (!personClaimed && Sets.asSet(PersonUpdateOperation.UPDATE_EMAIL, PersonUpdateOperation.UPDATE_HOMEPAGE).contains(operation)) {
				this.permissionDBManager.ensureAdminAccess(this.loginUser);
			}

			// TODO: this should be done in the manager
			person.setChangeDate(new Date());
			person.setChangedBy(this.loginUser.getName());

			switch (operation) {
				case UPDATE_ORCID: 
					this.personDBManager.updateOrcid(person, session);
					break;
				case UPDATE_RESEARCHERID:
					this.personDBManager.updateResearcherid(person, session);
					break;
				case UPDATE_ACADEMIC_DEGREE:
					this.personDBManager.updateAcademicDegree(person, session);
					break;
				case UPDATE_NAMES:
					this.updatePersonNames(person, session);
					break;
				case UPDATE_COLLEGE:
					this.personDBManager.updateCollege(person, session);
					break;
				case UPDATE_EMAIL:
					this.personDBManager.updateEmail(person, session);
					break;
				case UPDATE_HOMEPAGE:
					this.personDBManager.updateHomepage(person, session);
					break;
				case LINK_USER:
					this.permissionDBManager.ensureIsAdminOrSelf(this.loginUser, claimedUser);
					// first unlink with the old person
					this.personDBManager.unlinkUser(claimedUser, session);
					this.personDBManager.updateUserLink(person, session);
					break;
				case UPDATE_ALL:
					this.personDBManager.updatePerson(person, session);
					// TODO: why is this not called in the manager?
					this.updatePersonNames(person, session);
					break;
				default:
					throw new UnsupportedOperationException("The requested method is not yet implemented.");
			}
		}
	}

	private void updatePersonNames(final Person person, final DBSession session) {
		this.ensureLoggedIn();
		if (!present(person.getNames())) {
			return;
		}
		setMainNameIfNoneSet(person);

		session.beginTransaction();
		try {
			final List<PersonName> oldNames = this.personDBManager.getPersonNames(person.getPersonId(), session);

			final Map<PersonName, PersonName> oldNamesMap = buildIdentityNamesMapFromNames(oldNames);
			final Map<PersonName, PersonName> newNamesMap = buildIdentityNamesMapFromNames(person.getNames());
			for (final PersonName oldName : oldNames) {
				final PersonName newName = newNamesMap.get(oldName);
				if (newName != null) {
					if (!newName.equalsWithDetails(oldName)) {
						newName.setChangedAt(new Date());
						newName.setChangedBy(this.loginUser.getName());
						newName.setPersonNameChangeId(oldName.getPersonNameChangeId());
						this.personDBManager.updatePersonName(newName, this.loginUser, session);
					}
				} else {
					this.personDBManager.removePersonName(oldName.getPersonNameChangeId(), this.loginUser, session);
				}
			}
			for (final PersonName newName : person.getNames()) {
				final PersonName oldName = oldNamesMap.get(newName);
				if (oldName == null) {
					newName.setChangedAt(new Date());
					newName.setChangedBy(this.loginUser.getName());
					this.personDBManager.createPersonName(newName, session);
				}
			}
			session.commitTransaction();
		} finally {
			session.endTransaction();
		}
	}

	private static Map<PersonName, PersonName> buildIdentityNamesMapFromNames(final List<PersonName> names) {
		final Map<PersonName, PersonName> namesMap = new HashMap<>();
		for (final PersonName name : names) {
			namesMap.put(name, name);
		}
		return namesMap;
	}

	private static void setMainNameIfNoneSet(final Person person) {
		boolean mainNameFound = false;
		for (final PersonName name : person.getNames()) {
			if (name.isMain()) {
				if (mainNameFound) {
					name.setMain(false);
				} else {
					mainNameFound = true;
				}
			}
		}

		if (!mainNameFound) {
			person.getNames().get(0).setMain(true);
		}
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.PersonLogicInterface#getPersonById(int)
	 */
	@Override
	public Person getPersonById(final PersonIdType idType, final String id) {
		// TODO: implement a chain

		try (final DBSession session = this.openSession()) {
			Person person;
			if (PersonIdType.PERSON_ID == idType) {
				person = this.personDBManager.getPersonById(id, session);
			} else if (PersonIdType.DNB_ID == idType) {
				person = this.personDBManager.getPersonByDnbId(id, session);
				// } else if (PersonIdType.ORCID == idType) {
				//	TODO: implement
			} else if (PersonIdType.USER == idType) {
				person = this.personDBManager.getPersonByUser(id, session);
			} else {
				throw new UnsupportedOperationException("person cannot be found by it type " + idType);
			}
			return person;
		}
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.PersonLogicInterface#getPersonByAdditionalKey(String, String)
	 */
	@Override
	public Person getPersonByAdditionalKey(final String key, final String value) {
		try (final DBSession session = this.openSession()) {
			return this.personDBManager.getPersonByAdditionalKey(key, value, session);
		}
	}

	/* (non-Javadoc)
	 * @see org.bibsonomy.model.logic.PersonLogicInterface#removePersonName(int)
	 */
	@Override
	public void removePersonName(final Integer personChangeId) {
		this.ensureLoggedInAndNoSpammer();
		try (final DBSession session = this.openSession()) {
			this.personDBManager.removePersonName(personChangeId, this.loginUser, session);
		}
	}

	@Override
	public void createPersonName(final PersonName personName) {
		this.ensureLoggedInAndNoSpammer();
		try (final DBSession session = this.openSession()) {
			this.personDBManager.createPersonName(personName, session);
		}
	}

	@Override
	public void unlinkUser(final String username) {
		this.ensureLoggedInAndNoSpammer();
		try (final DBSession session = this.openSession()) {
			this.personDBManager.unlinkUser(username, session);
		}
	}

	@Override
	public List<ResourcePersonRelation> getResourceRelations(final ResourcePersonRelationQuery query) {
		try (final DBSession session = this.openSession()) {
			final List<ResourcePersonRelation> personPublicationRelations = this.personDBManager.queryForResourcePersonRelations(new QueryAdapter<>(query, this.loginUser), session);
			return this.postProcess(personPublicationRelations, query);
		}
	}

	@Deprecated // FIXME solve this with a query or the index
	private List<ResourcePersonRelation> postProcess(final List<ResourcePersonRelation> rVal, final ResourcePersonRelationQuery query) {
		List<ResourcePersonRelation> relations = rVal;
		if (query.isGroupByInterhash()) {
			final Map<String, ResourcePersonRelation> byInterHash = new HashMap<>();
			for (final ResourcePersonRelation rpr : relations) {
				final String interhash = rpr.getPost().getResource().getInterHash();
				if (byInterHash.containsKey(interhash)) {
					continue;
				}
				byInterHash.put(interhash, rpr);
			}
			relations = new LinkedList<>(byInterHash.values());
		}
		final PersonResourceRelationOrder order = query.getOrder();
		if (order == PersonResourceRelationOrder.PublicationYear) {
			relations.sort((o1, o2) -> {
				try {
					final int year1 = Integer.parseInt(o1.getPost().getResource().getYear().trim());
					final int year2 = Integer.parseInt(o2.getPost().getResource().getYear().trim());
					if (year1 != year2) {
						return year2 - year1;
					}
				} catch (final Exception e) {
					log.warn(e);
				}
				return System.identityHashCode(o1) - System.identityHashCode(o2);
			});
		} else if (order != null) {
			throw new UnsupportedOperationException();
		}
		return relations;
	}

	/**
	 * 
	 * @param personID
	 * @return a list of all matches for a person
	 */
	@Override
	public List<PersonMatch> getPersonMatches(String personID) {
		try (final DBSession session = this.openSession()) {
			if (present(this.loginUser.getName())) {
				return this.personDBManager.getMatchesForFilterWithUserName(personID, this.loginUser.getName(), session);
			}

			return this.personDBManager.getMatchesFor(personID, session);
		}
	}

	/**
	 * increases the deny counter of a match and denys it after a threshold is reached
	 * 
	 * @param match
	 * @return
	 */
	@Override
	public void denyPersonMerge(PersonMatch match) {
		try (final DBSession session = this.openSession()) {
			if (present(this.loginUser.getName())) {
				this.personDBManager.denyMatch(match, this.loginUser.getName(), session);
			}
		}
	}
	
	/**
	 * performs a merge that has no conflicts
	 * @param match
	 * @return
	 */
	@Override
	public boolean acceptMerge(PersonMatch match) {
		try (final DBSession session = this.openSession()) {
			if (present(this.loginUser.getName())) {
				return this.personDBManager.mergePersons(match, this.loginUser, session);
			}
			return false;
		}
	}
	
	/**
	 * 
	 * @param matchID
	 * @return the match with given matchID
	 */
	@Override
	public PersonMatch getPersonMergeRequest(int matchID) {
		try (final DBSession session = this.openSession()) {
			return personDBManager.getMatch(matchID, session);
		}
	}

	/**
	 * resolves conflicts and performs a merge
	 * @param formMatchId of the match to merge
	 * @param map of conflict fields with new values
	 * @return
	 */
	@Override
	public Boolean mergePersonsWithConflicts(int formMatchId, Map<String, String> map) {
		final DBSession session = this.openSession();
		if (present(this.loginUser.getName())) {
			return this.personDBManager.mergePersonsWithConflicts(formMatchId, map, this.loginUser, session);
		}
		return false;
	}

	@Override
	public Statistics getStatistics(final Query query) {
		try (final DBSession session = this.openSession()) {
			return this.getStatistics(query, session);
		}
	}

	private <Q extends Query> Statistics getStatistics(final Q query, final DBSession session) {
		// cast is safe
		final StatisticsProvider<Q> statisticsProvider = (StatisticsProvider<Q>) this.allStatisticDatabaseMangers.get(query.getClass());
		return statisticsProvider.getStatistics(query, this.loginUser, session);
	}

	@Override
	public List<Project> getProjects(final ProjectQuery query) {
		try (final DBSession session = this.openSession()) {
			return this.projectDatabaseManager.getProjects(query, this.loginUser, session);
		}
	}

	@Override
	public Project getProjectDetails(final String projectId) {
		final boolean fullDetails = SimpleAuthUtils.hasAtLeastUserRole(this.loginUser, Role.REPORTING_USER);
		try (final DBSession session = this.openSession()) {
			return this.projectDatabaseManager.getProjectDetails(projectId, fullDetails, session);
		}
	}

	@Override
	public JobResult createProject(final Project project) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.projectDatabaseManager.createProject(project, this.loginUser, session);
		}
	}

	@Override
	public JobResult updateProject(final String projectId, final Project project) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.projectDatabaseManager.updateProject(projectId, project, this.loginUser, session);
		}
	}

	@Override
	public JobResult deleteProject(String projectId) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.projectDatabaseManager.deleteProject(projectId, this.loginUser, session);
		}
	}

	@Override
	public JobResult createCRISLink(CRISLink link) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.crisLinkDatabaseManager.createCRISLink(link, this.loginUser, session);
		}
	}

	@Override
	public JobResult updateCRISLink(CRISLink link) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.crisLinkDatabaseManager.updateCRISLink(link, this.loginUser, session);
		}
	}

	@Override
	public JobResult deleteCRISLink(Linkable source, Linkable target) {
		this.permissionDBManager.ensureAdminAccess(this.loginUser);
		try (final DBSession session = this.openSession()) {
			return this.crisLinkDatabaseManager.deleteCRISLink(source, target, this.loginUser, session);
		}
	}

	@Override
	public <R> R getMetaData(MetaDataQuery<R> query) {
		return this.getMetaDataProvider(query).getMetaData(query);
	}

	private <R> MetaDataProvider<R> getMetaDataProvider(MetaDataQuery<R> query) {
		return (MetaDataProvider<R>) this.metaDataProvidersMap.get(query.getClass());
	}

	/**
	 * @param projectDatabaseManager the projectDatabaseManager to set
	 */
	public void setProjectDatabaseManager(ProjectDatabaseManager projectDatabaseManager) {
		this.projectDatabaseManager = projectDatabaseManager;
	}

	/**
	 * @param crisLinkDatabaseManager the crisLinkDatabaseManager to set
	 */
	public void setCrisLinkDatabaseManager(CRISLinkDatabaseManager crisLinkDatabaseManager) {
		this.crisLinkDatabaseManager = crisLinkDatabaseManager;
	}

	/**
	 * @param personResourceRelationManager the personResourceRelationManager to set
	 */
	public void setPersonResourceRelationManager(PersonResourceRelationDatabaseManager personResourceRelationManager) {
		this.personResourceRelationManager = personResourceRelationManager;
	}

	/**
	 * @param metaDataProvidersMap the metaDataProvidersMap to set
	 */
	public void setMetaDataProvidersMap(Map<Class<?>, MetaDataProvider<?>> metaDataProvidersMap) {
		this.metaDataProvidersMap = metaDataProvidersMap;
	}

	/**
	 * @param dbSessionFactory the dbSessionFactory to set
	 */
	public void setDbSessionFactory(DBSessionFactory dbSessionFactory) {
		this.dbSessionFactory = dbSessionFactory;
	}

	/**
	 * @param publicationReader the publicationReader to set
	 */
	public void setPublicationReader(BibTexReader publicationReader) {
		this.publicationReader = publicationReader;
	}

	/**
	 * @param loginUser the loginUser to set
	 */
	public void setLoginUser(User loginUser) {
		this.loginUser = loginUser;
	}

	/**
	 *
	 * @param personID
	 * @return the match with given personID
	 */
	@Override
	public List<PhDRecommendation> getPhdAdvisorRecForPerson(String personID) {
		try (final DBSession session = this.openSession()) {
			return personDBManager.getPhdAdvisorRecForPerson(personID, session);
		}
	}
}