/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.managers;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.common.enums.ProfilePrivlevel;
import org.bibsonomy.common.enums.Role;
import org.bibsonomy.common.enums.UserRelation;
import org.bibsonomy.common.enums.UserUpdateOperation;
import org.bibsonomy.common.errors.ErrorMessage;
import org.bibsonomy.common.errors.FieldLengthErrorMessage;
import org.bibsonomy.common.exceptions.DatabaseException;
import org.bibsonomy.common.exceptions.UnsupportedRelationException;
import org.bibsonomy.database.systemstags.search.NetworkRelationSystemTag;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.User;
import org.bibsonomy.model.UserSettings;
import org.bibsonomy.model.user.remote.RemoteUserId;
import org.bibsonomy.model.user.remote.SamlRemoteUserId;
import org.bibsonomy.testutil.CommonModelUtils;
import org.bibsonomy.testutil.ParamUtils;
import org.bibsonomy.testutil.TestUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests related to users.
 * 
 * @author Miranda Grahl
 * @author Christian Schenk
 * @author Anton Wilhelm
 */
public class UserDatabaseManagerTest extends AbstractDatabaseManagerTest {

	/* name of testuser to be created / updated */
	private static final String RANDOM_TESTUSER = "userrandom";
	private static UserDatabaseManager userDb;
	private static GroupDatabaseManager groupDb;
	private static BibTexDatabaseManager bibTexDb;

	/**
	 * set up managers
	 */
	@BeforeClass
	public static void setup() {
		// managers
		userDb = UserDatabaseManager.getInstance();
		bibTexDb = BibTexDatabaseManager.getInstance();
		groupDb = GroupDatabaseManager.getInstance();
	}

	/**
	 * tests getUsersBySearch
	 */
	@Test
	public void getUsersBySearch() {
		final List<User> foundUsers = userDb.getUsersBySearch("testuser1", 10, this.dbSession);
		assertEquals(1, foundUsers.size());
	}

	/**
	 * tests getFriendsOfUser
	 */
	@Test
	public void getFriendsOfUser() {
		final List<User> friends = userDb.getUserRelation("testuser1", UserRelation.OF_FRIEND, null, this.dbSession);
		assertNotNull(friends);
		assertEquals(2, friends.size());
	}

	/**
	 * tests getPendingUsers
	 */
	@Ignore
	// TODO: document FIXME: fix me
	@Test
	public void getPendingUsers() {
		final List<User> users = userDb.getPendingUsers(0, Integer.MAX_VALUE, this.dbSession);
		assertEquals(2, users.size());
	}

	/**
	 * tests getPendingUserByActivationCode
	 */
	@Test
	public void getPendingUserByActivationCode() {
		final List<User> users = userDb.getPendingUserByActivationCode("ac47d3f92b90c89e46170f7049beda37", 0, Integer.MAX_VALUE, this.dbSession);
		assertEquals("ac47d3f92b90c89e46170f7049beda37", users.get(0).getActivationCode());
	}

	/**
	 * tests getPendingUserByUsername
	 */
	@Ignore
	// TODO: document FIXME: fix me
	@Test
	public void getPendingUserByUsername() {
		final List<User> users = userDb.getPendingUserByUsername("activationtestuser1", 0, Integer.MAX_VALUE, this.dbSession);
		assertEquals("activationtestuser1", users.get(0).getName());
	}

	/**
	 * tests getAllUsers
	 */
	@Ignore
	// TODO: document FIXME: fix me
	@Test
	public void getAllUsers() {
		List<User> users = userDb.getAllUsers(0, 10, this.dbSession);
		// there're 6 users that aren't spammers
		assertEquals(6, users.size());

		// make sure limit and offset work
		users = userDb.getAllUsers(0, 2, this.dbSession);
		assertEquals(2, users.size());
	}

	/**
	 * tests getUserDetails
	 */
	@Test
	public void getUserDetails() {
		final User user = userDb.getUserDetails("testuser1", this.dbSession);
		// TODO: checke every entity that should be present in the user object
		assertEquals("testuser1", user.getName());
		assertEquals("http://www.bibsonomy.org/user/testuser1", user.getHomepage().toString());
		assertEquals("11111111111111111111111111111111", user.getApiKey());
		assertNotNull(user.getClipboard());
		assertEquals(Role.ADMIN, user.getRole());
	}

	/**
	 * Retrieve the names of users present in a group with given group ID
	 */
	@Ignore
	// FIXME: deleteUser() test deletes user "testuser2"
	@Test
	public void getUserNamesOfGroupId() {
		final List<String> users = userDb.getUserNamesByGroupId(ParamUtils.TESTGROUP1, this.dbSession);
		final String[] testgroup1User = new String[] { "testuser1", "testuser2" };
		assertTrue(users.containsAll(Arrays.asList(testgroup1User)));
		assertEquals(testgroup1User.length, users.size());
	}

	/**
	 * tests createUser
	 */
	@Test
	public void createUser() {
		final User newUser = new User(RANDOM_TESTUSER);
		newUser.setRealname("New Testuser");
		newUser.setEmail("new-testuser@bibsonomy.org");
		newUser.setHomepage(ParamUtils.EXAMPLE_URL);
		newUser.setPassword("password");
		newUser.setApiKey("00000000000000000000000000000000");
		newUser.getSettings().setDefaultLanguage("zv");
		newUser.setSpammer(false);
		newUser.setRole(Role.DEFAULT);
		newUser.setToClassify(1);
		newUser.setAlgorithm(null);
		final String userName = userDb.createUser(newUser, this.dbSession);
		assertEquals(RANDOM_TESTUSER, userName);
		userDb.activateUser(newUser, this.dbSession);

		final User user = userDb.getUserDetails(RANDOM_TESTUSER, this.dbSession);
		newUser.setActivationCode(null);
		
		CommonModelUtils.assertPropertyEquality(newUser, user, Integer.MAX_VALUE, null, new String[] { "apiKey", "IPAddress", "clipboard", "gender", "interests", "hobbies", "profession", "institution", "openURL", "place", "spammer", "settings", "toClassify", "updatedBy", "reminderPassword", "registrationDate", "reminderPasswordRequestDate", "updatedAt", "profilePicture" });
	}

	/**
	 * tests wrong usage of
	 * {@link UserDatabaseManager#createUser(User, org.bibsonomy.database.common.DBSession)}
	 */
	@Test(expected = Exception.class)
	public void createUserWrongUsage() {
		userDb.createUser(null, this.dbSession);
	}

	/**
	 * tests activateUser
	 */
	@Test
	public void activateUser() {
		final User user = userDb.getPendingUserByUsername("activationtestuser1", 0, Integer.MAX_VALUE, this.dbSession).get(0);
		userDb.activateUser(user, this.dbSession);
		final User testuser = userDb.getUserDetails("activationtestuser1", this.dbSession);
		assertEquals("Test Activation User 1", testuser.getRealname());
	}

	/**
	 * tests updateUser
	 */
	@Test
	public void changeUser() {
		final String testusername = "testuser1";
		User newTestuser = userDb.getUserDetails(testusername, this.dbSession);
		assertEquals("Test User 1", newTestuser.getRealname());
		// FIXME: it should be possible to change almost all properties of a
		// user - implement me...
		final String newRealName = "New TestUser";
		newTestuser.setRealname(newRealName);
		final String userName = userDb.updateUser(newTestuser, newTestuser, this.dbSession);
		assertEquals(testusername, userName);
		newTestuser = userDb.getUserDetails(testusername, this.dbSession);
		assertEquals(newRealName, newTestuser.getRealname());

		// you can't change the user's name
		try {
			newTestuser.setName(newTestuser.getName() + "-changed");
			userDb.updateUser(newTestuser, newTestuser, this.dbSession);
			fail("expected exception");
		} catch (final RuntimeException ignore) {
		}
	}

	/**
	 * tests the {@link UserUpdateOperation#UPDATE_CORE} operation
	 */
	@Test
	public void updateUserProfile() {
		final String username = "testuser1";
		final ProfilePrivlevel level = ProfilePrivlevel.FRIENDS;

		/*
		 * get user details
		 */
		final User testUser = userDb.getUserDetails(username, this.dbSession);
		final String newRealname = testUser.getRealname() + " 12";

		/*
		 * change profile
		 */
		testUser.setRealname(newRealname);

		final UserSettings testUserSettings = testUser.getSettings();
		testUserSettings.setProfilePrivlevel(level);
		testUserSettings.setTagboxTooltip(2); // to check if UpdateCore was executed

		/*
		 * update profile
		 */
		userDb.updateUserProfile(testUser, testUser, this.dbSession);

		/*
		 * save user
		 */
		final User savedTestuser = userDb.getUserDetails(username, this.dbSession);
		final UserSettings savedSettings = savedTestuser.getSettings();
		assertEquals(level, savedSettings.getProfilePrivlevel());
		assertEquals(newRealname, savedTestuser.getRealname());
		// check if more than the core was updated
		assertThat(2, not(equalTo(savedSettings.getTagboxTooltip())));
	}

	/**
	 * tests updateApiKeyForUser
	 */
	@Test
	public void updateApiKeyForUser() {
		final String testuser = "testuser3";
		final User user = new User(testuser);
		final String apiKey = userDb.getApiKeyForUser(testuser, this.dbSession);
		assertNotNull(apiKey);
		assertEquals(32, apiKey.length());
		userDb.updateApiKeyForUser(user, user, this.dbSession);
		final String updatedApiKey = userDb.getApiKeyForUser(testuser, this.dbSession);
		assertNotNull(updatedApiKey);
		assertEquals(32, updatedApiKey.length());
		assertThat(apiKey, not(equalTo(updatedApiKey)));

		try {
			final User noUsernameUser = new User(ParamUtils.NOUSER_NAME);
			userDb.updateApiKeyForUser(noUsernameUser, noUsernameUser, this.dbSession);
			fail("expected exception");
		} catch (final Exception ignore) {
			// ok
		}
	}

	/**
	 * tests deleteUser
	 * 
	 * This test flags a user which is not a group as spammer and all of his
	 * posts will be also flagged
	 */
	@Test
	public void deleteUser() {
		// create a new user object, use a name of the test databases, in this
		// case "testuser2"
		final User user = new User("testuser2");

		// get groups for this user. testuser should be member of testgroup1
		List<Group> groups = groupDb.getGroupsForUser(user.getName(), true, this.dbSession);
		assertNotNull(groups);
		assertEquals(2, groups.size());
		assertEquals("testgroup1", groups.get(0).getName());

		// calls the deleteUser method of the UserDataBaseManager class
		// this method is overloaded so you have one method with a String
		// parameter
		// and the new one with a User object parameter
		userDb.deleteUser(user.getName(), USER_TESTUSER_1, this.dbSession);

		// get the old user details out of the testdb
		final User newTestuser = userDb.getUserDetails(user.getName(), this.dbSession);

		// the user have to be available in the test db ...
		assertNotNull(newTestuser.getName());

		// after deleting the user, he shouldn't have any memberships in groups
		// get groups for this user. testuser should be member of testgroup1
		groups = groupDb.getGroupsForUser(user.getName(), true, this.dbSession);
		assertEquals(0, groups.size());

		/*
		 * TODO: reviews / comments
		 */

		// but it should be flagged as spammer
		assertEquals(true, newTestuser.getSpammer());

		// create a spammer group id by adding an old group id to the interger
		// min value
		final int groupid = Integer.MAX_VALUE + 1;

		// get posts for this user
		final List<Post<BibTex>> posts = bibTexDb.getPostsForUser(user.getName(), user.getName(), HashID.INTER_HASH, groupid, new ArrayList<>(), null, null, 10, 0, null, this.dbSession);

		// there should be at least more then one post with that negative group
		// id
		assertNotNull(posts);
	}

	@Test(expected = RuntimeException.class)
	public void deleteGroup() {
		// create a new user object which has a groupname
		final User testUserIsGroup = new User("testgroup1");
		// if anybody tries to delete a user which is a group should get an
		// exception
		userDb.deleteUser(testUserIsGroup.getName(), USER_TESTUSER_1, this.dbSession);
	}

	/**
	 * Test the user authentication via API key
	 */
	@Test
	public void validateUserAccessByAPIKey() {
		final String apiKeyForUser = userDb.getApiKeyForUser("testuser1", this.dbSession);
		// not logged in (wrong apikey) = unknown user
		assertNull(userDb.validateUserAccessByAPIKey("testuser1", "ThisIsJustAFakeAPIKey", this.dbSession).getName());
		// the correct key
		assertEquals("testuser1", userDb.validateUserAccessByAPIKey("testuser1", apiKeyForUser, this.dbSession).getName());

		// the user "testspammer" hasn't got an Api key
		for (final String name : new String[] { "", " ", null, "testspammer" }) {
			for (final String key : new String[] { "", " ", null, "hurz" }) {
				assertNull(userDb.validateUserAccessByAPIKey(name, key, this.dbSession).getName());
			}
		}
	}

	/**
	 * tests getRelatedUsersBySimilarity
	 */
	@Test
	public void getRelatedUsersBySimilarity() {
		/*
		 * fetch the two related users of testuser1 by jaccard measure
		 */
		final String requestedUserName = "testuser3";
		List<User> users = userDb.getRelatedUsersBySimilarity(requestedUserName, null, UserRelation.JACCARD, 0, 10, this.dbSession);
		assertEquals(1, users.size());
		assertEquals("testuser1", users.get(0).getName());
		assertThat(users.get(0).getPrediction(), equalTo(10));

		/*
		 * we don't have data for cosine similarity in the DB
		 */
		users = userDb.getRelatedUsersBySimilarity(requestedUserName, null, UserRelation.COSINE, 0, 10, this.dbSession);
		assertEquals(0, users.size());
	}

	/**
	 * tests getUserFollowers
	 */
	@Test
	public void getUserFollowers() {
		final String authUser = "testuser2";

		final List<User> userFollowers = userDb.getUserRelation(authUser, UserRelation.OF_FOLLOWER, null, this.dbSession);
		assertNotNull(userFollowers);
		assertEquals(1, userFollowers.size());
		assertEquals("testuser1", userFollowers.get(0).getName());
	}

	/**
	 * tests getUserFollowers
	 */
	@Test
	public void getFollowersOfUser() {
		final String authUser = "testuser1";

		final List<User> userFollowers = userDb.getUserRelation(authUser, UserRelation.FOLLOWER_OF, null, this.dbSession);
		assertNotNull(userFollowers);
		assertEquals(2, userFollowers.size());
		assertEquals("testuser2", userFollowers.get(0).getName());
		assertEquals("testuser3", userFollowers.get(1).getName());
	}

	/**
	 * tests insert AND delete
	 */
	@Test
	public void insertAndDeleteUserRelations() {
		final String sourceUser = "testuser3";
		final String targetUser = "testuser1";

		userDb.createUserRelation(sourceUser, targetUser, UserRelation.FOLLOWER_OF, null, this.dbSession);
		userDb.createUserRelation(sourceUser, targetUser, UserRelation.OF_FRIEND, null, this.dbSession);

		// FIXME: not FOLLOWERS but followees!
		List<User> followedByUser = userDb.getUserRelation(sourceUser, UserRelation.FOLLOWER_OF, null, this.dbSession);
		List<User> friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, NetworkRelationSystemTag.BibSonomyFriendSystemTag, this.dbSession);

		assertEquals(1, followedByUser.size());
		assertEquals("testuser1", followedByUser.get(0).getName());

		assertEquals(1, friendsOfUser.size());
		assertEquals("testuser1", friendsOfUser.get(0).getName());

		userDb.deleteUserRelation(sourceUser, targetUser, UserRelation.FOLLOWER_OF, null, this.dbSession);
		userDb.deleteUserRelation(sourceUser, targetUser, UserRelation.OF_FRIEND, NetworkRelationSystemTag.BibSonomyFriendSystemTag, this.dbSession);

		followedByUser = userDb.getUserRelation(sourceUser, UserRelation.FOLLOWER_OF, null, this.dbSession);
		assertEquals(0, followedByUser.size());

		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, null, this.dbSession);
		assertEquals(0, friendsOfUser.size());
	}

	/**
	 * tests insert AND delete
	 */
	@Test
	public void insertAndDeleteTaggedUserRelations() {
		final String sourceUser = "testuser_taggedrelations";
		final String targetUser0 = "testuser_taggedrelations0";
		final String targetUser1 = "testuser_taggedrelations1";
		final String targetUser2 = "testuser_taggedrelations2";

		final String tag0 = "football";
		final String tag1 = "sys:network:facebook";
		final String tag2 = "this_tag_should_not_occur";
		final String tag3 = "abc";

		// --------------------------------------------------------------------
		// followers: tagged follower relations are not allowed
		// --------------------------------------------------------------------
		try {
			userDb.createUserRelation(sourceUser, targetUser0, UserRelation.FOLLOWER_OF, tag1, this.dbSession);
			fail("Tagged follower relations are not supported!");
		} catch (final UnsupportedRelationException e) {
			// nop
		}

		final List<User> followedByUser = userDb.getUserRelation(sourceUser, UserRelation.FOLLOWER_OF, null, this.dbSession);
		assertEquals(0, followedByUser.size());

		// --------------------------------------------------------------------
		// friends: allow tagged friendship relations as well as
		// BibSonomy-Friendship relations (default)
		// --------------------------------------------------------------------
		// tag target user with tag0
		userDb.createUserRelation(sourceUser, targetUser0, UserRelation.OF_FRIEND, tag0, this.dbSession);
		List<User> friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, tag0, this.dbSession);
		assertEquals(1, friendsOfUser.size());
		assertEquals(targetUser0, friendsOfUser.get(0).getName());
		assertEquals(1, friendsOfUser.get(0).getTags().size());
		assertEquals(tag0, friendsOfUser.get(0).getTags().get(0).getName());

		// look for users tagged with tag2 (no one)
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, tag2, this.dbSession);
		assertEquals(0, friendsOfUser.size());

		// additionally tag target user with tag1
		userDb.createUserRelation(sourceUser, targetUser1, UserRelation.OF_FRIEND, tag1, this.dbSession);
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, tag0, this.dbSession);
		assertEquals(1, friendsOfUser.size());
		assertEquals(targetUser0, friendsOfUser.get(0).getName());
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, tag1, this.dbSession);
		assertEquals(1, friendsOfUser.size());
		assertEquals(targetUser1, friendsOfUser.get(0).getName());

		// there should be no BibSonomy friendship relation among source and
		// target user
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, NetworkRelationSystemTag.BibSonomyFriendSystemTag, this.dbSession);
		assertEquals(0, friendsOfUser.size());

		// add a bibsonomy friendship link
		userDb.createUserRelation(sourceUser, targetUser2, UserRelation.OF_FRIEND, NetworkRelationSystemTag.BibSonomyFriendSystemTag, this.dbSession);
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, NetworkRelationSystemTag.BibSonomyFriendSystemTag, this.dbSession);
		assertEquals(1, friendsOfUser.size());
		assertEquals(targetUser2, friendsOfUser.get(0).getName());

		// all together there should be three relations from source to target
		// user
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, null, this.dbSession);
		assertEquals(3, friendsOfUser.size());

		// remove tag 1 from target user
		userDb.deleteUserRelation(sourceUser, targetUser0, UserRelation.OF_FRIEND, tag1, this.dbSession);
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, tag1, this.dbSession);
		assertEquals(1, friendsOfUser.size());
		userDb.deleteUserRelation(sourceUser, targetUser0, UserRelation.OF_FRIEND, tag0, this.dbSession);
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, tag0, this.dbSession);
		assertEquals(0, friendsOfUser.size());
		// tag 1 should still be assigned to target user
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, tag1, this.dbSession);
		assertEquals(1, friendsOfUser.size());
		assertEquals(targetUser1, friendsOfUser.get(0).getName());

		userDb.createUserRelation(sourceUser, targetUser2, UserRelation.OF_FRIEND, tag3, this.dbSession);
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, null, this.dbSession);
		assertEquals(2, friendsOfUser.size());
		userDb.deleteUserRelation(sourceUser, targetUser2, UserRelation.OF_FRIEND, tag1, this.dbSession);
		userDb.deleteUserRelation(sourceUser, targetUser2, UserRelation.OF_FRIEND, NetworkRelationSystemTag.BibSonomyFriendSystemTag, this.dbSession);
		userDb.deleteUserRelation(sourceUser, targetUser2, UserRelation.OF_FRIEND, tag3, this.dbSession);
		userDb.createUserRelation(sourceUser, targetUser1, UserRelation.OF_FRIEND, tag3, this.dbSession);
		friendsOfUser = userDb.getUserRelation(sourceUser, UserRelation.OF_FRIEND, null, this.dbSession);
		assertEquals(1, friendsOfUser.size());
		assertEquals(2, friendsOfUser.get(0).getTags().size());
	}

	@Test
	public void testMaxFieldLength() {
		final User user = userDb.getUserDetails("testuser1", this.dbSession);
		user.setHobbies("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
		try {
			userDb.updateUser(user, user, this.dbSession);
			fail("missing DatabaseException");
		} catch (final DatabaseException e) {
			final List<ErrorMessage> errorMessages = e.getErrorMessages("testuser1");
			assertEquals(1, errorMessages.size());
			final FieldLengthErrorMessage errorMessage = (FieldLengthErrorMessage) errorMessages.get(0);
			assertNotNull(errorMessage.getMaxLengthForField("hobbies"));
		}
		
		user.setHobbies("");
		user.setHomepage(TestUtils.createURL("http://Loremipsumdolorsitamet,consecteturadipisicingelit,seddoeiusmodtemporincididuntutlaboreetdoloremagnaaliqua.Utenimadminimveniam,quisnostrudexercitationullamcolaborisnisiutaliquipexeacommodoconsequat.Duisauteiruredolorinreprehenderitinvoluptatevelitessecillumdoloreeufugiatnullapariatur.Excepteursintoccaecatcupidatatnonproident,suntinculpaquiofficiadeseruntmollitanimidestlaborum.com"));
		try {
			userDb.updateUser(user, user, this.dbSession);
			fail("missing DatabaseException");
		} catch (final DatabaseException e) {
			final List<ErrorMessage> errorMessages = e.getErrorMessages("testuser1");
			assertEquals(1, errorMessages.size());
			final FieldLengthErrorMessage errorMessage = (FieldLengthErrorMessage) errorMessages.get(0);
			assertNotNull(errorMessage.getMaxLengthForField("homepage"));
		}
	}

	/**
	 * Tests implementation of SamlRemoteIds
	 */
	@Test
	public void samlUserTest() {
		// Retrieve samlRemoteUserId of testuser1
		User user = userDb.getUserDetails("testuser1", this.dbSession);
		// Check if samlRemoteUserId was correctly retrieved
		SamlRemoteUserId user_sruid = this.userHasSamlRemoteId(user, "samlUserId1", "saml");
		if (user_sruid == null) {
			fail("testuser1 does not have proper SamlRemoteId");
			return;
		}
		// SamlRemoteUserId was correctly retrieved!

		// getUsernameByRemoteUser() Test
		String username = userDb.getUsernameByRemoteUser(user_sruid, dbSession);
		assertEquals(username, "testuser1");

		// We now change SamlRemoteUserId and call updateUser()
		user_sruid.setUserId("samlUserId1_changed");
		user_sruid.setIdentityProviderId("saml_changed");
		userDb.updateUser(user, user, this.dbSession);
		user = userDb.getUserDetails("testuser1", this.dbSession);
		if (user.getRemoteUserIds().size() > 1) {
			fail("testuser1 should have only one SamlRemoteId");
		}
		user_sruid = this.userHasSamlRemoteId(user, "samlUserId1_changed", "saml_changed");
		if (user_sruid == null) {
			fail("SamlRemoteId was not changed correctly");
		}
	}

	/**
	 * This Method tests if a user has a SamlRemoteUserId containing samlUserId
	 * and identity_provider
	 * 
	 * @param user
	 * @param samlUserId
	 * @param identity_provider
	 * @return SamlRemoteUserId
	 */
	private SamlRemoteUserId userHasSamlRemoteId(User user, String samlUserId, String identity_provider) {
		SamlRemoteUserId user_sruid = null;
		for (RemoteUserId ruid : user.getRemoteUserIds()) {
			SamlRemoteUserId temp_sruid = (SamlRemoteUserId) ruid;
			if (temp_sruid.getUserId().equals(samlUserId) & temp_sruid.getIdentityProviderId().equals(identity_provider)) {
				user_sruid = temp_sruid;
				break;
			}
		}
		return user_sruid;
	}
}