/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.managers;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.bibsonomy.common.JobResult;
import org.bibsonomy.common.enums.GroupID;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.common.enums.Status;
import org.bibsonomy.common.exceptions.DatabaseException;
import org.bibsonomy.database.common.DBSession;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.GoldStandardPublication;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.User;
import org.bibsonomy.model.enums.GoldStandardRelation;
import org.bibsonomy.model.util.GroupUtils;
import org.bibsonomy.testutil.ModelUtils;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests for {@link GoldStandardPublicationDatabaseManager}
 *
 * @author dzo
 */
public class GoldStandardPublicationDatabaseManagerTest extends AbstractDatabaseManagerTest {

    private static final List<Integer> VISIBLE_GROUPS = Collections.singletonList(GroupID.PUBLIC.getId());
    private static final String WRONG_INTERHASH = "interhashorintrahashorhashor";
    private static final String INTERHASH_GOLD_1 = "097248439469d8f5a1e7fad6b02cbfcd";
    private static final String INTERHASH_GOLD_2 = "ac6aa3ccb181e61801cefbc1401d409a";

    private static GoldStandardPublicationDatabaseManager goldPubManager;

    private static final User loginUser = new User("testuser1");

    /**
     * sets the gold standard publication database manager
     */
    @BeforeClass
    public static void setGoldStandardPublicationManager() {
        goldPubManager = GoldStandardPublicationDatabaseManager.getInstance();
    }

    /**
     * tests {@link GoldStandardPublicationDatabaseManager#createPost(Post, User, org.bibsonomy.database.common.DBSession)}
     */
    @Test
    public void testCreatePost() {
        final String interhash = this.createGoldStandardPublication();

        // clear database
        this.deletePost(interhash);
    }

    /**
     * @return the interhash of the created gold standard
     */
    private String createGoldStandardPublication() {
        assertFalse(this.pluginMock.isOnGoldStandardCreate());

        // create post
        final Post<GoldStandardPublication> post = this.generateGoldPublication();
        final JobResult jobResult = goldPubManager.createPost(post, null, this.dbSession);
        assertThat(jobResult.getStatus(), is(Status.OK));

        final String interhash = post.getResource().getInterHash();
        assertNotNull(goldPubManager.getPostDetails("", interhash, "", VISIBLE_GROUPS, this.dbSession).getResource());

        assertTrue(this.pluginMock.isOnGoldStandardCreate());
        return interhash;
    }

    /**
     * tests {@link GoldStandardPublicationDatabaseManager#createPost(Post, User, org.bibsonomy.database.common.DBSession)}
     */
    @Test
    public void updatePost() {
        assertFalse(this.pluginMock.isOnGoldStandardUpdate());
        assertFalse(this.pluginMock.isOnGoldStandardCreate());

        // create post
        final Post<GoldStandardPublication> post = this.generateGoldPublication();
        goldPubManager.createPost(post, null, this.dbSession);

        // test listeners
        assertTrue(this.pluginMock.isOnGoldStandardCreate());
        assertFalse(this.pluginMock.isOnGoldStandardUpdate());
        this.pluginMock.reset();

        // fetch post
        final GoldStandardPublication goldStandard = post.getResource();
        String interhash = goldStandard.getInterHash();
        assertNotNull(goldPubManager.getPostDetails("", interhash, "", VISIBLE_GROUPS, this.dbSession).getResource());

        // change a value and update the gold standard
        final String newYear = "2010";
        goldStandard.setYear(newYear);
        goldStandard.recalculateHashes();

        goldPubManager.updatePost(post, interhash, loginUser, null, this.dbSession);

        // test listeners
        assertFalse(this.pluginMock.isOnGoldStandardCreate());
        assertTrue(this.pluginMock.isOnGoldStandardUpdate());

        interhash = goldStandard.getInterHash();

        // delete gold standard
        final Post<GoldStandardPublication> postDetails = goldPubManager.getPostDetails("testuser1", interhash, "", null, this.dbSession);
        assertEquals(newYear, postDetails.getResource().getYear());

        this.deletePost(interhash);
    }

    /**
     * tests {@link GoldStandardPublicationDatabaseManager#updatePost(Post, String, User, PostUpdateOperation, DBSession)} without changing the inter-/intraHash
     */
    @Test
    public void updatePostWithoutChangingHash() {
        assertFalse(this.pluginMock.isOnGoldStandardUpdate());
        assertFalse(this.pluginMock.isOnGoldStandardCreate());

        // create post
        final Post<GoldStandardPublication> post = this.generateGoldPublication();
        goldPubManager.createPost(post, null, this.dbSession);

        // test listeners
        assertTrue(this.pluginMock.isOnGoldStandardCreate());
        assertFalse(this.pluginMock.isOnGoldStandardUpdate());
        this.pluginMock.reset();

        // fetch post
        final GoldStandardPublication goldStandard = post.getResource();
        String interhash = goldStandard.getInterHash();
        assertNotNull(goldPubManager.getPostDetails("", interhash, "", null, this.dbSession).getResource());

        // change a value and update the gold standard
        final String newSchool = "schooool";
        goldStandard.setSchool(newSchool);
        goldStandard.recalculateHashes();

        goldPubManager.updatePost(post, interhash, loginUser, null, this.dbSession);

        // test listeners
        assertFalse(this.pluginMock.isOnGoldStandardCreate());
        assertTrue(this.pluginMock.isOnGoldStandardUpdate());

        interhash = goldStandard.getInterHash();

        // delete gold standard
        final Post<GoldStandardPublication> postDetails = goldPubManager.getPostDetails("testuser1", interhash, "", null, this.dbSession);
        assertEquals(newSchool, postDetails.getResource().getSchool());

        this.deletePost(interhash);
    }

    /**
     * tests getPostDetails including references
     */
    @Test
    public void testReferences() {
        final Post<GoldStandardPublication> post = goldPubManager.getPostDetails("", INTERHASH_GOLD_1, "", VISIBLE_GROUPS, this.dbSession);
        final Set<BibTex> references = post.getResource().getReferences();
        assertEquals(1, references.size());
        assertEquals(1, post.getResource().getReferencedBy().size());
        final BibTex ref1 = references.iterator().next();
        assertEquals(INTERHASH_GOLD_2, ref1.getInterHash());
    }

    /**
     * tests if duplications can be created
     */
    @Test(expected = DatabaseException.class)
    public void testCreateDuplicate() {
        final Post<GoldStandardPublication> post = goldPubManager.getPostDetails("", INTERHASH_GOLD_1, "", VISIBLE_GROUPS, this.dbSession);
        goldPubManager.createPost(post, null, this.dbSession);
    }

    /**
     * tests if a post can be updated that isn't stored in the db
     */
    @Test(expected = DatabaseException.class)
    public void testUpdateUnkownPost() {
        final Post<GoldStandardPublication> post = goldPubManager.getPostDetails("", INTERHASH_GOLD_1, "", null, this.dbSession);
        goldPubManager.updatePost(post, WRONG_INTERHASH, loginUser, null, this.dbSession);
    }

    /**
     * tests if a duplicated standard post can be created
     */
    @Test(expected = DatabaseException.class)
    public void testUpdatePostToPostInDB() {
        final Post<GoldStandardPublication> post = goldPubManager.getPostDetails("", INTERHASH_GOLD_1, "", VISIBLE_GROUPS, this.dbSession);
        post.getResource().recalculateHashes();
        goldPubManager.updatePost(post, INTERHASH_GOLD_2, loginUser, null, this.dbSession);
    }

    /**
     * tests {@link GoldStandardDatabaseManager#addRelationsToPost(String, String, Set, GoldStandardRelation, DBSession)} (String, String, Set, org.bibsonomy.database.common.DBSession)} and {@link GoldStandardDatabaseManager#removeRelationsFromPost(String, String, Set, GoldStandardRelation, DBSession)} (String, String, Set, org.bibsonomy.database.common.DBSession)}
     */
    @Test
    public void testAddRemoveReferences() {
        final String interHash = this.createGoldStandardPublication();
        GoldStandardRelation relation = GoldStandardRelation.REFERENCE;
        goldPubManager.addRelationsToPost("", INTERHASH_GOLD_1, Collections.singleton(interHash), relation, this.dbSession);

        final Post<GoldStandardPublication> post = goldPubManager.getPostDetails("", INTERHASH_GOLD_1, "", null, this.dbSession);
        assertEquals(1 + 1, post.getResource().getReferences().size()+post.getResource().getReferenceThisPublicationIsPublishedIn().size());

        goldPubManager.removeRelationsFromPost("", INTERHASH_GOLD_1, Collections.singleton(interHash), relation, this.dbSession);

        final Post<GoldStandardPublication> postAfterRemove = goldPubManager.getPostDetails("", INTERHASH_GOLD_1, "", null, this.dbSession);
        assertEquals(1, postAfterRemove.getResource().getReferences().size()+post.getResource().getReferenceThisPublicationIsPublishedIn().size());

        this.deletePost(interHash);
    }

    /**
     * tests if the plugin updates the hashes after updating the references and if the plugin deletes the references when post was deleted
     */
    @Test
    public void testUpdateReferencePlugin() {
        final Post<GoldStandardPublication> post = goldPubManager.getPostDetails("", INTERHASH_GOLD_1, "", VISIBLE_GROUPS, this.dbSession);

        final GoldStandardPublication standard = post.getResource();
        assertEquals(1, standard.getReferences().size());
        final String oldYear = standard.getYear();
        standard.setYear("2010");
        standard.recalculateHashes();
        goldPubManager.updatePost(post, INTERHASH_GOLD_1, loginUser, null, this.dbSession);

        assertTrue(this.pluginMock.isOnGoldStandardUpdate());

        final String newInterHash = standard.getInterHash();
        final Post<GoldStandardPublication> afterUpdate = goldPubManager.getPostDetails("", newInterHash, "", VISIBLE_GROUPS, this.dbSession);
        assertEquals(1, afterUpdate.getResource().getReferences().size());
        // Restore post to previous state to leave database untouched for the other tests
        final GoldStandardPublication old = afterUpdate.getResource();
        old.setYear(oldYear);
        old.recalculateHashes();
        goldPubManager.updatePost(afterUpdate, newInterHash, loginUser, null, this.dbSession);

    }

    private void deletePost(final String interhash) {
        this.pluginMock.reset();
        assertFalse(this.pluginMock.isOnGoldStandardDelete());

        // delete post
        goldPubManager.deletePost("", interhash, loginUser, this.dbSession);
        assertNull(goldPubManager.getPostDetails("", interhash, "", null, this.dbSession));

        assertTrue(this.pluginMock.isOnGoldStandardDelete());
    }

    private Post<GoldStandardPublication> generateGoldPublication() {
        final Post<GoldStandardPublication> post = new Post<>();

        // groups
        final Group group = GroupUtils.buildPublicGroup();
        post.getGroups().add(group);

        post.setDescription("trallalla");
        post.setDate(new Date());
        post.setUser(ModelUtils.getUser());
        post.setResource(ModelUtils.getGoldStandardPublication());

        return post;
    }
}