/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.managers;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;

import org.bibsonomy.common.enums.ClassifierMode;
import org.bibsonomy.common.enums.ClassifierSettings;
import org.bibsonomy.common.enums.GroupRole;
import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.common.enums.InetAddressStatus;
import org.bibsonomy.database.managers.discussion.DiscussionDatabaseManager;
import org.bibsonomy.database.managers.discussion.DiscussionDatabaseManagerTest;
import org.bibsonomy.database.managers.discussion.ReviewDatabaseManager;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.GoldStandardPublication;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.GroupRequest;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Review;
import org.bibsonomy.model.User;
import org.bibsonomy.model.util.GroupUtils;
import org.bibsonomy.testutil.TestDatabaseManager;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Robert Jäschke
 * @author Stefan Stützer
 */
public class AdminDatabaseManagerTest extends AbstractDatabaseManagerTest {

	private static AdminDatabaseManager adminDb;
	private static BookmarkDatabaseManager bookmarkDb;
	private static BibTexDatabaseManager publicationDb;
	private static DiscussionDatabaseManager discussionDatabaseManager;
	private static TestDatabaseManager testDatabaseManager;
	private static ReviewDatabaseManager reviewDatabaseManager;
	private static UserDatabaseManager userDatabaseManager;
	private static GroupDatabaseManager groupDatabaseManager;

	/**
	 * sets up required managers
	 */
	@BeforeClass
	public static void setupManager() {
		adminDb = AdminDatabaseManager.getInstance();
		bookmarkDb = BookmarkDatabaseManager.getInstance();
		publicationDb = BibTexDatabaseManager.getInstance();
		discussionDatabaseManager = DiscussionDatabaseManager.getInstance();
		testDatabaseManager = new TestDatabaseManager();
		reviewDatabaseManager = ReviewDatabaseManager.getInstance();
		userDatabaseManager = UserDatabaseManager.getInstance();
		groupDatabaseManager = GroupDatabaseManager.getInstance();
	}

	/**
	 * tests getInetAddressStatus
	 */
	@Test
	public void getInetAddressStatus() {
		try {
			final InetAddress address = InetAddress.getByName("192.168.0.1");
			assertEquals(InetAddressStatus.UNKNOWN, adminDb.getInetAddressStatus(address, this.dbSession));
		} catch (final UnknownHostException ignore) {
			// ignore, we don't need host name resolution
		}
	}

	/**
	 * tests addInetAdressStatus
	 */
	@Test
	public void addInetAdressStatus() {
		try {
			final InetAddress address = InetAddress.getByName("192.168.1.1");
			final InetAddressStatus status = InetAddressStatus.WRITEBLOCKED;
			// write
			adminDb.addInetAddressStatus(address, status, this.dbSession);
			// read
			final InetAddressStatus writtenStatus = adminDb.getInetAddressStatus(address, this.dbSession);
			// check
			assertEquals(status, writtenStatus);
		} catch (final UnknownHostException ignore) {
			// ignore, we don't need host name resolution
		}
	}

	/**
	 * tests deleteInetAdressStatus
	 */
	@Test
	public void deleteInetAdressStatus() {
		try {
			final InetAddress address = InetAddress.getByName("192.168.1.1");
			// read
			InetAddressStatus status = adminDb.getInetAddressStatus(address, this.dbSession);
			// delete
			adminDb.deleteInetAdressStatus(address, this.dbSession);
			// read
			status = adminDb.getInetAddressStatus(address, this.dbSession);
			// check
			assertEquals(InetAddressStatus.UNKNOWN, status);
		} catch (final UnknownHostException ex) {
			// ignore, we don't need host name resolution
		}
	}

	/**
	 * tests getClassifierSettings
	 */
	@Test
	public void getClassifierSettings() {
		final ClassifierSettings settingsKey = ClassifierSettings.ALGORITHM;
		final String value = adminDb.getClassifierSettings(settingsKey, this.dbSession);
		assertEquals("weka.classifiers.lazy.IBk", value);
	}

	/**
	 * tests updateClassifierSettings
	 */
	@Test
	public void updateClassifierSettings() {
		final ClassifierSettings settingsKey = ClassifierSettings.MODE;
		final String value = ClassifierMode.NIGHT.getAbbreviation();

		adminDb.updateClassifierSettings(settingsKey, value, this.dbSession);

		final String result = adminDb.getClassifierSettings(settingsKey, this.dbSession);
		assertEquals(value, result);
	}

	/**
	 * tests logging when flagging and unflagging spammers
	 */
	@Test
	public void updatePredictionLogs() {
		final User user = new User();
		user.setName("testspammer");
		user.setSpammer(true);
		user.setToClassify(0);
		user.setPrediction(1);
		user.setConfidence(0.2);
		user.setMode("D");
		user.setAlgorithm("testlogging");
		//flag spammer (flagging does not change: user is no spammer)
		final String result = adminDb.flagSpammer(user, "classifier", "off", this.dbSession);
		assertEquals(user.getName(), result);
	}

	/**
	 * flags and unflags an user as spammer
	 */
	@Test
	public void flagUnflagSpammer() {
		final User user = new User();
		final String userName = "testuser1";
		user.setName(userName);

		final List<Integer> visibleGroupsUser2 = Collections.singletonList(PUBLIC_GROUP_ID);
		final String loginUserName2 = "testuser2";
		List<Post<Bookmark>> publicBookmarkUserPosts = bookmarkDb.getPostsForUser(loginUserName2, userName, HashID.INTRA_HASH, PUBLIC_GROUP_ID, visibleGroupsUser2, null, null, 10, 0, null, this.dbSession);
		List<Post<BibTex>> publicPublicationUserPosts = publicationDb.getPostsForUser(loginUserName2, userName, HashID.INTRA_HASH, PUBLIC_GROUP_ID, visibleGroupsUser2, null, null, 10, 0, null, this.dbSession);
		assertEquals(1, publicPublicationUserPosts.size());
		assertEquals(1, publicBookmarkUserPosts.size());

		user.setSpammer(true);
		user.setToClassify(0);
		user.setPrediction(1);
		user.setConfidence(1.0);
		user.setMode("D");
		user.setAlgorithm("test");

		final List<Group> userGroups = groupDatabaseManager.getGroupsForUser(userName, true, this.dbSession);
		try {
			adminDb.flagSpammer(user, "not-classifier", "off", this.dbSession);
			Assert.fail("User cannot be marked as spammer since he is member of at least one group.");
		} catch (final IllegalStateException ex) {
			for (final Group group : userGroups) {
				groupDatabaseManager.removeUserFromGroup(group.getName(), userName, false, USER_TESTUSER_1, this.dbSession);
			}
			adminDb.flagSpammer(user, "not-classifier", "off", this.dbSession);
		}

		/*
		 * after the user is marked as spammer testuser2 shouldn't see any posts of testuser1
		 */
		publicBookmarkUserPosts = bookmarkDb.getPostsForUser(loginUserName2, userName, HashID.INTRA_HASH, PUBLIC_GROUP_ID, visibleGroupsUser2, null, null, 10, 0, null, this.dbSession);
		publicPublicationUserPosts = publicationDb.getPostsForUser(loginUserName2, userName, HashID.INTRA_HASH, PUBLIC_GROUP_ID, visibleGroupsUser2, null, null, 10, 0, null, this.dbSession);
		assertEquals(0, publicPublicationUserPosts.size());
		assertEquals(0, publicBookmarkUserPosts.size());

		// TODO: user should see his own posts

		// TODO: check tas and grouptas table

		/*
		 * check if discussion items are invisible to non-spammers
		 */
		assertEquals(0, discussionDatabaseManager.getDiscussionSpaceForResource(DiscussionDatabaseManagerTest.HASH_WITH_RATING, loginUserName2, visibleGroupsUser2, this.dbSession).size());

		/*
		 * check if the review ratings cache was updated
		 */
		assertEquals(0, testDatabaseManager.getReviewRatingsArithmeticMean(DiscussionDatabaseManagerTest.HASH_WITH_RATING), 0);

		/*
		 * now unflag testuser1 again
		 */
		user.setSpammer(false);
		adminDb.flagSpammer(user, "admin", "off", this.dbSession);
		for (final Group group : userGroups) {
			groupDatabaseManager.addPendingMembership(group.getName(), userName, true, GroupRole.INVITED, USER_TESTUSER_1, this.dbSession);
			groupDatabaseManager.addUserToGroup(group.getName(), userName, true, GroupRole.USER, USER_TESTUSER_1, this.dbSession);
		}

		publicBookmarkUserPosts = bookmarkDb.getPostsForUser(loginUserName2, userName, HashID.INTRA_HASH, PUBLIC_GROUP_ID, visibleGroupsUser2, null, null, 10, 0, null, this.dbSession);
		publicPublicationUserPosts = publicationDb.getPostsForUser(loginUserName2, userName, HashID.INTRA_HASH, PUBLIC_GROUP_ID, visibleGroupsUser2, null, null, 10, 0, null, this.dbSession);
		assertEquals(1, publicPublicationUserPosts.size());
		assertEquals(1, publicBookmarkUserPosts.size());

		// TODO: check tas and grouptas table
		assertEquals(2, discussionDatabaseManager.getDiscussionSpaceForResource(DiscussionDatabaseManagerTest.HASH_WITH_RATING, loginUserName2, visibleGroupsUser2, this.dbSession).size());

		/*
		 * check if the review ratings cache was updated
		 */
		assertEquals(4.0, testDatabaseManager.getReviewRatingsArithmeticMean(DiscussionDatabaseManagerTest.HASH_WITH_RATING), 0);

		final float rating = 3.5f;
		final String newHash = "thisisastrangehash";
		final Review review = new Review();
		final User spammer = userDatabaseManager.getUserDetails("testspammer", this.dbSession);
		review.setUser(spammer);
		review.setRating(rating);
		review.setResourceType(GoldStandardPublication.class);
		review.setGroups(Collections.singleton(GroupUtils.buildPublicSpamGroup()));
		assertTrue(reviewDatabaseManager.createDiscussionItemForResource(newHash, review, this.dbSession));
		assertEquals(0.0, testDatabaseManager.getReviewRatingsArithmeticMean(newHash), 0);

		spammer.setSpammer(false);
		spammer.setAlgorithm("admin");
		adminDb.flagSpammer(spammer, "admin", this.dbSession);

		assertEquals(rating, testDatabaseManager.getReviewRatingsArithmeticMean(newHash), 0);

		spammer.setSpammer(true);
		adminDb.flagSpammer(spammer, "admin", this.dbSession);

		assertEquals(0.0, testDatabaseManager.getReviewRatingsArithmeticMean(newHash), 0);
	}

	@Test
	public void markUserMemberOfGroupsAsSpammer() {
		final User user = new User();
		final String userName = "testuser1";
		user.setName(userName);
		final Group newGroup = new Group();
		final String groupName = "testgroupnew";
		newGroup.setName(groupName.toUpperCase());
		final GroupRequest groupRequest = new GroupRequest();
		groupRequest.setUserName(userName);
		groupRequest.setReason("testrequestreason1");
		newGroup.setGroupRequest(groupRequest);

		groupDatabaseManager.createPendingGroup(newGroup, this.dbSession);
		groupDatabaseManager.activateGroup(newGroup.getName(), USER_TESTUSER_1, this.dbSession);

		user.setSpammer(Boolean.TRUE);

		try {
			adminDb.flagSpammer(user, "admin", this.dbSession);
			// this should throw an exception
			Assert.fail("User was flagged as spammer while being member of a group.");
		} catch (final IllegalStateException ex) {
			assertTrue(!userDatabaseManager.getUserDetails(user.getName(), this.dbSession).isSpammer());
		}
	}

	/**
	 * tests populating given user object with spam flags
	 */
	@Test
	public void getClassifierUserDetails() {
		final User user = new User();
		user.setName("testspammer2");
		user.setEmail("testMail@nomail.com");
		user.setHobbies("spamming, flaming");
		user.setSpammer(Boolean.TRUE);
		user.setToClassify(1);
		user.setPrediction(1);
		user.setConfidence(0.2);
		user.setMode("D");
		user.setAlgorithm("testlogging");
		// flag spammer (flagging does not change: user is no spammer)
		adminDb.flagSpammer(user, "fei", "off", this.dbSession);

		// remove spam informations
		user.setSpammer(null);
		user.setToClassify(null);
		user.setPrediction(null);
		user.setConfidence(null);
		user.setMode(null);

		// populate user with spam informations
		final User userRead = adminDb.getClassifierUserDetails(user, this.dbSession);

		// assure, that spam data is read but no other informations lost
		assertEquals(user, userRead);
		assertEquals("testMail@nomail.com", user.getEmail());
		assertEquals("spamming, flaming", user.getHobbies());
		// assertEquals(true, user.isSpammer());
		// assertEquals(0, user.getToClassify());
		assertThat(user.getPrediction(), equalTo(1));
		assertEquals(0.2, user.getConfidence(), 0.001);
		assertEquals("D", user.getMode());
		assertEquals("testlogging", user.getAlgorithm());
	}
}