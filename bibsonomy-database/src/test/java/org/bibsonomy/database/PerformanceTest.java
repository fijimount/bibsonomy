/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database;

import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.database.managers.AbstractDatabaseManagerTest;
import org.bibsonomy.database.managers.BookmarkDatabaseManager;
import org.bibsonomy.database.params.BookmarkParam;
import org.bibsonomy.testutil.ParamUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Performance tests for database methods.
 * 
 * @author Christian Schenk
 */
@Ignore
public class PerformanceTest extends AbstractDatabaseManagerTest {

	private static final Log log = LogFactory.getLog(PerformanceTest.class);
	
	private static BookmarkDatabaseManager bookmarkDb;
	
	/**
	 * sets up managers
	 */
	@BeforeClass
	public static void setupManager() {
		bookmarkDb = BookmarkDatabaseManager.getInstance();
	}

	/** This is used for the great switch statement in callMethod() */
	private enum Method {
		/** getBookmarkByTagNames */
		getBookmarkByTagNames,
		/** getBookmarkByTagNamesForUser */
		getBookmarkByTagNamesForUser,
		/** getBookmarkByConceptForUser */
		getBookmarkByConceptForUser,
		/** getBookmarkByUserFriends */
		getBookmarkByUserFriends,
		/** getBookmarkForHomepage */
		getBookmarkForHomepage,
		/** getBookmarkPopular */
		getBookmarkPopular,
		/** getBookmarkByHash */
		getBookmarkByHash,
		/** getBookmarkByHashCount */
		getBookmarkByHashCount,
		/** getBookmarkByHashForUser */
		getBookmarkByHashForUser,
		/** getBookmarkSearch */
		getBookmarkSearch,
		/** getBookmarkSearchCount */
		getBookmarkSearchCount,
		/** getBookmarkViewable */
		getBookmarkViewable,
		/** getBookmarkForGroup */
		getBookmarkForGroup,
		/** getBookmarkForGroupCount */
		getBookmarkForGroupCount,
		/** getBookmarkForGroupByTag */
		getBookmarkForGroupByTag,
		/** getBookmarkForUser */
		getBookmarkForUser,
		/** getBookmarkForUserCount */
		getBookmarkForUserCount
	}

	/**
	 * Executes all methods we'd like to evaluate.
	 */
	@Test
	public void testPerf() {
		for (final Method method : Method.values()) {
			this.runPerfTest(method);
		}
	}

	/**
	 * Runs one method several times and prints statistics to the debug logger.
	 */
	private void runPerfTest(final Method method) {
		int totalQueries = 0;
		try {
			final BookmarkParam param = ParamUtils.getDefaultBookmarkParam();
			final String methodname = method.name();
			long all = 0;

			// make 20 runs of the same task, so we get a good average
			for (int i = 0; i <= 20; i++) {
				// save the start time
				final long start = System.currentTimeMillis();

				// run method 5 times
				for (int j = 0; j < 5; j++) {
					/*
					 * HERE we call the database method
					 */
					this.callMethod(method, param);
					totalQueries++;
				}

				// save the time once the task is finished
				final long end = System.currentTimeMillis();

				// on the first run iBATIS is starting up and we don't want to
				// measure that. Even though this isn't correct after the first
				// run for the following methods, we leave it in and execute
				// one unnecessary call for every method.
				if (i == 0) {
					continue;
				}

				all += (end - start);
			}

			log.debug("Executed " + (totalQueries - 5) + " queries of " + methodname + " in: " + all + " ms");
			log.debug("5 queries of " + methodname + " took: " + (all / 20) + " ms");
			log.debug("1 query   of " + methodname + " took: " + ((all / 20) / 5) + " ms");
			log.debug("Under this circumstances " + (1000 / ((all / 20) / 5)) + " queries could be executed in one second");
		} catch (final Throwable ex) {
			// ex.printStackTrace();
			fail("Exception: " + ex.getMessage());
		}
	}

	/**
	 * Calls the specified method.
	 */
	private void callMethod(final Method method, final BookmarkParam param) {
		switch (method) {
		case getBookmarkByTagNames:
			bookmarkDb.getPostsByTagNames(param.getGroupId(), param.getTagIndex(), param.getSortKey(), param.getLimit(), param.getOffset(), this.dbSession);
			break;
		case getBookmarkByTagNamesForUser:
			bookmarkDb.getPostsByTagNamesForUser(param.getUserName(), param.getRequestedUserName(), param.getTagIndex(), param.getGroupId(), param.getGroups(), param.getLimit(), param.getOffset(), null, param.getFilters(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkByConceptForUser:
			bookmarkDb.getPostsByConceptForUser(param.getUserName(), param.getRequestedUserName(), param.getGroups(), param.getTagIndex(), param.isCaseSensitiveTagNames(), param.getLimit(), param.getOffset(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkByUserFriends:
			bookmarkDb.getPostsByUserFriends(param.getUserName(), HashID.getSimHash(param.getSimHash()), param.getLimit(), param.getOffset(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkForHomepage:
			bookmarkDb.getPostsForHomepage(param.getFilters(), param.getStartDate(), param.getEndDate(), param.getLimit(), param.getOffset(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkPopular:
			bookmarkDb.getPostsPopular(param.getDays(), param.getLimit(), param.getOffset(), HashID.getSimHash(param.getSimHash()), this.dbSession);
			break;
		case getBookmarkByHash:
			bookmarkDb.getPostsByHash(null, param.getHash(), HashID.getSimHash(param.getSimHash()), param.getGroupId(), null, param.getLimit(), param.getOffset(), this.dbSession);
			break;
		case getBookmarkByHashCount:
			bookmarkDb.getPostsByHashCount(param.getHash(), HashID.getSimHash(param.getSimHash()), this.dbSession);
			break;
		case getBookmarkByHashForUser:
			bookmarkDb.getPostsByHashForUser(param.getUserName(), param.getHash(), param.getRequestedUserName(), new ArrayList<Integer>(), HashID.INTRA_HASH, this.dbSession);
			break;
		case getBookmarkViewable:
			bookmarkDb.getPostsViewable(param.getRequestedGroupName(), param.getUserName(), param.getGroupId(), HashID.getSimHash(param.getSimHash()), param.getLimit(), param.getOffset(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkForGroup:
			bookmarkDb.getPostsForGroup(param.getGroupId(), param.getGroups(), param.getUserName(), HashID.getSimHash(param.getSimHash()), null, param.getFilters(), param.getLimit(), param.getOffset(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkForGroupCount:
			bookmarkDb.getPostsForGroupCount(param.getRequestedUserName(), param.getUserName(), param.getGroupId(), param.getGroups(), this.dbSession);
			break;
		case getBookmarkForGroupByTag:
			bookmarkDb.getPostsForGroupByTag(param.getGroupId(), param.getGroups(), param.getUserName(), param.getTagIndex(), null, param.getFilters(), param.getLimit(), param.getOffset(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkForUser:
			bookmarkDb.getPostsForUser(param.getUserName(), param.getRequestedUserName(), HashID.getSimHash(param.getSimHash()), param.getGroupId(), param.getGroups(), null, param.getFilters(), param.getLimit(), param.getOffset(), param.getSystemTags(), this.dbSession);
			break;
		case getBookmarkForUserCount:
			bookmarkDb.getPostsForUserCount(param.getRequestedUserName(), param.getUserName(), param.getGroupId(), param.getGroups(), this.dbSession);
			break;
		default:
			throw new RuntimeException("The method " + method.name() + " can't be found in the switch");
		}
	}
}