/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.plugin.plugins;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.database.common.enums.ConstantID;
import org.bibsonomy.database.common.params.beans.TagIndex;
import org.bibsonomy.database.managers.AbstractDatabaseManagerTest;
import org.bibsonomy.database.managers.BibTexDatabaseManager;
import org.bibsonomy.database.managers.BookmarkDatabaseManager;
import org.bibsonomy.database.managers.GroupDatabaseManager;
import org.bibsonomy.database.managers.TagRelationDatabaseManager;
import org.bibsonomy.database.params.BibTexParam;
import org.bibsonomy.database.params.BookmarkParam;
import org.bibsonomy.database.params.GroupParam;
import org.bibsonomy.database.params.TagParam;
import org.bibsonomy.database.params.TagRelationParam;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.User;
import org.bibsonomy.testutil.ParamUtils;
import org.bibsonomy.testutil.TestDatabaseManager;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * The first methods test the syntax of the methods of the class: Logging.java.
 * The SQL methods test if the SQL statements of the Logging.java class are
 * semantically correct.
 *
 * @author Anton Wilhelm
 */
@Ignore // FIXME adapt to new test db
public class LoggingTest extends AbstractDatabaseManagerTest {

	private static BookmarkDatabaseManager bookmarkDb;
	private static BibTexDatabaseManager publicationDb;
	private static GroupDatabaseManager groupDb;
	private static TagRelationDatabaseManager tagRelDb;
	private static TestDatabaseManager testDb;
	private static final User loginUser = new User("testuser");

	/**
	 * sets up the used managers
	 */
	@BeforeClass
	public static void setupDatabaseManager() {
		bookmarkDb = BookmarkDatabaseManager.getInstance();
		groupDb = GroupDatabaseManager.getInstance();
		publicationDb = BibTexDatabaseManager.getInstance();
		tagRelDb = TagRelationDatabaseManager.getInstance();
		testDb = new TestDatabaseManager();
	}

	/**
	 * tests onBibTexDelete
	 */
	@Test
	public void onBibTexDelete() {
		final int anyContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		pluginRegistry.onPublicationDelete(anyContentId, this.dbSession);
	}

	/**
	 * tests onBibTexUpdate
	 */
	@Test
	public void onBibTexUpdate() {
		final int anyContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		pluginRegistry.onPublicationUpdate(anyContentId, anyContentId - 1, this.dbSession);
	}

	/**
	 * tests onBookmarkDelete
	 */
	@Test
	public void onBookmarkDelete() {
		final int anyContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		pluginRegistry.onBookmarkDelete(anyContentId, this.dbSession);
	}

	/**
	 * tests onBookmarkUpdate
	 */
	@Test
	public void onBookmarkUpdate() {
		final int anyContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		pluginRegistry.onBookmarkUpdate(anyContentId, anyContentId - 1, this.dbSession);
	}

	/**
	 * tests onTagRelationDelete
	 */
	@Test
	public void onTagRelationDelete() {
		testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		pluginRegistry.onTagRelationDelete("upperTagName", "lowerTagName", "userName", this.dbSession);
	}

	/**
	 * tests onTagDelete
	 */
	@Test
	public void onTagDelete() {
		final int anyContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		pluginRegistry.onTagDelete(anyContentId, this.dbSession);
	}

	/**
	 * tests removeUserFromGroup
	 */
	@Test
	public void removeUserFromGroup() {
		testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		final Group testgroup1 = groupDb.getGroup("", "testgroup1", false, false, this.dbSession);
		pluginRegistry.onChangeUserMembershipInGroup(testgroup1, "username", USER_TESTUSER_1, this.dbSession);
	}

	/**
	 *
	 * SQL - methods
	 * ------------------------------------------------------------------------------
	 *
	 *
	 * The procedure of all following methods can be describes in the following
	 * way
	 *
	 * 1) Search for each Method any Object (BibTex, Bookmark, etc.) with
	 * parameter: ContentID, Name, Hash, etc. 2) Build a param for this Object
	 * ans set the parameter 3) Count in the log_<OBJECT> table for the choosen
	 * Object, it must be 0 4) Do the Logging ( for example:
	 * this.bibTexDb.storePost(...); ) 5) Count it again in the log_<OBJECT>
	 * table, it must 1
	 *
	 * All* methods which are calling by the generalDb access to the log_<OBJECT>
	 * table Example: countNewContentIdFromBibTex(...) access to the log_bibtex
	 * table * only "countTasIds()" is special
	 */

	/**
	 * tests onBibTexUpdateSQL
	 */
	@Test
	public void onBibTexUpdateSQL() {
		final String HASH = "00078c9690694eb9a56ca7866b5101c6"; // this is an INTER-hash
		final BibTexParam param = ParamUtils.getDefaultBibTexParam();
		param.setHash(HASH);
		param.setSimHash(HashID.INTER_HASH);
		final Post<BibTex> someBibTexPost = publicationDb.getPostsByHash(null, HASH, HashID.INTER_HASH, PUBLIC_GROUP_ID, null, 50, 0, this.dbSession).get(0);

		Integer currentContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		// +1 for the future contentId
		param.setNewContentId(currentContentId + 1);
		Integer result = testDb.countNewContentIdFromBibTex(param);
		assertThat(result, is(0));
		publicationDb.updatePost(someBibTexPost, someBibTexPost.getResource().getIntraHash(), loginUser, PostUpdateOperation.UPDATE_ALL, this.dbSession);

		currentContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		param.setNewContentId(currentContentId);
		result = testDb.countNewContentIdFromBibTex(param);
		assertThat(result, is(1));
	}

	/**
	 * tests onBibTexDeleteSQL
	 */
	@Test
	public void onBibTexDeleteSQL() {
		final String intraHash = "a0bda74e39a8f4c286a81fc66e77f69d";
		// ContentId of the BibTex with the Hash above
		final int contentId = 711342;
		final BibTexParam param = ParamUtils.getDefaultBibTexParam();
		param.setRequestedContentId(contentId);
		param.setHash(intraHash);
		param.setSimHash(HashID.INTRA_HASH);
		final Post<BibTex> someBibTexPost = publicationDb.getPostsByHash(null, intraHash, HashID.INTER_HASH, PUBLIC_GROUP_ID, null, 50, 0, this.dbSession).get(0);

		int result = testDb.countRequestedContentIdFromBibTex(param);
		assertEquals(0, result);

		publicationDb.deletePost(someBibTexPost.getUser().getName(), intraHash, null, this.dbSession);

		result = testDb.countRequestedContentIdFromBibTex(param);
		assertEquals(1, result);
	}

	/**
	 * tests onBookmarkUpdateSQL
	 */
	@Test
	public void onBookmarkUpdateSQL() {
		final String HASH = "0008bae834cc2af4a63fead1fd04b3e1";
		final BookmarkParam param = ParamUtils.getDefaultBookmarkParam();
		param.setHash(HASH);
		final Post<Bookmark> someBookmarkPost = bookmarkDb.getPostsByHash(null, HASH, HashID.INTRA_HASH, PUBLIC_GROUP_ID, null, 10, 0, this.dbSession).get(0);

		Integer currentContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		param.setNewContentId(currentContentId + 1); // +1, next content_id
		int result = testDb.countNewContentIdFromBookmark(param);
		assertEquals(0, result);

		bookmarkDb.updatePost(someBookmarkPost, HASH, loginUser, PostUpdateOperation.UPDATE_ALL, this.dbSession);

		currentContentId = testDb.getCurrentContentId(ConstantID.IDS_CONTENT_ID);
		param.setNewContentId(currentContentId);
		result = testDb.countNewContentIdFromBookmark(param);
		assertEquals(1, result);
	}

	/**
	 * tests onBookmarkDeleteSQL
	 */
	@Test
	public void onBookmarkDeleteSQL() {
		final String HASH = "00319006d9b0105704533e49661ffab6";
		// ContentId of the Bookmark with the Hash above
		final int contentId = 716849;
		final Post<Bookmark> someBookmarkPost = bookmarkDb.getPostsByHash(null, HASH, HashID.INTRA_HASH, PUBLIC_GROUP_ID, null, 10, 0, this.dbSession).get(0);

		final BookmarkParam param = ParamUtils.getDefaultBookmarkParam();
		param.setRequestedContentId(contentId);
		param.setHash(HASH);
		int result = testDb.countRequestedContentIdFromBookmark(param);
		assertEquals(0, result);

		bookmarkDb.deletePost(someBookmarkPost.getUser().getName(), HASH, null, this.dbSession);

		result = testDb.countRequestedContentIdFromBookmark(param);
		assertEquals(1, result);
	}

	/**
	 * For Testing the onTagDelete() method you must first build a BibTex and
	 * then delete it, the Tags will be deleted automatically by the delete
	 * method of the PostDatabaseManager
	 *
	 * 2nd assertion: countTasIds() count the number of TAS with the choosen
	 * ContentID in the original table: bibtex countLoggedTasIds() count it in
	 * the logging table: log_bibtex At the end it will be comparing
	 * (res_original, res_logging)
	 */
	@Test
	public void onTagDeleteSQL() {
		// final String HASH = "00078c9690694eb9a56ca7866b5101c6"; INTERHASH
		final String HASH = "a0bda74e39a8f4c286a81fc66e77f69d"; // INTRAHASH
		// ContentId of the BibTex with the Hash above
		final int contentId = 711342;
		final BibTexParam param = ParamUtils.getDefaultBibTexParam();
		param.setRequestedContentId(contentId);
		param.setHash(HASH);
		param.setSimHash(HashID.INTRA_HASH);
		final TagParam tagparam = ParamUtils.getDefaultTagParam();
		tagparam.setRequestedContentId(contentId);
		final Post<BibTex> someBibTexPost = publicationDb.getPostsByHash(null, HASH, HashID.INTRA_HASH, PUBLIC_GROUP_ID, null, 50, 0, this.dbSession).get(0);

		final Integer res_original = testDb.countTasIds(tagparam);
		int result = testDb.countRequestedContentIdFromBibTex(param);
		assertEquals(0, result);

		publicationDb.deletePost(someBibTexPost.getUser().getName(), HASH, null, this.dbSession);

		result = testDb.countRequestedContentIdFromBibTex(param);
		assertEquals(1, result);
		final Integer res_logging = testDb.countLoggedTasIds(tagparam);
		assertEquals(res_original, res_logging);
	}

	/**
	 * 2nd assertion: It is like in the onTagDeleteSQL() method
	 * getBibTexByConceptForUser() will be access before and after logging (in
	 * the original table!) At the end the tests checks if the TagRelation
	 * decreases in the orignial table
	 *
	 */
	@Test
	public void onTagRelationDeleteSQL() {
		final List<TagIndex> tagIndex = new ArrayList<>();
		final String user = "jaeschke", lower = "shannon", upper = "researcher";
		final List<Integer> visibleGroupIDs = new ArrayList<>();
		tagIndex.add(new TagIndex("researcher", 1));
		final int countBefore = publicationDb.getPostsByConceptForUser(user, user, visibleGroupIDs, tagIndex, false, 100, 0, null, this.dbSession).size();
		final TagRelationParam trp = new TagRelationParam();
		trp.setOwnerUserName(user);
		trp.setLowerTagName(lower);
		trp.setUpperTagName(upper);
		int result = testDb.countTagRelation(trp);
		assertEquals(0, result);
		tagRelDb.deleteRelation(upper, lower, user, this.dbSession);
		final int countAfter = publicationDb.getPostsByConceptForUser(user, user, visibleGroupIDs, tagIndex, false, 100, 0, null, this.dbSession).size();
		result = testDb.countTagRelation(trp);
		assertTrue(countBefore > countAfter);
		assertEquals(1, result);
	}

	/**
	 * tests onRemoveUserFromGroupSQL
	 */
	@Test
	public void onRemoveUserFromGroupSQL() {
		final String user = "jaeschke", groupname = "kde";
		final GroupParam param = new GroupParam();
		param.setUserName(user);
		param.setGroupId(TESTGROUP1_ID);

		int result = testDb.countGroup(param);
		assertEquals(0, result);

		groupDb.removeUserFromGroup(groupname, user, false, USER_TESTUSER_1, this.dbSession);
		result = testDb.countGroup(param);
		assertEquals(1, result);
	}
}