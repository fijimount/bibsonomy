/**
 * BibSonomy-Database - Database for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.testutil;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.bibsonomy.database.common.params.beans.TagIndex;
import org.junit.Test;

/**
 * @author dzo
 */
public class DBTestUtilsTest {
	
	private static final String TAG_STRING_1 = "testtag";
	private static final String TAG_STRING_2 = "tag2";
	private static final String TAG_STRING_3 = "test2";

	/**
	 * tests {@link DBTestUtils#getTagIndex(String...)}
	 */
	@Test
	public void tagIndexBuild() {		
		final List<TagIndex> index = DBTestUtils.getTagIndex(TAG_STRING_1, TAG_STRING_2);
		assertEquals(2, index.size());
		
		final TagIndex firstIndex = index.get(0);
		assertEquals(TAG_STRING_1, firstIndex.getTagName());
		assertEquals(1, firstIndex.getIndex());
		
		final TagIndex secondIndex = index.get(1);
		assertEquals(TAG_STRING_2, secondIndex.getTagName());
		assertEquals(2, secondIndex.getIndex());
	}
	
	/**
	 * tests {@link DBTestUtils#addToTagIndex(List, String...)}
	 */
	@Test
	public void tagIndexAdd() {
		final List<TagIndex> index = DBTestUtils.getTagIndex(TAG_STRING_1, TAG_STRING_2);
		DBTestUtils.addToTagIndex(index, TAG_STRING_3);
		
		final TagIndex thirdIndex = index.get(2);
		assertEquals(TAG_STRING_3, thirdIndex.getTagName());
		assertEquals(3, thirdIndex.getIndex());
	}
}
