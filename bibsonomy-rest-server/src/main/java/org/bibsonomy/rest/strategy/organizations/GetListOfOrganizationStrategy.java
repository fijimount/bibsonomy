/**
 * BibSonomy-Rest-Server - The REST-server.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.bibsonomy.rest.strategy.organizations;

import org.bibsonomy.model.Group;
import org.bibsonomy.model.logic.query.GroupQuery;
import org.bibsonomy.rest.ViewModel;
import org.bibsonomy.rest.strategy.AbstractGetListStrategy;
import org.bibsonomy.rest.strategy.Context;
import org.bibsonomy.util.UrlBuilder;

import java.io.Writer;
import java.util.List;

/**
 * Strategy to get a list of organization.
 *
 * @author kchoong
 */
public class GetListOfOrganizationStrategy extends AbstractGetListStrategy<List<Group>> {

	private final String internalId;

	/**
	 * @param context
	 */
	public GetListOfOrganizationStrategy(final Context context) {
		super(context);
		internalId = context.getStringAttribute("internalId", null);
	}

	@Override
	public String getContentType() {
		return "organizations";
	}

	@Override
	protected UrlBuilder getLinkPrefix() {
		return this.getUrlRenderer().getUrlBuilderForGroups();
	}

	@Override
	protected List<Group> getList() {
		final ViewModel view = this.getView();
		final GroupQuery groupQuery = GroupQuery.builder().start(view.getStartValue()).end(view.getEndValue()).
				pending(false).externalId(this.internalId).organization(true).build();
		return this.getLogic().getGroups(groupQuery);
	}

	@Override
	protected void render(final Writer writer, final List<Group> resultList) {
		this.getRenderer().serializeGroups(writer, resultList, getView());
	}
}
