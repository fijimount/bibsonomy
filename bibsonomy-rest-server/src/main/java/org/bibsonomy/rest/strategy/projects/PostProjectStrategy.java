/**
 * BibSonomy-Rest-Server - The REST-server.
 * <p>
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 * University of Kassel, Germany
 * http://www.kde.cs.uni-kassel.de/
 * Data Mining and Information Retrieval Group,
 * University of Würzburg, Germany
 * http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 * L3S Research Center,
 * Leibniz University Hannover, Germany
 * http://www.l3s.de/
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.strategy.projects;

import org.bibsonomy.common.JobResult;
import org.bibsonomy.common.enums.Status;
import org.bibsonomy.common.errors.ErrorMessage;
import org.bibsonomy.model.cris.Project;
import org.bibsonomy.rest.exceptions.BadRequestOrResponseException;
import org.bibsonomy.rest.strategy.AbstractCreateStrategy;
import org.bibsonomy.rest.strategy.Context;

import java.io.Writer;
import java.util.stream.Collectors;

/**
 * strategy to create a new project
 *
 * @author pda
 */
public class PostProjectStrategy extends AbstractCreateStrategy {
    public PostProjectStrategy(Context context) {
        super(context);
    }

    @Override
    protected void render(Writer writer, String projectID) {
        this.getRenderer().serializeProjectId(writer, projectID);
    }

    @Override
    protected String create() {
        final Project project = this.getRenderer().parseProject(this.doc);
        final JobResult jobResult = this.getLogic().createProject(project);
        if (Status.FAIL.equals(jobResult.getStatus())) {
            throw new BadRequestOrResponseException(jobResult.getErrors().stream().
                    map(ErrorMessage::getDefaultMessage).collect(Collectors.joining(",")));
        }
        return jobResult.getId();
    }
}
