/**
 * BibSonomy-Rest-Server - The REST-server.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.strategy;

import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.bibsonomy.common.exceptions.InternServerException;
import org.bibsonomy.rest.RESTConfig;
import org.bibsonomy.rest.ViewModel;
import org.bibsonomy.rest.exceptions.BadRequestOrResponseException;
import org.bibsonomy.util.SortUtils;
import org.bibsonomy.util.UrlBuilder;

import java.io.ByteArrayOutputStream;
import java.io.Writer;
import java.util.List;

import static org.bibsonomy.util.ValidationUtils.present;

/**
 * @author Jens Illig
 * @param <L> 
 */
public abstract class AbstractGetListStrategy<L extends List<?>> extends Strategy {
	private final ViewModel view;
	
	/**
	 * @param context
	 */
	public AbstractGetListStrategy(final Context context) {
		super(context);
		this.view = new ViewModel();
		this.view.setStartValue(context.getIntAttribute(RESTConfig.START_PARAM, 0));
		this.view.setEndValue(context.getIntAttribute(RESTConfig.END_PARAM, 20));
		
		try {
			final String sortKeysAsString = context.getStringAttribute(RESTConfig.SORT_KEY_PARAM, null);
			final String sortOrdersAsString = context.getStringAttribute(RESTConfig.SORT_ORDER_PARAM, null);
			if (present(sortKeysAsString)) {
				List<SortKey> sortKeys = SortUtils.parseSortKeys(sortKeysAsString);
				this.view.setSortKeys(sortKeys);
			}
			if (present(sortOrdersAsString)) {
				List<SortOrder> sortOrders = SortUtils.parseSortOrders(sortOrdersAsString);
				this.view.setSortOrders(sortOrders);
			}
		} catch (final IllegalArgumentException e) {
			// the client send a wrong query param throw correct exception
			throw new BadRequestOrResponseException(e);
		}
		
		if (view.getStartValue() > view.getEndValue()) {
			throw new BadRequestOrResponseException("start must be less than or equal end");
		}
	}

	@Override
	public final void perform(final ByteArrayOutputStream outStream) throws InternServerException { final L resultList = getList();
		
		if (resultList.size() != (getView().getEndValue() - getView().getStartValue())) {
			this.view.setEndValue( resultList.size() + this.view.getStartValue());
		} else {
			this.view.setUrlToNextResources( buildNextLink() );
		}
		render(writer, resultList);
	}

	protected abstract void render(Writer writer, L resultList);

	protected abstract L getList();

	private final String buildNextLink() {
		final UrlBuilder urlBuilder = this.getLinkPrefix();
		final int endValue = getView().getEndValue();
		urlBuilder.addParameter(RESTConfig.START_PARAM, String.valueOf(endValue));
		urlBuilder.addParameter(RESTConfig.END_PARAM, String.valueOf(endValue + endValue - getView().getStartValue()));
		return urlBuilder.asString();
	}

	protected abstract UrlBuilder getLinkPrefix();
	
	protected ViewModel getView() {
		return this.view;
	}
}