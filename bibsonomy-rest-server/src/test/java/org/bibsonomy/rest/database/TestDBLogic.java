/**
 * BibSonomy-Rest-Server - The REST-server.
 * <p>
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 * University of Kassel, Germany
 * http://www.kde.cs.uni-kassel.de/
 * Data Mining and Information Retrieval Group,
 * University of Würzburg, Germany
 * http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 * L3S Research Center,
 * Leibniz University Hannover, Germany
 * http://www.l3s.de/
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.database;

import org.bibsonomy.common.SortCriteria;
import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.QueryScope;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.TagSimilarity;
import org.bibsonomy.common.enums.UserRelation;
import org.bibsonomy.common.enums.UserUpdateOperation;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.GroupMembership;
import org.bibsonomy.model.PersonName;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.ResourcePersonRelation;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.LogicInterfaceFactory;
import org.bibsonomy.model.logic.query.GroupQuery;
import org.bibsonomy.model.logic.query.PostQuery;
import org.bibsonomy.model.logic.query.ResourcePersonRelationQuery;
import org.bibsonomy.model.logic.querybuilder.PostQueryBuilder;
import org.bibsonomy.model.logic.util.AbstractLogicInterface;
import org.bibsonomy.model.statistics.Statistics;
import org.junit.Ignore;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class is used for demonstrating purposes only. It is not designed to
 * verify any algorithm nor any strategy. Testing strategies with this class is
 * not possible, because one would only test the testcase's algorithm itself.<br/>
 *
 * Furthermore the implementation is not complete; especially unimplemented are:
 * <ul>
 * <li>start and end value</li>
 * <li>class-relations of tags (subclassing/ superclassing)</li>
 * <li>popular- and added-flag at the posts-query</li>
 * <li>viewable-stuff</li>
 * </ul>
 *
 * @author Manuel Bork <manuel.bork@uni-kassel.de>
 * @author Christian Kramer
 * @author Jens Illig
 */
@Ignore
public class TestDBLogic extends AbstractLogicInterface {

	private final User loginUser;

	private final Map<String, Group> dbGroups;
	private final Map<String, User> dbUsers;
	private final Map<String, Tag> dbTags;
	private final Map<String, Resource> dbResources;
	private final Date date;

	/**
	 * a factory for this implementation
	 */
	public static final LogicInterfaceFactory factory = new TestDBLogicInterfaceFactory();

	/**
	 * creates a new LogicInterface Implementation for tests
	 * @param authUserName name of the user in whose name testoperations shall be performed 
	 */
	public TestDBLogic(final String authUserName) {
		this.loginUser = new User(authUserName);

		// use the linked map because ordering matters for the junit tests..
		this.dbGroups = new LinkedHashMap<>();
		this.dbUsers = new LinkedHashMap<>();
		this.dbTags = new LinkedHashMap<>();
		this.dbResources = new LinkedHashMap<>();

		final Calendar cal = Calendar.getInstance();
		cal.clear();
		this.date = cal.getTime();

		try {
			fillDataBase();
		} catch (final MalformedURLException ex) {
			throw new RuntimeException(ex);
		}
	}


	@Override
	public User getUserDetails(final String userName) {
		return this.dbUsers.get(userName);
	}

	@Override
	public Post<? extends Resource> getPostDetails(final String resourceHash, final String userName) {
		final User user = this.dbUsers.get(userName);
		if (user != null) {
			for (final Post<? extends Resource> p : user.getPosts()) {
				if (p.getResource().getInterHash().equals(resourceHash)) {
					return p;
				}
			}
		}
		return null;
	}

	@Override
	public List<Group> getGroups(final GroupQuery query) {
		return new LinkedList<>(this.dbGroups.values());
	}

	@Override
	public Group getGroupDetails(final String groupName, final boolean pending) {
		return this.dbGroups.get(groupName);
	}

	/**
	 * note: the regex is currently not considered
	 */
	@Override
	public List<Tag> getTags(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags_, final String hash, final String search, final String regex, final TagSimilarity relation, final SortKey sortKey, final Date startDate, final Date endDate, final int start, final int end) {
		return this.getTags(resourceType, grouping, groupingName, tags_, hash, search, QueryScope.LOCAL, regex, relation, sortKey, startDate, endDate, start, end);
	}

	/**
	 * note: the regex is currently not considered
	 */
	@Override
	public List<Tag> getTags(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags_, final String hash, final String search, final QueryScope queryScope, final String regex, final TagSimilarity relation, final SortKey sortKey, final Date startDate, final Date endDate, final int start, final int end) {
		final List<Tag> tags = new LinkedList<>();

		switch (grouping) {
			case VIEWABLE:
				// simply use groups
			case GROUP:
				if (this.dbGroups.get(groupingName) != null) {
					for (final Post<? extends Resource> post : this.dbGroups.get(groupingName).getPosts()) {
						tags.addAll(post.getTags());
					}
				}
				break;
			case USER:
				if (this.dbUsers.get(groupingName) != null) {
					for (final Post<? extends Resource> post : this.dbUsers.get(groupingName).getPosts()) {
						tags.addAll(post.getTags());
					}
				}
				break;
			default: // ALL
				tags.addAll(this.dbTags.values());
				break;
		}

		return tags;
	}

	@Override
	public Tag getTagDetails(final String tagName) {
		return this.dbTags.get(tagName);
	}

	@Override
	public <T extends Resource> List<Post<T>> getPosts(final PostQuery<T> query) {
		final GroupingEntity grouping = query.getGrouping();
		final String groupingName = query.getGroupingName();
		final Class<T> resourceType = query.getResourceClass();
		final String hash = query.getHash();
		final List<String> tags = query.getTags();
		final List<Post<? extends Resource>> posts = new LinkedList<>();
		// do grouping stuff
		switch (grouping) {
			case USER:
				if (this.dbUsers.get(groupingName) != null) {
					posts.addAll(this.dbUsers.get(groupingName).getPosts());
				}
				break;
			case VIEWABLE:
				// simply use groups
			case GROUP:
				if (this.dbGroups.get(groupingName) != null) {
					posts.addAll(this.dbGroups.get(groupingName).getPosts());
				}
				break;
			default: // ALL
				for (final User user : this.dbUsers.values()) {
					posts.addAll(user.getPosts());
				}
				break;
		}

		// check resourceType
		if (resourceType == Bookmark.class) {
			posts.removeIf(post -> !(post.getResource() instanceof Bookmark));
		} else if (resourceType == BibTex.class) {
			posts.removeIf(post -> !(post.getResource() instanceof BibTex));
		}

		// now this cast is ok
		@SuppressWarnings({"unchecked", "rawtypes"}) final List<Post<T>> rVal = ((List) posts);
		// check hash
		if (hash != null) {
			rVal.removeIf(tPost -> !tPost.getResource().getInterHash().equals(hash));
		}

		// do tag filtering
		if (tags.size() > 0) {
			for (final Iterator<Post<T>> it = rVal.iterator(); it.hasNext(); ) {
				boolean drin = false;
				for (final Tag tag : it.next().getTags()) {
					for (final String searchTag : tags) {
						if (tag.getName().equals(searchTag)) {
							drin = true;
							break;
						}
					}

				}
				if (!drin) {
					it.remove();
				}
			}
		}
		return rVal;
	}

	/**
	 * Inserts some test data into the local maps
	 */
	private void fillDataBase() throws MalformedURLException {
		// a group
		final Group publicGroup = new Group();
		publicGroup.setName("public");
		this.dbGroups.put(publicGroup.getName(), publicGroup);

		// users
		final User userManu = new User();
		userManu.setEmail("manuel.bork@uni-kassel.de");
		userManu.setHomepage(new URL("http://www.manuelbork.de"));
		userManu.setName("mbork");
		userManu.setRealname("Manuel Bork");
		userManu.setRegistrationDate(new Date(System.currentTimeMillis()));
		this.dbUsers.put(userManu.getName(), userManu);
		userManu.getGroups().add(publicGroup);

		final User userAndreas = new User();
		userAndreas.setEmail("andreas.hotho@uni-kassel.de");
		userAndreas.setHomepage(new URL("http://www.bibsonomy.org"));
		userAndreas.setName("hotho");
		userAndreas.setRealname("Andreas Hotho");
		userAndreas.setRegistrationDate(new Date(System.currentTimeMillis()));
		this.dbUsers.put(userAndreas.getName(), userAndreas);
		userAndreas.getGroups().add(publicGroup);

		final User userButonic = new User();
		userButonic.setEmail("joern.dreyer@uni-kassel.de");
		userButonic.setHomepage(new URL("http://www.butonic.org"));
		userButonic.setName("butonic");
		userButonic.setRealname("Joern Dreyer");
		userButonic.setRegistrationDate(new Date(System.currentTimeMillis()));
		this.dbUsers.put(userButonic.getName(), userButonic);
		userButonic.getGroups().add(publicGroup);

		// tags
		final Tag spiegelTag = new Tag();
		spiegelTag.setName("spiegel");
		spiegelTag.setUsercount(1);
		spiegelTag.setGlobalcount(1);
		this.dbTags.put(spiegelTag.getName(), spiegelTag);

		final Tag hostingTag = new Tag();
		hostingTag.setName("hosting");
		hostingTag.setUsercount(1);
		hostingTag.setGlobalcount(1);
		this.dbTags.put(hostingTag.getName(), hostingTag);

		final Tag lustigTag = new Tag();
		lustigTag.setName("lustig");
		lustigTag.setUsercount(1);
		lustigTag.setGlobalcount(1);
		this.dbTags.put(lustigTag.getName(), lustigTag);

		final Tag nachrichtenTag = new Tag();
		nachrichtenTag.setName("nachrichten");
		nachrichtenTag.setUsercount(1);
		nachrichtenTag.setGlobalcount(2);
		this.dbTags.put(nachrichtenTag.getName(), nachrichtenTag);

		final Tag semwebTag = new Tag();
		semwebTag.setName("semweb");
		semwebTag.setUsercount(1);
		semwebTag.setGlobalcount(4);
		this.dbTags.put(semwebTag.getName(), semwebTag);

		final Tag vorlesungTag = new Tag();
		vorlesungTag.setName("vorlesung");
		vorlesungTag.setUsercount(1);
		vorlesungTag.setGlobalcount(1);
		this.dbTags.put(vorlesungTag.getName(), vorlesungTag);

		final Tag ws0506Tag = new Tag();
		ws0506Tag.setName("ws0506");
		ws0506Tag.setUsercount(1);
		ws0506Tag.setGlobalcount(1);
		this.dbTags.put(ws0506Tag.getName(), ws0506Tag);

		final Tag weltformelTag = new Tag();
		weltformelTag.setName("weltformel");
		weltformelTag.setUsercount(1);
		weltformelTag.setGlobalcount(1);
		this.dbTags.put(weltformelTag.getName(), weltformelTag);

		final Tag mySiteTag = new Tag();
		mySiteTag.setName("mySite");
		mySiteTag.setUsercount(1);
		mySiteTag.setGlobalcount(1);
		this.dbTags.put(mySiteTag.getName(), mySiteTag);

		final Tag wowTag = new Tag();
		wowTag.setName("wow");
		wowTag.setUsercount(2);
		wowTag.setGlobalcount(2);
		this.dbTags.put(wowTag.getName(), wowTag);

		final Tag lehreTag = new Tag();
		lehreTag.setName("lehre");
		lehreTag.setUsercount(2);
		lehreTag.setGlobalcount(2);
		this.dbTags.put(lehreTag.getName(), lehreTag);

		final Tag kddTag = new Tag();
		kddTag.setName("kdd");
		kddTag.setUsercount(1);
		kddTag.setGlobalcount(1);
		this.dbTags.put(kddTag.getName(), kddTag);

		final Tag wwwTag = new Tag();
		wwwTag.setName("www");
		wwwTag.setUsercount(1);
		wwwTag.setGlobalcount(3);
		this.dbTags.put(wwwTag.getName(), wwwTag);

		// this.dbResources
		final Bookmark spiegelOnlineResource = new Bookmark();
		spiegelOnlineResource.setTitle("Spiegel");
		spiegelOnlineResource.setUrl("http://www.spiegel.de");
		spiegelOnlineResource.recalculateHashes();
		this.dbResources.put(spiegelOnlineResource.getIntraHash(), spiegelOnlineResource);

		final Bookmark hostingprojectResource = new Bookmark();
		hostingprojectResource.setTitle("Hostingproject");
		hostingprojectResource.setUrl("http://www.hostingproject.de");
		hostingprojectResource.recalculateHashes();
		this.dbResources.put(hostingprojectResource.getIntraHash(), hostingprojectResource);

		final Bookmark klabusterbeereResource = new Bookmark();
		klabusterbeereResource.setTitle("Klabusterbeere");
		klabusterbeereResource.setUrl("http://www.klabusterbeere.net");
		klabusterbeereResource.recalculateHashes();
		this.dbResources.put(klabusterbeereResource.getIntraHash(), klabusterbeereResource);

		final Bookmark bildschirmarbeiterResource = new Bookmark();
		bildschirmarbeiterResource.setTitle("Bildschirmarbeiter");
		bildschirmarbeiterResource.setUrl("http://www.bildschirmarbeiter.com");
		bildschirmarbeiterResource.recalculateHashes();
		this.dbResources.put(bildschirmarbeiterResource.getIntraHash(), bildschirmarbeiterResource);

		final Bookmark semwebResource = new Bookmark();
		semwebResource.setTitle("Semantic Web Seminar");
		semwebResource.setUrl("http://www.kde.cs.uni-kassel.de/lehre/ws2005-06/Semantic_Web");
		semwebResource.recalculateHashes();
		this.dbResources.put(semwebResource.getIntraHash(), semwebResource);

		final Bookmark butonicResource = new Bookmark();
		butonicResource.setTitle("Butonic");
		butonicResource.setUrl("http://www.butonic.de");
		butonicResource.recalculateHashes();
		this.dbResources.put(butonicResource.getIntraHash(), butonicResource);

		final Bookmark wowResource = new Bookmark();
		wowResource.setTitle("Worldofwarcraft");
		wowResource.setUrl("http://www.worldofwarcraft.com");
		wowResource.recalculateHashes();
		this.dbResources.put(wowResource.getIntraHash(), wowResource);

		final Bookmark dunkleResource = new Bookmark();
		dunkleResource.setTitle("Dunkleherzen");
		dunkleResource.setUrl("http://www.dunkleherzen.de");
		dunkleResource.recalculateHashes();
		this.dbResources.put(dunkleResource.getIntraHash(), dunkleResource);

		final Bookmark w3cResource = new Bookmark();
		w3cResource.setTitle("W3C");
		w3cResource.setUrl("http://www.w3.org/2001/sw/");
		w3cResource.recalculateHashes();
		this.dbResources.put(w3cResource.getIntraHash(), w3cResource);

		final Bookmark wikipediaResource = new Bookmark();
		wikipediaResource.setTitle("Wikipedia");
		wikipediaResource.setUrl("http://de.wikipedia.org/wiki/Semantic_Web");
		wikipediaResource.recalculateHashes();
		this.dbResources.put(wikipediaResource.getIntraHash(), wikipediaResource);

		final Bookmark kddResource = new Bookmark();
		kddResource.setTitle("KDD Seminar");
		kddResource.setUrl("http://www.kde.cs.uni-kassel.de/lehre/ss2006/kdd");
		kddResource.recalculateHashes();
		this.dbResources.put(kddResource.getIntraHash(), kddResource);

		// posts
		final Post<Resource> post_1 = new Post<>();
		post_1.setDescription("Neueste Nachrichten aus aller Welt.");
		post_1.setDate(this.date);
		post_1.setResource(spiegelOnlineResource);
		spiegelOnlineResource.getPosts().add(post_1);
		post_1.setUser(userManu);
		userManu.getPosts().add(post_1);
		post_1.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_1);
		post_1.getTags().add(spiegelTag);
		spiegelTag.getPosts().add(post_1);
		post_1.getTags().add(nachrichtenTag);
		nachrichtenTag.getPosts().add(post_1);

		final Post<Resource> post_2 = new Post<>();
		post_2.setDescription("Toller Webhoster und super Coder ;)");
		post_2.setDate(this.date);
		post_2.setResource(hostingprojectResource);
		hostingprojectResource.getPosts().add(post_2);
		post_2.setUser(userManu);
		userManu.getPosts().add(post_2);
		post_2.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_2);
		post_2.getTags().add(hostingTag);
		hostingTag.getPosts().add(post_2);

		final Post<Resource> post_3 = new Post<>();
		post_3.setDescription("lustiger blog");
		post_3.setDate(this.date);
		post_3.setResource(klabusterbeereResource);
		klabusterbeereResource.getPosts().add(post_3);
		post_3.setUser(userManu);
		userManu.getPosts().add(post_3);
		post_3.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_3);
		post_3.getTags().add(lustigTag);
		lustigTag.getPosts().add(post_3);

		final Post<Resource> post_4 = new Post<>();
		post_4.setDescription("lustiger mist ausm irc ^^");
		post_4.setDate(this.date);
		post_4.setResource(bildschirmarbeiterResource);
		bildschirmarbeiterResource.getPosts().add(post_4);
		post_4.setUser(userManu);
		userManu.getPosts().add(post_4);
		post_4.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_4);
		post_4.getTags().add(lustigTag);
		lustigTag.getPosts().add(post_4);

		final Post<Resource> post_5 = new Post<>();
		post_5.setDescription("Semantic Web Vorlesung im Wintersemester 0506");
		post_5.setDate(this.date);
		post_5.setResource(semwebResource);
		semwebResource.getPosts().add(post_5);
		post_5.setUser(userManu);
		userManu.getPosts().add(post_5);
		post_5.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_5);
		post_5.getTags().add(semwebTag);
		semwebTag.getPosts().add(post_5);
		post_5.getTags().add(vorlesungTag);
		vorlesungTag.getPosts().add(post_5);
		post_5.getTags().add(ws0506Tag);
		ws0506Tag.getPosts().add(post_5);

		final Post<Resource> post_6 = new Post<>();
		post_6.setDescription("joerns blog");
		post_6.setDate(this.date);
		post_6.setResource(butonicResource);
		butonicResource.getPosts().add(post_6);
		post_6.setUser(userButonic);
		userButonic.getPosts().add(post_6);
		post_6.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_6);
		post_6.getTags().add(mySiteTag);
		mySiteTag.getPosts().add(post_6);

		final Post<Resource> post_7 = new Post<>();
		post_7.setDescription("online game");
		post_7.setDate(this.date);
		post_7.setResource(wowResource);
		wowResource.getPosts().add(post_7);
		post_7.setUser(userButonic);
		userButonic.getPosts().add(post_7);
		post_7.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_7);
		post_7.getTags().add(wowTag);
		wowTag.getPosts().add(post_7);

		final Post<Resource> post_8 = new Post<>();
		post_8.setDescription("wow clan");
		post_8.setDate(this.date);
		post_8.setResource(dunkleResource);
		dunkleResource.getPosts().add(post_8);
		post_8.setUser(userButonic);
		userButonic.getPosts().add(post_8);
		post_8.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_8);
		post_8.getTags().add(wowTag);
		wowTag.getPosts().add(post_8);

		final Post<Resource> post_9 = new Post<>();
		post_9.setDescription("w3c site zum semantic web");
		post_9.setDate(this.date);
		post_9.setResource(w3cResource);
		w3cResource.getPosts().add(post_9);
		post_9.setUser(userAndreas);
		userAndreas.getPosts().add(post_9);
		post_9.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_9);
		post_9.getTags().add(semwebTag);
		semwebTag.getPosts().add(post_9);

		final Post<Resource> post_10 = new Post<>();
		post_10.setDescription("wikipedia site zum semantic web");
		post_10.setDate(this.date);
		post_10.setResource(wikipediaResource);
		wikipediaResource.getPosts().add(post_10);
		post_10.setUser(userAndreas);
		userAndreas.getPosts().add(post_10);
		post_10.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_10);
		post_10.getTags().add(semwebTag);
		semwebTag.getPosts().add(post_10);

		final Post<Resource> post_11 = new Post<>();
		post_11.setDescription("kdd vorlesung im ss06");
		post_11.setDate(this.date);
		post_11.setResource(kddResource);
		kddResource.getPosts().add(post_11);
		post_11.setUser(userAndreas);
		userAndreas.getPosts().add(post_11);
		post_11.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_11);
		post_11.getTags().add(lehreTag);
		lehreTag.getPosts().add(post_11);
		post_11.getTags().add(kddTag);
		kddTag.getPosts().add(post_11);

		final Post<Resource> post_12 = new Post<>();
		post_12.setDescription("semantic web vorlesung im ws0506");
		post_12.setDate(this.date);
		post_12.setResource(semwebResource);
		semwebResource.getPosts().add(post_12);
		post_12.setUser(userAndreas);
		userAndreas.getPosts().add(post_12);
		post_12.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_12);
		post_12.getTags().add(lehreTag);
		lehreTag.getPosts().add(post_12);
		post_12.getTags().add(semwebTag);
		semwebTag.getPosts().add(post_12);

		// publication resources & posts
		final BibTex publicationDemo = new BibTex();
		publicationDemo.setAuthor(Arrays.asList(new PersonName("Albert", "Einstein"), new PersonName("Leonardo", "da Vinci")));
		publicationDemo.setEditor(Arrays.asList(new PersonName("Luke", "Skywalker"), new PersonName(null, "Yoda")));
		publicationDemo.setTitle("Die Weltformel");
		publicationDemo.setType("Paper");
		publicationDemo.setYear("2006");
		publicationDemo.setEntrytype("article");
		publicationDemo.setBibtexKey("einstein2006weltformel");
		publicationDemo.recalculateHashes();
		this.dbResources.put(publicationDemo.getIntraHash(), publicationDemo);

		final BibTex publicationDemo1 = new BibTex();
		publicationDemo1.setAuthor(Arrays.asList(new PersonName("R.", "Fielding"), new PersonName("J.", "Gettys"), new PersonName("J.", "Mogul"), new PersonName("H.", "Frystyk"), new PersonName("L.", "Masinter"), new PersonName("P.", "Leach"), new PersonName("T.", "Berners-Lee")));
		publicationDemo1.setEditor(Collections.singletonList(new PersonName("", "")));
		publicationDemo1.setTitle("RFC 2616, Hypertext Transfer Protocol -- HTTP/1.1");
		publicationDemo1.setType("Paper");
		publicationDemo1.setYear("1999");
		publicationDemo1.setEntrytype("article");
		publicationDemo1.setBibtexKey("fielding1999hypertext");
		publicationDemo1.recalculateHashes();
		this.dbResources.put(publicationDemo1.getIntraHash(), publicationDemo1);

		final BibTex publicationDemo2 = new BibTex();
		publicationDemo2.setAuthor(Arrays.asList(new PersonName("Roy T.", "Fielding")));
		publicationDemo2.setEditor(Arrays.asList(new PersonName("", "")));
		publicationDemo2.setTitle("Architectural Styles and the Design of Network-based Software Architectures");
		publicationDemo2.setType("Paper");
		publicationDemo2.setYear("2000");
		publicationDemo2.setEntrytype("article");
		publicationDemo2.setBibtexKey("fielding2000architectural");
		publicationDemo2.recalculateHashes();
		this.dbResources.put(publicationDemo2.getIntraHash(), publicationDemo2);

		final BibTex publicationDemo3 = new BibTex();
		publicationDemo3.setAuthor(Arrays.asList(new PersonName("Tim", "Berners-Lee"), new PersonName("Mark", "Fischetti")));
		publicationDemo3.setEditor(Arrays.asList(new PersonName("", "")));
		publicationDemo3.setTitle("Weaving the web");
		publicationDemo3.setType("Paper");
		publicationDemo3.setYear("1999");
		publicationDemo3.setEntrytype("article");
		publicationDemo3.setBibtexKey("berners-lee1999weaving");
		publicationDemo3.recalculateHashes();
		this.dbResources.put(publicationDemo3.getIntraHash(), publicationDemo3);

		final Post<Resource> post_13 = new Post<>();
		post_13.setDescription("Beschreibung einer allumfassenden Weltformel. Taeglich lesen!");
		post_13.setDate(this.date);
		post_13.setResource(publicationDemo);
		publicationDemo.getPosts().add(post_13);
		post_13.setUser(userAndreas);
		userManu.getPosts().add(post_13);
		post_13.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_13);
		post_13.getTags().add(weltformelTag);
		weltformelTag.getPosts().add(post_13);
		post_13.getTags().add(nachrichtenTag);
		nachrichtenTag.getPosts().add(post_13);

		final Post<Resource> post_14 = new Post<>();
		post_14.setDescription("Grundlagen des www");
		post_14.setDate(this.date);
		post_14.setResource(publicationDemo1);
		publicationDemo1.getPosts().add(post_14);
		post_14.setUser(userManu);
		userManu.getPosts().add(post_14);
		post_14.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_14);
		post_14.getTags().add(wwwTag);
		wwwTag.getPosts().add(post_14);

		final Post<Resource> post_15 = new Post<>();
		post_15.setDescription("So ist unsers api konstruiert.");
		post_15.setDate(this.date);
		post_15.setResource(publicationDemo2);
		publicationDemo2.getPosts().add(post_15);
		post_15.setUser(userManu);
		userManu.getPosts().add(post_15);
		post_15.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_15);
		post_15.getTags().add(wwwTag);
		wwwTag.getPosts().add(post_15);

		final Post<Resource> post_16 = new Post<>();
		post_16.setDescription("das ist nur ein beispiel.");
		post_16.setDate(this.date);
		post_16.setResource(publicationDemo3);
		publicationDemo3.getPosts().add(post_16);
		post_16.setUser(userManu);
		userManu.getPosts().add(post_16);
		post_16.getGroups().add(publicGroup);
		publicGroup.getPosts().add(post_16);
		post_16.getTags().add(wwwTag);
		wwwTag.getPosts().add(post_16);
	}

	@Override
	public String createUser(final User user) {
		this.dbUsers.put(user.getName(), user);
		return null;
	}

	@Override
	public User getAuthenticatedUser() {
		return loginUser;
	}

	@Override
	public String updateUser(final User user, final UserUpdateOperation operation) {
		final String username = user.getName();
		this.dbUsers.put(username, user);
		return username;
	}

	@Override
	public List<User> getUsers(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final SortKey sortKey, final UserRelation relation, final String search, final int start, final int end) {
		final List<User> users = new LinkedList<>();
		if (GroupingEntity.ALL.equals(grouping)) {
			users.addAll(this.dbUsers.values());
		}
		if (GroupingEntity.GROUP.equals(grouping) && groupingName != null && !groupingName.equals("")) {
			final Group group = this.dbGroups.get(groupingName);
			if (group != null) {
				for (final GroupMembership membership : group.getMemberships()) {
					users.add(membership.getUser());
				}
			}
		}
		return users;
	}

	@Override
	public Statistics getPostStatistics(Class<? extends Resource> resourceType, GroupingEntity grouping, String groupingName, List<String> tags, String hash, String search, Set<Filter> filters, SortKey sortKey, Date startDate, Date endDate, int start, int end) {
		return new Statistics(0);
	}

	@Override
	public List<ResourcePersonRelation> getResourceRelations(ResourcePersonRelationQuery query) {
		return new ArrayList<>(); //FIXME (AD) provide stubs
	}
}