/**
 * BibSonomy-Synchronization - Handles user synchronization between BibSonomy authorities
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.synchronization;

import static org.bibsonomy.util.ValidationUtils.present;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.common.enums.Role;
import org.bibsonomy.common.errors.DuplicatePostErrorMessage;
import org.bibsonomy.common.errors.ErrorMessage;
import org.bibsonomy.common.exceptions.DatabaseException;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.sync.SyncService;
import org.bibsonomy.model.sync.SynchronizationData;
import org.bibsonomy.model.sync.SynchronizationDirection;
import org.bibsonomy.model.sync.SynchronizationPost;
import org.bibsonomy.model.sync.SynchronizationStatus;
import org.bibsonomy.rest.client.RestLogicFactory;

/**
 * This client synchronizes PUMA with BibSonomy.
 * PUMA is the server, BibSonomy is the client.
 * 
 * @author wla
 */
public abstract class AbstractSynchronizationClient {
	private static final Log log = LogFactory.getLog(AbstractSynchronizationClient.class);

	private static final String SLASH = "/";
	
	/**
	 * @param api
	 * @return
	 */
	private static String buildApiUrl(URI api) {
		String apiUrl = api.toString();
		if (!apiUrl.endsWith(RestLogicFactory.API_SUBPATH)) {
			if (!apiUrl.endsWith(SLASH)) {
				apiUrl += SLASH;
			}
			return apiUrl + RestLogicFactory.API_SUBPATH;
		}
		return apiUrl;
	}

	
	/** own URI */
	protected URI ownUri;

	/**
	 * Looks up the credentials for the given syncServer. If no credentials
	 * could be found, <code>null</code> is returned.
	 *  
	 * @param clientLogic
	 * @param service
	 * @return syncService
	 */
	public SyncService getServerByURI(final LogicInterface clientLogic, final URI service) {
		final List<SyncService> syncServers = clientLogic.getSyncServiceSettings(clientLogic.getAuthenticatedUser().getName(), service, true);

		// return the first found service (there should be at most one with the given URI! 
		if (present(syncServers)) {
			return syncServers.get(0);
		}
		return null;
	}

	/**
	 * Creates an instance of the LogicInterface for the given syncService
	 * 
	 * @param syncServer
	 * @return the logic interface for the sync server
	 */
	protected LogicInterface getServerLogic(final SyncService syncServer) {
		final Properties serverUser = syncServer.getServerUser();
		log.info("get server logic for: " + syncServer.getService() + " (api: " + syncServer.getSecureAPI() +")");
		URI api = syncServer.getSecureAPI();
		
		if (!present(api)) {
			api = syncServer.getService();
		}
		
		final String apiUrl = buildApiUrl(api);
		final RestLogicFactory factory = new RestLogicFactory(apiUrl);
		return factory.getLogicAccess(serverUser.getProperty("userName"), serverUser.getProperty("apiKey"));
	}

	/**
	 * Used in a synchronization process, in case that server logic already created 
	 * 
	 * @param serverLogic
	 * @param serverUserName
	 * @param resourceType
	 * @return
	 */
	protected SynchronizationData getLastSyncData(final LogicInterface serverLogic, final String serverUserName, final Class<? extends Resource> resourceType) {
		return serverLogic.getLastSyncData(serverUserName, ownUri, resourceType);
	}
	
	/**
	 * Used in SettingsPageController, to show syncData, gets own logic
	 * @param syncService
	 * @param resourceType
	 * @return last sync data
	 */
	public SynchronizationData getLastSyncData(final SyncService syncService, final Class<? extends Resource> resourceType) {
		final LogicInterface serverLogic = getServerLogic(syncService);
		return getLastSyncData(serverLogic, serverLogic.getAuthenticatedUser().getName(), resourceType);
	}
	
	/**
	 * 
	 * @param syncService
	 * @param resourceType
	 * @param syncDate
	 */
	public void deleteSyncData(final SyncService syncService, final Class<? extends Resource> resourceType, final Date syncDate) {
		final LogicInterface serverLogic = getServerLogic(syncService);
		serverLogic.deleteSyncData(serverLogic.getAuthenticatedUser().getName(), ownUri, resourceType, syncDate);
	}
	
	
	/**
	 * Updates the status of the last synchronization to newStatus, but only if 
	 * its status is oldStatus.
	 * 
	 * @param serverLogic
	 * @param serverUserName
	 * @param resourceType
	 * @param oldStatus
	 * @param newStatus
	 * @param info
	 * @param isSecureSync
	 */
	protected void updateSyncData(final LogicInterface serverLogic, final String serverUserName, final Class<? extends Resource> resourceType, final SynchronizationStatus oldStatus, final SynchronizationStatus newStatus, final String info, boolean isSecureSync) {
		final SynchronizationData data = serverLogic.getLastSyncData(serverUserName, ownUri, resourceType);
		if (!present(data)) {
			/*
			 * sync data seems not to have been stored --> error!
			 */
			throw new RuntimeException("No sync data found for " + serverUserName + " on " + ownUri + " and resource type " + resourceType.getSimpleName());
		}
		/*
		 * check if oldStatus is correct
		 */
		if (oldStatus.equals(data.getStatus())) {
			final Date newSyncDate;
			if (isSecureSync) {
				newSyncDate = null;
			} else {
				newSyncDate = new Date();
			}
			serverLogic.updateSyncData(serverUserName, ownUri, resourceType, data.getLastSyncDate(), newStatus, info, newSyncDate);
		} else {
			throw new RuntimeException("no " + oldStatus + " synchronization found for " + serverUserName + " on " + ownUri + " to store result");
		}
	}

	protected String synchronize(final LogicInterface clientLogic, final LogicInterface serverLogic, final List<SynchronizationPost> syncPlan, final SynchronizationDirection direction) {
		/*
		 * add sync access to both users = allow users to modify the dates of
		 * posts
		 * FIXME: must be secured using crypto
		 */
		final User serverUser = serverLogic.getAuthenticatedUser();
//		final Role serverUserRole = serverUser.getRole();
		// FIXME: cleanup serverUser.setRole(Role.SYNC); 
		final User clientUser = clientLogic.getAuthenticatedUser();
		final Role clientUserRole = clientUser.getRole();
		clientUser.setRole(Role.SYNC);
		/*
		 * create target lists
		 */
		final List<Post<? extends Resource>> createOnClient = new ArrayList<>();
		final List<Post<? extends Resource>> createOnServer = new ArrayList<>();
		final List<Post<? extends Resource>> updateOnClient = new ArrayList<>();
		final List<Post<? extends Resource>> updateOnServer = new ArrayList<>();
		final List<String> deleteOnServer = new ArrayList<>();
		final List<String> deleteOnClient = new ArrayList<>();

		/*
		 * iterate over all posts and put each post into the target list
		 */
		for (final SynchronizationPost post: syncPlan) {
			final String postIntraHash = post.getIntraHash();
			
			final Post<? extends Resource> postToHandle;
			switch (post.getAction()) {
			case CREATE_SERVER:
				postToHandle = clientLogic.getPostDetails(postIntraHash, clientUser.getName());
				postToHandle.setUser(serverUser);
				createOnServer.add(postToHandle);
				break;
			case CREATE_CLIENT:
				postToHandle = post.getPost();
				postToHandle.setUser(clientUser);
				createOnClient.add(postToHandle);
				break;
			case DELETE_SERVER:
				deleteOnServer.add(postIntraHash);
				break;
			case DELETE_CLIENT:
				deleteOnClient.add(postIntraHash);
				break;
			case UPDATE_SERVER:
				postToHandle = clientLogic.getPostDetails(postIntraHash, clientUser.getName());
				postToHandle.setUser(serverUser);
				updateOnServer.add(postToHandle);
				break;
			case UPDATE_CLIENT:
				postToHandle = post.getPost();
				postToHandle.setUser(clientUser);
				updateOnClient.add(postToHandle);
				break;
			default:
				break;
			}
		}

		/*
		 *  Apply changes to both systems.
		 */
		final StringBuilder result = new StringBuilder();
		
		/*
		 * create posts on client 
		 */
		int duplicatesOnClient = 0;
		if (!createOnClient.isEmpty()) {
			assert !SynchronizationDirection.CLIENT_TO_SERVER.equals(direction);
			try {
				clientLogic.createPosts(createOnClient);
				result.append("created on client: " + createOnClient.size() + ", ");
			} catch (final DatabaseException e) {
				/*
				 *  This can happen if some duplicate posts exists.
				 *  FIXME: currently, we only check for duplicate errors
				 *  check other possibilities to throw Database Exception
				 */
				duplicatesOnClient = getDuplicateCount(e);
			}
		}

		/*
		 * create posts on server
		 */
		int duplicatesOnServer = 0;
		if (!createOnServer.isEmpty()) {
			assert !SynchronizationDirection.SERVER_TO_CLIENT.equals(direction);
			try {
				serverLogic.createPosts(createOnServer);
				result.append("created on server: " + createOnServer.size() + ", ");
			} catch (final DatabaseException e) {
				/*
				 *  This can happen if some duplicate posts exists.
				 *  FIXME: currently, we only check for duplicate errors
				 *  check other possibilities to throw Database Exception
				 */
				duplicatesOnServer = getDuplicateCount(e);
			}
		}

		/*
		 * update posts on client 
		 */
		if (!updateOnClient.isEmpty()) {
			assert !SynchronizationDirection.CLIENT_TO_SERVER.equals(direction); 
			clientLogic.updatePosts(updateOnClient, PostUpdateOperation.UPDATE_ALL);
			result.append("updated on client: " + updateOnClient.size() + ", ");
		}

		/*
		 * update posts on server
		 */
		if (!updateOnServer.isEmpty()) {
			assert !SynchronizationDirection.SERVER_TO_CLIENT.equals(direction);
			serverLogic.updatePosts(updateOnServer, PostUpdateOperation.UPDATE_ALL);
			result.append("updated on server: " + updateOnServer.size() + ", ");
		}

		/*
		 * delete posts on client
		 */
		if (!deleteOnClient.isEmpty()) {
			assert !SynchronizationDirection.CLIENT_TO_SERVER.equals(direction); 
			clientLogic.deletePosts(clientUser.getName(), deleteOnClient);
			result.append("deleted on client: " + deleteOnClient.size() + ", ");
		}

		/*
		 * delete posts no server
		 */
		if (!deleteOnServer.isEmpty()) {
			assert !SynchronizationDirection.SERVER_TO_CLIENT.equals(direction);
			serverLogic.deletePosts(serverUser.getName(), deleteOnServer);
			result.append("deleted on server: " + deleteOnServer.size());
		}

		/*
		 * generate result string
		 */
		if (duplicatesOnClient > 0) {
			result.insert(0, duplicatesOnClient + "duplicates on client detected, ");
		}
		if (duplicatesOnServer > 0) {
			result.insert(0, duplicatesOnServer + "duplicates on server detected, ");
		}
	
		final int length = result.length();
		if (length == 0) {
			result.append("no changes");
		} else if (result.lastIndexOf(", ") == length - 2) {
			result.delete(length - 2, length);
		}

//		FIXME: cleanup
//		serverUser.setRole(serverUserRole);
		clientUser.setRole(clientUserRole);
		return result.toString();
	}

	/**
	 * Counts duplicate error messages 
	 * 
	 * @param exception
	 * @return
	 */
	private static int getDuplicateCount(final DatabaseException exception) {
		int duplicatesOnClient = 0;
		final Collection<List<ErrorMessage>> values = exception.getErrorMessages().values();
		for (final List<ErrorMessage> errorMessages : values) {
			for (final ErrorMessage em: errorMessages) {
				if (em instanceof DuplicatePostErrorMessage) {
					duplicatesOnClient++;
				}
			}
		}
		return duplicatesOnClient;
	}
	
	/**
	 * @param ownUri the ownUri to set
	 */
	public void setOwnUri(final URI ownUri) {
		this.ownUri = ownUri;
	}

	/**
	 * @return the ownUri
	 */
	public URI getOwnUri() {
		return ownUri;
	}
}
