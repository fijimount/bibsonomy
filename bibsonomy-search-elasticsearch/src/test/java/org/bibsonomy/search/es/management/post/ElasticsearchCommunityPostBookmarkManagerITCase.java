package org.bibsonomy.search.es.management.post;

import static org.bibsonomy.search.es.management.post.ElasticsearchCommunityPostPublicationManagerITCase.buildQuery;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.bibsonomy.model.GoldStandardBookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.ResultList;
import org.bibsonomy.model.User;
import org.bibsonomy.search.es.EsSpringContextWrapper;
import org.bibsonomy.search.es.search.post.ElasticsearchPostSearch;
import org.bibsonomy.search.es.testutil.AbstractCommunityPostSearchTest;
import org.junit.Test;

/**
 * tests for the {@link ElasticsearchCommunityPostManager} of the bookmark entries
 *
 * @author dzo
 */
public class ElasticsearchCommunityPostBookmarkManagerITCase extends AbstractCommunityPostSearchTest<GoldStandardBookmark> {

	private static final User anonymUser = new User();

	private static final ElasticsearchPostSearch<GoldStandardBookmark> COMMUNITY_BOOKMARK_SEARCH = EsSpringContextWrapper.getContext().getBean("elasticsearchCommunityBookmarkSearch", ElasticsearchPostSearch.class);

	@Test
	public void testGenerate() {
		// the indices are generated by the abstract class

		// check if gold standard publications are indexed
		final ResultList<Post<GoldStandardBookmark>> communityResults = COMMUNITY_BOOKMARK_SEARCH.getPosts(anonymUser, buildQuery("Universität Kassel"));
		assertThat(communityResults.size(), is(1));

		// check if also normal posts are indexed
		final ResultList<Post<GoldStandardBookmark>> normalPosts = COMMUNITY_BOOKMARK_SEARCH.getPosts(anonymUser, buildQuery("KDE"));
		assertThat(normalPosts.size(), is(1));
		assertThat(normalPosts.getTotalCount(), is(1));

		final Post<GoldStandardBookmark> firstResult = normalPosts.get(0);
		assertThat(firstResult.getResource().getTitle(), is("kde"));
	}

	@Override
	protected ElasticsearchCommunityPostManager<GoldStandardBookmark> getManager() {
		return EsSpringContextWrapper.getContext().getBean("elasticsearchCommunityBookmarkManager", ElasticsearchCommunityPostManager.class);
	}
}