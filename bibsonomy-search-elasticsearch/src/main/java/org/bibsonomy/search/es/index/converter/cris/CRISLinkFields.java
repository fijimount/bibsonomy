package org.bibsonomy.search.es.index.converter.cris;

/**
 * fields for {@link org.bibsonomy.model.cris.CRISLink} in the index
 *
 * @author dzo
 */
public interface CRISLinkFields {

	/** the start date of the cris link */
	String START_DATE = "start_date";
	/** the end date of the cris link */
	String END_DATE = "end_date";
}
