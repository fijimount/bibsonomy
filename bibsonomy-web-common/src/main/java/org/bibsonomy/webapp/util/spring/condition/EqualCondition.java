/**
 * BibSonomy-Web-Common - Common things for web
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.util.spring.condition;

/**
 * a condition that checks for value is equal
 *
 * @author dzo
 * @param <T> 
 */
public class EqualCondition<T> implements Condition {
	
	private T value;
	private T expected;
	
	/**
	 * @param value
	 * @param expected
	 */
	public EqualCondition(T value, T expected) {
		super();
		this.value = value;
		this.expected = expected;
	}
	
	/* (non-Javadoc)
	 * @see org.bibsonomy.webapp.util.spring.condition.Condition#eval()
	 */
	@Override
	public boolean eval() {
		return value != null && value.equals(expected);
	}
}
