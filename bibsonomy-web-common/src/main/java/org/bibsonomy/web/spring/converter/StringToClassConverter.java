/**
 * BibSonomy-Web-Common - Common things for web
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.web.spring.converter;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bibsonomy.model.Resource;
import org.bibsonomy.model.factories.ResourceFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.ClassUtils;

/**
 * converts strings to class objects with special handling for our resource classes
 *
 * @author dzo
 */
public class StringToClassConverter implements Converter<String, Class<?>> {

	private ClassLoader loader = ClassUtils.getDefaultClassLoader();

	private final List<Package> modelPackages = getModelPackages();
	
	@Override
	public Class<?> convert(String text) {
		if (present(text)) {
			text = text.trim();
			final Class<? extends Resource> clazz = ResourceFactory.getResourceClass(text);
			
			if (present(clazz)) {
				return clazz;
			}

			/*
			 * check for classes in our model package and all sub packages
			 */

			final String normedClassName = text.substring(0, 1).toUpperCase() + text.substring(1);
			for (final Package aPackage : this.modelPackages) {
				final String name = aPackage.getName();

				try {
					final Class<?> modelClass = ClassUtils.resolveClassName(name + "." + normedClassName, this.loader);
					if (present(modelClass)) {
						return modelClass;
					}
				} catch (IllegalArgumentException e) {
					// ignore
				}
			}

			return ClassUtils.resolveClassName(text, this.loader);
		}
		return null;
	}

	private static List<Package> getModelPackages() {
		final Package modelPackage = Resource.class.getPackage();
		return Arrays.stream(Package.getPackages()).filter(aPackage -> aPackage.getName().startsWith(modelPackage.getName())).collect(Collectors.toList());
	}

	/**
	 * @param loader the loader to set
	 */
	public void setLoader(final ClassLoader loader) {
		this.loader = loader;
	}
}
