/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.nowpublishers;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;

import org.bibsonomy.scraper.junit.RemoteTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Scraper URL tests #285 #286

 * @author rja
 */
@Category(RemoteTest.class)
public class NowPublishersScraperTest {

	/**
	 * starts URL test with id url_285
	 */
	@Test
	public void urlTest1Run(){
		final String url = "http://www.nowpublishers.com/article/Details/INR-043";
		final String resultFile = "NowPublishersScraperUnitURLTest1.bib";
		assertScraperResult(url, null, NowPublishersScraper.class, resultFile);
	}
	
	/**
	 * starts URL test with id url_286
	 */
	@Test
	public void urlTest2Run(){
		final String url = "http://www.nowpublishers.com/article/Details/INR-012";
		final String resultFile = "NowPublishersScraperUnitURLTest2.bib";
		assertScraperResult(url, null, NowPublishersScraper.class, resultFile);
	}
}
