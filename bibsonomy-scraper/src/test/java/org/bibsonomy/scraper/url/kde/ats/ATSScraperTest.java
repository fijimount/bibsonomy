/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.ats;

import static org.bibsonomy.scraper.junit.RemoteTestAssert.assertScraperResult;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.bibsonomy.scraper.ScrapingContext;
import org.bibsonomy.scraper.junit.RemoteTest;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * @author clemens
 */
@Category(RemoteTest.class)
public class ATSScraperTest {

	/**
	 * starts URL test with id url_212
	 */
	@Test
	public void urlTest1Run() {
		final String url = "http://www.atsjournals.org/doi/abs/10.1513/pats.201101-004MW";
		final String resultFile = "ATSScraperUnitURLTest1.bib";
		assertScraperResult(url, null, ATSScraper.class, resultFile);
	}
	/**
	 * starts URL test with id url_213
	 */
	@Test
	public void urlTest2Run() {
		final String url = "http://www.atsjournals.org/doi/abs/10.1164/rccm.201106-1094OC";
		final String resultFile = "ATSScraperUnitURLTest2.bib";
		assertScraperResult(url, null, ATSScraper.class, resultFile);
	}
	/**
	 * starts URL test with id url_214
	 */
	@Test
	public void urlTest3Run() {
		final String url = "http://www.atsjournals.org/doi/abs/10.1165/rcmb.2011-0134OC";
		final String resultFile = "ATSScraperUnitURLTest3.bib";
		assertScraperResult(url, null, ATSScraper.class, resultFile);
	}
	
	/*
	 * ignoring this for now, low priority
	 */
	@Ignore
	@Test
	public void testCitedby() throws Exception {
		final ScrapingContext sc = new ScrapingContext(new URL("http://www.atsjournals.org/doi/abs/10.1513/pats.201101-004MW#.VK_cBnvveUk"));
		ATSScraper as = new ATSScraper();
		assertTrue(as.scrape(sc));
		assertTrue(as.scrapeCitedby(sc));

		final String cby = sc.getCitedBy();
		assertNotNull(cby);
		assertTrue(cby.length() > 100);
		System.out.println("test");
		assertEquals("<div class=\"sectionInfo\"><div class=\"sectionHeading\">Cited by</div></div><div class=\"cited".trim(), cby.substring(0, 90).trim());
		System.out.println("test");
		assertTrue(cby.contains(" Markers of Vascular Perturbation Correlate"));
	}
}
