/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.bibsonomy.common.Pair;
import org.bibsonomy.scraper.exceptions.ScrapingException;
import org.bibsonomy.util.ValidationUtils;

/**
 * Scrapers of this type can decide using only the URL, if they
 * support the given {@link ScrapingContext} or not. 
 * 
 * @author rja
 */
public abstract class AbstractUrlScraper implements UrlScraper {

	/**
	 * If a scraper does not need to check the path (or host) of a 
	 * the URL, it should return this value as pattern. 
	 */
	protected static final Pattern EMPTY_PATTERN = null;

	
	/* (non-Javadoc)
	 * @see org.bibsonomy.scraper.UrlScraper#getUrlPatterns()
	 */
	@Override
	public abstract List<Pair<Pattern,Pattern>> getUrlPatterns();

	/* (non-Javadoc)
	 * @see org.bibsonomy.scraper.UrlScraper#supportsUrl(java.net.URL)
	 */
	@Override
	public boolean supportsUrl(final URL url) {
		if (ValidationUtils.present(url)) {
			final List<Pair<Pattern, Pattern>> urlPatterns = this.getUrlPatterns();

			/*
			 * possible matching combinations:
			 * first = true && second = true
			 * first = true && second = null
			 * first = null && second = true
			 */
			for (final Pair<Pattern, Pattern> tuple: urlPatterns){
				final Pattern hostPattern = tuple.getFirst();
				final boolean hostMatch = (hostPattern == EMPTY_PATTERN) || hostPattern.matcher(url.getHost()).find();
				
				final Pattern pathPattern = tuple.getSecond();
				final boolean pathMatch = (pathPattern == EMPTY_PATTERN) || pathPattern.matcher(url.getPath()).find();
				
				if (hostMatch && pathMatch) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Builds a href to the URL with the given anchor text.
	 *  
	 * @param url
	 * @param text
	 * @return a href html element
	 */
	public static String href(final String url, final String text) {
		return "<a href=\"" + url + "\">" + text + "</a>";
	}

	/**
	 * Scrapes the given context.
	 * 
	 * @see org.bibsonomy.scraper.Scraper#scrape(org.bibsonomy.scraper.ScrapingContext)
	 */
	@Override
	public boolean scrape(final ScrapingContext sc) throws ScrapingException {
		if (ValidationUtils.present(sc) && this.supportsScrapingContext(sc)) {
			return this.scrapeInternal(sc);
		}
		return false;
	}
	
	
	/** This method is called by {@link #scrape(ScrapingContext)}, when the URL is supported
	 * by the scraper.
	 *  
	 * @param scrapingContext
	 * @return <code>true</code> if the scraping was successful.
	 * @throws ScrapingException
	 */
	protected abstract boolean scrapeInternal(final ScrapingContext scrapingContext) throws ScrapingException;

	@Override
	public Collection<Scraper> getScraper() {
		return Collections.singletonList(this);
	}
	
	@Override
	public boolean supportsScrapingContext(final ScrapingContext scrapingContext) {
		return this.supportsUrl(scrapingContext.getUrl());
	}
}
