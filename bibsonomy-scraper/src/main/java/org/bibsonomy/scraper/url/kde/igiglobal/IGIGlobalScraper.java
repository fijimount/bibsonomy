/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.igiglobal;

import static org.bibsonomy.util.ValidationUtils.present;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.bibsonomy.common.Pair;
import org.bibsonomy.scraper.AbstractUrlScraper;
import org.bibsonomy.scraper.ScrapingContext;
import org.bibsonomy.scraper.converter.RisToBibtexConverter;
import org.bibsonomy.scraper.exceptions.InternalFailureException;
import org.bibsonomy.scraper.exceptions.ScrapingException;
import org.bibsonomy.scraper.exceptions.ScrapingFailureException;
import org.bibsonomy.util.WebUtils;

/**
 * scraper igi global
 *
 * @author Haile
 */
public class IGIGlobalScraper extends AbstractUrlScraper {

	private static final String SITE_NAME = "IGI Global";
	private static final String SITE_URL = "http://www.igi-global.com";
	private static final String INFO = "This scraper parses a publication page from the " + href(SITE_URL, SITE_NAME);

	private static final Pattern EVENTVALIDATION = Pattern.compile("<input type=\"hidden\" name=\"__EVENTVALIDATION\" id=\"__EVENTVALIDATION\" value=\"(.*?)\" />");
	private static final Pattern EVENTTARGET = Pattern.compile("<input type=\"hidden\" name=\"__EVENTTARGET\" id=\"__EVENTTARGET\" value=\"(.*?)\" />");
	private static final Pattern EVENTARGUMENT = Pattern.compile("<input type=\"hidden\" name=\"__EVENTARGUMENT\" id=\"__EVENTARGUMENT\" value=\"(.*?)\" />");
	private static final Pattern VIEWSTATE = Pattern.compile("<input type=\"hidden\" name=\"__VIEWSTATE\" id=\"__VIEWSTATE\" value=\"(.*?)\" />");

	private static final List<Pair<Pattern, Pattern>> URL_PATTERNS = Collections.singletonList(new Pair<>(Pattern.compile(".*" + "igi-global.com"), AbstractUrlScraper.EMPTY_PATTERN));

	private static final RisToBibtexConverter RIS2BIB = new RisToBibtexConverter();

	@Override
	protected boolean scrapeInternal(final ScrapingContext scrapingContext) throws ScrapingException {
		scrapingContext.setScraper(this);
		try {
			final String inRIS = getCitationInRIS(scrapingContext.getUrl());
			final String bibtex = RIS2BIB.toBibtex(inRIS);

			if (present(bibtex)) {
				scrapingContext.setBibtexResult(bibtex);
				return true;
			}

			throw new ScrapingFailureException("getting bibtex failed");
		} catch (final Exception e) {
			throw new InternalFailureException(e);
		}
	}
	private static String getCitationInRIS(final URL url) throws Exception {
		final String html = WebUtils.getContentAsString(url);

		final Matcher m_eventvalidation = EVENTVALIDATION.matcher(html);
		String eventvalidation = "";
		if(m_eventvalidation.find())
			eventvalidation = m_eventvalidation.group(1);

		final Matcher m_eventtarget = EVENTTARGET.matcher(html);
		String eventtarget = "";
		if(m_eventtarget.find())
			eventtarget = m_eventtarget.group(1);

		final Matcher m_eventargument = EVENTARGUMENT.matcher(html);
		String eventargument = "";
		if (m_eventargument.find())
			eventargument = m_eventargument.group(1);

		final Matcher m_viewstate = VIEWSTATE.matcher(html);
		String viewstate = "";
		if(m_viewstate.find())
			viewstate = m_viewstate.group(1);

		final List<NameValuePair> postData = new ArrayList<>();
		
		postData.add(new BasicNameValuePair("ctl00$ctl00$ucBookstoreSearchTop$txtSearch", "Search title, author, ISBN..."));
		postData.add(new BasicNameValuePair("ctl00$ctl00$cphMain$cphFeatured$ucCiteContent$lnkSubmitToEndNote.x", "30"));
		postData.add(new BasicNameValuePair("ctl00$ctl00$cphMain$cphFeatured$ucCiteContent$lnkSubmitToEndNote.y", "7"));
		postData.add(new BasicNameValuePair("ctl00$ctl00$cphMain$cphSidebarRightTop$ucInfoSciOnDemandSidebar$txtSearchPhrase", "Full text search term(s)"));
		postData.add(new BasicNameValuePair("__EVENTVALIDATION", eventvalidation));
		postData.add(new BasicNameValuePair("__EVENTTARGET", eventtarget));
		postData.add(new BasicNameValuePair("__EVENTARGUMENT", eventargument));
		postData.add(new BasicNameValuePair("__VIEWSTATE", viewstate));

		return WebUtils.getContentAsString(url.toExternalForm(), null, postData, null);
	}

	@Override
	public String getSupportedSiteName() {
		return SITE_NAME;
	}

	@Override
	public String getSupportedSiteURL() {
		return SITE_URL;
	}

	@Override
	public String getInfo() {
		return INFO;
	}

	@Override
	public List<Pair<Pattern, Pattern>> getUrlPatterns() {
		return URL_PATTERNS;
	}
}