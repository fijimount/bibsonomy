/**
 * BibSonomy-Scraper - Web page scrapers returning BibTeX for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.scraper.url.kde.metapress;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.HttpClient;
import org.bibsonomy.common.Pair;
import org.bibsonomy.scraper.AbstractUrlScraper;
import org.bibsonomy.scraper.ScrapingContext;
import org.bibsonomy.scraper.converter.RisToBibtexConverter;
import org.bibsonomy.scraper.exceptions.InternalFailureException;
import org.bibsonomy.scraper.exceptions.PageNotSupportedException;
import org.bibsonomy.scraper.exceptions.ScrapingException;
import org.bibsonomy.scraper.exceptions.ScrapingFailureException;
import org.bibsonomy.util.ValidationUtils;
import org.bibsonomy.util.WebUtils;

/**
 * Scraper for RIS citations from Metapress.com
 * @author tst
 *
 * 2018-03-21, rja: site does no longer host content?!.
 *
 */
@Deprecated
public class MetapressScraper extends AbstractUrlScraper {

	private static final String SITE_URL = "http://metapress.com/";
	private static final String SITE_NAME = "Meta Press";
	private static final String INFO = "Scraper for publications from " + href(SITE_URL, SITE_NAME)+".";

	private static final String HOST = "metapress.com";

	private static final String PREFIX_DOWNLOAD_URL = "http://www.metapress.com/export.mpx?code=";

	private static final String SUFFIX_DOWNLOAD_URL = "&mode=ris";

	private static final Pattern patternHref = Pattern.compile("content/([^/]*)/");

	private static final List<Pair<Pattern, Pattern>> patterns = Collections.singletonList(new Pair<Pattern, Pattern>(Pattern.compile(".*" + HOST), AbstractUrlScraper.EMPTY_PATTERN));
	private static final RisToBibtexConverter RIS2BIB = new RisToBibtexConverter();

	@Override
	public String getInfo() {
		return INFO;
	}
	
	@Override
	protected boolean scrapeInternal(ScrapingContext sc)throws ScrapingException {
		sc.setScraper(this);

		final Matcher matcherHref = patternHref.matcher(sc.getUrl().toString());
		if (matcherHref.find()) {
			
			try {
				final String url = PREFIX_DOWNLOAD_URL + matcherHref.group(1) + SUFFIX_DOWNLOAD_URL;

				/*
				 * some kind of exclusive cookie management so rather get an own client
				 */
				final HttpClient client = WebUtils.getHttpClient();

				/*
				 * we just have to get it twice, maybe they don't like robots.
				 */
				final String ris = WebUtils.getContentAsString(client, url, null, null, url);
				
				if (ValidationUtils.present(ris)) {
					String bibtex = RIS2BIB.toBibtex(ris);

					//replace /r with /n
					bibtex = bibtex.replace("\r", "\n");

					if (bibtex != null) {
						sc.setBibtexResult(bibtex);
						return true;
					}
					throw new ScrapingFailureException("convert to bibtex failed");
				}
				throw new ScrapingFailureException("ris download failed");
			} catch (final IOException ex) {
				throw new InternalFailureException(ex);
			}
		}
		throw new PageNotSupportedException("no RIS download available");
	}
	
	@Override
	public List<Pair<Pattern, Pattern>> getUrlPatterns() {
		return patterns;
	}

	@Override
	public String getSupportedSiteName() {
		return SITE_NAME;
	}

	@Override
	public String getSupportedSiteURL() {
		return SITE_URL;
	}
}
