/**
 * BibSonomy-Rest-Common - Common things for the REST-client and server.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.renderer;

import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.bibsonomy.common.exceptions.InternServerException;
import org.bibsonomy.model.*;
import org.bibsonomy.model.cris.CRISLink;
import org.bibsonomy.model.cris.Project;
import org.bibsonomy.model.sync.SynchronizationData;
import org.bibsonomy.model.sync.SynchronizationPost;
import org.bibsonomy.model.util.data.DataAccessor;
import org.bibsonomy.rest.ViewModel;
import org.bibsonomy.rest.exceptions.BadRequestOrResponseException;

/**
 * This interface should be implemented by classes that intend to add additional
 * rendering capabilities to the system.<br/>
 * 
 * Note that it also includes functionality to read the data, that has been
 * rendered with it.
 * 
 * @author Manuel Bork <manuel.bork@uni-kassel.de>
 */
public interface Renderer {

	/**
	 * Serializes a {@link List} of {@link Post}s.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param posts
	 *            a {@link List} of {@link Post} objects.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 * @throws InternServerException
	 */
	public void serializePosts(Writer writer, List<? extends Post<? extends Resource>> posts, ViewModel viewModel) throws InternServerException;

	/**
	 * Serializes one {@link Post}.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param post
	 *            one {@link Post} object.
	 * @param model
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializePost(Writer writer, Post<? extends Resource> post, ViewModel model);

	/**
	 * serializes one {@link Document}
	 * @param writer
	 * @param document
	 */
	public void serializeDocument(Writer writer, Document document);
	
	/**
	 * Serializes a {@link List} of {@link User}s.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param users
	 *            a {@link List} of {@link User} objects.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeUsers(Writer writer, List<User> users, ViewModel viewModel);

	/**
	 * Serializes one {@link User}.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param user
	 *            one {@link User} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeUser(Writer writer, User user, ViewModel viewModel);

	/**
	 * Serializes one {@link Person}.
	 *
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param person
	 *            one {@link Person} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	void serializePerson(Writer writer, Person person, ViewModel viewModel);

	/**
	 * Serializes a list of persons
	 * @param writer
	 * @param persons
	 * @param viewModel
	 */
	void serializePersons(Writer writer, List<Person> persons, ViewModel viewModel);

	/**
	 * Serializes one {@link Person}.
	 *  @param writer
	 *            a {@link Writer} to use.
	 * @param match
	 *            one {@link PersonMatch} object.
	 * @param viewModel
 *            the {@link ViewModel} encapsulates additional information,
	 */
	void serializePersonMatch(Writer writer, PersonMatch match, ViewModel viewModel);

	/**
	 * Serializes one {@link Project}.
	 *
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param project
	 *            one {@link Project} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeProject(Writer writer, Project project, ViewModel viewModel);


	/**
	 * Serializes a list of {@link Project}
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param projects
	 *            list of {@link Project} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeProjects(Writer writer, List<Project> projects, ViewModel viewModel);

	/**
	 * Serializes one {@link Person}.
	 *
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param crisLink
	 *            one {@link CRISLink} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeCRISLink(Writer writer, CRISLink crisLink, ViewModel viewModel);

	/**
	 * Serializes one {@link ResourcePersonRelation}.
	 *
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param resourcePersonRelation
	 *            one {@link ResourcePersonRelation} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeResourcePersonRelation(Writer writer, ResourcePersonRelation resourcePersonRelation, ViewModel viewModel);

	/**
	 * Serializes a list of {@link ResourcePersonRelation}.
	 * @param writer
	 * @param relations
	 */
	public void serializeResourcePersonRelations(Writer writer, List<ResourcePersonRelation> relations);

	/**
	 * Serializes a personid
	 *
	 * @param writer   the {@link Writer} to use.
	 * @param personId the personId to send
	 */
	public void serializePersonId(Writer writer, String personId);

	/**
	 * Serializes a projectid
	 *
	 * @param writer   the {@link Writer} to use.
	 * @param projectId the personId to send
	 */
	public void serializeProjectId(Writer writer, String projectId);

	/**
	 * Serializes a cris link id
	 *
	 * @param writer   the {@link Writer} to use.
	 * @param linkId the personId to send
	 */
	public void serializeCRISLinkId(Writer writer, String linkId);

	/**
	 * Serializes a {@link List} of {@link Tag}s.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param tags
	 *            a {@link List} of {@link Tag} objects.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeTags(Writer writer, List<Tag> tags, ViewModel viewModel);

	/**
	 * Serializes a {@link Tag}'s details, including {@link List} of subtags,
	 * {@link List} of supertags and {@link List} of correlated tags
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param tag
	 *            one {@link Tag} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeTag(Writer writer, Tag tag, ViewModel viewModel);

	/**
	 * Serializes a list of {@link Group}s.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param groups
	 *            a {@link List} of {@link Group} objects.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeGroups(Writer writer, List<Group> groups, ViewModel viewModel);

	/**
	 * Serializes one {@link Group}.
	 * 
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param group
	 *            one {@link Group} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeGroup(Writer writer, Group group, ViewModel viewModel);

	/**
	 * Serializes a collection of {@link GroupMembership}.
	 *
	 * @param writer
	 *            a {@link Writer} to use.
	 * @param groupMemberships
	 *            a collection of {@link GroupMembership} object.
	 * @param viewModel
	 *            the {@link ViewModel} encapsulates additional information,
	 */
	public void serializeGroupMemberships(Writer writer, Collection<GroupMembership> groupMemberships, ViewModel viewModel);

	/**
	 * Serializes an errormessage.
	 * 
	 * @param writer
	 *            the {@link Writer} to use.
	 * @param errorMessage
	 *            the error message to send.
	 */
	public void serializeError(Writer writer, String errorMessage);

	/**
	 * Serializes a plain OK.
	 * 
	 * @param writer
	 *            the {@link Writer} to use.
	 */
	public void serializeOK(Writer writer);

	/**
	 * Serializes a failure status.
	 * 
	 * @param writer
	 *            the {@link Writer} to use.
	 */
	public void serializeFail(Writer writer);

	/**
	 * Serializes a resourcehash.
	 * 
	 * @param writer
	 *            the {@link Writer} to use.
	 * @param hash
	 *            the hash to send.
	 */
	public void serializeResourceHash(Writer writer, String hash);

	/**
	 * Serializes a userid.
	 * 
	 * @param writer
	 *            the {@link Writer} to use.
	 * @param userId
	 *            the userId to send.
	 */
	public void serializeUserId(Writer writer, String userId);

	/**
	 * Serializes an uri of an resource
	 * 
	 * @param writer
	 * @param uri
	 */
	public void serializeURI(Writer writer, String uri);

	/**
	 * Serializes a groupid.
	 * 
	 * @param writer
	 *            the {@link Writer} to use.
	 * @param groupId
	 *            the groupId to send.
	 */
	public void serializeGroupId(Writer writer, String groupId);

	/**
	 * Serializes a list of synchronization posts
	 * @param writer
	 * @param posts
	 */
	public void serializeSynchronizationPosts(Writer writer, List<? extends SynchronizationPost> posts);

	/**
	 * Serializes Map of synchronization data
	 * @param writer
	 * @param syncData
	 */
	public void serializeSynchronizationData(Writer writer, SynchronizationData syncData);

	/**
	 * Serializes reference
	 * @param writer
	 * @param referenceHash
	 */
	public void serializeReference(Writer writer, String referenceHash);

	/**
	 * @param reader
	 * @return
	 * @throws BadRequestOrResponseException
	 */
	public SynchronizationData parseSynchronizationData(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads list of synchronization posts
	 * @param reader
	 * @return
	 * @throws BadRequestOrResponseException
	 */
	public List<SynchronizationPost> parseSynchronizationPostList(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads an errormessage from a {@link Reader}
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return an error string
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parseError(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads an resource hash from a {@link Reader}
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return an resource hash
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parseResourceHash(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads an user id from a {@link Reader}
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return an resource hash
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parseUserId(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads an user id from a {@link Reader}
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a resource hash
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parsePersonId(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a project id from a {@link Reader}
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a resource hash
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parseProjectId(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a group id from a {@link Reader}
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return an resource hash
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parseGroupId(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads the status from a {@link Reader}
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return the status ("ok" or "fail")
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public String parseStat(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a List of {@link User}s from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a {@link List} of {@link User} objects.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public List<User> parseUserList(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link User} from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link User} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public User parseUser(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link Person} from a {@link Reader}.
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link Person} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public Person parsePerson(Reader reader) throws BadRequestOrResponseException;

	/**
	 * reads a list of {@link Person}s from a {@link Reader}
	 * @param reader
	 * @return
	 */
	List<Person> parsePersons(Reader reader);

	/**
	 * Reads one {@link Project} from a {@link Reader}.
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link Project} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public Project parseProject(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a list of {@link Project} from a {@link Reader}.
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a list of {@link Project} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public List<Project> parseProjects(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link Person} from a {@link Reader}.
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link CRISLink} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public CRISLink parseCRISLink(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link ResourcePersonRelation} from a {@link Reader}.
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link ResourcePersonRelation} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public ResourcePersonRelation parseResourcePersonRelation(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link ResourcePersonRelation} from a {@link Reader}.
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link CRISLink} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	//public CRISLink parseCRISLink(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a list of {@link ResourcePersonRelation}s from a {@link Reader}.
	 * @param reader  the {@link Reader} to use.
	 * @return a list of {@link ResourcePersonRelation}s
	 */
	List<ResourcePersonRelation> parseResourcePersonRelations(Reader reader);

	/**
	 * Reads a List of {@link Post}s from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @param uploadedFileAcessor 
	 * @return a {@link List} of {@link Post} objects.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public List<Post<? extends Resource>> parsePostList(Reader reader, DataAccessor uploadedFileAcessor) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link Post} from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @param uploadedFileAccessor provides access to referenced attachments in the read data
	 * @return one {@link Post} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public Post<? extends Resource> parsePost(Reader reader, DataAccessor uploadedFileAccessor) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link Document} from a {@link Reader}
	 * 
	 * @param reader
	 * 			  the {@link Reader} to use.
	 * @param uploadFileAccessor provides access to referenced attachments in the read data
	 * @return one {@link Document} object
	 * @throws BadRequestOrResponseException
	 * 				if the document within the reader is errorenous.
	 */
	public Document parseDocument(Reader reader, DataAccessor uploadFileAccessor) throws BadRequestOrResponseException;
	
	/**
	 * Reads one standard {@link Post} from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link Post} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public Post<? extends Resource> parseCommunityPost(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a List of {@link Group}s from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a {@link List} of {@link Group} objects.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public List<Group> parseGroupList(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link Group} from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link Group} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public Group parseGroup(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a collection of {@link GroupMembership} from a {@link Reader}.
	 *
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a collections of {@link GroupMembership} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public Collection<GroupMembership> parseGroupMemberships(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads a List of {@link Tag}s from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return a {@link List} of {@link Tag} objects.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public List<Tag> parseTagList(Reader reader) throws BadRequestOrResponseException;

	/**
	 * Reads one {@link Tag} from a {@link Reader}.
	 * 
	 * @param reader
	 *            the {@link Reader} to use.
	 * @return one {@link Post} object.
	 * @throws BadRequestOrResponseException
	 *             if the document within the reader is errorenous.
	 */
	public Tag parseTag(Reader reader) throws BadRequestOrResponseException;

	/**
	 * @param reader the reader to use
	 * @return a list of references (their interhashes) from the reader
	 */
	public Set<String> parseReferences(Reader reader);
}