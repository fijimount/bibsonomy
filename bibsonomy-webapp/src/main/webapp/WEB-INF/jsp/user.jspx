<?xml version="1.0" ?>
<jsp:root version="2.0"
		  xmlns="http://www.w3.org/1999/xhtml"
		  xmlns:jsp="http://java.sun.com/JSP/Page"
		  xmlns:c="http://java.sun.com/jsp/jstl/core"
		  xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
		  xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
		  xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
		  xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
		  xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
		  xmlns:users="urn:jsptagdir:/WEB-INF/tags/users"
		  xmlns:fn="http://java.sun.com/jsp/jstl/functions"
		  xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
		  xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
		  xmlns:group="urn:jsptagdir:/WEB-INF/tags/resources/group"
		  xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
		  xmlns:adminUser="urn:jsptagdir:/WEB-INF/tags/admin/users"
		  xmlns:nav="urn:jsptagdir:/WEB-INF/tags/nav">

	<c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>
	
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />
	<layout:resourceLayout
			pageTitle="${command.pageTitle}"
			command="${command}"
			requPath="${requPath}"
			requQueryString="${requQueryString}"
			activeTab="${(command.requestedUser eq command.context.loginUser.name) ? 'my' : ''}"
			showSortInfoHeadline="${true}"
			sortPage="${command.sortPage}">
		<jsp:attribute name="heading">
			<c:choose>
				<c:when test="${not empty command.scope and command.scope.name() ne 'LOCAL' and command.scope.name() ne 'SEARCHINDEX'}">
					<c:set var="pathMessageKey" value="navi.federatedUser"/>
					<c:set var="userUrl" value="${relativeUrlGenerator.getUserUrlByUserName(command.requestedUser)}?scope=${command.scope.name()}"/>
				</c:when>
				<c:otherwise>
					<c:set var="pathMessageKey" value="navi.user"/>
					<c:set var="userUrl" value="${relativeUrlGenerator.getUserUrlByUserName(command.requestedUser)}"/>
				</c:otherwise>
			</c:choose>
			<parts:search scope="${command.scope}" pathMessageKey="${pathMessageKey}" formAction="${userUrl}" formInputName="tag"  requestedUser="${command.requestedUser}">
				<jsp:attribute name="pathPart">
					<a href="${userUrl}"><user:username username="${command.requestedUser}" scope="${command.scope.name()}"/></a> 
				</jsp:attribute>
			</parts:search>
		</jsp:attribute>

		<jsp:attribute name="breadcrumbs">
			<nav:breadcrumbs>
				<jsp:attribute name="crumbs">
					<nav:usercrumb requestedUser="${command.requestedUser}" loginUser="${command.context.loginUser}" active="${true}" scope="${command.scope.name()}"/>
				</jsp:attribute>
			</nav:breadcrumbs>
		</jsp:attribute>
		
		
		<jsp:attribute name="sidebar">
			<c:set var="isAdmin" value="${command.context.loginUser.role == 'ADMIN'}" />
			<sidebar:socializer command="${command}" user="${command.user}" />
			
			<c:if test="${isAdmin}">
				spammer: <adminUser:spamActions user="${command.user}" />
			</c:if>
			
			<!-- Did you know? -->
			<c:if test="${	command.context.userLoggedIn and 
							command.context.loginUser.name eq command.requestedUser and 
							not empty command.didYouKnowMessage and
							command.context.loginUser.settings.layoutSettings.simpleInterface}">
				<bs:didYouKnow command="${command}"/>
			</c:if>
			
			<!-- link to discussed user page -->
			<c:if test="${not command.context.userLoggedIn or command.context.loginUser.name ne command.requestedUser}">
				<sidebar:sidebarItem textKey="post.resource.discussion.headline">
					<jsp:attribute name="content">
						<ul class="discussionSidebarContent sidebar_collapse_content">
							<li>
								<h5 class="discussion-headline sidebar-headline">
										<fmt:message key="discussion.byUser.help" var="title">
											<fmt:param value="${fn:escapeXml(command.requestedUser)}"/>
										</fmt:message>
								</h5>
								
								<c:set var="urlGeneratorDiscussed" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'personalized')}" />
								<a href="${urlGeneratorDiscussed.getUserUrlByUserName(fn:escapeXml(command.requestedUser))}" title="${title}">
									<fmt:message key="discussion.byUser"><fmt:param value="${fn:escapeXml(command.requestedUser)}"/></fmt:message>
								</a>
							</li>
						</ul>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>
			
			<sidebar:conceptsItem 
				cmd="${command.concepts}" 
				usersOwnRelations="${command.context.loginUser.name eq command.requestedUser}" 
				requestedUserName="${command.requestedUser}"
				headingLink="${relativeUrlGenerator.getConceptsUrlByString(command.requestedUser)}"	
				headingTitleKey="tagrelations.user.title"
				scope="${command.scope.name()}"/>
			
			<!-- display similar users -->
			<sidebar:sidebarItem textKey="users.similar">
				<jsp:attribute name="content">
					<c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>
					<ul class="list-inline sidebar-setting-box clearfix">
						<li>(<a href="${mtl:setParam(requPathAndQuery, 'userSimilarity', mtl:toggleUserSimilarity(command.userSimilarity)) }"><fmt:message key="more"/>...</a>)</li>
					</ul>
					<users:related cmd="${command.relatedUserCommand}" displayFollowLink="${false}" personalized="${true}" userSimilarity="${command.userSimilarity}"/>
				</jsp:attribute>
			</sidebar:sidebarItem>
				
			<!-- display groups that the users share -->
			<sidebar:sidebarItem textKey="user.sharedGroups">
				<jsp:attribute name="content">
					<ul class="list-inline sidebar-setting-box clearfix">
						<c:forEach var="group" items="${command.sharedGroups}">
							<li>
								<a href="${relativeUrlGenerator.getGroupUrlByGroupName(group.name)}">
									<group:groupname group="${group}" noLink="${false}" showRealname="${false}"/>
								</a>
							</li>
						</c:forEach>
					</ul>
				</jsp:attribute>
			</sidebar:sidebarItem>
			
			<sidebar:sidebarTagCloudItem requPath="${requPath}" cmd="${command.tagcloud}" tagboxMinfreqStyle="user" tagSizeMode="user" showTooltips="${command.context.loginUser.settings.tagboxTooltip}" scope="${command.scope.name()}"/>

            <!-- Claimed person -->
            <c:if test="${not empty command.claimedPerson}">
				<sidebar:sidebarItem textKey="settings.sidebar.claimedPerson">
					<jsp:attribute name="content">
						<a href="${relativeUrlGenerator.getPersonUrl(command.claimedPerson.personId)}">
							${mtl:cleanBibtex(command.claimedPerson.mainName.firstName)} ${mtl:cleanBibtex(command.claimedPerson.mainName.lastName)}
						</a>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>

			<c:if test="${isAdmin}">
				<sidebar:sidebarItem textKey="user.details">
					<jsp:attribute name="content">
						<user:details user="${command.user}"/>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>
		</jsp:attribute>
	</layout:resourceLayout>
</jsp:root>