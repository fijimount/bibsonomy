<?xml version="1.0" ?>
<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld">
	 
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />
	
	<c:set var="hasTags" value="${not empty command.requestedTags}" />
	<fmt:message var="pageTitle" key="navi.search" />
	
	<c:set var="requPathAndQuery" value="/${requPath}${requQueryString}"/>
	
	<layout:resourceLayout pageTitle="${pageTitle}" command="${command}" requPath="${requPath}">
		
		<jsp:attribute name="additionalSorting">
			<!-- relevance sorting -->
			<c:choose>
				<c:when test="${command.sortPage eq 'relevance' and command.sortPageOrder eq 'desc'}">
					<li class="sort-selection sort-selected"><a href="${mtl:setParam(mtl:setParam(requPathAndQuery, 'sortPage', 'relevance'), 'sortPageOrder', 'asc')}">
						<fontawesome:icon icon="sort-amount-down" spaceAfter="5"/><fmt:message key="sort.rank"/><fontawesome:icon icon="arrow-down" spaceBefore="5"/>
					</a></li>
				</c:when>
				<c:when test="${command.sortPage eq 'relevance' and command.sortPageOrder eq 'asc'}">
					<li class="sort-selection sort-selected"><a href="${mtl:setParam(mtl:setParam(requPathAndQuery, 'sortPage', 'relevance'), 'sortPageOrder', 'desc')}">
						<fontawesome:icon icon="sort-amount-up" spaceAfter="5"/><fmt:message key="sort.rank"/><fontawesome:icon icon="arrow-up" spaceBefore="5"/>
					</a></li>
				</c:when>
				<c:otherwise>
					<li class="sort-selection"><a href="${mtl:setParam(mtl:setParam(requPathAndQuery, 'sortPage', 'relevance'), 'sortPageOrder', 'desc')}">
						<fontawesome:icon icon="sort-amount-down" spaceAfter="5" /><fmt:message key="sort.rank"/>
					</a></li>
				</c:otherwise>
			</c:choose>
		</jsp:attribute>

		<jsp:attribute name="headerExt">
			<script type="text/javascript" src="${resdir}/javascript/extendedSearch.js"><!-- keep me --></script>
		</jsp:attribute>
		
		<jsp:attribute name="heading">
			<c:choose>
				<c:when test="${not empty command.scope and command.scope.name() ne 'LOCAL' and command.scope.name() ne 'SEARCHINDEX'}">
					<c:set var="pathMessageKey" value="navi.${command.scope.name().toLowerCase()}"/>
				</c:when>
				<c:otherwise>
					<c:set var="pathMessageKey" value="navi.search"/>
				</c:otherwise>
			</c:choose>
			<parts:search scope="${command.scope}" pathMessageKey="${pathMessageKey}" formAction="/redirect" formInputName="search" formInputValue="${command.requestedSearch}">
				<jsp:attribute name="extraButtons">
					<a class="btn btn-default" id="expandSearchButton" onclick="toggleExtendedSearch('#extendedSearchInput')">
						<fontawesome:icon icon="angle-double-down"/>
					</a>
				</jsp:attribute>
			</parts:search>
		</jsp:attribute>

		<jsp:attribute name="content">
			<div class="row">
				<div class="col-md-12">
					<parts:extendedSearch headerMessageKey="search.extended.header" searchPlaceholderKey="navi.search.hint"
										  formAction="/redirect" formInputValue="${command.requestedSearch}"/>
				</div>
			</div>
		</jsp:attribute>
	
		<!--+
			| sidebar
			+-->
		<jsp:attribute name="sidebar">
			<c:set var="requPathToUse" value="${requPath}" />
			<c:if test="${hasTags}">
				<c:set var="requPathToUse" value="${mtl:getLowerPath(requPath)}" />
			</c:if>
			<sidebar:sidebarTagCloudItem requPath="${requPathToUse}" cmd="${command.tagcloud}" tagboxMinfreqStyle="user" tagSizeMode="user" showTooltips="${command.context.loginUser.settings.tagboxTooltip}" scope="${command.scope.name()}"/>
		</jsp:attribute>
	
	</layout:resourceLayout>
</jsp:root>