<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user">

	<!-- 
        Shows form fields to choose group(s) the post should be viewable for.
        Consists of three radio buttons: public, private, other
        and a filter box which contains the group "friends" plus other possible groups.
    
     -->
    <jsp:directive.attribute name="groups" type="java.util.List" required="true"/>
	<jsp:directive.attribute name="friends" type="java.util.List" required="true"/>
	<jsp:directive.attribute name="labelColSpan" type="java.lang.Integer" required="false"/>
	<jsp:directive.attribute name="chosenPersonsOrGroups" type="java.util.Set" required="false"/>
	<c:set var="message"><fmt:message key="clickToRemove"/></c:set>
	<c:if test="${empty labelColSpan}">
		<c:set var="labelColSpan" value="2" />
	</c:if>
	<div class="form-group">
		<label class="col-sm-${labelColSpan} control-label" for="${fn:escapeXml(id)}">
			<fmt:message key="post.resource.groupsandfriends.send" />
		</label>
		
		<div class="col-sm-${12 - labelColSpan}">
			<div class="row">
				<div class="col-sm-4">
					<c:if test="${not empty friends}">
						<div class="radio">
							<label>
								<form:radiobutton cssClass="radio-viewable-group" path="friendsOrGroups" value="friends" tabindex="2" id="post.friends" /> <fmt:message key="post.groups.friends"/>
							</label>
						</div>
					</c:if>
					<c:if test="${not empty groups}">
						<div class="radio">
							<label>
								<form:radiobutton cssClass="radio-viewable-group" path="friendsOrGroups" value="groups" tabindex="2" id="post.groups" /> <fmt:message key="groups"/>
							</label>
						</div>
					</c:if>
				</div>
				<div class="col-sm-8">
					<c:if test="${not empty groups}">
						<select name="post.groups.form" id="postgroupsdropdown" class="form-control">
							<c:forEach items="${groups}" var="group">
								<option value="${group.name}">${group.name}</option>
							</c:forEach>
						</select>
					</c:if>
					<c:if test="${not empty friends}">
						<select name="post.friends.form" id="postfriendsdropdown" class="form-control">
							<c:forEach items="${friends}" var="user">
								<option value="${user.name}">${user.name}</option>
							</c:forEach>
						</select>
					</c:if>
					<c:if test="${(empty friends) and (empty groups)}">
						<div id="noFriendsAndGroups">There are no friends or groups</div>
					</c:if>
				</div>
				<div id="friendsOrGroupsLabels">
					<ul id="friendsOrGroups" class="recommended list-inline">
						<!-- This comment is needed, otherwise this will result in a self-closing element -->
					</ul>
				</div>
			</div>
			<c:if test="${not empty formErrors}">
				<p class="help-block">${formErrors}</p>
			</c:if>
		</div>
	</div>
			<script type="text/javascript">
			<![CDATA[
				/* this script controls the visibility of the group filter field. if the 'other' radio button is checked or hovered show the filter field, otherwise hide it
				*/
		
				$(document).ready(function() {
			var checkedgroups = document.getElementById("post.groups");
			var checkedfriends = document.getElementById("post.friends");
			var groupsform = document.getElementById("postgroupsdropdown");
			var friendsform = document.getElementById("postfriendsdropdown");
			
			var allowedfriends=[];
			$("#postfriendsdropdown option").each(function() {
				allowedfriends.push($(this).val());
			});
			
			var allowedgroups=[];
			$("#postgroupsdropdown option").each(function() {
				allowedgroups.push($(this).val());
			});
			
			var saved=$("#inpf_tags").val();
			$("#inpf_tags").val(saved.trim()+" ");
			
			var friends=[];
			var groups=[];
			
			var tempfriends=[];
			var tempgroups=[];
			
			var tags;
			
			$(groupsform).hide();
			
			$(checkedfriends).prop("checked", true);
			$(friendsform).show();
			$(friendsform).prop("selectedIndex", -1);
			
			if( $(friendsform).has('option').length == 0 ) {
				$(checkedgroups).prop("checked", true);
				$(checkedfriends).prop("checked", false);
				$(groupsform).show();
				$(groupsform).prop("selectedIndex", -1);
			}
			
						
			function check_radio_button() {
				$(checkedfriends).click(function() {
					choose_radio_button();
				});
				$(checkedgroups).click(function() {
					choose_radio_button();
				});
			}
			
			function choose_radio_button() {
				
				if ($(checkedfriends).is(":checked") == true) {
					$(friendsform).show();
					$(friendsform).prop("selectedIndex", -1);
					$(groupsform).hide();
				} else if ($(checkedgroups).is(":checked") == true) {
					$(friendsform).hide();
					$(groupsform).show();
					$(groupsform).prop("selectedIndex", -1);
				}
			}
			
			check_radio_button();
			
			function dropdowns() {
				$(friendsform).change(function() {
					var selected=$(friendsform).val();
					var found=false;
					for (i = 0; i < friends.length; i++) {
						if(friends[i] == selected) {
							found=true;
						}
					}
					if (found == false) {
						friends.push(selected);
						var number=friends.indexOf(selected);
						$("#friendsOrGroups").append("<li tabindex=1 value=\""+number+"\" class=\"delete-friend\" id=\"delete-friend-"+number+"\"><data-toggle=\"popover\" title=\"${message}\"><span class=\"fa fa-times fa-icon-position\" aria-hidden=\"true\"></span>"+selected+"</li>");
						tags=$("#inpf_tags").val();
						tags=tags+"send:"+selected+" ";
						$("#inpf_tags").val(tags);
						
							$("li.delete-friend").click(function() {
								var number=$(this).val();
								
								$("#delete-friend-"+number+"").remove();
								tags=tags.replace("send:"+friends[number]+" ","");
								
								$("#inpf_tags").val(tags);
								friends[number]="";
								
							});
						
					}
					$(friendsform).prop("selectedIndex", -1);
				});
				$(groupsform).change(function() {
					var selected=$(groupsform).val();
					var found=false;
					for (i = 0; i < groups.length; i++) {
						if(groups[i] == selected) {
							found=true;
						}
					}
					if (found == false) {
						groups.push(selected);
						var number=groups.indexOf(selected);
						$("#friendsOrGroups").append("<li tabindex=1 value=\""+number+"\" class=\"delete-group\" id=\"delete-group-"+number+"\"><data-toggle=\"popover\" title=\"${message}\"><span class=\"fa fa-times fa-icon-position\" aria-hidden=\"true\"></span>"+selected+"</li>");
						tags=$("#inpf_tags").val();
						tags=tags+"for:"+selected+" ";
						$("#inpf_tags").val(tags);
						
							$("li.delete-group").click(function() {
								var number=$(this).val();
								
								$("#delete-group-"+number+"").remove();
								tags=tags.replace("for:"+groups[number]+" ","");
								
								$("#inpf_tags").val(tags);
								groups[number]="";
								
							});
						
					}
					$(groupsform).prop("selectedIndex", -1);
				});
			}
			
			function add_labels() {			
				tempfriends=[];
				friends=[];
				
				tags=$("#inpf_tags").val();
				
				var ref = /(send:|sent:)+([a-z]+)/g;
				tempfriends=tags.match(ref);
				
				var foundfriend=false;
				
				if (tempfriends != null) {
					var friend;
					for (var i = 0; i < tempfriends.length ; i++) {
						friend=tempfriends[i];
						friend = friend.replace(ref, "$2");
						for (var z = 0; z < allowedfriends.length ; z++) {
							if (friend == allowedfriends[z]) {
								foundfriend=true;
								break;
							}
						}
						if (foundfriend == true) {
							friends.push(friend);
							foundfriend=false;
						}
						
					}
				}
				
				var selected;
				
				
				if (friends != null) {
					for (i = 0; i < friends.length; i++) {
						//friends[i]=(friends[i]).replace("send:","");
						//friends[i]=(friends[i]).replace("sent:","");
						selected=friends[i];
						$("#friendsOrGroups").append("<li tabindex=1 value=\""+i+"\" class=\"delete-friend-labels\" id=\"delete-friend-"+i+"\"><span class=\"fa fa-times fa-icon-position\" aria-hidden=\"true\"></span><data-toggle=\"popover\" title=\"${message}\">"+selected+"</li>");
					} 
					$('[data-toggle="popover"]').popover();
					
					tags=$("#inpf_tags").val();
					
					$("li.delete-friend-labels").click(function() {
						addfriendsdelete=false;
						var number=$(this).val();
						
						tags=tags.replace("send:"+friends[number]+" ","");
						tags=tags.replace("sent:"+friends[number]+" ","");
						
						$("#delete-friend-"+number+"").remove();
						
						$("#inpf_tags").val(tags);
						friends[number]="";
						
					});
				} else {
					friends=[];
					
				}
				tempgroups=[];
				groups=[];
				
				tags=$("#inpf_tags").val();
				
				var reg = /(for:)+([a-z]+)/g;
				tempgroups=tags.match(reg);
				
				var foundgroup=false;
				
				if (tempgroups != null) {
					var group;
					for (var i = 0; i < tempgroups.length ; i++) {
						group=tempgroups[i];
						group = group.replace(reg, "$2");
						for (var z = 0; z < allowedgroups.length ; z++) {
							if (group == allowedgroups[z]) {
								foundgroup=true;
								break;
							}
						}
						if (foundgroup == true) {
							groups.push(group);
							foundgroup=false;
						}
					}
				}
				
				var selected;
				
				if (groups != null) {
					for (i = 0; i < groups.length; i++) {
						//groups[i]=(groups[i]).replace("for:","");
						selected=groups[i];
						$("#friendsOrGroups").append("<li tabindex=1 value=\""+i+"\" class=\"delete-group-labels\" id=\"delete-group-"+i+"\"><span class=\"fa fa-times fa-icon-position\" aria-hidden=\"true\"></span><data-toggle=\"popover\" title=\"${message}\">"+selected+"</li>");
					}
					$('[data-toggle="popover"]').popover();
		
					tags=$("#inpf_tags").val();
					
					$("li.delete-group-labels").click(function() {
						addgroupsdelete=false;
						var number=$(this).val();
						
						tags=tags.replace("for:"+groups[number]+" ","");
						$("#delete-group-"+number+"").remove();
						$("#inpf_tags").val(tags);
						groups[number]="";
						
						
					});
				} else {
					groups=[];
					
				}
			}
			
			add_labels();
			
			
			dropdowns();
			
			function print_labels() {
				$("#inpf_tags").keyup(function() {
					$("#friendsOrGroups").empty();
					add_labels();
				});
				$("#ui-id-1").click(function() {
					$("#friendsOrGroups").empty();
					add_labels();
				});
			}
			
			print_labels();
		});
		  
		]]>
	</script>
</jsp:root>