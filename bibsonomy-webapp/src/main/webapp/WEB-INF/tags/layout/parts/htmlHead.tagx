<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:spring="http://www.springframework.org/tags">
	
	<jsp:directive.attribute name="headerExt" fragment="true" />
	<jsp:directive.attribute name="loginUser" type="org.bibsonomy.model.User" required="true"/>
	<jsp:directive.attribute name="requestedUser" type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="pageTitle" type="java.lang.String" required="true"/>	
	<jsp:directive.attribute name="requPath" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="personalized" type="java.lang.Boolean" required="false"/>
	
	<!--+
		|
		| setting (global) variables
		|
		+-->
	<!-- TODO: extract into separate JSP file? -->
	<c:set var="resdir" scope="application" value="/resources" />
	
	<!--+ 
		| To have the locale available on all pages we set the scope to "request".
		| see http://stackoverflow.com/questions/333729/how-do-i-access-locale-from-a-jsp 
		|-->
	<c:set var="locale" value="${pageContext.response.locale}" scope="request"/>
	<!--+
		| At some places we need the complete path and query
		+-->
	<c:set var="requPathAndQuery" value="${requPath}${requQueryString}" scope="request" />

	<!-- prepare hidden system tags for handling in custom.js --> 
	<c:set var="hiddenSystemTags" value="" />
	<c:forEach var="markUpSystemTags" items="${systemTagFactory.markUpSystemTags}" varStatus="status">
		<c:set var="hiddenSystemTags" value='${hiddenSystemTags} { name: "${markUpSystemTags.name}", toHide: ${markUpSystemTags.toHide}, hasArguments: ${markUpSystemTags.hasArguments()} },' />
	</c:forEach>
	<c:forEach var="executableSystemTags" items="${systemTagFactory.executableSystemTags}">
		<c:set var="hiddenSystemTags" value='${hiddenSystemTags} { name: "${executableSystemTags.name}", toHide: ${executableSystemTags.toHide}, hasArguments: ${executableSystemTags.hasArguments()} },' />
	</c:forEach>
	<!--  remove last comma -->
	<c:set var="hiddenSystemTags" value="${fn:substring(hiddenSystemTags, 0, fn:length(hiddenSystemTags) - 1 )}" />

	<c:set var="groupsString" value="" />
	<!-- groups of the logged in user -->
	<c:forEach var="group" items="${loginUser.groups}">
		<c:set var="groupsString" value='${groupsString} "${group.name}",'/>
	</c:forEach>
	<c:choose>
		<c:when test="groupsString != ''">
			<c:set var="groupsString" value="[${fn:substring(groupsString, 0, fn:length(groupsString) - 1 )}]" />
		</c:when>
		<c:otherwise>
			<c:set var="groupsString" value="null" />
		</c:otherwise>
	</c:choose>

	<!--+
		|
		| real content starts here
		|
		+-->
	<head>
		<meta charset="UTF-8" />
		
		<!--+
			| BOOTSTRAP SETTINGS 
			+-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />  
		
		<!-- latest compiled and minified CSS -->
		<!--<mtl:styleSheet path="${resdir}/css/bootstrap-style.css" />-->
		<link rel="stylesheet/less" type="text/css" href="/resources/css/bootstrap-style.less" />
		<!--+
			| END BOOTSTRAP SETTINGS 
			+ -->
		<c:if test="${not empty personalized and personalized}">
			<!-- FIXME: personalized style sheet -->
			<!-- <link rel="stylesheet" type="text/css" href="${resdir}/css/personalized.css" /> -->
		</c:if>
		
		<link rel="icon" href="${resdir}/image/favicon.png" type="image/png" />
		
		<!--+
			|
			| JavaScript files 
			| 
		 	+-->
		<!--+ 
		 	| merged jquery.js, functions.js, style.js
		 	| and jQuery plugins jquery.corner.js
			| 
			| To add other files, add a line to ${resdir}/javascript/merged/global.js and 
		 	| include it in the "aggregate" section of the yuicompressor-maven-plugin in the pom.xml.
		 	+-->


		<script type="text/javascript">
			less = {
				env: "development",
				async: false,
				fileAsync: false,
				poll: 1000,
				functions: {},
				dumpLineNumbers: "comments",
				relativeUrls: false,
				rootpath: ":/a.com/"
			};

		</script>

		<script type="text/javascript">
			less = {
				env: "development",
				async: false,
				fileAsync: false,
				poll: 1000,
				functions: {},
				dumpLineNumbers: "comments",
				relativeUrls: false,
				rootpath: ":/a.com/"
			};

		</script>

		<script type="text/javascript" src="/resources/javascript/less/less.js"><!-- keep me --></script>
		<script type="text/javascript" src="${resdir}/javascript/merged/global.js"><!-- keep me --></script>

		<c:set var="requestedUserString" value='"${fn:escapeXml(requestedUser)}"' />
		<script type="text/javascript">
				<![CDATA[
					var projectName = "${properties['project.name']}";
					var ckey = "${command.context.ckey}";
					var currUser = "${loginUser.name}";
					var userSettings = {
						"logging" : ${loginUser.settings.logLevel != 1 and not systemReadOnly},
						"tagbox" : {
							"style" : ${loginUser.settings.tagboxStyle},
							"sort"  : ${loginUser.settings.tagboxSort},
							"minfreq" : ${loginUser.settings.tagboxMinfreq}
						},
						"confirmDelete" : ${loginUser.settings.confirmDelete}
					};
					var groups =
						${groupsString}
					;
					var requUser = ${(requestedUser ne '') ? requestedUserString : "null"};
					var hiddenSystemTags = [
						${hiddenSystemTags}
					];
				]]>
		</script>
		
		<parts:messages locale="${locale}" />
		
		<!-- include tooltip JavaScript only for users that have tooltips enabled -->
		<c:if test="${loginUser.settings.tagboxTooltip == 1}">	
			<script type="text/javascript" src="${resdir}/javascript/tooltip.js"><!-- keep me --></script>
		</c:if>
		
		<!-- enable on-site spammer killing when user is admin -->
		<c:if test="${loginUser.role eq 'ADMIN'}">
			<script type="text/javascript" src="${resdir}/javascript/admin/spammer.js"><!-- keep me --></script>
		</c:if>
		
		<spring:theme code="javascript.additional" var="additionalJS" text="" />
		<c:if test="${not empty additionalJS}">
			<script type="text/javascript" src="${additionalJS}"><!-- keep me --></script>
		</c:if>
		
		<!--+
			|
			| Piwik
			|
			+--> 
		<c:set var="statisticsUrl" value="${properties['project.statistics.service.url']}" />
		<c:if test="${not empty statisticsUrl}">
			<c:set var="siteid" value="${properties['project.statisitics.siteid']}" />
			<script type="text/javascript">
				<![CDATA[
					var _paq = _paq || [];
					_paq.push(['trackPageView']);
					_paq.push(['enableLinkTracking']);
					(function() {
						var u="//${statisticsUrl}";
						_paq.push(['setTrackerUrl', u+'piwik.php']);
						_paq.push(['setSiteId', ${siteid}]);
						var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
						g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
					})();
				]]>
			</script>
		</c:if>
		<!-- End Piwik Tracking Code --> 
	
<!--	<meta name="email" content="${properties['project.email']}"/> -->
		<parts:meta fieldName="author" />
<!--	<parts:meta fieldName="copyright" /> -->
		<parts:meta fieldName="keywords" />
		<parts:meta fieldName="description" />
		<!-- TODO: use feed links for bibsonomy in puma? -->
		<link rel="alternate" type="application/atom+xml" title="${properties['project.name']} Blog - Atom" href="http://blog.bibsonomy.org/feeds/posts/default" />
		<link rel="alternate" type="application/rss+xml"  title="${properties['project.name']} Blog - RSS"  href="http://blog.bibsonomy.org/feeds/posts/default?alt=rss" />
		<title><c:if test="${not empty pageTitle}"><c:out value="${pageTitle}"/> | </c:if>${properties['project.name']}</title>
		<jsp:invoke fragment="headerExt"/>
	</head>
</jsp:root>