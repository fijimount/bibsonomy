<?xml version="1.0" ?>
<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:errors="urn:jsptagdir:/WEB-INF/tags/errors"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:post="urn:jsptagdir:/WEB-INF/tags/post"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons">

    <jsp:directive.attribute name="command" type="org.bibsonomy.webapp.command.actions.EditPostCommand" required="true"/>
    <jsp:directive.attribute name="requPath" type="java.lang.String" required="true"/>
    <jsp:directive.attribute name="resourceType" type="java.lang.String" required="true" description="The type of the resource this page handles (e.g., 'Bookmark', or 'Publication'). First letter upper case, remaining lower case."/>
    <jsp:directive.attribute name="activeTab" type="java.lang.String" required="false"/>
    <jsp:directive.attribute name="hideGroupsAndTags" type="java.lang.Boolean" required="false"/>
    <jsp:directive.attribute name="tagsAndGroups" fragment="true" required="false" />
        
    <jsp:directive.attribute name="groupsandfriends" fragment="true" required="false" />
    
    <jsp:directive.attribute name="headerExtResource" fragment="true" required="false" />
    <jsp:directive.attribute name="generalInformation" fragment="true" required="true" description="The form fields to enter general information."/>
    <jsp:directive.attribute name="detailedInformation" fragment="true" required="false" description="The form fields to enter detailed information."/>
    <jsp:directive.attribute name="tags" fragment="true" required="false" />
    <jsp:directive.attribute name="groups" fragment="true" required="false" />
    <jsp:directive.attribute name="extraInformation" fragment="true" required="false" description="Extra information which shall be displayed outside the form."/>
    <jsp:directive.attribute name="postApproval" fragment="true" required="false" description="A checkbox inorder to approve goldstandard posts"/>
    <jsp:directive.attribute name="postOwner" type="org.bibsonomy.model.User" fragment="false" required="false" description="The owner of the current post" />
	
	<fmt:message key="navi.edit${resourceType}" var="editPageTitle" />

	<layout:layout
		loginUser="${command.context.loginUser}"
		pageTitle="${editPageTitle}" 
		requPath="${requPath}" 
		activeTab="${activeTab}"
		noSidebar="${true}"
		>
		
	    <!--+
    	 	| html header extension: page specific javascript
    	 	+-->
		<jsp:attribute name="headerExt">
			
			<!-- TODO: document why we need this jquery plugin -->
	    	<script type="text/javascript" src="${resdir}/jquery/plugins/form/jquery.form.js"><!-- keep me --></script>
	    	
            <!--
               | one script just for the post page
               -->
            <jsp:invoke fragment="headerExtResource"/>
            
            <script type="text/javascript" src="${resdir}/javascript/editPost.js"><!-- keep me --></script>
            
            <!--+
				| JAVASCRIPT FOR TAG RECOMMENDATION
				+-->
			<script type="text/javascript">
			   var userName = "${mtl:encodePathSegment(command.context.loginUser.name)}";
			   //<mtl:cdata>
			   <![CDATA[
			      $(function(){
			                  
					$.ajax(tagRecoOptions).done(function(data) {
						handleRecommendedTags(data);
					});
					
			        // get user's tags for suggestions
			        $.ajax({
			            type: "GET",
			            url: "/json/tags/user/" + userName + "?tagcloud.maxCount=100000",
			            dataType: "jsonp",
			            success: function(json){
			               populateSuggestionsFromJSON(json);
			            }
			        });
			       }
			      );
			
			   ]]> 
			   //</mtl:cdata>
			</script>
			
	    </jsp:attribute>
	    
        <jsp:attribute name="heading">
        	<parts:search pathMessageKey="navi.edit${resourceType}"/>
        </jsp:attribute>
      	<!--
   			  | 
  			  | TODO:
     		  |   - Einblendung des existierenden Eintrages (bzw. des geposteten - der
   			  |     existierende wird editiert, der gepostete ist in "diffPost") 
		      -->
		<jsp:attribute name="content">
		
			<div class="row">
		
				<div class="col-md-9 col-sm-12 col-xs-12">
			
					<c:choose>
						<c:when test="${not empty postOwner and postOwner ne command.context.loginUser}">
							<h1 class="content-heading"><fmt:message key="post.resource.editGroup${resourceType}" /></h1>
						</c:when>
						<c:otherwise>
							<h1 class="content-heading"><fmt:message key="post.resource.edit${resourceType}" /></h1>
						</c:otherwise> 
					</c:choose>
					
					<!--+ 
						| show global errors / warnings
						+ -->
					<errors:global />

					<c:if test="${not empty postOwner and postOwner ne command.context.loginUser}">
						<div class="row">
							<div class="alert alert-warning alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&amp;times;</span></button>
								<fmt:message key="post.resource.editGroupPost.warning" />
							</div>
						</div>
					</c:if>
				
		            <!--+
		                | form starts here
		                +-->
		            <form:form id="postForm" class="form-horizontal" role="form" action="/edit${resourceType}?ckey=${ckey}" method="post">
						
		
						<!--+ 
						    | user is posting an existing resource ...
						    +-->
						<c:if test="${not empty command.diffPost}">
		                 	
		                 	<bs:alert style="warning" dismissable="true">
		                 		<jsp:attribute name="alertBody">
		                 			<h4><fmt:message key="note"/>: <fmt:message key="post.edit.existent" /></h4>
		                 			<p><fmt:message key="post.edit.existent.help" /></p>
		                 		</jsp:attribute>
		                 	</bs:alert>

						</c:if>
						<!--+
		                    | general information 
		                    +-->
				 		<a name="generalInformation"><!--  --></a>
				 		
						<bsform:fieldset legendKey="post.resource.fields.general">
							<jsp:attribute name="content">
								<jsp:invoke fragment="generalInformation"/>
							</jsp:attribute>
						</bsform:fieldset>
		
						<jsp:invoke fragment="tagsAndGroups" />
						<!-- FIXME: change name to groupAndFriends; do we need this check here? -->
						<c:if test="${(properties['groupsandfriendsfeature.activated'] eq 'true') and resourceType eq 'Publication'}">
							<jsp:invoke fragment="groupsandfriends" />
						</c:if>
						<!--+
							| detailed information  
							+-->
						<c:if test="${not empty detailedInformation}">
							<jsp:invoke fragment="detailedInformation"/>
						</c:if>
						
						
							
						<!--+
							| a Captcha for our spammers ...
							+-->
						<c:if test="${not empty command.captchaHTML}">
							<bsform:fieldset legendKey="captcha">
								<jsp:attribute name="content">
									<c:if test="${resourceType eq 'Publication'}">
										<p>
											<fmt:message key="post.edit.spam_note">
												<fmt:param value="user ${postOwner.name} was flagged as spammer"/>
												<fmt:param value="${properties['project.noSpamEmail']}" />
											</fmt:message>
										</p>
									</c:if>
									<bsform:captcha />
								</jsp:attribute>
							</bsform:fieldset>
						</c:if>
						<!--+
							| goldStandard post's approval  
							+-->
						<c:if test="${not empty postApproval and (command.context.loginUser.hasGroupLevelPermission('COMMUNITY_POST_INSPECTION') or command.context.loginUser.role eq 'ADMIN')}">
							<jsp:invoke fragment="postApproval"/>
						</c:if>
						
						<!-- FIXME: remove the following from this file; only publications have a  -->
						<c:if test="${resourceType eq 'Publication'}">
						<c:if test="${command.personRole eq 'ADVISOR'}">
							<bsform:fieldset legendKey="post.person.role">
								<jsp:attribute name="content">
									<post:personRole_groupBox />
								</jsp:attribute>
							</bsform:fieldset>
						</c:if>
						</c:if>
		
		                <!--+
		               
		               		| buttons 
		                    +-->
						<div class="fieldset-margin">
							<div class="form-group">
								<form:hidden path="post.resource.interHash"/>
								<input type="hidden" name="user" value="${command.user}"/>
								<input type="hidden" name="postID" value="${command.postID}"/>
								<input type="hidden" name="referer" value="${fn:escapeXml(command.referer)}"/>
								<input type="hidden" name="ckey" value="${fn:escapeXml(command.context.ckey)}"/>
								<form:hidden path="intraHashToUpdate" />
								<c:if test="${not empty command.groupUser}">
									<form:hidden path="groupUser" />
								</c:if>
								<div class="col-sm-offset-3 col-sm-9">
									<fmt:message key="save" var="save" />
									<fmt:message key="save.and.rate" var="saveAndRate" />
									
									
									<div class="btn-toolbar cust-btn-toolbar">
										<div class="btn-group">
											<bsform:button type="submit" id="save-post-button" size="defaultSize" value="${save}" style="primary" onclick="clear_tags();" tabindex="2"/>
										</div>
										<div class="btn-group">
											<bsform:button type="submit" id="save-rate-post-button" size="defaultSize" value="${saveAndRate}" name="saveAndRate" style="primary" onclick="clear_tags();" tabindex="2"/>
										</div>
	
										<c:if test="${not empty properties['bibliography.bibliographyUser']}">
											<fmt:message key="save.and.send_to" var="saveAndSendTo">
												<fmt:param value="${properties['bibliography.bibliographyUser']}"/>
											</fmt:message>
									
											<div class="btn-group">
												<bsform:button type="submit" id="save-send-post-button" size="defaultSize" value="${saveAndSendTo}" name="saveAndSendTo" style="primary" onclick="sendToBibliography('${properties['bibliography.bibliographyUser']}');" tabindex="2"/>
											</div>
										</c:if>
										<c:if test="${not empty groups}">
											<fmt:message key="save.and.send_to" var="saveAndSendTo">
												<fmt:param value="${groups}"/>
											</fmt:message>
									
											<div class="btn-group">
												<bsform:button type="submit" id="save-send-post-button" size="defaultSize" value="${saveAndSendTo}" name="saveAndSendTo" style="primary" onclick="sendGroupToBibliography('${groups}');" tabindex="2"/>
											</div>
										</c:if>			
									</div>
								</div>
							</div>
						</div>	
					</form:form>
		
		            <c:if test="${not empty extraInformation}">
		                <jsp:invoke fragment="extraInformation"/>
		            </c:if>
			
					<div id="hiddenUpload" style="display:none;">
		           		<!-- temporary content -->
					</div>
				</div>
				
				<div class="col-md-3 hidden-xs hidden-sm">
					<div data-spy="affix" data-offset-top="0" data-offset-bottom="500">
 						<div class="list-group">
 							<a class="list-group-item active" onclick="javascript: activateAffixEntry(this)" href="#generalInformation"><fmt:message key="post.resource.fields.general" /></a>
 							<c:if test="${not empty tagsAndGroups}">
 								<a class="list-group-item" onclick="javascript: activateAffixEntry(this)" href="#tags"><fmt:message key="post.resource.tags"/></a>
 								<a class="list-group-item" onclick="javascript: activateAffixEntry(this)" href="#groups"><fmt:message key="post.resource.groups"/></a>
 							</c:if>
 							<c:if test="${not empty command.context.loginUser.groups and not fn:startsWith(resourceType, 'GoldStandard')}">
 								<a class="list-group-item" onclick="javascript: activateAffixEntry(this)" href="#group-options"><fmt:message key="post.resource.groups.options"/></a>
 							</c:if>
 							<c:if test="${not empty detailedInformation}">
	 							<c:if test="${resourceType ne 'GoldStandardPublication'}">
	 								<a class="list-group-item" onclick="javascript: activateAffixEntry(this)" href="#uploadedFiles"><fmt:message key="post.bibtex.files" /></a>
	 							</c:if>
 								<a class="list-group-item" onclick="javascript: activateAffixEntry(this)" href="#detailedInformation"><fmt:message key="post.resource.fields.detailed"/></a>
 								<a class="list-group-item" onclick="javascript: activateAffixEntry(this)" href="#comments"><fmt:message key="post.resource.fields.comments"/></a>
 							</c:if>
						</div>
						<div class="btn-toolbar cust-btn-toolbar">
							<div class="btn-group">
								<a class="btn btn-primary btn-sm save-post-item" onclick="javascript: activateAffixEntry(this); $('#save-post-button').trigger('click');"><fmt:message key="save"/></a>
							</div>
							<div class="btn-group">
								<a class="btn btn-primary btn-sm save-rate-post-item" onclick="javascript: activateAffixEntry(this); $('#save-post-button').trigger('click');"><fmt:message key="save.and.rate" /></a>
							</div>
							
							<c:if test="${not empty properties['bibliography.bibliographyUser']}">
								<fmt:message key="save.and.send_to" var="saveAndSendTo">
									<fmt:param value="${properties['bibliography.bibliographyUser']}"/>
								</fmt:message>
						
								<div class="btn-group">
									<a class="btn btn-primary btn-sm save-rate-post-item" onclick="javascript: activateAffixEntry(this); $('#save-send-post-button').trigger('click');">${saveAndSendTo}</a>
								</div>
							</c:if>
							
						</div>
					</div>
				</div>
				
			</div>
			<!-- margins -->
			<p>&amp;nbsp;<c:out value=" " /></p>
		</jsp:attribute>
		
	</layout:layout>

</jsp:root>