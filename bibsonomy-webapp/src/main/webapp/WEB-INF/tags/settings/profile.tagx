<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons">
	
	<jsp:directive.attribute name="command"  type="org.bibsonomy.webapp.command.BaseCommand" required="true"/>

	<!-- ############################################# profile tab ############################################# -->
	<c:set var="user" value="${command.user}" />
		
	<div id="selTab0" class="tab-pane ${(command.selTab eq 0) ? 'active' : ''}">
	
		<fieldset class="fsOuter">	
			
			<form:form cssClass="form-horizontal" name="account" method="post" action="/updateUserProfile?ckey=${ckey}" enctype="multipart/form-data">
				<fieldset>
					<legend><fmt:message key="navi.myprofile" /></legend>
			
				<!--+
					|
					| profile, contact, about 
					| 
				 	+-->
				
					<!-- Der Text der oben steht -->
					<p>
						<c:set var="urlGeneratorFOAF" value="${urlGeneratorFactory.createURLGeneratorForPrefix(properties['project.home'], 'foaf')}" />
						<fmt:message key="settings.profileInfo">
							<fmt:param><a href="${urlGeneratorFOAF.getUserUrl(user)}">/foaf/user/<c:out value="${user.name}"/></a></fmt:param>
							<fmt:param><fmt:message key="settings.profileViewableFor"/></fmt:param>
							<fmt:param value="/cv/user/${mtl:encodePathSegment(user.name)}"/>
						</fmt:message>
					</p>
				</fieldset>

			
			
				<!--+
					| username, realname, gender, birthday, place, profile privacy level
					+-->
				<!-- GENERAL INFORMATION -->
					
				<!-- Ueberschrift -->
				<fieldset>
					<legend><fmt:message key="post.resource.fields.general" /></legend>
					
						<!-- USER NAME -->
						<bsform:input path="user.name" labelColSpan="3" inputColSpan="9" noHelp="true" disabled="true" />
						
						<!-- REAL NAME -->
						<bsform:input path="user.realname" labelColSpan="3" inputColSpan="9" />
						
						<!-- GENDER -->
						<div class="form-group clearfix">
							<form:label cssClass="col-sm-3 control-label" path="user.gender"><fmt:message key="user.gender"/></form:label>
							
							<fmt:message var="male" key="settings.male"/>
							<fmt:message var="female" key="settings.female"/>
							<div class="col-sm-9">
								<form:select cssClass="form-control" path="user.gender">
									<form:option label="${male}" value="m"/>
									<form:option label="${female}" value="f"/>
								</form:select>
							</div>
							
							<div class="dissError"><form:errors path="user.gender" /></div>
						</div>
						
						<!-- BIRTHDAY -->
						<c:set var="formErrors"><form:errors path="user.birthday" /></c:set>					
						<div class="form-group clearfix ${not empty formErrors ? ' has-error' : ''}">
							<label class="col-sm-3 control-label" for="user.birthday"><fmt:message key="user.birthday"/></label>
							<div class="col-sm-9">
							   	<fmt:formatDate value="${command.user.birthday}" pattern="yyyy-MM-dd" var="userBirthday" />
							    <div class="input-group date" id="dp3" data-date="${empty userBirthday ? '1989-01-01' : userBirthday }" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
							    	<input class="form-control" type="text" name="user.birthday" value="${userBirthday }"/>
							    	<span class="input-group-addon"><i class="fa fa-calendar"><!--  --></i></span>
							    </div>
							    
							    <c:if test="${not empty formErrors}">
							    	<bsform:errors path="user.birthday" />
							    </c:if>
							</div>
						</div>
						
						<!-- PLACE -->
						<bsform:input path="user.place" labelColSpan="3" inputColSpan="9"  noHelp="true"/>
						
						<!-- PROFILE VIEWABLE FOR -->
						<div class="form-group clearfix">
							<form:label cssClass="col-sm-3 control-label" path="user.settings.profilePrivlevel"><fmt:message key="settings.profileViewableFor"/></form:label>
							
							<div class="col-sm-9">
								<form:select cssClass="form-control" path="user.settings.profilePrivlevel">
									<form:option label="public" value="PUBLIC"/>
									<form:option label="private" value="PRIVATE"/>
									<form:option label="friends" value="FRIENDS"/>
								</form:select>
							</div>
							
							<div class="dissError"><form:errors path="user.settings.profilePrivlevel" /></div>
						</div>
						
				</fieldset>

				<!--+ 
				 	| Contact (email, homepage, OpenURL)
				 	+-->
				<fieldset>
					<legend><fmt:message key="settings.contact" /></legend>
					
						<bsform:input path="user.email" required="true" labelColSpan="3" inputColSpan="9" />
						<bsform:input path="user.homepage" labelColSpan="3" inputColSpan="9" />
						<bsform:input path="user.openURL" labelColSpan="3" inputColSpan="9" />
						
				</fieldset>
		
				<!--+ 
				 	| hobbies, profession, institution, interests
				 	+-->
				<fieldset>
					<legend><fmt:message key="settings.aboutMe" /></legend>
					
						<bsform:input path="user.profession" noHelp="true" labelColSpan="3" inputColSpan="9" />
						<bsform:input path="user.institution" noHelp="true" labelColSpan="3" inputColSpan="9"/>
						<bsform:textarea id="user.interests" path="user.interests" noHelp="true" labelColSpan="3" inputColSpan="9"/>
						<bsform:textarea id="user.hobbies" path="user.hobbies" noHelp="true" labelColSpan="3" inputColSpan="9"/>
						
				</fieldset>
		
				<input type="hidden" name="action" value="update"/>
				<input type="hidden" name="selTab" value="0"/>

					<fieldset>
						<legend><fmt:message key="settings.picture.header" /></legend>
						
							<!-- current picture -->
							<div class ="form-group clearfix">
								<form:label cssClass="col-sm-3 control-label" path="file"><fmt:message key="settings.picture.current"/></form:label>
								<div class="col-sm-9">
									<a title="${mtl:encodePathSegment(user.name)}" href="/user/${mtl:encodePathSegment(user.name)}" class="img-thumbnail img-responsive">
										<img class="user-avatar" src="${relativeUrlGenerator.getUserPictureUrlByUsername(user.name)}" />
										<span><user:username user="${user}" noLink="${true}"/></span>
									</a>
								</div>
							</div>
							
							<!-- Picture Source -->
							<div class="form-group clearfix">
							
								<form:label cssClass="col-sm-3 control-label" path="user.useExternalPicture">
									<fmt:message key="user.useExternalPicture"/>
								</form:label>
								
								<fmt:message var="useExtService" key="user.useExternalPicture.service" />
								<fmt:message var="useExtTrue" key="user.useExternalPicture.true">
									<fmt:param>${useExtService}</fmt:param>
								</fmt:message>
								<fmt:message var="useExtFalse" key="user.useExternalPicture.false" />
								
								<div class="col-sm-9">
									<form:select cssClass="form-control help-popover" path="user.useExternalPicture" >
										<form:option label="${useExtTrue}" value="${true}"/>
										<form:option label="${useExtFalse}" value="${false}"/>
									</form:select >
									<div class="help help-header hide"><fmt:message key="user.useExternalPicture"/></div>
									<div class="help help-content hide">
										<p><fmt:message key="user.useExternalPicture.help"/></p>
										<ol>
											<li>
												<fmt:message key="user.useExternalPicture.help.true">
													<fmt:param>${useExtTrue}</fmt:param>
													<fmt:param>${useExtService}</fmt:param>
												</fmt:message>
											</li>
											<li>
												<fmt:message key="user.useExternalPicture.help.false">
													<fmt:param>${useExtFalse}</fmt:param>
												</fmt:message>
											</li>
										</ol>
									</div>
								</div>
								
								<div class="dissError"><form:errors path="user.gender" /></div>
							</div>
							
							<!-- New Picture + delete picture -->
							<div class="form-group clearfix">
								
								<!-- New Picture -->
								<label for="useIntPath" class="col-sm-3 control-label"><fmt:message key="settings.picture.file"/></label>
								<div class="col-sm-9">
									<input type="file" name="picturefile" id="useIntPath"/>
									<p class="help-block"><fmt:message key="settings.picture.help"/></p>
								</div>
								
								<!-- Delete picture -->
								<div class="col-sm-offset-3 col-sm-9">
									<div class="checkbox">
										<label>
											<input name="deletePicture" type="checkbox"/> <fmt:message key="settings.picture.delete"/>
										</label>
									</div>
									<p class="help-block"> <fmt:message key="settings.picture.delete.help"/> </p>
								</div>
								
							</div>
							

							
							<!-- save changes button -->
							<p class="clearfix"><!--  --></p>
							<div class="col-sm-offset-3">
								<fmt:message var="submitButton" key="settings.saveChanges" />
								<bsform:button type="submit" style="primary" size="defaultSize" value="${submitButton}" />
							</div>
							
						</fieldset>
					</form:form>
				</fieldset>
	</div>
</jsp:root>