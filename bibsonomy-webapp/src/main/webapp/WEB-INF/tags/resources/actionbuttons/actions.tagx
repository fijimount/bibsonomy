<!-- TODO: abstract view for actions @see bookmark/actions -->
<jsp:root version="2.0" xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons">
	
	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" />
	<jsp:directive.attribute name="loginUserName" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="disableResourceLinks" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="resourceType" type="java.lang.String" required="true"/>
	

	<c:set var="buttonClasses" value="btn btn-default btn-xs "/>
	<c:set var="isRemotePost" value="${not empty post.systemUrl and post.systemUrl ne properties['project.home']}"/>

	<div class="edit-media-buttons btn-group btn-group-xs dropdown dropdown-align-right">
		<!-- edit group entry -->
		<c:set var="editGroupPost" value="false"/>
		<c:forEach items="${command.context.loginUser.groups}" var="group">
			<c:if test="${group.name eq post.user.name}">
				<c:forEach items="${group.memberships}" var="g">
					<c:if test="${g.groupRole eq 'MODERATOR' || g.groupRole eq 'ADMINISTRATOR' }">
						<c:set var="editGroupPost" value="true"/>
					</c:if>
				</c:forEach>
			</c:if>
		</c:forEach>
		<c:if test="${editGroupPost}">
			<buttons:editGroupPostButton
				post ="${post}" 
				resourceType="${resourceType}"
				buttonClasses="${buttonClasses}"/>
		</c:if>
		
		<!-- edit/copy -->
		<c:choose>
			<c:when test="${command.context.userLoggedIn and (loginUserName eq post.user.name) and not isRemotePost}">
				<buttons:editPostButton
					post ="${post}" 
					resourceType="${resourceType}"
					buttonClasses="${buttonClasses}"/>
			</c:when>
			<c:otherwise>
				<buttons:copyPostButton 
					post ="${post}" 
					userLoggedIn="${command.context.userLoggedIn}" 
					resourceType="${resourceType}"
					buttonClasses="${buttonClasses}"/>
			</c:otherwise>
		</c:choose>

		<!-- remove/delete -->
		<c:choose>
			<c:when test="${post.inboxPost}">
				<buttons:inboxRemoveButton 
					post ="${post}" 
					resourceType="${resourceType}"
					buttonClasses="${buttonClasses}"/>
			</c:when>
			<c:otherwise>
				<buttons:deletePostButton 
					post ="${post}" 
					loginUser="${command.context.loginUser}"
					userLoggedIn="${command.context.userLoggedIn}" 
					resourceType="${resourceType}"
					buttonClasses="${buttonClasses}"/>
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when test="${resourceType eq 'bookmark'}">
				<buttons:mementoButton post="${post}" buttonClasses="${buttonClasses}"/>
			</c:when>
			<c:otherwise>
				<!-- pick/unpick -->
				<buttons:pickUnpickButton 
					post ="${post}" 
					userLoggedIn="${command.context.userLoggedIn}" 
					buttonClasses="${buttonClasses}"/>
			</c:otherwise>
		</c:choose>


		<buttons:dropDownList>
			<jsp:attribute name="listContent">
				<li><buttons:communityPostMenuItem post="${post}" resourceType="${resourceType}"/></li>
			
				<buttons:historyMenuItem 
					post ="${post}" 
					loginUserName="${loginUserName}" 
					userLoggedIn="${command.context.userLoggedIn}" 
					resourceType="${resourceType}"
					buttonClasses="${buttonClasses}"/>
					
				<c:if test="${command.context.userLoggedIn and post.user.name ne loginUserName}">
					<li class="divider"><!--  --></li>
					<buttons:reportUserMenuItem 
						post="${post}"
						userName="${post.user.name}" 
						loginUser="${command.context.loginUser}"
						buttonClasses="${buttonClasses}"/>
				</c:if>

				<c:if test="${resourceType eq 'bibtex'}">

					<spring:eval var="intraHashId" expression="T(org.bibsonomy.common.enums.HashID).INTRA_HASH.id" />

					<li class="divider"><!--  --></li>
					<!--+
						| RESOURCE LINKS (URL, DOI, BIBTEX, OPEN URL)
						+-->
					<c:if test="${not disableResourceLinks}">
						<!-- URL -->
						<c:choose>
							<c:when test="${not empty post.resource.url}">
								<li>
									<fmt:message key="bibtex.actions.url.title" var="urlTitle" />
									<a class="litem" href="${fn:escapeXml(mtl:cleanUrl(post.resource.url))}" title="${urlTitle}"><fmt:message key="bibtex.actions.url"/></a>
								</li>
							</c:when>
							<c:otherwise>
								<li class="disabled">
									<fmt:message key="bibtex.actions.url.inactive" var="urlTitle" />
									<a href="#" title="${urlTitle}"><fmt:message key="bibtex.actions.url"/></a>
								</li>
							</c:otherwise>
						</c:choose>

						<!-- DOI -->
						<c:set var="doi" value="${mtl:extractDOI(post.resource.miscFields['doi'])}" />
						<c:choose>
							<c:when test="${not empty doi}">
								<li>
									<fmt:message key="bibtex.actions.doi" var="urlTitle"/>
									<a class="litem" href="https://doi.org/${mtl:encodePathSegment(doi)}" title="${urlTitle}"><fmt:message key="bibtex.actions.doi"/></a>
								</li>
							</c:when>
							<c:otherwise>
								<li class="disabled">
									<fmt:message key="bibtex.actions.doi.inactive" var="urlTitle"/>
									<a href="#" title="${urlTitle}"><fmt:message key="bibtex.actions.doi"/></a>
								</li>
							</c:otherwise>
						</c:choose>
						
						<li class="divider"><!--  --></li>
						
						<!-- BibTeX or Endnote or JABREF or CSL-->
						<!-- checks whether the source is "simple" which gets a special treatment,
							or JABREF / CSL which will be assigned a source URL by generator-->
						<c:forEach items="${command.context.loginUser.settings.favouriteLayouts}" var="citation">
							<li>
								<c:set var="name"><mtl:favouriteLayoutDisplayName favouriteLayout="${citation}"/></c:set>
								<c:set var="shortName" value="${mtl:shorten(name, 25)}" />
								<fmt:message key="publication.export.${fn:toLowerCase(citation.source)}_${fn:toLowerCase(citation.style)}.title" var="exportTitle" />
								<c:if test="${fn:startsWith(exportTitle, '???')}">
									<fmt:message key="publication.export.title" var="exportTitle">
										<fmt:param value="${name}" />
									</fmt:message>
								</c:if>
								
								<a class="litem publ-export" data-source="${citation.source}" data-style="${citation.style}" href="${relativeUrlGenerator.getPostExportUrl(post, citation)}?formatEmbedded=true" title="${exportTitle}">
									<c:out value="${shortName}" />
								</a>
							</li>
						</c:forEach>
						
						
						<!-- OPENURL -->
						<c:set var="userOpenURL" value="${command.context.loginUser.openURL}" />
						<c:set var="systemOpenURLProvider" value="${properties['system.openurl.provider']}" />
						<c:if test="${command.context.userLoggedIn and (not empty userOpenURL or not empty systemOpenURLProvider)}">
							<spring:eval var="openUrl" expression="T(org.bibsonomy.model.util.BibTexUtils).getOpenurl(post.resource)" />
							<li class="divider"><!-- keep me --></li>
							<c:choose>
								<c:when test="${not empty userOpenURL}">
									<fmt:message key="bibtex.actions.openurl.title" var="openurlTitle" />
									<li>
										<a class="litem" href="${fn:escapeXml(userOpenURL)}?${fn:escapeXml(openUrl)}" title="${openurlTitle}"><fmt:message key="bibtex.actions.openurl"/></a>
									</li>
								</c:when>
								<c:when test="${empty systemOpenURLProvider}">
									<li>
										<fmt:message key="bibtex.actions.openurl.inactive" var="openurlTitle" />
										<span class="ilitem" title="${openurlTitle}"><fmt:message key="bibtex.actions.openurl"/></span>
									</li>
								</c:when>
							</c:choose>
							<c:if test="${not empty systemOpenURLProvider}">
								<fmt:message key="bibtex.actions.openurl.system.title" var="openurlTitle" />
								<li>
									<a class="litem" href="${fn:escapeXml(systemOpenURLProvider)}?${fn:escapeXml(openUrl)}" title="${openurlTitle}"><fmt:message key="bibtex.actions.openurl.system"/></a>
								</li>
							</c:if>
						</c:if>
					</c:if>

					<c:if test="${mtl:userIsGroup(command.context.loginUser)}">
						<li class="divider"><!--  --></li>
						<li>
							<!-- TODO: get the group the group user represents -->
							<c:forEach var="group" items="${command.context.loginUser.groups}">
								<c:set var="reportingUrl" value="${group.publicationReportingSettings.externalReportingUrl}" />
								<c:if test="${not empty reportingUrl and group.name eq command.context.loginUser.name}">
									<a class="item editqa" href="${reportingUrl}/edit?intraHash=${post.resource.intraHash}&amp;referer=${properties['project.home']}${fn:escapeXml(requPath)}"><!-- keep me --></a>
								</c:if>
							</c:forEach>
						</li>
					</c:if>

					<c:if test="${mtl:hasTagMyown(post) and properties['publication.reporting.mode'] eq 'GROUP' and not mtl:userIsGroup(command.context.loginUser)}">
						<li class="divider"><!--  --></li>
						<li>
							<c:forEach var="group" items="${command.context.loginUser.groups}">
								<c:set var="reportingUrl" value="${group.publicationReportingSettings.externalReportingUrl}" />
								<c:if test="${not mtl:hasReportedSystemTag(post.tags, group.name) and not empty reportingUrl}">
									<fmt:message key="group.reporting" var="reportDesc">
										<fmt:param value="${group.name}" />
									</fmt:message>
									<a class="item report" href="${reportingUrl}/report?intraHash=${post.resource.intraHash}&amp;user=${post.user.name}&amp;referer=${properties['project.home']}${fn:escapeXml(requPath)}" title="${reportDesc}"><!-- keep me --></a>
								</c:if>
							</c:forEach>
						</li>
					</c:if>
					<!-- UnAPI -->
					<li><abbr class="unapi-id" title="${post.resource.intraHash}/${fn:escapeXml(post.user.name)}"><c:out value=" "/></abbr></li>
				</c:if>

			</jsp:attribute>
		</buttons:dropDownList>
	</div>
</jsp:root>
	
	
