<jsp:root version="2.0" xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:fontawsome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:discussion="urn:jsptagdir:/WEB-INF/tags/resources/discussion">

	<jsp:directive.attribute name="discussionItem" type="org.bibsonomy.model.DiscussionItem" required="false" />
	<jsp:directive.attribute name="resource" type="org.bibsonomy.model.Resource" required="true" />
	<jsp:directive.attribute name="discussionItemClass" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="additionalInputs" fragment="true" required="false" />
	<jsp:directive.attribute name="additionalMenuItems" fragment="true" required="false" />
	<jsp:directive.attribute name="userName" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="postUserName" type="java.lang.String" required="true" />
	<jsp:directive.attribute name="rows" type="java.lang.Integer" required="false" />
	<jsp:directive.attribute name="parentItem" type="org.bibsonomy.model.DiscussionItem" required="false" />
	
	<c:set var="action" value="create"/>
	<c:if test="${not empty discussionItem}">
		<c:set var="action" value="update" />
	</c:if>
	<form action="/ajax/${discussionItemClass}s" method="post" class="simple-${discussionItemClass}-form ${action}${discussionItemClass}">
		<c:if test="${not empty discussionItem}">
			<input type="hidden" value="PUT" name="_method" />
		</c:if>
		<input name="ckey" value="${command.context.ckey}" type="hidden" />
		<input name="hash" value="${resource.interHash}" type="hidden" />
		<input name="postUserName" value="${postUserName}" type="hidden" />
		<input name="intraHash" value="${command.intraHash}" type="hidden" />

		<c:if test="${not empty discussionItem}">
			<input name="discussionItem.hash" value="${discussionItem.hash}" type="hidden" />
		</c:if>
		
		<c:if test="${not empty parentItem}">
			<input name="discussionItem.parentHash" value="${parentItem.hash}" type="hidden" />
		</c:if>
		
		<c:set var="grouping" value="public" />
		<c:if test="${not empty discussionItem.groups}">
			<c:set var="grouping" value="other" />
			<c:set var="groupName" value="${discussionItem.groups.iterator().next().name}" />
		</c:if>
		<input name="abstractGrouping" value="${grouping}" type="hidden" />
		<input name="groups" value="${groupName}" type="hidden" />
		
		<div class="media">
			<c:set var="colspan" value="12" />
			<c:if test="${empty discussionItem}">
				<div class="media-left">
					<a>
						<img src="${relativeUrlGenerator.getUserPictureUrlByUsername(command.context.loginUser.name)}" alt="userPicture" class="media-object img-circle simple-user-pic"/>
					</a>
				</div>
			</c:if>
			<div class="media-body">
				<c:if test="${empty rows}">
					<c:set var="rows" value="1" />
				</c:if>
				<c:if test="${empty discussionItem}">
					<fmt:message var="placeholder" key="post.resource.${discussionItemClass}.placeholder"/>
				</c:if>
				<c:if test="${not empty additionalInputs}">
					<jsp:invoke fragment="additionalInputs" />
				</c:if>
				<textarea placeholder="${placeholder}" class="form-control reqinput" rows="${rows}" name="discussionItem.text" data-autosize="autosize">
					<c:if test="${not empty discussionItem}">
						<c:out value="${discussionItem.text}" />
					</c:if>
				</textarea>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-md-offset-5">
				<div class="checkbox">
					<label>
						<c:choose>
							<c:when test="${discussionItem.anonymous}">
								<input type="checkbox" checked="checked" name="discussionItem.anonymous"/>
							</c:when>
							<c:otherwise>
								<input type="checkbox" name="discussionItem.anonymous"/>
							</c:otherwise>
						</c:choose>
						 <fmt:message key="post.resource.discussionItem.anonymous" />
					</label>
				</div>
			</div>
			<div class="col-md-5">
				<div class="btn-toolbar pull-right" role="toolbar">
					<div class="btn-group group-selector">
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="group">
								<c:choose>
									<c:when test="${grouping eq 'public'}">
										<fontawsome:icon icon="globe" /><c:out value=" " /><fmt:message key="post.resource.public" />
									</c:when>
									<c:when test="${grouping eq 'private'}">
										<fontawsome:icon icon="lock" /><c:out value=" " /><fmt:message key="post.resource.private" />
									</c:when>
									<c:when test="${grouping eq 'friends'}">
										<fontawsome:icon icon="users" /><c:out value=" " /><fmt:message key="post.resource.friends" />
									</c:when>
									<c:otherwise>
										<fontawsome:icon icon="users" /> <c:out value=" ${groupName}" />
									</c:otherwise>
								</c:choose>
							</span><c:out value=" " /><span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<c:set var="selected" value="${grouping eq 'public'}" />
							<li class="${selected ? 'checked' : ''}">
								<a href="#" data-group="" data-abstract-grouping="public"> <fontawsome:icon icon="globe" fixedWidth="${true}"/> <fmt:message key="post.resource.public" /></a>
							</li>
							<c:set var="selected" value="${grouping eq 'private'}" />
							<li class="${selected ? 'checked' : ''}">
								<a href="#" data-group="" data-abstract-grouping="private"><fontawsome:icon icon="lock" fixedWidth="${true}"/> <fmt:message key="post.resource.private" /></a>
							</li>
							<c:set var="selected" value="${grouping eq 'friends'}" />
							<li class="${selected ? 'checked' : ''}">
								<a href="#" data-group="" data-abstract-grouping="friends"><fontawsome:icon icon="users" fixedWidth="${true}"/> <fmt:message key="post.resource.friends" /></a>
							</li>
							<li role="separator" class="divider"></li>
							<c:forEach var="group" items="${command.context.loginUser.groups}">
								<c:set var="selected" value="${groupName eq group.name}" />
								<li class="${selected ? 'checked' : ''}">
									<a href="#" data-abstract-grouping="other" data-group="${group.name}"><fontawsome:icon icon="users" fixedWidth="${true}"/><c:out value=" ${group.name}"/></a>
								</li>
							</c:forEach>
						</ul>
					</div>
					<div class="btn-group">
						<button type="submit" class="btn btn-primary btn-xs"><fmt:message key="post.resource.${discussionItemClass}.${action}"/></button>
					</div>
				</div>
			</div>
		</div>
		
	</form>

</jsp:root>