<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:export="urn:jsptagdir:/WEB-INF/tags/export/bibtex"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form">
	
	<!--+
		|
		| The title and complete metadata of the publication, shown in a tab
		| with additional tabs to retrieve the metadata in a different format.
		|
		| NOTE: to get this working, the JavaScript code in publication.js to
		| initialize the TABs is required! Hence, if you re-use this tag file,
		| ensure that either the JavaScript code is included here (and removed
		| from publication.js) or ensure that publication.js is loaded. 
		| 
		+-->
	
	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true" /> 
	<jsp:directive.attribute name="isCommunityPost" type="java.lang.Boolean" required="false" />
	
	<c:if test="${empty isCommunityPost}">
		<c:set var="isCommunityPost" value="${false}" />
	</c:if>
	
	<div class="citation-box">
		<ul class="nav nav-tabs" data-publication-url="${relativeUrlGenerator.getPostUrl(post)}">
			<c:forEach items="${command.context.loginUser.settings.favouriteLayouts}" var="citation">
				<!-- checks whether the source is "simple" which gets a special treatment,
					or JABREF / CSL which will be assigned a source URL by generator-->
				<!-- source and style always uppercase! -->
				<c:choose>
					<c:when test="${citation.source=='SIMPLE'}">
						<!-- source and style always uppercase! -->
						<c:if test="${citation.style=='BIBTEX'}">
							<li>
								<a data-toggle="tab" href="#citation_BibTeX">BibTeX</a>
							</li>
						</c:if>
						<!-- source and style always uppercase! -->
						<c:if test="${citation.style=='ENDNOTE'}">
							<li>
								<a data-toggle="tab" href="#citation_EndNote">Endnote</a>
							</li>
						</c:if>
					</c:when>
					<c:otherwise>
						<li>

							<c:choose>
								<c:when test="${isCommunityPost}">
									<c:set var="exportUrl" value="${relativeUrlGenerator.getPostExportUrl(post, citation, null)}?formatEmbedded=true&amp;items=1" />
								</c:when>
								<c:otherwise>
									<c:set var="exportUrl" value="${relativeUrlGenerator.getPostExportUrl(post, citation)}?formatEmbedded=true&amp;items=1" />
								</c:otherwise>
							</c:choose>

							<!-- encoding ID jquery friendly, because CUSTOM files have format "CUSTOM_USERNAME_LAYOUTNAME.CSL" which jquery strongly dislikes -->
							<a class="favouriteLayoutDisplayName" data-source="${citation.source}" data-style="${citation.style}" data-toggle="tab" href="#citation_${fn:replace(fn:replace(citation.style, '.CSL', ''), ' ', '-')}" data-formatUrl="${exportUrl}">
								<c:set var="name"><mtl:favouriteLayoutDisplayName favouriteLayout="${citation}"/></c:set>
								<c:out value="${mtl:shorten(name, 25)}" />
							</a>
						</li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<li>
				<c:choose>
					<c:when test="${isCommunityPost}">
						<c:set var="exportUrl" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'export').getPublicationUrl(post.resource)}?formatEmbedded=true&amp;items=1" />
					</c:when>
					<c:otherwise>
						<c:set var="exportUrl" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'export').getPublicationUrlByPost(post)}?formatEmbedded=true" />
					</c:otherwise>
				</c:choose>
				<a id="citation-box-citation-all-button" data-toggle="tab" href="#citation_all" data-formatUrl="${exportUrl}"><fmt:message key="bibtex.all_exports" /></a>
			</li>
		</ul>
		
		<div id="citation-styles" class="tab-content pub-citation-tab-content">
			<c:forEach items="${command.context.loginUser.settings.favouriteLayouts}" var="citation">
				<!-- source and style always uppercase! -->
				<c:if test="${citation.source=='SIMPLE'}">
					<!-- source and style always uppercase! -->
					<c:if test="${citation.style=='BIBTEX'}">
						<div class="tab-pane" id="citation_BibTeX">
							<textarea style="width:100%;font-size:80%;border:none;" rows="15">
								<export:bibtex post="${post}" escapeXml="${true}" lastFirstNames="${true}" generatedBibtexKeys="${false}"/>
							</textarea>
						</div>
					</c:if>
					<!-- source and style always uppercase! -->
					<c:if test="${citation.style=='ENDNOTE'}">
						<div class="tab-pane" id="citation_EndNote">
							<textarea style="width:100%;font-size:80%;border:none;" rows="15" readonly="readonly">
								<export:endnote post="${post}" escapeXml="${true}" />
							</textarea>
						</div>
					</c:if>
				</c:if>
				<!-- source and style always uppercase! -->
				<c:if test="${(citation.source=='JABREF') || (citation.source=='CSL')}">
					<div class="tab-pane" id="citation_${fn:replace(fn:replace(citation.style, '.CSL', ''), ' ', '-')}">
						<!-- keep me -->
					</div>
				</c:if>
			</c:forEach>
			<div class="tab-pane" id="citation_all">
				<!-- keep -->
			</div>
			<!-- one button for all citations! -->
			<bsform:button id="copyToLocalClipboard_citationBoxButton" className="copyToLocalClipboard_citationBox" size="defaultSize" style="defaultStyle" valueKey="export.copyToLocalClipboard" icon="clipboard" block="${true}" />
		</div>
	</div>
</jsp:root>