<jsp:root version="2.0"
		  xmlns:c="http://java.sun.com/jsp/jstl/core"
		  xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
		  xmlns:fn="http://java.sun.com/jsp/jstl/functions"
		  xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
		  xmlns:spring="http://www.springframework.org/tags"
		  xmlns:jsp="http://java.sun.com/JSP/Page">
	
	<jsp:directive.attribute name="publication" type="org.bibsonomy.model.BibTex" required="true" />
	<jsp:directive.attribute name="useAbsoluteURL" type="java.lang.Boolean" required="false" />
	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="false"/>
	<jsp:directive.attribute name="restrictToFirstAuthors" type="java.lang.Boolean" required="false"/>
	<jsp:directive.attribute name="onlyLinkAuthorsWithPersons" type="java.lang.Boolean" required="false" />

	<c:if test="${empty restrictToFirstAuthors}">
		<c:set var="restrictToFirstAuthors" value="${true}" />
	</c:if>

	<c:if test="${empty onlyLinkAuthorsWithPersons}">
		<c:set var="onlyLinkAuthorsWithPersons" value="${false}" />
	</c:if>

	<c:choose>
		<c:when test="${not empty post}">
			<c:set var="resourceType" value="${post.resource.getClass().getSimpleName()}"/>
			<c:set var="isRemotePost" value="${not empty post.systemUrl and not mtl:isSameHost(post.systemUrl, properties['project.home'])}" />
		</c:when>
		<c:otherwise>
			<c:set var ="isRemotePost" value="false"/>
		</c:otherwise>
	</c:choose>
	<c:set var="urlGenerator" value="${relativeUrlGenerator}" />
	<c:if test="${useAbsoluteURL}">
		<c:set var="urlGenerator" value="${absoluteUrlGenerator}" />
	</c:if>
	
	<!-- initialize with empty list -->
	<c:set var="personList" value=""/>
	<c:set var="authorEditor" value=""/>
	<c:choose>
		<c:when test="${not empty publication.author}">
			<c:set var="personList" value="${publication.author}"/>
			<c:set var="authorEditor" value="author"/>
			<c:set var="authorEditorUppper" value="AUTHOR"/>
			<c:set var="personMessageKey" value="author" />
		</c:when>
		<c:when test="${not empty publication.editor}">
			<c:set var="personList" value="${publication.editor}"/>
			<c:set var="authorEditor" value="editor"/>
			<c:set var="authorEditorUppper" value="EDITOR"/>
			<c:set var="personMessageKey" value="editor" />
		</c:when>
	</c:choose>

	<c:set var="numberOfPersons" value="${fn:length(personList)}" />
	<c:set var="maxPersonsToShow" value="${restrictToFirstAuthors ? 10 : numberOfPersons}" />

	<c:set var="personsToShow" value="${personList}" />
	<c:if test="${numberOfPersons gt maxPersonsToShow}">
		<spring:eval var="personsToShow" expression="personList.subList(0, maxPersonsToShow)" />
	</c:if>

	<c:set var="numberOfPersonsNotShown" value="${numberOfPersons - maxPersonsToShow}" />
	<c:set var="personListRestricted" value="${numberOfPersonsNotShown gt 0}" />

	<span class="authorEditorList ${not restrictToFirstAuthors ? 'show-more-person-list' : ''}" data-person-type="${personMessageKey}">
		<c:forEach var="person" items="${personsToShow}" varStatus="loopStatus">
			<!--
			we don't need to escapeXml when building person name, because
			it is displayed later only via c:out, which escapes XML chars
			anyway.
			-->
			<c:if test="${not empty post}">
				<spring:eval expression="T(org.bibsonomy.model.util.PersonResourceRelationUtils).getRelationForIndex(post.resourcePersonRelations, loopStatus.index)" var="relation" />
			</c:if>

			<c:set var="firstName" value="${mtl:cleanBibtex(person.firstName)}" />
			<c:set var="lastName" value="${mtl:cleanBibtex(person.lastName)}" />
			<c:choose>
				<c:when test="${not empty firstName and not empty lastName}">
					<c:set var="personName" value="${fn:substring(person.firstName, 0, 1)}. ${lastName}" />
				</c:when>
				<c:otherwise>
					<c:set var="personName" value="${lastName}" />
				</c:otherwise>
			</c:choose>
			<c:if test="${loopStatus.last and not loopStatus.first and not personListRestricted}">
				<c:out value=" "/><fmt:message key="and"/><c:out value=" "/>
			</c:if>
			<c:choose>
				<c:when test="${properties['genealogy.activated'] eq 'true'}">
					<c:url var='pubAuthorUrl' value='${urlGenerator.getDisambiguationUrl(publication.interHash, authorEditorUppper, loopStatus.index)}'/>
				</c:when>
				<c:otherwise>
					<c:url var='pubAuthorUrl' value='${urlGenerator.getAuthorUrlByPersonName(person)}'/>
				</c:otherwise>
			</c:choose>
			<span>
				<c:choose>
					<c:when test="${isRemotePost}">
						<a href="${urlGeneratorFactory.createURLGeneratorForSystem(post.systemUrl).getAuthorUrlByPersonName(person)}">
							<c:out value="${personName}" />
						</a>
					</c:when>
					<c:otherwise>
						<span itemprop="${authorEditor}" itemscope="itemscope" itemtype="http://schema.org/Person">
							<c:choose>
								<c:when test="${not onlyLinkAuthorsWithPersons or onlyLinkAuthorsWithPersons and relation.present and relation.get().person.college eq properties['system.cris.college']}">
									<a href="${pubAuthorUrl}" itemprop="url" title="${fn:escapeXml(firstName)} ${fn:escapeXml(lastName)}">
										<span itemprop="name"><c:out value="${personName}"/></span>
									</a>
								</c:when>
								<c:otherwise>
									<span itemprop="name"><c:out value="${personName}"/></span>
								</c:otherwise>
							</c:choose>

						</span>
					</c:otherwise>
				</c:choose>
				<!-- append separator "," -->
				<c:if test="${not loopStatus.last}"><c:out value=", "/></c:if>
			</span>
		</c:forEach>
		<c:if test="${personListRestricted}">
			<c:out value=" "/><fmt:message key="and"/><c:out value=" ${numberOfPersonsNotShown} "/><fmt:message key="persons.others" /><c:out value=" "/><fmt:message key="post.resource.${personMessageKey}" />
		</c:if>
		<!-- append "(eds.)" -->
		<c:if test="${empty publication.author and not empty publication.editor}">
			<c:out value=" ("/><fmt:message key="bibtex.editors.abbr"/>).
		</c:if>
	</span>
	<c:out value=". "/>
</jsp:root>