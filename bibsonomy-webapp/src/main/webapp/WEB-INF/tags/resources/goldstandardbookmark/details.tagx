<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:book="urn:jsptagdir:/WEB-INF/tags/resources/bookmark"
	xmlns:discussion="urn:jsptagdir:/WEB-INF/tags/resources/discussion"
	xmlns:review="urn:jsptagdir:/WEB-INF/tags/resources/discussion/review"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:goldstandardbookmark="urn:jsptagdir:/WEB-INF/tags/resources/goldstandardbookmark"
	xmlns:common="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:goldstandard="urn:jsptagdir:/WEB-INF/tags/resources/goldstandard"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons">
	
	<jsp:directive.attribute name="post" type="org.bibsonomy.model.Post" required="true"/>
	<jsp:directive.attribute name="loginUser" type="org.bibsonomy.model.User" required="true"/>
	<jsp:directive.attribute name="postOfLoggedInUser" type="org.bibsonomy.model.Post" required="false"/>
	<jsp:directive.attribute name="postUserName" type="java.lang.String" required="true"/>
	<jsp:directive.attribute name="postIsPublic" type="java.lang.Boolean" required="true"/>
	
	<fmt:message key="goldStandard.approved.title" var="approved"/>
	<c:set var="goldstandard" value="${post.resource}" />
	<c:set var="isRealGoldstandard" value="${goldstandard['class'].simpleName eq 'GoldStandardBookmark' }" />
	
	<goldstandard:actionHeader showEditOnly="${true}" post="${post}" communityPostExists="${isRealGoldstandard}" resourceType="bookmark" loginUser="${loginUser}" postOfLoggedInUser="${postOfLoggedInUser}" />
	
	<c:set var="itemType" value="http://schema.org/WebPage"/>
	<div id="goldstandard" class="row">
		<div class="col-md-7 col-md-offset-1" itemtype="${itemType}" itemscope="itemscope" >
			<div class="content-header" id="info">
				<h1>
					<a href="${fn:escapeXml(goldstandard.url)}" ref="nofollow" itemprop="url">
						<span itemprop="name"><c:out value="${goldstandard.title}" /></span>
					</a>
					<c:if test="${post.approved}">
						<fmt:message key="goldStandard.approved.title" var="approved"/>
						<span class="fa fa-check-circle community-approved" data-toggle="tooltip" data-placement="bottom" title="${fn:escapeXml(approved)}"><!-- keep me --></span>
					</c:if>
				</h1>
			</div>
			
			<c:set var="description" value="${post.description}" />
			<c:if test="${not empty description}">
				<div class="meta" itemprop="description">
					${mtl:markdownToHtml(description)}
				</div>
			</c:if>
			
			<c:set var="imgUrl" value="${properties['project.img']}" />
			<c:if test="${not empty imgUrl}">
				<div class="preview row">
					<div class="col-md-12 text-center">
						<a href="${fn:escapeXml(goldstandard.url)}" rel="nofollow"><img itemprop="image" src="${imgUrl}/preview/bookmark/${goldstandard.intraHash}?preview=LARGE" /></a>
					</div>
				</div>
			</c:if>
			
			<!-- tags -->
			<goldstandard:tags tagCloudCommand="${command.tagcloud}" tagsOfLoggedinUser="${postOfLoggedInUser.tags}" />
			
			<!-- users -->
			<common:relatedUsers userList="${command.relatedUserCommand.relatedUsers}" />
			
			<div id="discussion-section">
				<!--+
					| discussion and rating
					+-->
				<h2 class="pub-details">
					<fmt:message key="publications.discussion.headline"/>
				</h2>
				
				<!-- Aggregated RatingInfo -->
				<review:reviewRatingsInfo resource="${post.resource}" />
		
				<!-- bookmark reference link -->
				<!--<discussion:linkBox resource="${post.resource}"/>-->
				
				<!-- discussion -->
				<discussion:discussion resource="${goldstandard}" loginUser="${loginUser}"  postUserName="${postUserName}" postIsPublic="${postIsPublic}"/>
			</div>
		</div>
		<div class="col-md-3 hidden-xs" id="sidebar-nav">
			<nav class="sidebar-nav" data-spy="affix" data-offset-top="275">
				<ul class="nav">
					<li>
						<a href="#info"><fmt:message key="community.generalinformation" /></a>
					</li>
					<li>
						<a href="#tags">Tags</a>
					</li>
					<li>
						<a href="#users"><span class="capitalize"><fmt:message key="users"/></span></a>
					</li>
					<li>
						<a href="#discussion-section"><fmt:message key="publications.discussion.headline"/></a>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</jsp:root>