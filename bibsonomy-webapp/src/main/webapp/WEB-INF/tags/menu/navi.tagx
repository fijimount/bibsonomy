<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:login="urn:jsptagdir:/WEB-INF/tags/actions/login"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:menu="urn:jsptagdir:/WEB-INF/tags/menu">
	
	<jsp:directive.attribute name="loginUser" type="org.bibsonomy.model.User" required="true"/>
	<jsp:directive.attribute name="activeTab" type="java.lang.String" required="false"/>
	
	<c:set var="loginUserName" value="${loginUser.name}" />
	<c:set var="userLoggedIn" value="${not empty loginUserName}" />

	<c:set var="showPopularNav" value="${properties['system.nav.popular']}" />
	<c:set var="showProjectsNav" value="${properties['system.cris.project.links']}" />

	<c:set var="crisSystem" value="${properties['system.cris'] eq 'true'}" />
	<c:set var="showCRISNavBar" value="${crisSystem and not userLoggedIn}"/>
	<c:set var="userIsAdmin" value="${loginUser.role eq 'ADMIN' }" />

	<nav class="navbar navbar-bibsonomy" role="navigation" id="navigation">
		<div class="container navi noborder">
			<div class="navbar-header">
				<!-- placeholder for collapsed menu -->
				<button type="button" class="navbar-toggle btn btn-default navbar-btn" data-toggle="collapse" data-target="#bs-navbar-collapse">
					<span class="sr-only"><fmt:message key="navi.navigation.toggle"/></span>
					<span class="icon-bar"><!--  --></span>
					<span class="icon-bar"><!--  --></span>
					<span class="icon-bar"><!--  --></span>
				</button>

				<!-- placeholder for user menu -->
				<button id="menu-user-icon-toggle-button" style="border:none" type="button" class="navbar-toggle btn btn-default navbar-btn" data-toggle="collapse" data-target="#bs-navbar-user-collapse">
					<span class="sr-only"><fmt:message key="navi.navigation.toggle"/></span>
					<span class="fa fa-user"><!-- KEEP ME --></span>
					<b id="caret-with-margin" class="caret">&amp;nbsp;</b>
				</button>
				<c:if test="${not empty loginUserName}">
					<span class="navbar-toggle navbar-btn visible-xs"><user:username user="${loginUser}" /></span>
				</c:if>
			</div>
			
			<!--  hidden menu for user dropdown  -->
			<div id="bs-navbar-user-collapse" class="collapse">
				<ul class="navbar-nav nav visible-xs">
					<c:choose>
						<c:when test="${not empty loginUserName}" >
							<menu:userDropdown loginUser="${loginUser}" />
						</c:when>
						<c:otherwise>
							<menu:registerSignUpButtons />
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
			<c:set var="urlGeneratorDiscussed" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'discussed')}" />
			<div id="bs-navbar-collapse" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="${activeTab eq 'home' ? 'active' : ''}"><a href="${relativeUrlGenerator.getProjectHome()}"><fmt:message key="navi.home"/></a></li>
					<!--+ 
				 		| myProjectName
				 		+-->
					<c:if test="${not empty loginUserName}">
						<li class="dropdown ${activeTab eq 'my' ? 'active' : ''}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navi.my"/>${properties['project.name']}<b class="caret">&amp;nbsp;</b></a>
							<ul class="dropdown-menu">
								<li><a href="${relativeUrlGenerator.getUserUrlByUserName(loginUserName)}"><fmt:message key="navi.myPosts"/></a></li>
								<c:if test="${not loginUser.settings.layoutSettings.simpleInterface}">
									<li><a href="${relativeUrlGenerator.getViewablePrivateUrl()}"><fmt:message key="navi.privatePosts"/></a></li>
									<li><a href="${relativeUrlGenerator.getViewableFriendsUrl()}"><fmt:message key="navi.friendsPosts"/></a></li>
									<li><a href="${relativeUrlGenerator.getUserUrlByUserName(loginUserName)}?filter=JUST_PDF"><fmt:message key="navi.pdfs"/></a></li>
									<li><a href="${relativeUrlGenerator.getUserUrlByUserName(loginUserName)}?filter=DUPLICATES"><fmt:message key="navi.duplicates"/></a></li>
									<li> 
										<fmt:message var="concepts" key="navi.concepts" />
										<a href="${relativeUrlGenerator.getConceptsUrlByString(loginUserName)}" title="${concepts}">
											<fmt:message key="navi.concepts" />
										</a>
									</li>
								</c:if>
								<li><a href="${urlGeneratorDiscussed.getUserUrlByUserName(loginUserName)}"><fmt:message key="navi.discussedPosts"/></a></li>
								<li><a href="/followers"><fmt:message key="navi.followedPosts"/></a></li>
								<li><a href="/friends"><fmt:message key="navi.postsOfFriends"/></a></li>
								
								<c:if test="${not loginUser.settings.layoutSettings.simpleInterface}">
									<li class="divider"><!-- KEEP ME --></li>
									<li><a href="/cv/user/${fn:escapeXml(loginUserName)}"><fmt:message key="navi.cv"/></a></li>
									<li><a href="${relativeUrlGenerator.getMySearchUrl()}"><fmt:message key="navi.browse"/></a></li>
									<c:set var="urlGeneratorBib" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'bib')}" />
									<li><a href="${urlGeneratorBib.getUserUrlByUserName(loginUserName)}?items=1000"><fmt:message key="navi.bibtex"/></a></li>
								</c:if>
							</ul>
						</li>
						<!--+
							| post a post
							+-->
						<li class="dropdown ${activeTab eq 'post' ? 'active' : ''} ${systemReadOnly ? 'disabled' : ''}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navi.postPost"/><b class="caret">&amp;nbsp;</b></a>
							<ul class="dropdown-menu">
								<li class="${systemReadOnly ? 'disabled' : ''}"><a href="/postBookmark"><fmt:message key="navi.postBookmark"/></a></li>
								<li class="${systemReadOnly ? 'disabled' : ''}"><a href="/postPublication"><fmt:message key="navi.postPublication"/></a></li>
								<li class="${systemReadOnly ? 'disabled' : ''}"><a href="/import/bookmarks"><fmt:message key="navi.imports"/></a></li>
								<li class="${systemReadOnly ? 'disabled' : ''}"><a href="/import/publications"><fmt:message key="publication.import.title"/></a></li>
							</ul>
							
						</li>
					</c:if>
					<!--+
						| persons (cris system)
						| only to reorder the menu and to rename the tab to persons
						+-->
					<c:if test="${showCRISNavBar}">
						<li class="${activeTab eq 'person' ? 'active' : ''}">
							<a href="${relativeUrlGenerator.getPersonsUrl()}">
								<fmt:message key="persons"/>
							</a>
						</li>
					</c:if>

					<!--+
						| groups
						+-->
					<c:choose>
						<c:when test="${not showCRISNavBar}">
							<c:choose>
								<c:when test="${not empty loginUser.groups}">
									<li class="dropdown ${activeTab eq 'groups' ? 'active' : ''}">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navi.groups"/><b class="caret">&amp;nbsp;</b></a>
										<ul class="dropdown-menu">
											<li><a href="${relativeUrlGenerator.getGroupsUrl()}"><fmt:message key="navi.groups.all"/></a></li>
											<li class="divider"><!-- keep me --></li>
											<c:forEach var="grp" items="${loginUser.groups}">
												<c:set var="role" value="${grp.getGroupMembershipForUser(loginUser).groupRole}" />
												<c:if test="${role eq 'ADMINISTRATOR' or role eq 'MODERATOR' or role eq 'USER'}">
													<li class="dropdown-submenu">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><c:out value="${grp.name}"/></a>
														<ul class="dropdown-menu">
															<li><a href="${relativeUrlGenerator.getUserUrlByUserName(grp.name)}"><fmt:message key="navi.posts" /></a></li>
															<li><a href="${relativeUrlGenerator.getGroupUrlByGroupName(grp.name)}"><fmt:message key="navi.group.activity"/></a></li>
															<li><c:if test="${grp.sharedDocuments}">
															<a href="${relativeUrlGenerator.getGroupUrlByGroupName(grp.name)}?filter=JUST_PDF"><fmt:message key="navi.pdfs" /></a>
														</c:if>
															</li>
																<c:if test="${not loginUser.settings.layoutSettings.simpleInterface}">
																	<li class="divider"><!-- keep me --></li>
																	<li><a href="${relativeUrlGenerator.getRelevantForUrlByGroupName(grp.name)}"><fmt:message key="navi.relevantFor"/></a></li>
																	<li><a href="${relativeUrlGenerator.getViewableUrlByGroupName(grp.name)}"><fmt:message key="navi.viewable" /></a></li>
																	<li><a href="${urlGeneratorDiscussed.getGroupUrlByGroupName(grp.name)}"><fmt:message key="navi.discussedPosts"/></a></li>
																</c:if>
															<li class="divider"><!-- keep me --></li>
															<c:set var="urlGeneratorSettings" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'settings')}" />
															<li><a href="${urlGeneratorSettings.getGroupUrlByGroupName(grp.name)}"><fmt:message key="navi.group.settings" /></a></li>
														</ul>
													</li>
												</c:if>
											</c:forEach>
											<c:if test="${!loginUser.spammer}">
												<li class="divider"><!-- keep me --></li>
												<li><a href="/grouprequest"><fmt:message key="navi.group.groupRequest" /></a></li>
											</c:if>
										</ul>
									</li>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${not empty loginUserName}">
											<li class="dropdown ${activeTab eq 'groups' ? 'active' : ''}">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navi.groups"/><b class="caret">&amp;nbsp;</b></a>
												<ul class="dropdown-menu">
													<li><a href="${relativeUrlGenerator.getGroupsUrl()}"><fmt:message key="navi.groups.all"/></a></li>
													<c:if test="${!loginUser.spammer}">
														<li class="divider"><!-- keep me --></li>
														<li><a href="/grouprequest"><fmt:message key="navi.group.groupRequest" /></a></li>
													</c:if>
												</ul>
											</li>
										</c:when>
										<c:otherwise>
											<li class="${activeTab eq 'groups' ? 'active' : ''}"><a href="${relativeUrlGenerator.getGroupsUrl()}"><fmt:message key="navi.groups"/></a></li>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:when>
						<c:otherwise>
							<li class="${activeTab eq 'groups' ? 'active' : ''}"><a href="${relativeUrlGenerator.getOrganizationsUrl()}"><fmt:message key="person.show.organizations"/></a></li>
						</c:otherwise>
					</c:choose>

					<!--+
						| projects and publications
						+-->
					<c:if test="${crisSystem}">
						<li class="${activeTab eq 'publications' ? 'active' : ''}">
							<a href="${relativeUrlGenerator.getPublicationsUrl()}">
								<fmt:message key="publications"/>
							</a>
						</li>
						<c:if test="${showProjectsNav}">
							<c:choose>
								<c:when test="${not empty loginUser.claimedPerson.crisLinks}">
									<li class="dropdown ${activeTab eq 'projects' ? 'active' : ''}">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navi.projects"/><b class="caret">&amp;nbsp;</b></a>
										<ul class="dropdown-menu">
											<li><a href="${relativeUrlGenerator.getProjectsUrl()}"><fmt:message key="navi.projects.all"/></a></li>
											<li class="divider"><!-- keep me --></li>
											<c:forEach var="crisLink" items="${loginUser.claimedPerson.crisLinks}">
												<c:set var="pro" value="${crisLink.project}" />
												<c:set var="role" value="${pro.getProjectMembershipForUser(loginUser).groupRole}" />
												<c:if test="${role eq 'ADMINISTRATOR' or role eq 'MODERATOR' or role eq 'USER'}">
													<li class="dropdown-submenu">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown"><c:out value="${pro.title}"/></a>
														<ul class="dropdown-menu">
															<li><a href="${relativeUrlGenerator.getProjectUrlByProjectName(pro.title)}"><fmt:message key="navi.posts" /></a></li>
															<li class="divider"><!-- keep me --></li>
															<c:set var="urlGeneratorSettings" value="${urlGeneratorFactory.createURLGeneratorForPrefix(null, 'settings')}" />
															<li><a href="${urlGeneratorSettings.getProjectUrlByProjectName(pro.title)}"><fmt:message key="navi.project.settings" /></a></li>
														</ul>
													</li>
												</c:if>
											</c:forEach>
										</ul>
									</li>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${not empty loginUserName}">
											<li class="dropdown ${activeTab eq 'projects' ? 'active' : ''}">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navi.projects"/><b class="caret">&amp;nbsp;</b></a>
												<ul class="dropdown-menu">
													<li><a href="${relativeUrlGenerator.getProjectsUrl()}"><fmt:message key="navi.projects.all"/></a></li>
													<c:if test="${userIsAdmin}">
														<li class="divider"><!-- keep me --></li>
														<li><a href="${relativeUrlGenerator.getProjectEditUrl()}"><fmt:message key="project.actions.create" /></a></li>
													</c:if>
												</ul>
											</li>
										</c:when>
										<c:otherwise>
											<li class="${activeTab eq 'projects' ? 'active' : ''}">
												<a href="${relativeUrlGenerator.getProjectsUrl()}">
													<fmt:message key="navi.projects"/>
												</a>
											</li>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:if>

					<!--+
						| popular
						+-->
					<c:if test="${not showCRISNavBar and showPopularNav}">
						<li class="dropdown ${activeTab eq 'popular' ? 'active' : ''}">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message key="navi.popular" /><b class="caret">&amp;nbsp;</b></a>
							<ul class="dropdown-menu">
								<li><a href="/popular"><fmt:message key="navi.posts"/></a></li>
								<li><a href="/tags"><fmt:message key="navi.tags"/></a></li>
								<li><a href="/authors"><fmt:message key="navi.authors"/></a></li>
								<li><a href="/concepts"><fmt:message key="navi.relations"/></a></li>
								<li><a href="/discussed"><fmt:message key="navi.discussions"/></a></li>
							</ul>
						</li>
					</c:if>
					
					<c:if test="${properties['genealogy.activated'] eq 'true' and not showCRISNavBar}">
						<li class="${activeTab eq 'persons' ? 'active' : ''}"><a href="${relativeUrlGenerator.getPersonsUrl()}"><fmt:message key="navi.persons"/></a></li>
					</c:if>
					
					<c:if test="${userIsAdmin}">
						<li class="dropdown ${activeTab eq 'admin' ? 'active' : ''}" data-touch="false">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">admin<b class="caret">&amp;nbsp;</b></a>
							<ul class="dropdown-menu">
								<li><a href="${relativeUrlGenerator.getAdminUrlByName('spam')}">users</a></li>
								<li><a href="${relativeUrlGenerator.getAdminUrlByName('group')}">groups</a></li>
								<li><a href="${relativeUrlGenerator.getAdminUrlByName('fulltextsearch')}">full text search</a></li>
								<li><a href="${relativeUrlGenerator.getAdminUrlByName('recommender')}">recommenders</a></li>
								<li><a href="${relativeUrlGenerator.getAdminUrlByName('oauth')}">OAuth consumers</a></li>
								<li><a href="${relativeUrlGenerator.getAdminUrlByName('sync')}">synchronization</a></li>
								<li><a href="${relativeUrlGenerator.getAdminUrlByName('statistics')}">statistics</a></li>
							</ul>
						</li>
					</c:if>
				</ul>
				<ul class="nav navbar-nav navbar-right hidden-xs">
					<c:choose>
						<c:when test="${not empty loginUserName}" >
							<li class="visible-md visible-lg"><user:username user="${loginUser}" /></li>
							<li class="dropdown ${activeTab eq 'myprofile' ? 'active' : ''}">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="fa fa-user"><!-- KEEP ME --></span>
									<c:if test="${(loginUser.inbox.numPosts + loginUser.clipboard.numPosts) == 0}">
										<c:set var="counterStyle" value="display: none;" />
									</c:if>
									<b class="navi-info">
										<span id="inbox-clipboard-counter" style="${counterStyle}"><c:out value="${(loginUser.inbox.numPosts + loginUser.clipboard.numPosts)}"/></span>
									</b>
									<b class="caret">&amp;nbsp;</b></a>
								<ul class="dropdown-menu items-with-icons">
									<menu:userDropdown loginUser="${loginUser}" />
								</ul>
							</li>
						</c:when>
						<c:otherwise>
							<menu:registerSignUpButtons />
						</c:otherwise>
					</c:choose>
				</ul>
			</div>

		</div>
	</nav>
	
	<c:if test="${not userLoggedIn}">
		<login:loginModal />
	</c:if>
</jsp:root>