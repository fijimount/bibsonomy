<jsp:root version="2.0" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:bs="urn:jsptagdir:/WEB-INF/tags/bs"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:c="http://java.sun.com/jsp/jstl/core">
	
	<!-- Render a form textarea field in the way it should typically be done with bootstrap. -->

	<jsp:directive.attribute name="path" type="java.lang.String" required="true" description="Path to property for data binding (e.g., post.resource.journal), labels, and the help message (.help is appended)"/>
	<jsp:directive.attribute name="labelColSpan" type="java.lang.Integer" required="false" description="Column span width of the label. labelColSpan and inputColSpan must add up to 12" />
	<jsp:directive.attribute name="inputColSpan" type="java.lang.Integer" required="false" description="Column span width of the input field. labelColSpan and inputColSpan must add up to 12" />
	<jsp:directive.attribute name="required" type="java.lang.Boolean" required="false" description="Must the user fill out the form, i.e., is the content required (adds '*' before form and CSS class 'reqinput')?"/>
	<jsp:directive.attribute name="noHelp" type="java.lang.Boolean" required="false" description="Shall the fly-by help be shown?"/>
	
	<jsp:directive.attribute name="labelText" type="java.lang.String" required="false" description="Alternativly to path, you can define directly a description for the label." />
	<jsp:directive.attribute name="labelKey" type="java.lang.String" required="false" description="Alternativly to path and labelText you can define a message key for the label" />
	<jsp:directive.attribute name="helpText" fragment="true" required="false" description="Alternativly to path, you can define directly a help text."/>
	
	<jsp:directive.attribute name="helpPage" type="java.lang.String" required="false" description="A link to a help page."/>
	<jsp:directive.attribute name="small" type="java.lang.Boolean" required="false" description="Reduces the element width to 33 percent" />

	<!-- HTML attributes -->
	<jsp:directive.attribute name="id" type="java.lang.String" required="false" description="Id for input field"/>
	<jsp:directive.attribute name="placeholder" required="false" description="Placeholder for input field." />
	<jsp:directive.attribute name="rows" required="false" type="java.lang.Integer" description="Placeholder for input field." />
	
	<jsp:directive.attribute name="appendix" fragment="true" required="false" description="Additional items for input field"/>
	
	<!--+ 
		| calculates grid colSpan of label and input field
	 	+-->
	<c:choose>
		<c:when test="${ (empty labelColSpan and empty inputColSpan) or (inputColSpan gt 10) or (labelColSpan gt 6) }">
			<c:set var="labelColSpan" value="2" />
			<c:set var="inputColSpan" value="10" />
		</c:when>
		<c:when test="${empty labelColSpan and not empty inputColSpan}">
			<c:set var="labelColSpan" value="${ 12 - inputColSpan }" />
		</c:when>
		<c:otherwise>
			<c:set var="inputColSpan" value="${ 12 - labelColSpan }" />
		</c:otherwise>
	</c:choose>
	
	<c:if test="${empty labelText and empty labelKey}">
		<fmt:message var="labelText" key="${path}" />
	</c:if>
	
	<c:if test="${not empty labelKey}">
		<fmt:message var="labelText" key="${labelKey}" />
	</c:if>
	<c:choose>
		<c:when test="${empty helpText}">
			<fmt:message var="helpText" key="${path}.help" />
		</c:when>
		<c:otherwise>
			<c:set var="helpText"><jsp:invoke fragment="helpText" /></c:set>
		</c:otherwise>
	</c:choose>
	
	<c:set var="helpPopover" value="" />
	<c:if test="${empty noHelp or not noHelp}">
		<c:set var="helpPopover" value="help-popover" />
	</c:if>
		
	<c:set var="smallField" value="" />
	<c:if test="${not empty small or small}">
		<c:set var="classSmallField" value="smallField" />
	</c:if>

	<c:if test="${empty id}">
		<c:set var="id" value="${path}" />
	</c:if>
	
	<c:if test="${empty rows}">
		<c:set var="rows" value="2" />
	</c:if>
	
	<!-- errors -->
	<c:set var="formErrors"><form:errors path="${path}" /></c:set>
	
	
	
	<!--+
		| bootstrap 3 form-group 
		+-->
	<div class="form-group${not empty formErrors ? ' has-error' : ''}">
		<!--+
			| followed by output label 
			+-->
		<c:if test="${not empty labelText}">
			<label class="col-sm-${labelColSpan} control-label" for="${id}">
				<c:out value="${labelText}" />
			</label>
		</c:if>
		
		<!--+
			| followed by input field surrounded by div column container with width inputColSpan
			+-->
		<div class="col-sm-${inputColSpan}">
			<form:textarea path="${path}" cssClass="form-control ${helpPopover} ${small ? smallField : ''}" id="${id}" placeholder="${placeholder}" rows="${rows}"/>
			
			<!-- followed by popover help -->
			<c:if test="${empty noHelp or not noHelp}">
				<div class="help help-header hide"><c:out value="${labelText}" /></div>
				<div class="help help-content hide">
					<c:out value="${helpText}" />
				</div>
			</c:if>
			
			<!-- followed by appendix for recommendations or additional content -->
			<jsp:invoke fragment="appendix" />
			<jsp:invoke fragment="appendix" />
			<c:set var="formErrors"><form:errors path="${path}" /></c:set>
			<c:choose>
				<c:when test="${empty formErrors and required}">
					<span class="help-block"><fmt:message key="error.field.required" /></span>
				</c:when>
				<c:when test="${not empty formErrors}">
					<bsform:errors path="${path}" />
				</c:when>
			</c:choose>
		</div> <!-- /.col-sm-x -->
	</div> <!-- /.form-grpup -->
</jsp:root>
