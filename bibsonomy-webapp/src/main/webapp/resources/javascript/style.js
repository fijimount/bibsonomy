var request = null;
var style_sort = new Array("alph", "freq");
var style_show = new Array("cloud", "list");
var availableMinFreqCounts = new Array(1,2,5,10,50);
var userMinFreq = 1;
/*
 * points to the function with the current sort style, such that we can sort
 * tags again, if we retrieved them on a minFreq change 
 */
var currentSortStyle = null;

/**
 * XXX: variable "tagbox_minfreq_style" is defined in cloud.tagx!
 * 
 * Creates the link to change the tagbox 
 * 
 * @param tagbox
 * @param show
 * @param sort
 * @param minfreq
 * @return
 */
function init_tagbox(tagbox, show, sort, minfreq) {
	var styleList = $("<ul class='list-inline sidebar-setting-box clearfix'></ul>")
	.append(getStyleItem(tagbox, styleList, style_sort[sort], style_sort))
	.append(getStyleItem(tagbox, styleList, style_show[show], style_show));

	if (typeof tagbox_minfreq_style != "undefined") {
		if (tagbox_minfreq_style == "user") {
			styleList.append(getUserMinfreq(tagbox, styleList, minfreq));
		} else if (tagbox_minfreq_style == "default") {
			styleList.append(getUserMinfreq(tagbox, styleList, -1));  
		}
	}
	
	changeTagBox(tagbox, styleList, style_show[show]);
	changeTagBox(tagbox, styleList, style_sort[sort]);
	
	styleList.insertAfter($('.tagcloud-headline'));
}

function changeTagBox(tagbox, style_list, mode) {
	if (mode == "list" || mode == "cloud"){
		$(tagbox).attr("class", "tag" + mode);
		style_list.children().eq(1).replaceWith(getStyleItem(tagbox, style_list, mode, style_show));
	} else if (mode == "alph" || mode == "freq") {
		style_list.children().eq(0).replaceWith(getStyleItem(tagbox, style_list, mode, style_sort));
		mode == "alph" ? setTagBoxAlph(tagbox) : setTagBoxFreq(tagbox);
	}
}


/**
 * Creates the style change links for changing sorting (alph/freq) and style (cloud/list) of the tag cloud.
 * 
 * @param tagbox
 * @param style_list
 * @param activeStyle
 * @param style_arr
 * @return
 */
function getStyleItem(tagbox, style_list, activeStyle, style_arr) {
	var styleSort = $("<li> (</li>");

	for (var i = 0; i < style_arr.length; i++) {
		if (activeStyle == style_arr[i]) {
			/*
			 * current style
			 */
			styleSort.append(getString("tagbox." + style_arr[i]));
		} else {
			/*
			 * other style
			 */
			styleSort.append(
					$("<a>" + getString("tagbox." + style_arr[i]) + "</a>")
					.css("cursor", "pointer")
					.click({"style" : style_arr[i]}, function(event) { // XXX: to circumvent the closure behavior, we must give the style information as "eventData"
						changeTagBox(tagbox, style_list, event.data.style);
					})
			);
		}
		if (i + 1 < style_arr.length) styleSort.append(" | ");
	}
	return styleSort.append(") ");
}



/**
 * create minfreq links for users (which are loaded via AJAX)
 * 
 * @param count
 * @return
 */
function getUserMinfreq(tagbox, styleList, count) {
	var minfreqList = $("<li> (" + getString("tagbox.minfreq") + " </li>");

	/*
	 * build links for each supported number of tags
	 */
	for (var i = 0; i < availableMinFreqCounts.length; i++) {
		if (count == availableMinFreqCounts[i]) {
			/*
			 * append count
			 */
			minfreqList.append(count);
		} else {
			/*
			 * append link
			 */
			var a = $("<a>" + availableMinFreqCounts[i] + "</a>");
			a.click({"count" : availableMinFreqCounts[i]}, function(event) { // XXX: to circumvent the closure behavior, we must give the count information as "eventData"
				sendMinfreqRequ(tagbox, event.data.count);
			});
			a.css("cursor", "pointer");
			minfreqList.append(a);
		}
		if (i + 1 < availableMinFreqCounts.length) minfreqList.append(" | ");
	}
	minfreqList.append(") ");

	return minfreqList;
}


/**
 * Sorts the tag cloud in the sidebar alphabetically.
 * 
 * XXX: Don't try to use jQuery for this, it slows down the code (by a factor of approx. 2).
 * 
 * @return
 */
function setTagBoxAlph(tagbox) {
	currentSortStyle = setTagBoxAlph;
	var collection_tagname = new Array(); // array of tagnames
	var collection_li = new Object(); // map tagname -> li

	/* store tagbox */
	var litags = tagbox.getElementsByTagName("li");
	for (var x = 0; x < litags.length; x++){
		var tagname = litags[x].getElementsByTagName("a")[0].firstChild.nodeValue;
		collection_tagname.push(tagname);
		collection_li[tagname] = litags[x];//.cloneNode(true); // does new code work in all browsers?
	}

	/* sort tags */
	collection_tagname.sort(unicodeCollation);

	/* build new tagbox */
	for (var x = 0; x < collection_tagname.length; x++) {
		var newli = collection_li[collection_tagname[x]];
		newli.appendChild(document.createTextNode(" "));
		tagbox.appendChild(newli);
	}

	/* clean box */
	delete collection_tagname;
	delete collection_li;
}

/**
 * Sorts the tag cloud in the sidebar by frequency.
 * 
 * XXX: Don't try to use jQuery for this, it slows down the code (by a factor of approx. 2).
 * 
 * @return
 */
function setTagBoxFreq(tagbox) {
	currentSortStyle = setTagBoxFreq;
	var collection_tagname = new Array();
	var collection_li = new Object();
	var collection_tagposts = new Object();
	var collection_numberofposts = new Array();

	/* store tagbox */
	var litags = tagbox.getElementsByTagName("li");
	for (var x = 0; x < litags.length; x++) {
		var tags = litags[x].getElementsByTagName("a");
		var tagname = tags[0].firstChild.nodeValue;
		collection_tagname.push(tagname);
		collection_li[tagname] = litags[x];//.cloneNode(true); //NOTE: does new code work in all browsers?

		// extract post count
		var numberofpost = parseInt(tags[0].getAttribute("title").split(" ")[0]);
		collection_tagposts[tagname] = numberofpost;
		var newnumberofposts = true;
		for (y = 0; y < collection_numberofposts.length; y++) {
			if (collection_numberofposts[y] == numberofpost) {
				newnumberofposts = false;
			}
		}
		// remember post count
		if (newnumberofposts) {
			collection_numberofposts.push(numberofpost);
		}
	}

	/* sort by number of posts (descending) */ 
	collection_numberofposts.sort(unicodeCollation).reverse();

	/* build new tagbox */
	for (var x = 0; x < collection_numberofposts.length; x++){
		var tags = new Array();
		for (var y = 0; y < collection_tagname.length; y++){
			var tagname = collection_tagname[y];
			if (collection_tagposts[tagname] == collection_numberofposts[x]) {
				tags.push(tagname);
			}
		}
		// sort tags with the same number of posts alphabetically
		tags.sort(unicodeCollation);
		for(var y = 0; y < tags.length; y++) {
			var newli = collection_li[tags[y]];
			newli.appendChild(document.createTextNode(" "));
			tagbox.appendChild(newli);
		}
		delete tags;
	}

	/* clean up */
	delete collection_tagname;
	delete collection_li;
	delete collection_tagposts;
	delete collection_numberofposts;
}


/**
 * Retrieve the HTML tag cloud with the given parameters and replace the 
 * existing tag cloud with it.
 * 
 * @param tagbox
 * @param minfreq
 * @return
 */
function sendMinfreqRequ(tagbox, minfreq) {
	if (minfreq == null) minfreq = 1;
	userMinFreq = minfreq;

	$.ajax({
		url : "?tagcloud.minFreq=" + minfreq + "&tagstype=default&format=tagcloud",
		dataType : "html",
		success : function (html) {
			/*
			 * replace tags
			 */
			$(tagbox).empty().append($(html).find("ul.tagcloud li"));
			/*
			 * re-order tags
			 */
			currentSortStyle(tagbox);
			/*
			 * update minfreq links
			 */
			var styleList = $(tagbox).prev("ul");
			styleList.children().eq(2).replaceWith(getUserMinfreq(tagbox, styleList, minfreq));
		}
	});
}


/**
 * 
 * Switch the page default path to the full navigation path + form.
 * 
 * @param scope
 * @param event
 * @return
 */
function switchNavi(scope, element) {
	var element = $(element);
	/*
	 * XXX: a hack to "unhover" the list. the worst part of it: we have to wait 
	 * some time until we make the list visible again (though it's then 
	 * otherwise hidden by CSS).
	 */

	/*
	 * remove old scope inputs
	 */
	$("input[name='scope']").remove();
	
	/* if exist, remove #pathPart */
	$("#pathPart").remove();
	
	/*
	 * change form action to redirect with the given scope
	 */
	var form = $("#search form").attr("action", "/redirect").append("<input type='hidden' name='scope' value='" + scope + "'/>");

	/*
	 * Exchange text before form input field to the selected text.  
	 */
	var text = element.html();
	if (text.search(/- /) != -1) { // search in a group
		text = getString("navi.group") + ":" + text.substr(2); 
	}
	$("#search a:first .search-scope").html(text);

	/*
	 * heuristic to get the hint for the input field  
	 */
	var hint = getString("navi." + scope.replace(/\/.*/, "") + ".hint");
	if (hint.search(/\?\?\?.*\?\?\?/) != -1) { 
		hint = getString("navi.search.hint"); // fallback
	}

	/*
	 * prepare input field
	 */
	$("#inpf")
	.attr("name", "search") // always do a search
	//.val(hint) // set hint as value
	//.addClass('descriptiveLabel') // add class
	//.descrInputLabel({}) // make the label disappear on click/submit
	.attr('placeholder', hint); // set hint as placeholder
	
	//TODO: WHAT DOES THIS DO????
	$("#inpf").parents("li").removeClass("hidden"); // show form
	
	/*
	 * Start autocompletion if 'tag' in Search-navibar is chosen, otherwise quit autocompletion
	 */
	
	$("#inpf").autocomplete('disable');
	
	if (scope == "tag"){
		startTagAutocompletion($("#inpf"), false, true, false, false, false);
	}
	
	// TODO: finish suggestion service
	/*if(scope == "search") {
		startPostAutocompletion($("#inpf"));
	}*/
	
	return false;
}

$(function() {
	$('a.delete-toggle').hover(deleteToggleShowDeleteInfo);
	$('a.delete-toggle').mouseleave(deleteToggleShowDefaultInfo);
	
	$('body').on('click', function (e) {
		$('[data-toggle="popover"]').each(function () {
			// the 'is' for buttons that trigger popups
			// the 'has' for icons within a button that triggers a popup
			if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
				$(this).popover('hide');
			}
		});
	});
	
	$('form.popover-confirm > *[data-toggle="popover"]').on('shown.bs.popover', function () {
		$('.popover-content > .confirm').click(function() {
			var form = $(this).parents('form.popover-confirm');
			form.data("confirmed", "yes");
			form.submit();
			return false;
		});
	});
	$('form.popover-confirm').submit(function() {
		var confirmed = $(this).data("confirmed");
		if (confirmed !== "yes") {
			return false;
		}
	});
	
	$('a.doc-magnify').click(function() {
		var previewModal = $('#preview-modal');
		var image = $(this).parent().find('img');
		var imageUrl = image.attr('src');
		var imageTitle = image.attr('alt');
		if (previewModal.length == 0) {
			var modalElement = $(
				'<div class="modal fade" id="preview-modal" role="dialog">' +
					'<div class="modal-dialog">' + 
						'<div class="modal-content">' + 
							'<div class="modal-header">' +
								'<button type="button" class="close" data-dismiss="modal">&times;</button>' +
								'<h4 id="preview-title"></h4>' + 
							'</div>' + 
							'<div class="modal-body">' + 
								'<div class="text-center">' + 
									'<img width="75%" src="" id="preview-image" />' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>');
			$('body').append(modalElement);
			previewModal = $('#preview-modal');
		}
		
		$('#preview-image').attr('src', imageUrl);
		$('#preview-title').text(imageTitle);
		
		var modal = previewModal.modal({
			show: false
		});
		previewModal.modal('show');
		// previewModal.modal('show');
		
		return false;
	});
	
	autosize($('textarea[data-autosize]'));
	
	$('[data-toggle="tooltip"]').tooltip();
});

function deleteToggleShowDefaultInfo() {
	$(this).removeClass('btn-danger').addClass('btn-success');
	$(this).children('.fa').removeClass('fa-times').addClass('fa-check');
	$(this).children('.infotext').text($(this).data('default-text'));
}

function deleteToggleShowDeleteInfo() {
	$(this).removeClass('btn-success').addClass('btn-danger');
	$(this).children('.fa').removeClass('fa-check').addClass('fa-times');
	$(this).children('.infotext').text($(this).data('delete-text'));
}