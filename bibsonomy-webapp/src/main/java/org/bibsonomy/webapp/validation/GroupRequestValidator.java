/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.validation;

import org.bibsonomy.common.enums.GroupCreationMode;
import org.bibsonomy.common.enums.Role;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.User;
import org.bibsonomy.webapp.command.GroupRequestCommand;
import org.bibsonomy.webapp.util.Validator;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * validator for group requests
 * 
 * @author Mario Holtmueller
 * @author dzo
 */
public class GroupRequestValidator implements Validator<GroupRequestCommand> {
	
	private final GroupCreationMode groupCreationMode;
	
	/**
	 * @param groupCreationMode
	 */
	public GroupRequestValidator(GroupCreationMode groupCreationMode) {
		this.groupCreationMode = groupCreationMode;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return GroupRequestCommand.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Assert.notNull(target);
		final GroupRequestCommand command = (GroupRequestCommand) target;
		
		errors.pushNestedPath("group");
		
		final Group group = command.getGroup();
		
		/*
		 * delegate other check to the group validator
		 */
		ValidationUtils.invokeValidator(new GroupValidator(), group, errors);
		
		final User loggedinUser = command.getContext().getLoginUser();
		if (!Role.ADMIN.equals(loggedinUser.getRole()) && !GroupCreationMode.AUTOMATIC.equals(this.groupCreationMode)) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "groupRequest.reason", ERROR_FIELD_REQUIRED_KEY);
		}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", ERROR_FIELD_REQUIRED_KEY);
		
		errors.popNestedPath();
	}
}
