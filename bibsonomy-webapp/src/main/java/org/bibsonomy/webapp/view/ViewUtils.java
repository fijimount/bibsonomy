/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.view;

import java.util.Map;

import org.springframework.validation.BindingResult;


/**
 * @author dzo
 */
public abstract class ViewUtils {

	/**
	 * Gets the BindingResult (containing errors) from the model.
	 * @param model
	 * @return the binding result
	 */
	public static BindingResult getBindingResult(final Map<String, Object> model){
		for (final Object key : model.keySet() ){
			if (((String)key).startsWith(BindingResult.MODEL_KEY_PREFIX)) {
				return (BindingResult) model.get(key);
			}
		}
		
		return null;
	}

}
