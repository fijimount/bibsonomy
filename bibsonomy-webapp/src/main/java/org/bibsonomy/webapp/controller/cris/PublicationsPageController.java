package org.bibsonomy.webapp.controller.cris;

import org.bibsonomy.common.SortCriteria;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.bibsonomy.model.GoldStandardPublication;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.logic.querybuilder.PostQueryBuilder;
import org.bibsonomy.services.searcher.PostSearchQuery;
import org.bibsonomy.webapp.command.ListCommand;
import org.bibsonomy.webapp.command.cris.PublicationsPageCommand;
import org.bibsonomy.webapp.util.MinimalisticController;
import org.bibsonomy.webapp.util.View;
import org.bibsonomy.webapp.view.Views;

import java.util.Calendar;
import java.util.List;

import static org.bibsonomy.util.ValidationUtils.present;

/**
 * controller that lists all publications of a configurable college
 * - /publications
 *
 * @author dzo
 */
public class PublicationsPageController implements MinimalisticController<PublicationsPageCommand> {

	private final int PUB_ENTRIES = 10;

	private String college;
	private LogicInterface logic;

	@Override
	public PublicationsPageCommand instantiateCommand() {
		return new PublicationsPageCommand();
	}

	@Override
	public View workOn(final PublicationsPageCommand command) {
		ListCommand<Post<GoldStandardPublication>> goldStandardPublications = command.getPublications();
		goldStandardPublications.setEntriesPerPage(PUB_ENTRIES);
		final PostQueryBuilder queryBuilder = new PostQueryBuilder()
				.college(this.college)
				.entriesStartingAt(goldStandardPublications.getEntriesPerPage(), goldStandardPublications.getStart())
				.searchAndSortCriteria(command.getSearch(), new SortCriteria(SortKey.YEAR, SortOrder.DESC));

		PostSearchQuery<GoldStandardPublication> query = new PostSearchQuery<>(queryBuilder.createPostQuery(GoldStandardPublication.class));
		if (!present(command.getSearch())) {
			final Calendar calendar = Calendar.getInstance();
			query.setLastYear(String.valueOf(calendar.get(Calendar.YEAR)));
		}

		final List<Post<GoldStandardPublication>> posts = this.logic.getPosts(query);
		goldStandardPublications.setList(posts);

		return Views.PUBLICATIONS_OVERVIEW;
	}

	/**
	 * @param college the college to set
	 */
	public void setCollege(String college) {
		this.college = college;
	}

	/**
	 * @param logic the logic to set
	 */
	public void setLogic(LogicInterface logic) {
		this.logic = logic;
	}
}
