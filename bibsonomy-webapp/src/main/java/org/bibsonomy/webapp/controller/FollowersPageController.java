/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.controller;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.List;

import org.bibsonomy.common.enums.Duplicates;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.UserRelation;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.util.EnumUtils;
import org.bibsonomy.webapp.command.FollowersViewCommand;
import org.bibsonomy.webapp.command.ListCommand;
import org.bibsonomy.webapp.config.Parameters;
import org.bibsonomy.webapp.exceptions.MalformedURLSchemeException;
import org.bibsonomy.webapp.util.MinimalisticController;
import org.bibsonomy.webapp.util.RankingUtil;
import org.bibsonomy.webapp.util.View;
import org.bibsonomy.webapp.view.Views;

/**
 * @author Christian Kramer
 */
public class FollowersPageController extends SingleResourceListController implements MinimalisticController<FollowersViewCommand> {

	@Override
	public View workOn(final FollowersViewCommand command) {
		final String format = command.getFormat();
		this.startTiming(format);
		
		// you have to be logged in
		if (!command.getContext().isUserLoggedIn()) {
			throw new MalformedURLSchemeException("error.general.login"); // TODO: redirect to login page?!
		}
		
		final String pageSort = command.getSortPage();
		final SortKey sortKey = "ranking".equals(pageSort) ? SortKey.RANK : SortKey.DATE;
		command.setSortKey(sortKey);
		
		// set params
		final UserRelation userRelation = EnumUtils.searchEnumByName(UserRelation.values(), command.getUserSimilarity());
		GroupingEntity groupingEntity = GroupingEntity.FOLLOWER;
		String groupingName = null;
		final String requestedUser = command.getRequestedUser();
		if (present(requestedUser)) {
			groupingEntity = GroupingEntity.USER;
			groupingName = requestedUser;
		}
		
		// handle case when only tags are requested
		this.handleTagsOnly(command, groupingEntity, groupingName, null, null, null, Integer.MAX_VALUE, null);
		
		final String username = command.getContext().getLoginUser().getName();
		
		switch (sortKey) {
		case RANK:
			
			// ranking settings
			final int start = command.getRanking().getPeriod() * Parameters.NUM_RESOURCES_FOR_PERSONALIZED_RANKING;
			command.getRanking().setPeriodStart(start + 1);
			command.getRanking().setPeriodEnd(start + Parameters.NUM_RESOURCES_FOR_PERSONALIZED_RANKING);
			
			// personalization settings
			final int entriesPerPage = Parameters.NUM_RESOURCES_FOR_PERSONALIZED_RANKING;
			command.setPersonalized(true);
			command.setDuplicates(Duplicates.NO);
			
			// fetch all tags of logged-in user
			final List<Tag> loginUserTags = this.logic.getTags(Resource.class, GroupingEntity.USER, username, null, null, null, null, null, null, command.getStartDate(), command.getEndDate(), 0, Integer.MAX_VALUE);
			
			// fetch all tags of followed users TODO implement...
			final List<Tag> targetUserTags = this.logic.getTags(Resource.class, GroupingEntity.USER, username, null, null, null, null, null, null, command.getStartDate(), command.getEndDate(), 0, Integer.MAX_VALUE);		
			
			// retrieve and set the requested resource lists, along with total
			// counts
			for (final Class<? extends Resource> resourceType : this.getListsToInitialize(command)) {
				final ListCommand<?> listCommand = command.getListCommand(resourceType);
				
				final int origEntriesPerPage = listCommand.getEntriesPerPage();
				final int origStart = listCommand.getStart();
				listCommand.setStart(start);
				this.setList(command, resourceType, groupingEntity, groupingName, null, null, null, null, null, command.getStartDate(), command.getEndDate(), entriesPerPage);
				listCommand.setEntriesPerPage(origEntriesPerPage);
				listCommand.setStart(origStart);
				
				// compute the ranking for each post in the list
				RankingUtil.computeRanking(loginUserTags, targetUserTags, command.getListCommand(resourceType).getList(), command.getRanking().getMethodObj(), command.getRanking().getNormalize());

				// post-process & sort
				this.postProcessAndSortList(command, resourceType);

				// show only the top ranked resources for each resource type
				if (command.getListCommand(resourceType).getList().size() > origEntriesPerPage) {
					this.restrictResourceList(command, resourceType, listCommand.getStart(), listCommand.getStart() + origEntriesPerPage);
				}
				
				// set total count
				//this.setTotalCount(command, resourceType, groupingEntity, null, null, null, null, null, null, origEntriesPerPage, null);
			}
			break;
		default:
			for (final Class<? extends Resource> resourceType : this.getListsToInitialize(command)) {
				final ListCommand<?> listCommand = command.getListCommand(resourceType);
				final int resourceEntriesPerPage = listCommand.getEntriesPerPage();
				this.setList(command, resourceType, groupingEntity, groupingName, null, null, null, null, null, command.getStartDate(), command.getEndDate(), resourceEntriesPerPage);
				
				// post-process & sort
				this.postProcessAndSortList(command, resourceType);
				
				// set total count
				this.setTotalCount(command, resourceType, groupingEntity, groupingName, null, null, null, null, sortKey, null, null, resourceEntriesPerPage);
			}
			break;
		}

		// html format - retrieve tags and return HTML view
		if ("html".equals(format)) {
			command.setFollowersOfUser(this.logic.getUsers(null, GroupingEntity.FOLLOWER, null, null, null, null, UserRelation.FOLLOWER_OF, null, 0, 0));
			command.setUserIsFollowing(this.logic.getUsers(null, GroupingEntity.FOLLOWER, null, null, null, null, UserRelation.OF_FOLLOWER, null, 0, 0));

			// retrieve similar users, by the given user similarity measure
			final List<User> similarUsers = this.logic.getUsers(null, GroupingEntity.USER, username, null, null, null, userRelation, null, 0, 10);
			command.getRelatedUserCommand().setRelatedUsers(similarUsers);
			
			this.endTiming();
			return Views.FOLLOWERS;
		}
		
		this.endTiming();
		// export - return the appropriate view
		return Views.getViewByFormat(format);
	}

	@Override
	public FollowersViewCommand instantiateCommand() {
		return new FollowersViewCommand();
	}
}
