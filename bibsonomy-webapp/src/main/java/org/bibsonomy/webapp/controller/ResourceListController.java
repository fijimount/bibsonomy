/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.common.SortCriteria;
import org.bibsonomy.common.enums.Duplicates;
import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.FilterEntity;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.QueryScope;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.bibsonomy.common.enums.TagCloudSort;
import org.bibsonomy.common.enums.TagCloudStyle;
import org.bibsonomy.common.enums.TagsType;
import org.bibsonomy.database.systemstags.SystemTagsExtractor;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.UserSettings;
import org.bibsonomy.model.factories.ResourceFactory;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.logic.querybuilder.PostQueryBuilder;
import org.bibsonomy.model.util.BibTexUtils;
import org.bibsonomy.model.util.TagUtils;
import org.bibsonomy.util.Sets;
import org.bibsonomy.util.SortUtils;
import org.bibsonomy.webapp.command.ListCommand;
import org.bibsonomy.webapp.command.ResourceViewCommand;
import org.bibsonomy.webapp.command.SimpleResourceViewCommand;
import org.bibsonomy.webapp.command.TagCloudCommand;
import org.bibsonomy.webapp.view.Views;
import org.springframework.beans.factory.annotation.Required;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.bibsonomy.util.ValidationUtils.present;

/**
 * controller for retrieving multiple windowed lists with resources. 
 * These are currently the bookmark and the bibtex list
 * 
 * @author Jens Illig
 */
public abstract class ResourceListController extends DidYouKnowMessageController {
	/** default values for sorting when jabref layouts are to be rendered */
	private static final String DEFAULT_SORTPAGE_JABREF_LAYOUTS = "year|author|title";
	private static final String DEFAULT_SORTPAGEORDER_JABREF_LAYOUTS = "desc|asc|asc";
	
	private static final Log log = LogFactory.getLog(ResourceListController.class);

	protected static <T> Set<T> intersection(final Collection<? extends T> col1, final Collection<? extends T> col2) {
		if (!present(col1) || !present(col2)) {
			return Collections.emptySet();
		}
		
		final Set<T> set = new HashSet<>(col1);
		set.retainAll(col2);
		return set;
	}
	
	protected LogicInterface logic;
	protected UserSettings userSettings;
	private long startTime;
	
	/**
	 * these resource classes are supported by the controller
	 */
	protected Set<Class<? extends Resource>> supportedResources;
	
	/**
	 * these resource classes the controller should always initialize
	 */
	private Set<Class<? extends Resource>> forcedResources;
	
	/**
	 * if <code>true</code> the controller should not initalize any resource
	 * (e.g. only tags are handled)
	 */
	private boolean initializeNoResources;

	/**
	 * Retrieve a set of tags from the database logic and add them to the command object
	 * 
	 * @param cmd the command
	 * @param resourceType the resource type
	 * @param groupingEntity the grouping entity
	 * @param groupingName the grouping name
	 * @param regex regular expression for tag filtering
	 * @param sortKey forced sortKey
	 * @param tags list of tags
	 */
	protected void setTags(final ResourceViewCommand cmd, final Class<? extends Resource> resourceType, final GroupingEntity groupingEntity, final String groupingName, final String regex, final SortKey sortKey, final List<String> tags, final String hash, final int max, final String search) {
		this.setTags(cmd, resourceType, groupingEntity, groupingName, regex, sortKey, tags, hash, max, search, QueryScope.LOCAL);
	}
	
	/**
	 * Retrieve a set of tags from the database logic and add them to the command object
	 * 
	 * @param cmd the command
	 * @param resourceType the resource type
	 * @param groupingEntity the grouping entity
	 * @param groupingName the grouping name
	 * @param regex regular expression for tag filtering
	 * @param sortKey forced sort key
	 * @param tags list of tags
	 * @param hash 
	 * @param max 
	 * @param search 
	 * @param queryScope
	 * @param queryScope
	 */
	protected void setTags(final ResourceViewCommand cmd, final Class<? extends Resource> resourceType, final GroupingEntity groupingEntity, final String groupingName, final String regex, SortKey sortKey, final List<String> tags, final String hash, final int max, final String search, final QueryScope queryScope) {
		final TagCloudCommand tagCloudCommand = cmd.getTagcloud();
		// retrieve tags
		log.debug("getTags " + " " + groupingEntity + " " + groupingName);
		SortKey tagSortKey = null;
		int tagMax = max;
		/*
		 * check parameters from URL
		 */
		if ((tagCloudCommand.getMinFreq() == 0) && (tagCloudCommand.getMaxCount() == 0)) { // no parameter was set via URL
			/*
			 * check user's settings
			 */
			if (this.userSettings.getIsMaxCount()) {
				tagSortKey = SortKey.FREQUENCY;
				tagMax = Math.min(max, this.userSettings.getTagboxMaxCount());
			} else {
				// overwrite minFreq because it is not explicitly set by URL param
				tagCloudCommand.setMinFreq(this.userSettings.getTagboxMinfreq());
			}
		} else { //parameter set via URL
			if (tagCloudCommand.getMinFreq() == 0) {
				tagSortKey = SortKey.FREQUENCY;
				tagMax = tagCloudCommand.getMaxCount();
			}
		}
		
		/*
		 * allow controllers to overwrite max and order
		 * FIXME: In the hurry I found no nice way to do this. :-( 
		 */
		tagMax = this.getFixedTagMax(tagMax);
		tagSortKey = this.getFixedTagSortKey(tagSortKey);
		
		if (present(sortKey)) {
			tagSortKey = sortKey;
		}
		
		tagCloudCommand.setTags(this.logic.getTags(resourceType, groupingEntity, groupingName, tags, hash, search, queryScope,regex, null, tagSortKey, cmd.getStartDate(), cmd.getEndDate(), 0, tagMax));
		// retrieve tag cloud settings
		tagCloudCommand.setStyle(TagCloudStyle.getStyle(this.userSettings.getTagboxStyle()));
		tagCloudCommand.setSort(TagCloudSort.getSort(this.userSettings.getTagboxSort()));
		tagCloudCommand.setMaxFreq(TagUtils.getMaxUserCount(tagCloudCommand.getTags()));
	}

	/**
	 * If a sub-classing controller wants to enforce a fixed order of
	 * the tag cloud, it should overwrite this method and return the
	 * fixed order.
	 * 
	 * @param sortKey
	 * @return
	 */
	protected SortKey getFixedTagSortKey(final SortKey sortKey) {
		return sortKey;
	}
	
	/**
	 * If a sub-classing controller wants to enforce a fixed number
	 * of tags in the tag cloud, it should overwrite this method and
	 * return the number of tags.
	 * 
	 * @param tagMax
	 * @return
	 */
	protected int getFixedTagMax(final int tagMax) {
		return tagMax;
	}

	/**
	 * Initialize tag list, depending on chosen resourcetype
	 * 
	 * @param cmd
	 * @param groupingEntity
	 * @param groupingName
	 * @param regex
	 * @param tags
	 * @param hash
	 * @param search
	 */
	protected void handleTagsOnly(final ResourceViewCommand cmd, final GroupingEntity groupingEntity, final String groupingName, final String regex, final List<String> tags, final String hash, final int max, final String search) {
		final TagsType  tagsType = cmd.getTagstype();
		if (present(tagsType)) {

			// if tags are requested (not related tags), remove non-systemtags from tags list
			if (TagsType.DEFAULT.equals(tagsType) && (tags != null) ) {
				SystemTagsExtractor.removeAllNonSystemTags(tags);
			}

			// check if limitation to a single resourcetype is requested
			Class<? extends Resource> resourcetype = Resource.class;
			if (this.isPublicationOnlyRequested(cmd)) {
				resourcetype = BibTex.class;
			} else if (this.isBookmarkOnlyRequested(cmd)) {
				resourcetype = Bookmark.class;
			}

			// fetch tags, store them in bean
			this.setTags(cmd, resourcetype, groupingEntity, groupingName, regex, null, tags, hash, max, search);

			// when tags only are requested, we don't need any resources
			this.setInitializeNoResources(true);
		}
	}


	/**
	 * do some post processing with the retrieved resources
	 * 
	 * @param cmd
	 * @param posts 
	 */
	protected void postProcessAndSortList(final ResourceViewCommand cmd, final List<Post<BibTex>> posts) {
		// if a jabref layout is to be rendered and no special order is given, set to default order 
		if (Views.LAYOUT.getName().equalsIgnoreCase(cmd.getFormat()) &&
				ResourceViewCommand.DEFAULT_SORTPAGE.equalsIgnoreCase(cmd.getSortPage()) &&
				ResourceViewCommand.DEFAULT_SORTPAGEORDER.equals(cmd.getSortPageOrder())) {
			cmd.setSortPage(DEFAULT_SORTPAGE_JABREF_LAYOUTS);
			cmd.setSortPageOrder(DEFAULT_SORTPAGEORDER_JABREF_LAYOUTS);
		}
		
		// remove duplicates
		if (Duplicates.NO.equals(cmd.getDuplicates())) {
			BibTexUtils.removeDuplicates(posts);
			// re-sort list by date in descending order, if nothing else requested
			if (ResourceViewCommand.DEFAULT_SORTPAGE.equals(cmd.getSortPage())) {
				cmd.setSortPage("date");
			}
			if (ResourceViewCommand.DEFAULT_SORTPAGEORDER.equals(cmd.getSortPageOrder())) {
				cmd.setSortPageOrder("desc");
			}
		}
		
		// merge duplicates
		if (Duplicates.MERGED.equals(cmd.getDuplicates())) {
			BibTexUtils.mergeDuplicates(posts);
		}
		
		if (!ResourceViewCommand.DEFAULT_SORTPAGE.equals(cmd.getSortPage()) ||
				! ResourceViewCommand.DEFAULT_SORTPAGEORDER.equals(cmd.getSortPageOrder())) {
			BibTexUtils.sortBibTexList(posts, SortUtils.parseSortKeys(cmd.getSortPage()), SortUtils.parseSortOrders(cmd.getSortPageOrder()) );
		}
	}

	/**
	 * retrieve a list of posts from the database logic and add them to the command object
	 * 
	 * @param <T> extends Resource
	 * @param cmd the command object
	 * @param resourceType the resource type
	 * @param groupingEntity the grouping entity
	 * @param groupingName the grouping name
	 * @param tags 
	 * @param hash 
	 * @param search 
	 * @param filter 
	 * @param sortKey
	 * @param startDate 
	 * @param endDate 
	 * @param itemsPerPage number of items to be displayed on each page
	 */
	protected <T extends Resource> void setList(final SimpleResourceViewCommand cmd, final Class<T> resourceType, final GroupingEntity groupingEntity, final String groupingName, final List<String> tags, final String hash, final String search, final FilterEntity filter, final SortKey sortKey, final Date startDate, final Date endDate, final int itemsPerPage) {
		this.setList(cmd, resourceType, groupingEntity, groupingName, tags, hash, search, QueryScope.LOCAL, filter, sortKey, startDate, endDate, itemsPerPage);
	}

	/**
	 * retrieve a list of posts from the database logic using ElasticSearch and add them to the command object
	 *
	 * @param <T> extends Resource
	 * @param cmd the command object
	 * @param resourceType the resource type
	 * @param groupingEntity the grouping entity
	 * @param groupingName the grouping name
	 * @param tags
	 * @param hash
	 * @param search
	 * @param searchType
	 * @param filter
	 * @param sortKey
	 * @param startDate
	 * @param endDate
	 * @param itemsPerPage number of items to be displayed on each page
	 */
	protected <T extends Resource> void setList(final SimpleResourceViewCommand cmd, final Class<T> resourceType, final GroupingEntity groupingEntity, final String groupingName, final List<String> tags, final String hash, final String search, final QueryScope searchType, final FilterEntity filter, final SortKey sortKey, final Date startDate, final Date endDate, final int itemsPerPage) {
		final List<SortCriteria> sortCriteria = Collections.singletonList(new SortCriteria(sortKey, SortOrder.DESC));
		this.setList(cmd, resourceType, groupingEntity, groupingName, tags, hash, search, searchType, filter, sortCriteria, startDate, endDate, itemsPerPage);
	}

		/**
		 * retrieve a list of posts from the database logic using ElasticSearch and add them to the command object
		 *
		 * @param <T> extends Resource
		 * @param cmd the command object
		 * @param resourceType the resource type
		 * @param groupingEntity the grouping entity
		 * @param groupingName the grouping name
		 * @param tags
		 * @param hash
		 * @param search
		 * @param queryScope
	 * @param filter
	 * @param sortCriteria
		 * @param startDate
		 * @param endDate
		 * @param itemsPerPage number of items to be displayed on each page
		 */
	protected <T extends Resource> void setList(final SimpleResourceViewCommand cmd, final Class<T> resourceType, final GroupingEntity groupingEntity, final String groupingName, final List<String> tags, final String hash, final String search, final QueryScope queryScope, final FilterEntity filter, final List<SortCriteria> sortCriteria, final Date startDate, final Date endDate, final int itemsPerPage) {
		final ListCommand<Post<T>> listCommand = cmd.getListCommand(resourceType);
		// retrieve posts
		log.debug("getPosts " + resourceType + " " + queryScope + " " + groupingEntity + " " + groupingName + " " + listCommand.getStart() + " " + itemsPerPage + " " + filter);
		final int start = listCommand.getStart();
		final Set<Filter> filters = new HashSet<>();
		if (present(filter)) {
			filters.add(filter);
		}

		final PostQueryBuilder queryBuilder = new PostQueryBuilder();
		queryBuilder.setGrouping(groupingEntity)
				.setGroupingName(groupingName)
				.setTags(tags)
				.setHash(hash)
				.search(search)
				.setFilters(filters)
				.setSortCriteria(sortCriteria)
				.setStartDate(startDate)
				.setEndDate(endDate)
				.setScope(queryScope)
				.entriesStartingAt(itemsPerPage, start);

		final List<Post<T>> posts = this.logic.getPosts(queryBuilder.createPostQuery(resourceType));
		listCommand.setList(posts);
		// list settings
		listCommand.setEntriesPerPage(itemsPerPage);
	}

	/**
	 * retrieve the number of posts from the database logic and add it to the command object
	 * 
	 * @param <T> extends Resource
	 * @param cmd the command object
	 * @param resourceType the resource type
	 * @param groupingEntity the grouping entity
	 * @param groupingName the grouping name
	 * @param itemsPerPage number of items to be displayed on each page
	 */
	protected <T extends Resource> void setTotalCount(final SimpleResourceViewCommand cmd, final Class<T> resourceType, final GroupingEntity groupingEntity, final String groupingName, final List<String> tags, final String hash, final String search, final FilterEntity filter, final SortKey sortKey, final Date startDate, final Date endDate, final int itemsPerPage) {
		final ListCommand<Post<T>> listCommand = cmd.getListCommand(resourceType);
		// check if total count already set by resource search
		if (listCommand.getTotalCountAsInteger() != null) {
			return;
		}
		log.debug("getPostStatistics " + resourceType + " " + groupingEntity + " " + groupingName + " " + listCommand.getStart() + " " + itemsPerPage + " " + filter);
		final int start = listCommand.getStart();
		final int totalCount = this.logic.getPostStatistics(resourceType, groupingEntity, groupingName, tags, hash, search, Sets.asSet(filter), sortKey, startDate, endDate, start, start + itemsPerPage).getCount();
		listCommand.setTotalCount(totalCount);
	}
	
	protected void startTiming(final String format) {
		log.info("Handling Controller: " + this.getClass().getSimpleName() + ", format: " + format);
		this.startTime = System.currentTimeMillis();
	}

	protected void endTiming() {
		final long elapsed = System.currentTimeMillis() - this.startTime;
		log.info("[" + this.getClass().getSimpleName() + "] Processing time: " + elapsed + " ms");
	}

	/**
	 * Check if only publications are requested
	 * 
	 * @param cmd - the current command object
	 * @return true if only publications are requested, false otherwise
	 */
	private boolean isPublicationOnlyRequested(final ResourceViewCommand cmd) {
		final Set<Class<? extends Resource>> listsToInitialize = this.getListsToInitialize(cmd);
		return (listsToInitialize.size() == 1) && listsToInitialize.contains(BibTex.class);
	}

	/**
	 * Check if only bookmarks are requested
	 * 
	 * @param cmd - the current command object
	 * @return true if only bookmarks are requested, false otherwise
	 */
	private boolean isBookmarkOnlyRequested(final ResourceViewCommand cmd) {
		final Set<Class<? extends Resource>> listsToInitialize = this.getListsToInitialize(cmd);
		return (listsToInitialize.size() == 1) && listsToInitialize.contains(Bookmark.class);
	}

	/**
	 * Restrict result lists by range from startIndex to endIndex.
	 * 
	 * @param cmd - the command object
	 * @param resourceType - the requested resourcetype
	 * @param startIndex - start index
	 * @param endIndex - end index
	 */
	protected void restrictResourceList(final SimpleResourceViewCommand cmd, final Class<? extends Resource> resourceType, final int startIndex, final int endIndex) {			
		if (BibTex.class.equals(resourceType)) {
			cmd.getBibtex().setList(cmd.getBibtex().getList().subList(startIndex, endIndex));
		}
		if (Bookmark.class.equals(resourceType)) {
			cmd.getBookmark().setList(cmd.getBookmark().getList().subList(startIndex, endIndex));
		}
	}

	/**
	 * @param supportedResources the supportedResources to set
	 */
	public void setSupportedResources(final Set<Class<? extends Resource>> supportedResources) {
		this.supportedResources = supportedResources;
	}

	/**
	 * @param initializeNoResources the noResourcesToInitialize to set
	 */
	public void setInitializeNoResources(final boolean initializeNoResources) {
		this.initializeNoResources = initializeNoResources;
	}

	private Set<Class<? extends Resource>> getUserResourcesFromSettings() {
		final Set<Class<? extends Resource>> resources = new HashSet<Class<? extends Resource>>();
		
		if (this.userSettings.isShowBookmark()) {
			resources.add(Bookmark.class);
		}
		
		if (this.userSettings.isShowBibtex()) {
			resources.add(BibTex.class);
		}
		
		return resources;
	}

	private Set<? extends Class<? extends Resource>> getResourcesForFormat(final String format) {
		if (Views.isPublicationOnlyFormat(format)) {
			return Collections.singleton(BibTex.class);
		}
		
		if (Views.isBookmarkOnlyFormat(format)) {
			return Collections.singleton(Bookmark.class);
		}
		
		return new HashSet<Class<? extends Resource>>(ResourceFactory.getAllResourceClasses());
	}
	
	/**
	 * Choose which resource lists (e.g. bookmarks, publications) a controller has to initialize.
	 * Hereby the resources requested/supported by (i) the controller itself, (ii) the format param, 
	 * (iii) URL param and (iv) user settings are considered. Parameter settings override user settings,
	 * if possible.
	 * @param command
	 * 
	 * @return all resources that must be initialized by this controller
	 */
	public Set<Class<? extends Resource>> getListsToInitialize(final ResourceViewCommand command) {
		final Set<Class<? extends Resource>> resourcesToInitializeFromUser = command.getResourcetype();
		final String format = command.getFormat();
		final Set<Class<? extends Resource>> resourcesToInitialize = new HashSet<Class<? extends Resource>>(resourcesToInitializeFromUser);
		// explicitly don't initialize resources (e.g. when only tags are requested)
		if (this.initializeNoResources) {
			resourcesToInitialize.clear();
		} else if (present(this.forcedResources)) { // some pages (e.g. CV) don't allow to change resources by settings / url params
			resourcesToInitialize.clear();
			resourcesToInitialize.addAll(this.forcedResources);
		} else {
			// resources supported by format parameter 
			final Set<Class<? extends Resource>> supportFormat = intersection(this.supportedResources, this.getResourcesForFormat(format));
			// resources given by URL params
			final Set<Class<? extends Resource>> supportParam = intersection(this.supportedResources, resourcesToInitializeFromUser);
			// resources selected by user settings
			final Set<Class<? extends Resource>> supportUser = intersection(this.supportedResources, this.getUserResourcesFromSettings());
			// we start with an empty return set
			resourcesToInitialize.clear();
			// controller does not support resources required by format param -> empty list
			if (!present(supportFormat) && !present(supportParam)) {
				// do nothing -> return empty set
			} else if (present(supportFormat)) {
				final Set<Class<? extends Resource>> supportFormatParam = intersection(supportFormat, supportParam);
				if (present(supportFormatParam)) {
					// controller supports resources requested by format & URL param
					resourcesToInitialize.addAll(supportFormatParam);
				} else {
					final Set<Class<? extends Resource>> supportFormatUser = intersection(supportFormat, supportUser);
					if (present(supportFormatUser)) {
						// controller supports resources requested by format & user settings
						resourcesToInitialize.addAll(supportFormatUser);
					} else {
						// controller supports resources requested by format
						resourcesToInitialize.addAll(supportFormat);
					}
				}
			} else if (!present(supportFormat) && present(supportParam)) {
				// resources requested by URL param don't match resources supported by format -> empty list
				// do nothing -> return empty set
			} else {
				// default: initialize all resources supported by current controller
				resourcesToInitialize.addAll(this.supportedResources);
			}
		}
		command.setResourcetype(resourcesToInitialize);
		return resourcesToInitialize;
	}
	
	/**
	 * @param userSettings the loginUsers userSettings
	 */
	public void setUserSettings(final UserSettings userSettings) {
		this.userSettings = userSettings;
	}

	/**
	 * @param logic logic interface
	 */
	@Required
	public void setLogic(final LogicInterface logic) {
		this.logic = logic;
	}
	
	/**
	 * @param forcedResources the forcedResources to set
	 */
	public void setForcedResources(final Set<Class<? extends Resource>> forcedResources) {
		this.forcedResources = forcedResources;
	}
}