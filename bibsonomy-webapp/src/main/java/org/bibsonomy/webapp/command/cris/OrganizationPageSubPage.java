package org.bibsonomy.webapp.command.cris;

/**
 * @author dzo
 */
public enum OrganizationPageSubPage {

	/** info page */
	INFO,

	/** persons page */
	PERSONS,

	/** publication page */
	PUBLICATIONS,

	/** projects page */
	PROJECTS
}
