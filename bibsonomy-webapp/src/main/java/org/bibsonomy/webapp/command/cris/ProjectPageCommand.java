package org.bibsonomy.webapp.command.cris;

import org.bibsonomy.model.cris.Project;
import org.bibsonomy.webapp.command.BaseCommand;

/**
 * Command for a project details page
 *
 * @author dzo
 */
public class ProjectPageCommand extends BaseCommand {

	private String requestedProjectId;

	private Project project;

	private String members;

	/**
	 * @return
	 */
	public String getMembers() {
		return members;
	}

	/**
	 * @param members
	 */
	public void setMembers(String members) {
		this.members = members;
	}

	/**
	 * @return the requestedProjectId
	 */
	public String getRequestedProjectId() {
		return requestedProjectId;
	}

	/**
	 * @param requestedProjectId the requestedProjectId to set
	 */
	public void setRequestedProjectId(String requestedProjectId) {
		this.requestedProjectId = requestedProjectId;
	}

	/**
	 * @return the project
	 */
	public Project getProject() {
		return project;
	}

	/**
	 * @param project the project to set
	 */
	public void setProject(Project project) {
		this.project = project;
	}
}
