/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.command.actions;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author jensi
 */
public class PublicationRendererCommand extends PostPublicationCommand {
	private MultipartFile pica;
	private MultipartFile marc;
	/**
	 * @return the pica
	 */
	public MultipartFile getPica() {
		return this.pica;
	}
	/**
	 * @param pica the pica to set
	 */
	public void setPica(MultipartFile pica) {
		this.pica = pica;
	}
	/**
	 * @return the marc
	 */
	public MultipartFile getMarc() {
		return this.marc;
	}
	/**
	 * @param marc the marc to set
	 */
	public void setMarc(MultipartFile marc) {
		this.marc = marc;
	}
	
}
