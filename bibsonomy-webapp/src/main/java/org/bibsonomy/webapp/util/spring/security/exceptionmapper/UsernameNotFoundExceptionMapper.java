/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.util.spring.security.exceptionmapper;

import javax.servlet.http.HttpSession;

import org.bibsonomy.model.User;
import org.bibsonomy.webapp.util.spring.security.exceptions.LdapUsernameNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Maps the data from {@link UsernameNotFoundException} subclass onto our 
 * user object.
 * 
 * @author rja
 */
public abstract class UsernameNotFoundExceptionMapper {

	private String redirectUrl;
	
	/**
	 * Checks if this mapper can handle the given exception.
	 * 
	 * @param e
	 * @return <code>true</code> if the given exception is a subclass of {@link LdapUsernameNotFoundException}.
	 */
	public abstract boolean supports(final UsernameNotFoundException e);

	/**
	 * Maps the user data from the LDAP/OpenID/whatever server to our user object.
	 * 
	 * @param e
	 * @return A user containing the information from the LDAP server.
	 */
	public abstract User mapToUser(final UsernameNotFoundException e);
	
	/**
	 * @param session
	 * @param e 
	 */
	public void writeAdditionAttributes(HttpSession session, UsernameNotFoundException e) {
		// noop
	}

	/**
	 * @return The URL to which the user shall be redirected if she/he needs to
	 * register first.
	 */
	public String getRedirectUrl() {
		return this.redirectUrl;
	}

	/**
	 * The URL to which the user shall be redirected if she/he needs to
	 * register first.
	 * 
	 * @param redirectUrl
	 */
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}