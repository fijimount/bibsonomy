/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.util.spring.factorybeans;

import org.bibsonomy.common.enums.Role;
import org.bibsonomy.database.DBLogicUserInterfaceFactory;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.logic.util.ReadOnlyLogic;
import org.springframework.beans.factory.FactoryBean;

/**
 * Lets {@link DBLogicUserInterfaceFactory} appear as a FactoryBean, which itself
 * is customizable in the spring context.
 * 
 * This bean returns an implementation of the LogicInterface which has admin
 * access enabled. This is done by giving it a user which has the Role "admin".
 *  
 * @see FactoryBean
 * @see Role
 * @author rja
 */
public abstract class AdminLogicFactoryBean extends DBLogicUserInterfaceFactory implements FactoryBean<LogicInterface> {

	private final User user;
	private LogicInterface instance = null;
	private boolean readOnly;
	
	/**
	 * Creates a new instance of the AdminLogicFactoryBean.
	 * 
	 */
	public AdminLogicFactoryBean() {
		this.user = new User();
		user.setRole(Role.ADMIN);
	}
	
	@Override
	public LogicInterface getObject() throws Exception {
		if (instance == null) {
			instance = ReadOnlyLogic.maskLogic(this.getLogicAccess(user.getName(), ""), this.readOnly);
		}
		return instance;
	}
	
	@Override
	protected User getLoggedInUser(String loginName, String password) {
		// return the admin user
		return user;
	}

	@Override
	public Class<?> getObjectType() {
		return LogicInterface.class;
	}

	@Override
	public boolean isSingleton() {
		return false;  // TODO: check if singleton is really only singleton in the scope of the factorybean 
	}
	
	/** Set the name the admin user will have.
	 * 
	 * @param adminUserName
	 */
	public void setAdminUserName(final String adminUserName) {
		this.user.setName(adminUserName);
	}
	
	/** Get the name of the admin user.
	 * 
	 * @return The name of the admin user.
	 */
	public String getAdminUserName() {
		return this.user.getName();
	}

	/**
	 * @param readOnly the readOnly to set
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
}
