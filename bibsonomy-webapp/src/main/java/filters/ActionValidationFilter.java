/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package filters;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.model.User;
import org.bibsonomy.util.StringUtils;
import org.bibsonomy.util.spring.security.AuthenticationUtils;

/**
 * This filter checks the credential of the user and sets a corresponding request 
 * attribute with the result of the check.
 * 
 * Resources in /resources are ignored.
 */
public class ActionValidationFilter implements Filter {
	private final static Log log = LogFactory.getLog(ActionValidationFilter.class);

	/**
	 * TODO: improve documentation
	 */
	public static final String REQUEST_ATTRIB_VALID_CREDENTIAL = "validckey"; // true or false
	
	/**
	 * TODO: improve documentation
	 */
	public static final String REQUEST_ATTRIB_CREDENTIAL = "ckey"; // current ckey
	
	private static final String REQUEST_PARAM_CREDENTIAL = "ckey"; // ckey from request
	

	protected FilterConfig filterConfig = null;

	/**
	 * the resources (css, js) path
	 */
	public static final String STATIC_RESOURCES = "/resources";

	/**
	 * the api path
	 */
	public static final String API = "/api";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest)request;
		String requPath = httpServletRequest.getServletPath();
		/*
		 * ignore resource files (CSS, JPEG/PNG, JavaScript) ... 
		 */
		if (requPath.startsWith(STATIC_RESOURCES) || requPath.startsWith(API)) {
			chain.doFilter(request, response);
			return;
		} 

		/*
		 * get sessions credential storage variable
		 */
		String storedCredential = (String) request.getAttribute(REQUEST_ATTRIB_CREDENTIAL);
		/*
		 * if null, create new one
		 */
		if (storedCredential == null) {
			final User user = AuthenticationUtils.getUser();
			storedCredential = getNewCredential(user, httpServletRequest.getSession());
			request.setAttribute(REQUEST_ATTRIB_CREDENTIAL, storedCredential);
		}
		log.debug("credential = " + storedCredential);
		
		/*
		 * get credential from request parameter
		 * 
		 * FIXME: This does not work with multipart-requests! Thus, on such
		 * requests we must otherwise send the ckey.
		 */
		String requestCredential = request.getParameter(REQUEST_PARAM_CREDENTIAL);
		
		boolean valid = storedCredential.equals(requestCredential);
		if (!valid && log.isDebugEnabled()) {
			log.debug("requested ckey not valid: Expected '" + storedCredential + "' but was '" + requestCredential + "' for '" + ((HttpServletRequest) request).getRequestURI() + "'");
		}
		/*
		 * check and propagate correctness 
		 */
		request.setAttribute(REQUEST_ATTRIB_VALID_CREDENTIAL, valid);

		// Pass control on to the next filter
		chain.doFilter(request, response);

	}
	/** Static method to check validity of the sent credential.
	 * @param request
	 * @return <true> iff ckey is valid
	 */
	public static boolean isValidCkey (final ServletRequest request) {
		final Boolean validCredential = (Boolean) request.getAttribute(REQUEST_ATTRIB_VALID_CREDENTIAL);
		return validCredential != null && validCredential.booleanValue();
	}
	
	/** Creates the user-dependent credential.
	 * 
	 * Currently we use the hashed email-address + session-id of user. If the
	 * email-address is not set we just use the session-id. Another (better) 
	 * option would be the password + session-id. 
	 * Even better would be separate credentials for each specific action
	 * (delete, change, email, etc.).
	 * 
	 * @param user
	 * @param session
	 * @return
	 */
	private static String getNewCredential(User user, HttpSession session) {
		final String userMail = user.getEmail();
		return (StringUtils.getMD5Hash(userMail + session.getId()));
	}
	
	/**
	 * Place this filter into service.
	 *
	 * @param filterConfig The filter configuration object
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	/**
	 * Take this filter out of service.
	 */
	@Override
	public void destroy() {
		this.filterConfig = null;
	}
}
