/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.webapp.controller;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.TagsType;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.GoldStandardPublication;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.UserSettings;
import org.bibsonomy.webapp.command.ResourceViewCommand;
import org.junit.Before;
import org.junit.Test;

/**
 * tests the {@link ResourceListController#getListsToInitialize(String, Set)} method
 * 
 * @author dzo
 */
public class ResourceListControllerTest {
	
	private static class TestResourceListController extends ResourceListController {
		@Override
		protected void handleTagsOnly(ResourceViewCommand cmd, GroupingEntity groupingEntity, String groupingName, String regex, List<String> tags, String hash, int max, String search) {
			// no logic no web request
			this.setInitializeNoResources(true);
		}
	}
	
	private static Set<Class<? extends Resource>> STANDARD_VIEW_CLASSES;
	private static Set<Class<? extends Resource>> ALL_CLASSES;
	private static Set<Class<? extends Resource>> BOOKMARK_CLASS;
	private static Set<Class<? extends Resource>> PUBLICATION_CLASS;
	private static Set<Class<? extends Resource>> GOLD_PUB_CLASS;
	
	private static UserSettings DEFAULT_SETTINGS;
	
	private static final UserSettings getSettings(final boolean showPublication, final boolean showBookmark) {
		final UserSettings settings = new UserSettings();
		settings.setShowBibtex(showPublication);
		settings.setShowBookmark(showBookmark);
		return settings;
	}
		
	@Before
	public void createSets() {
		STANDARD_VIEW_CLASSES = new HashSet<Class<? extends Resource>>();
		STANDARD_VIEW_CLASSES.add(Bookmark.class);
		STANDARD_VIEW_CLASSES.add(BibTex.class);
		
		ALL_CLASSES = new HashSet<Class<? extends Resource>>(STANDARD_VIEW_CLASSES);
		ALL_CLASSES.add(GoldStandardPublication.class);
		
		BOOKMARK_CLASS = new HashSet<Class<? extends Resource>>();
		BOOKMARK_CLASS.add(Bookmark.class);
		
		PUBLICATION_CLASS = new HashSet<Class<? extends Resource>>();
		PUBLICATION_CLASS.add(BibTex.class);
		
		GOLD_PUB_CLASS =  new HashSet<Class<? extends Resource>>();
		GOLD_PUB_CLASS.add(GoldStandardPublication.class);
		
		DEFAULT_SETTINGS = getSettings(true, true);
	}
	
	@Test
	public void handleTagsOnly() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(ALL_CLASSES));
		testController.setUserSettings(DEFAULT_SETTINGS);
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setTagstype(TagsType.DEFAULT);
		testController.handleTagsOnly(cmd, null, null, null, null, null, 0, null);
		cmd.setFormat("");
		cmd.setResourcetype(Collections.<Class<? extends Resource>>emptySet());
		assertEquals(Collections.emptySet(), testController.getListsToInitialize(cmd));
	}
	
	@Test
	public void test10() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(BOOKMARK_CLASS));
		testController.setUserSettings(getSettings(true, false));
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setFormat("html");
		cmd.setResourcetype(Collections.<Class<? extends Resource>>emptySet());
		assertEquals(BOOKMARK_CLASS, testController.getListsToInitialize(cmd));
		
		cmd.setFormat("bibtex");
		cmd.setResourcetype(PUBLICATION_CLASS);
		assertEquals(Collections.emptySet(), testController.getListsToInitialize(cmd));
		
		cmd.setFormat("bibtex");
		cmd.setResourcetype(Collections.<Class<? extends Resource>>emptySet());
		assertEquals(Collections.emptySet(), testController.getListsToInitialize(cmd));
		
		cmd.setFormat("bibtex");
		cmd.setResourcetype(BOOKMARK_CLASS);
		assertEquals(Collections.emptySet(), testController.getListsToInitialize(cmd));
	}
	
	@Test
	public void JSONFormat() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(ALL_CLASSES));
		testController.setUserSettings(DEFAULT_SETTINGS);
		/*
		 *  NEW
		 */
		final String format = "json";
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setFormat(format);
		cmd.setResourcetype(Collections.<Class<? extends Resource>>emptySet());
		assertEquals(STANDARD_VIEW_CLASSES, testController.getListsToInitialize(cmd));
		cmd.setResourcetype(PUBLICATION_CLASS);
		assertEquals(PUBLICATION_CLASS, testController.getListsToInitialize(cmd));
		cmd.setResourcetype(GOLD_PUB_CLASS);
		assertEquals(GOLD_PUB_CLASS, testController.getListsToInitialize(cmd));
	}
	
	@Test
	public void wrongUsage() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(ALL_CLASSES));
		testController.setUserSettings(DEFAULT_SETTINGS);
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setFormat("bookpubl");
		cmd.setResourcetype(PUBLICATION_CLASS);
		// TODO: DISCUSS: bookmark vs empty list (here the format is enforcing to initialize bookmarks)
		assertEquals(BOOKMARK_CLASS, testController.getListsToInitialize(cmd));
	}
	
	@Test
	public void wrongUsagePublication() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(ALL_CLASSES));
		testController.setUserSettings(DEFAULT_SETTINGS);
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setFormat("bibtex");
		cmd.setResourcetype(BOOKMARK_CLASS);
		// TODO: DISCUSS: publication vs empty list (here the format is enforcing to initialize publications)
		assertEquals(PUBLICATION_CLASS, testController.getListsToInitialize(cmd));
	}
	
	@Test
	public void respectUserSettings() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(ALL_CLASSES));
		
		/*
		 * bookmark and publication settings activated
		 */
		testController.setUserSettings(getSettings(true, true));
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setFormat("html");
		cmd.setResourcetype(Collections.<Class<? extends Resource>>emptySet());
		assertEquals(STANDARD_VIEW_CLASSES, testController.getListsToInitialize(cmd));
		
		/*
		 * respects user settings
		 */
		testController.setUserSettings(getSettings(true, false));
		cmd.setResourcetype(Collections.<Class<? extends Resource>>emptySet()); // reset command
		assertEquals(PUBLICATION_CLASS, testController.getListsToInitialize(cmd));
		
		/*
		 * url param "overrides" user settings
		 */
		cmd.setResourcetype(BOOKMARK_CLASS);
		assertEquals(BOOKMARK_CLASS, testController.getListsToInitialize(cmd));
		
		
		testController.setUserSettings(getSettings(false, true));
		cmd.setFormat("bibtex");
		cmd.setResourcetype(BOOKMARK_CLASS); // reset command
		assertEquals(PUBLICATION_CLASS, testController.getListsToInitialize(cmd));
	}
	
	@Test
	public void complexOne() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(PUBLICATION_CLASS));
		testController.setUserSettings(DEFAULT_SETTINGS);
		
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setFormat("bookpubl");
		cmd.setResourcetype(PUBLICATION_CLASS);
		
		assertEquals(Collections.emptySet(), testController.getListsToInitialize(cmd));
	}
	
	@Test
	public void enforceTest() {
		final TestResourceListController testController = new TestResourceListController();
		testController.setSupportedResources(new HashSet<Class<? extends Resource>>(ALL_CLASSES));
		testController.setUserSettings(DEFAULT_SETTINGS);
		testController.setForcedResources(STANDARD_VIEW_CLASSES);
		
		final ResourceViewCommand cmd = new ResourceViewCommand();
		cmd.setFormat("bibtex");
		cmd.setResourcetype(GOLD_PUB_CLASS);
		
		assertEquals(STANDARD_VIEW_CLASSES, testController.getListsToInitialize(cmd));
	}
}
