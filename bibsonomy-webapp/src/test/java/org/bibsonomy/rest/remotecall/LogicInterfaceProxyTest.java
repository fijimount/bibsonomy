/**
 * BibSonomy-Webapp - The web application for BibSonomy.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.remotecall;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.common.JobResult;
import org.bibsonomy.common.SortCriteria;
import org.bibsonomy.common.enums.Filter;
import org.bibsonomy.common.enums.GroupRole;
import org.bibsonomy.common.enums.GroupUpdateOperation;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.common.enums.QueryScope;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.TagSimilarity;
import org.bibsonomy.common.enums.UserRelation;
import org.bibsonomy.common.enums.UserUpdateOperation;
import org.bibsonomy.common.exceptions.ObjectMovedException;
import org.bibsonomy.common.exceptions.ObjectNotFoundException;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.Group;
import org.bibsonomy.model.GroupMembership;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.ResourcePersonRelation;
import org.bibsonomy.model.Tag;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.model.logic.LogicInterfaceFactory;
import org.bibsonomy.model.logic.query.GroupQuery;
import org.bibsonomy.model.logic.query.PostQuery;
import org.bibsonomy.model.logic.query.ResourcePersonRelationQuery;
import org.bibsonomy.model.logic.querybuilder.PostQueryBuilder;
import org.bibsonomy.model.logic.util.AbstractLogicInterface;
import org.bibsonomy.rest.BasicAuthenticationHandler;
import org.bibsonomy.rest.RestServlet;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.bibsonomy.rest.renderer.RendererFactory;
import org.bibsonomy.rest.renderer.RenderingFormat;
import org.bibsonomy.rest.renderer.UrlRenderer;
import org.bibsonomy.services.filesystem.extension.ListExtensionChecker;
import org.bibsonomy.testutil.CommonModelUtils;
import org.bibsonomy.testutil.ModelUtils;
import org.bibsonomy.util.HashUtils;
import org.bibsonomy.util.SortUtils;
import org.bibsonomy.util.file.ServerFileLogic;
import org.bibsonomy.webapp.util.file.document.ServerDocumentFileLogic;
import org.easymock.EasyMock;
import org.easymock.IArgumentMatcher;
import org.eclipse.jetty.server.AbstractConnector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.bio.SocketConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.MultipartFilter;

import javax.servlet.DispatcherType;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Tests remote calls via an LogicInterface remote proxy.
 * This test starts the whole rest-server webapplication in its own servlet
 * container. It initializes the RestServlet with a special
 * LogicInterfaceFactory-backend, which actually is an easymock object that
 * is used to check if all calls on the remote proxy go through the REST
 * based protocol without being harmed and trigger the same calls on the
 * server side LogicInterface mock as expected. The returnvalues also get
 * recorded into the mock object on the server and are compared with the
 * actual returnvalues of the remote proxy on the client side.
 * 
 * The class itself implements LogicInterface to ensure all methods are
 * contained in this test and to allow testruns of the methods with
 * different arguments passed by method calls in the test methods. 
 * 
 * TODO: we should go over all "excluded properties" and try to unify them,
 *  so that we don't have to modify several locations when a new property 
 *  is added that is to be excluded
 * 
 * @author Jens Illig
 * @author Christian Kramer
 */
public class LogicInterfaceProxyTest extends AbstractLogicInterface {
	
	/*
	 * FIXME: clean up this mess :-(
	 */
	private static final String COMMON_USER_PROPERTIES = "apiKey|homepage|realname|email|password|date|openURL|gender|place|interests|hobbies|IPAddress|clipboard|inbox|profession|institution|place|spammer|settings|toClassify|registrationLog|updatedBy|gravatarAddress";
	private static final String[] IGNORE1 = new String[] {"[0].date", "[0].user.apiKey", "[0].user.email", "[0].user.homepage", "[0].user.password", "[0].user.passwordSalt", "[0].user.realname", "[0].user.confidence", "[0].resource.scraperId", "[0].resource.openURL", "[0].resource.numberOfRatings", "[0].resource.rating", "[0].user.IPAddress", "[0].user.clipboard", "[0].user.inbox", "[0].user.gender", "[0].user.interests", "[0].user.hobbies", "[0].user.profession", "[0].user.institution", "[0].user.openURL", "[0].user.place", "[0].user.spammer", "[0].user.settings", "[0].user.algorithm", "[0].user.prediction", "[0].user.mode", "[0].user.updatedBy", "[0].user.toClassify", "[0].user.registrationLog", "[0].user.reminderPassword", "[0].user.openID", "[0].user.ldapId", "[0].user.activationCode", "[0].user.remoteUserIds", "[0].user.gravatarAddress"};
	private static final String[] IGNORE2 = new String[] {"activationCode", "apiKey", "email", "homepage", "password", "passwordSalt", "realname", "date", "openURL", "gender", "place", "IPAddress", "clipboard", "inbox", "profession", "spammer", "settings", "hobbies", "interests", "toClassify", "registrationLog", "updatedBy", "reminderPassword", "openID", "ldapId", "institution", "remoteUserIds", "gravatarAddress"};
	private static final String[] IGNORE3 = new String[] {"date", "user.activationCode", "user.apiKey", "user.email", "user.homepage", "user.password", "user.passwordSalt", "user.realname", "resource.scraperId", "resource.openURL", "resource.numberOfRatings", "resource.rating", "user.IPAddress", "user.clipboard", "user.inbox", "user.gender", "user.interests", "user.hobbies", "user.profession", "user.institution", "user.openURL", "user.place", "user.spammer", "user.confidence", "user.settings", "user.algorithm", "user.prediction", "user.mode", "user.toClassify", "user.registrationLog", "user.updatedBy", "user.reminderPassword", "user.openID", "user.ldapId", "user.remoteUserIds", "user.gravatarAddress"};
	
	private static final int PORT = 41252;

	private static final Log log = LogFactory.getLog(LogicInterfaceProxyTest.class);
	
	private static final String LOGIN_USER_NAME = LogicInterfaceProxyTest.class.getSimpleName().toLowerCase();
	private static final String API_KEY = "A P I äöü K e y";
	private static Server server;
	private static String apiUrl;
	private static LogicInterfaceFactory clientLogicFactory;
	
	
	private LogicInterface clientLogic;
	private LogicInterface serverLogic;

	@Override
	public List<ResourcePersonRelation> getResourceRelations(ResourcePersonRelationQuery query) {
		return new ArrayList<>(); //FIXME (AD) implement stub
	}

	/**
	 * MultipartFilter that does not require a SpringContext
	 * @author Jens Illig
	 */
	public static class CommonsMultiPartFilter extends MultipartFilter {
		private static final MultipartResolver resolver = new CommonsMultipartResolver();
		@Override
		protected MultipartResolver lookupMultipartResolver(final HttpServletRequest request) {
			return resolver;
		}
	}
	
	/**
	 * configures the server and the webapp and starts the server
	 */
	@BeforeClass
	public static void initServer() {
		initServer(RenderingFormat.XML);
	}
	
	public static void initServer(final RenderingFormat renderingFormat) {
		try {
			server = new Server();
			final AbstractConnector connector = new SocketConnector();
			connector.setHost("127.0.0.1");
			connector.setPort(PORT);
			
			apiUrl = "http://localhost:" + PORT + "/api";
			server.addConnector(connector);
			final ServletContextHandler servletContext = new ServletContextHandler();
			servletContext.setContextPath("/api");
			
			final RestServlet restServlet = new RestServlet();
			final UrlRenderer urlRenderer = new UrlRenderer(apiUrl);
			restServlet.setUrlRenderer(urlRenderer);
			restServlet.setRendererFactory(new RendererFactory(urlRenderer));
			restServlet.setFileLogic(createFileLogic());
			
			try {
				final BasicAuthenticationHandler handler = new BasicAuthenticationHandler();
				handler.setLogicFactory(new MockLogicFactory());
				restServlet.setAuthenticationHandlers(Arrays.asList(handler));
			} catch (final Exception e) {
				throw new RuntimeException("problem while instantiating " + MockLogicFactory.class.getName(), e);
			}
			
			servletContext.addServlet(RestServlet.class, "/*").setServlet(restServlet);
			
			servletContext.addFilter(CommonsMultiPartFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
			
			server.setHandler(servletContext);
			server.start();
			connector.start();
			
			clientLogicFactory = new RestLogicFactory(apiUrl, renderingFormat);
		} catch (final Exception ex) {
			log.fatal(ex.getMessage(),ex);
			throw new RuntimeException(ex);
		}
	}

	protected static ServerFileLogic createFileLogic() {
		final ServerFileLogic fileLogic = new ServerFileLogic();
		final ServerDocumentFileLogic documentLogic = new ServerDocumentFileLogic(getTmpDir());
		documentLogic.setExtensionChecker(new ListExtensionChecker(Arrays.asList("pdf", "ps", "txt")));
		fileLogic.setDocumentFileLogic(documentLogic);
		return fileLogic;
	}

	private static String getTmpDir() {
		File f;
		try {
			f = File.createTempFile("dummy", "tmp");
		} catch (final IOException ex) {
			throw new RuntimeException(ex);
		}
		try {
			return f.getParent() + File.separator;
		} finally {
			f.delete();
		}
	}

	/**
	 * stops the servlet container after all tests have been run
	 */
	@AfterClass
	public static void shutdown() {
		try {
			server.stop();
		} catch (final Exception ex) {
			log.fatal(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
	}

	private static List<String> convertToHashes(List<JobResult> list) {
		return list.stream().map(JobResult::getId).collect(Collectors.toList());
	}

	private static void assertLogin() {
		assertEquals(LOGIN_USER_NAME, MockLogicFactory.getRequestedLoginName());
		assertEquals(API_KEY, MockLogicFactory.getRequestedApiKey());
	}
	
	/**
	 * builds a new mock backend on the serer and a new remote proxy on the client for each test-run
	 */
	@Before
	public void setUp() {
		this.clientLogic = clientLogicFactory.getLogicAccess(LOGIN_USER_NAME, API_KEY);
		this.serverLogic = EasyMock.createMock(LogicInterface.class);
		EasyMock.expect(this.serverLogic.getAuthenticatedUser()).andReturn(new User(LOGIN_USER_NAME)).anyTimes();
		MockLogicFactory.init(this.serverLogic);
	}

	/**
	 * resets the mock object on the server
	 */
	@After
	public void tearDown() {
		EasyMock.reset(this.serverLogic);
	}
	
	private interface Checker<T> {
		boolean check(T obj);
	}
	
	private static class CheckerDelegatingMatcher<T> implements IArgumentMatcher {

		private final Checker<T> checker;

		private CheckerDelegatingMatcher(final Checker<T> checker) {
			this.checker = checker;
		}
		
		@SuppressWarnings("unchecked") // classcastexception is ok in testcase 
		@Override
		public boolean matches(final Object argument) {
			return this.checker.check((T) argument);
		}

		@Override
		public void appendTo(final StringBuffer buffer) {
			buffer.append("checker: " + this.checker);
		}
		
		/**
		 * Tells {@link EasyMock} to have the argument checked by the given Checker
		 * @param checker
		 * @return unimportant
		 */
		public static <T> T check(final Checker<T> checker) {
			EasyMock.reportMatcher(new CheckerDelegatingMatcher<T>(checker));
			return null;
		}
	}
	
	
	/** IArgumentMatcher Implementation that wraps an object and compares it with another 
	 * @param <T> type of stuff to be compared */
	private static class PropertyEqualityArgumentMatcher<T> implements IArgumentMatcher {
		private final T a;
		private final String[] excludeProperties; 
		
		private PropertyEqualityArgumentMatcher(final T a, final String... excludeProperties) {
			this.a = a;
			this.excludeProperties = excludeProperties;
		}
		
		@Override
		public void appendTo(final StringBuffer arg0) {
			arg0.append("hurz");
		}

		@Override
		public boolean matches(final Object b) {
			try {
				CommonModelUtils.assertPropertyEquality(this.a, b, 5, null, this.excludeProperties);
			} catch (final Throwable t) {
				log.error(t,t);
				return false;
			}
			return true;
		}
		
		/**
		 * tells easymock how the next argument has to be compared and returns the next argument itself
		 * @param <T> Type of the argument
		 * @param a the argument
		 * @param excludeProperties array of propertynames, which shall be left out in comparison 
		 * @return the next argument
		 */
		public static <T> T eq(final T a, final String... excludeProperties) {
			EasyMock.reportMatcher(new PropertyEqualityArgumentMatcher<>(a, excludeProperties));
			return a;
		}
	}


	/**
	 * runs the test defined by {@link #createGroup(Group)} with certain arguments
	 */
	@Test
	public void createGroupTest() {
		this.createGroup(ModelUtils.getGroup());
	}
	
	@Override
	public String createGroup(final Group group) {

		/*
		 * FIXME: remove this line. It is here only, because privlevel is not included 
		 * in the XML and hence not transported to the serverLogic.
		 */
		group.setPrivlevel(null); 
		
		EasyMock.expect(this.serverLogic.createGroup(PropertyEqualityArgumentMatcher.eq(group, "groupId", "id"))).andReturn(group.getName() + "-new");
		EasyMock.replay(this.serverLogic);
		assertEquals(group.getName() + "-new", this.clientLogic.createGroup(group));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return null;
	}
	
	/**
	 * runs the test defined by {@link #createPosts(List)} with a populated Bookmark Post as the argument
	 */
	@Test
	public void createPostTestBookmark() {
		final List<Post<?>> posts = new LinkedList<>();
		posts.add(ModelUtils.generatePost(Bookmark.class));
		this.createPosts(posts);
	}
	/**
	 * runs the test defined by {@link #createPosts(List)} with a populated Publication Post as the argument
	 */
	@Test
	public void createPostTestPublication() {
		final List<Post<?>> posts = new LinkedList<>();
		posts.add(ModelUtils.generatePost(BibTex.class));
		this.createPosts(posts);
	}

	@Override
	public List<JobResult> createPosts(final List<Post<?>> posts) {
		final Post<?> post = posts.get(0);
		post.getUser().setName(LOGIN_USER_NAME);

		final List<JobResult> singletonList = Collections.singletonList(JobResult.buildSuccess(post.getResource().getIntraHash()));

		EasyMock.expect(this.serverLogic.createPosts(PropertyEqualityArgumentMatcher.eq(posts, IGNORE1))).andReturn(singletonList);
		EasyMock.replay(this.serverLogic);
		final List<JobResult> clientList = this.clientLogic.createPosts(posts);
		assertEquals(convertToHashes(singletonList), convertToHashes(clientList));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return null;
	}

	/**
	 * runs the test defined by {@link #createUser(User)} with a certain argument
	 */
	@Test
	public void createUserTest() {
		this.createUser(ModelUtils.getUser());
	}
	
	@Override
	public String createUser(final User user) {

		final User eqUser = PropertyEqualityArgumentMatcher.eq(user, IGNORE2);
		
		final String userName = this.serverLogic.createUser(eqUser);
		
		EasyMock.expect(userName).andReturn(user.getName() + "-new");
		EasyMock.replay(this.serverLogic);
		assertEquals(user.getName() + "-new", this.clientLogic.createUser(user));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return null;
	}

	/**
	 * runs the test defined by {@link #deleteGroup(String, boolean, boolean)} with a certain
	 * argument. TODO: Implement this!
	 * 
	 * TODO: re-enable test as soon as rest server supports deleting groups
	 */
	@Ignore
	@Test
	public void deleteGroupTest() {
		this.deleteGroup("hurzelGroupName", false, false);
	}
	
	@Override
	public void deleteGroup(final String groupName, boolean pending, boolean quickDelete) {
		this.serverLogic.deleteGroup(groupName, false, false);
		EasyMock.replay(this.serverLogic);
		this.clientLogic.deleteGroup(groupName, false, false);
		EasyMock.verify(this.serverLogic);
		assertLogin();
	}

	/**
	 * runs the test defined by {@link #deletePosts(String, List)} with certain arguments
	 */
	@Test
	public void deletePostTest() {
		this.deletePosts("hurzelUserName", Collections.singletonList(ModelUtils.getBookmark().getIntraHash()));
	}
	
	@Override
	public void deletePosts(final String userName, final List<String> resourceHashes) {
		this.serverLogic.deletePosts(userName, resourceHashes);
		EasyMock.replay(this.serverLogic);
		this.clientLogic.deletePosts(userName, resourceHashes);
		EasyMock.verify(this.serverLogic);
		assertLogin();
	}

	/**
	 * runs the test defined by {@link #deleteUser(String)} with a certain argument
	 */
	@Test
	public void deleteUserTest() {
		this.deleteUser("hurzelUserName");
	}
	
	@Override
	public void deleteUser(final String userName) {
		this.serverLogic.deleteUser(userName);
		EasyMock.replay(this.serverLogic);
		this.clientLogic.deleteUser(userName);
		EasyMock.verify(this.serverLogic);
		assertLogin();
	}

	@Override
	public User getAuthenticatedUser() {
		// no need to test this as it is part of the client
		return null;
	}

	/**
	 * runs the test defined by {@link #getGroupDetails(String, boolean)} with a certain argument
	 */
	@Test
	public void getGroupDetailsTest() {
		this.getGroupDetails("hurzelGroupName", false);
	}
	
	@Override
	public Group getGroupDetails(final String groupName, final boolean pending) {
		final Group returnedGroupExpectation = ModelUtils.getGroup();
		
		/*
		 * FIXME: remove this line. It is here only, because privlevel is not included 
		 * in the XML and hence not transported to the serverLogic.
		 */
		returnedGroupExpectation.setPrivlevel(null); 
		
		final List<User> users = new ArrayList<>();
		users.add(ModelUtils.getUser());
		users.get(0).setName("Nr1");
		users.add(ModelUtils.getUser());
		for (final User u : users) {
			u.setApiKey(null);
			u.setPassword(null);
			final GroupMembership groupMembership = new GroupMembership();
			groupMembership.setUser(u);
			returnedGroupExpectation.getMemberships().add(groupMembership);
		}
		EasyMock.expect(this.serverLogic.getGroupDetails(groupName, false)).andReturn(returnedGroupExpectation);
		EasyMock.replay(this.serverLogic);
		final Group returnedGroup = this.clientLogic.getGroupDetails(groupName, false);

		CommonModelUtils.assertPropertyEquality(returnedGroupExpectation, returnedGroup, 5, Pattern.compile(".*users.*\\.(" + COMMON_USER_PROPERTIES + ")|.*\\.date|.*\\.scraperId|.*\\.openURL|.*groupId|user.*|.*id"));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returnedGroup;
	}
	
	/**
	 * runs the test defined by {@link #getGroups(GroupQuery)} with certain arguments
	 */
	@Test
	public void getGroupsTest() {
		final GroupQuery groupQuery = GroupQuery.builder().start(64).end(219).build();
		this.getGroups(groupQuery);
	}

	@Override
	public List<Group> getGroups(GroupQuery query) {
		final List<Group> expectedList = new ArrayList<>();
		expectedList.add(ModelUtils.getGroup());
		expectedList.get(0).setName("Group1");
		expectedList.get(0).setGroupId(42);
		/*
		 * FIXME: remove this line. It is here only, because privlevel is not included
		 * in the XML and hence not transported to the serverLogic.
		 */
		expectedList.get(0).setPrivlevel(null);
		expectedList.add(ModelUtils.getGroup());
		expectedList.get(1).setName("Group2");
		expectedList.get(0).setGroupId(23);
		/*
		 * FIXME: remove this line. It is here only, because privlevel is not included
		 * in the XML and hence not transported to the serverLogic.
		 */
		expectedList.get(1).setPrivlevel(null);
		EasyMock.expect(this.serverLogic.getGroups(PropertyEqualityArgumentMatcher.eq(query))).andReturn(expectedList);
		EasyMock.replay(this.serverLogic);
		final List<Group> returnedGroups = this.clientLogic.getGroups(query);
		CommonModelUtils.assertPropertyEquality(expectedList, returnedGroups, 3, Pattern.compile(".*\\.groupId|.*\\.id"));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returnedGroups;
	}

	/**
	 * runs the test defined by {@link #getPostDetails(String, String)} with certain arguments
	 */
	@Test
	public void getPostDetailsTest() {
		this.getPostDetails(ModelUtils.getBibTex().getIntraHash(), "testUser");
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Post<? extends org.bibsonomy.model.Resource> getPostDetails(final String resourceHash, final String userName) {
		final Post<BibTex> expectedPublicationPost = ModelUtils.generatePost(BibTex.class);
		final Post<Bookmark> expectedBookmarkPost = ModelUtils.generatePost(Bookmark.class);
		
		try {
			EasyMock.expect(this.serverLogic.getPostDetails(resourceHash, userName)).andReturn((Post) expectedPublicationPost);
		} catch (final ObjectNotFoundException | ObjectMovedException ex) {
			// ignore
		}
		try {
			EasyMock.expect(this.serverLogic.getPostDetails(resourceHash, userName)).andReturn((Post) expectedBookmarkPost);
		} catch (final ObjectNotFoundException | ObjectMovedException ex) {
			// ignore
		}
		EasyMock.replay(this.serverLogic);
		
		Post<? extends org.bibsonomy.model.Resource> returnedPublicationPost = null;
		try {
			returnedPublicationPost = this.clientLogic.getPostDetails(resourceHash,userName);
		} catch (final ObjectNotFoundException | ObjectMovedException ex) {
			// ignore
		}
		CommonModelUtils.assertPropertyEquality(expectedPublicationPost, returnedPublicationPost, 5, null, IGNORE3);
		Post<? extends org.bibsonomy.model.Resource> returnedBookmarkPost = null;
		try {
			returnedBookmarkPost = this.clientLogic.getPostDetails(resourceHash,userName);
		} catch (final ObjectNotFoundException | ObjectMovedException ex) {
			// ignore
		}
		CommonModelUtils.assertPropertyEquality(expectedBookmarkPost, returnedBookmarkPost, 5, null, IGNORE3);
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returnedPublicationPost;
	}
	
	/**
	 * runs the test with arguments as used for the getBookmarkByTagName query
	 */
	@Test
	public void getPostsTestBookmarkByTag() {
		final PostQueryBuilder postQueryBuilder = new PostQueryBuilder();
		postQueryBuilder.setGrouping(GroupingEntity.ALL)
						.setTags(Arrays.asList("bla", "blub"))
						.setScope(QueryScope.LOCAL)
						.fromTo(7, 1264);
		this.getPosts(postQueryBuilder.createPostQuery(BibTex.class));
	}
	
	/**
	 * runs the test with arguments as used for the getPublicationForGroupAndTag query
	 */
	@Test
	public void getPostsTestPublicationByGroupAndTag() {
		final PostQueryBuilder postQueryBuilder = new PostQueryBuilder();
		postQueryBuilder.setGrouping(GroupingEntity.GROUP)
						.setGroupingName("testGroup")
						.setTags(Arrays.asList("blub", "bla"))
						.setScope(QueryScope.LOCAL)
						.fromTo(0, 1);
		this.getPosts(postQueryBuilder.createPostQuery(BibTex.class));
	}
	
	/**
	 * tests whether tags with umlauts can be queried correctly
	 */
	@Test
	public void getPostsTestPublicationByTagWithUmlaut() {
		final PostQueryBuilder postQueryBuilder = new PostQueryBuilder();
		postQueryBuilder.setGrouping(GroupingEntity.ALL)
						.setTags(Collections.singletonList("blüb"))
						.setScope(QueryScope.LOCAL)
						.fromTo(0, 5);
		this.getPosts(postQueryBuilder.createPostQuery(BibTex.class));
	}
	
	/**
	 * runs the test with arguments as used for the getPublicationByHashForUser query
	 */
	@Test
	public void getPostsTestPublicationByUserAndHash() {
		final PostQueryBuilder postQueryBuilder = new PostQueryBuilder();
		postQueryBuilder.setGrouping(GroupingEntity.USER)
						.setGroupingName("testUser")
						.setHash(ModelUtils.getBibTex().getIntraHash())
						.setSortCriteria(SortUtils.singletonSortCriteria(SortKey.FOLKRANK))
						.setScope(QueryScope.LOCAL)
						.fromTo(0, 5);
		this.getPosts(postQueryBuilder.createPostQuery(BibTex.class));
	}
	
	@Test
	public void getPostsTestWithSearchAndSortKey() {
		// FIXME: search with folkrank sort does not make sense
		final PostQueryBuilder postQueryBuilder = new PostQueryBuilder();
		postQueryBuilder.setGrouping(GroupingEntity.ALL)
						.search("search")
						.setSortCriteria(SortUtils.singletonSortCriteria(SortKey.FOLKRANK))
						.setScope(QueryScope.LOCAL)
						.fromTo(0, 5);
		this.getPosts(postQueryBuilder.createPostQuery(BibTex.class));
	}

	@Override
	public <R extends Resource> List<Post<R>> getPosts(final PostQuery<R> query) {
		// set some default values of the rest server
		if (query.getTags() == null) {
			query.setTags(Collections.emptyList());
		}
		if (query.getSortCriteria() == null) {
			query.setSortCriteria(Collections.emptyList());
		}

		final Class<R> resourceType = query.getResourceClass();
		final List<Post<R>> expectedPosts = new ArrayList<>();
		expectedPosts.add(ModelUtils.generatePost(resourceType));
		expectedPosts.get(0).setDescription("erstes");
		expectedPosts.add(ModelUtils.generatePost(resourceType));
		if (resourceType == org.bibsonomy.model.Resource.class) {
			expectedPosts.add((Post) ModelUtils.generatePost(Bookmark.class));
			expectedPosts.add((Post) ModelUtils.generatePost(BibTex.class));
		}

		EasyMock.expect(this.serverLogic.getPosts(PropertyEqualityArgumentMatcher.eq(query))).andReturn(expectedPosts);
		EasyMock.replay(this.serverLogic);

		final List<Post<R>> returnedPosts = this.clientLogic.getPosts(query);
		CommonModelUtils.assertPropertyEquality(expectedPosts, returnedPosts, 5, Pattern.compile(".*\\.user\\.(" + COMMON_USER_PROPERTIES + "|confidence|activationCode|reminderPassword|openID|ldapId|remoteUserIds|prediction|algorithm|mode)|.*\\.date|.*\\.scraperId|.*\\.openURL|.*\\.numberOfRatings|.*\\.rating"));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returnedPosts;
	}

	/**
	 * runs the test defined by {@link #getTagDetails(String)} with a certain argument
	 */
	@Test
	public void getTagDetailsTest() {
		this.getTagDetails("testzeug");
	}	
	
	@Override
	public Tag getTagDetails(final String tagName) {
		final Tag expected = ModelUtils.getTag();		
		EasyMock.expect(this.serverLogic.getTagDetails(tagName)).andReturn(expected);
		EasyMock.replay(this.serverLogic);
		
		final Tag returned = this.clientLogic.getTagDetails(tagName);
		CommonModelUtils.assertPropertyEquality(expected, returned, 3, Pattern.compile("(.*\\.)?(id|stem)"));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returned;
	}

	/**
	 * runs the test defined by {@link #getTags(Class, GroupingEntity, String, String, List, String, SortKey, int, int, String, TagSimilarity)} with certain arguments
	 */
	@Test
	public void getTagsTest() {
		this.getTags(org.bibsonomy.model.Resource.class, GroupingEntity.GROUP, "testGroup", null, null, null, "regex", null, null, null, null, 4, 22);
	}
	
	
	@Override
	public List<Tag> getTags(final Class<? extends Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final String search, final String regex, final TagSimilarity relation, final SortKey sortKey, final Date startDate, final Date endDate, final int start, final int end) {
		final List<Tag> expected = ModelUtils.buildTagList(3, "testPrefix", 1);		
		EasyMock.expect(this.serverLogic.getTags(resourceType, grouping, groupingName, tags, null, null, regex, null, sortKey, null, null, start, end)).andReturn(expected);
		EasyMock.replay(this.serverLogic);
		
		final List<Tag> returned = this.clientLogic.getTags(resourceType, grouping, groupingName, tags, null, null, regex, null, sortKey, null, null, start, end);
		CommonModelUtils.assertPropertyEquality(expected, returned, 5, Pattern.compile("(.*\\.)?(id|stem)"));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returned;
	}

	/**
	 * runs the test defined by {@link #getUserDetails(String)} with a certain arguments
	 */
	@Test
	public void getUserDetailsTest() {
		this.getUserDetails("usrName");
	}
	
	@Override
	public User getUserDetails(final String userName) {
		final User expected = ModelUtils.getUser();		
		EasyMock.expect(this.serverLogic.getUserDetails(userName)).andReturn(expected);
		EasyMock.replay(this.serverLogic);
		
		final User returned = this.clientLogic.getUserDetails(userName);
		CommonModelUtils.assertPropertyEquality(expected, returned, 3, null, IGNORE2);
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returned;
	}

	/**
	 * runs the test defined by {@link #updateGroup(Group, GroupUpdateOperation)} with certain group argument
	 */
	@Test
	public void updateGroupTest() {
		this.updateGroup(ModelUtils.getGroup(), GroupUpdateOperation.UPDATE_ALL, null);
	}
	
	/**
	 * runs the test to add a user to a group 
	 */	
	@Test
	public void addUserToGroupTest() {
		final Group group = new Group("groupName");
		final GroupMembership membership = new GroupMembership(new User("testUser1"), GroupRole.USER, false);
		this.updateGroup(group, GroupUpdateOperation.ADD_MEMBER, membership);
	}
	
	/**
	 * runs the test defined by {@link #deleteUserFromGroup(String, String)} with certain arguments
	 */
	@Test
	public void deleteUserFromGroupTest() {
		final Group group = new Group("grooouuup!");
		final GroupMembership membership = new GroupMembership();
		membership.setUser(new User("userTest"));
		
		this.updateGroup(group, GroupUpdateOperation.REMOVE_MEMBER, membership);
	}
	
	@Override
	public String updateGroup(final Group group, final GroupUpdateOperation operation, final GroupMembership membership) {
		final String groupName = group.getName();
		switch (operation) {
		case ADD_MEMBER:
			EasyMock.expect(this.serverLogic.updateGroup(PropertyEqualityArgumentMatcher.eq(group, "groupId", "id"),
					PropertyEqualityArgumentMatcher.eq(operation),
					PropertyEqualityArgumentMatcher.eq(membership))).andReturn("OK");
			EasyMock.replay(this.serverLogic);
			assertEquals("OK", this.clientLogic.updateGroup(group, operation, membership));
			EasyMock.verify(this.serverLogic);
			assertLogin();
			break;
		case REMOVE_MEMBER:
			EasyMock.expect(this.serverLogic.updateGroup(PropertyEqualityArgumentMatcher.eq(group, "groupId", "id"),
					PropertyEqualityArgumentMatcher.eq(operation),
					PropertyEqualityArgumentMatcher.eq(membership))).andReturn(groupName);
			EasyMock.replay(this.serverLogic);
			assertEquals("OK", this.clientLogic.updateGroup(group, operation, membership));
			EasyMock.verify(this.serverLogic);
			assertLogin();
			break;

		default:
			/*
			 * FIXME: remove this line. It is here only, because privlevel is not included 
			 * in the XML and hence not transported to the serverLogic.
			 */
			group.setPrivlevel(null); 
			
			EasyMock.expect(this.serverLogic.updateGroup(PropertyEqualityArgumentMatcher.eq(group, "groupId", "id"),
					PropertyEqualityArgumentMatcher.eq(operation, ""),
					PropertyEqualityArgumentMatcher.eq(membership))).andReturn(groupName + "-new");
			EasyMock.replay(this.serverLogic);
			assertEquals(groupName + "-new", this.clientLogic.updateGroup(group, operation, null));
			EasyMock.verify(this.serverLogic);
			assertLogin();
			break;
		}
		
		return null;
	}

	/**
	 * runs the test defined by {@link #updatePosts(List, PostUpdateOperation)} with a fully populated Publication Post as argument
	 */
	@Test
	public void updatePostTestPublication() {
		final List<Post<?>> posts = new LinkedList<>();
		posts.add(ModelUtils.generatePost(BibTex.class));

		this.updatePosts(posts, PostUpdateOperation.UPDATE_ALL);
	}
	
	/**
	 * runs the test defined by {@link #updatePosts(List, PostUpdateOperation)} with a fully populated Bookmark Post as argument
	 */
	@Test
	public void updatePostTestBookmark() {
		final List<Post<?>> posts = new LinkedList<>();
		posts.add(ModelUtils.generatePost(Bookmark.class));
		
		this.updatePosts(posts, PostUpdateOperation.UPDATE_ALL);
	}
	
	@Override
	public List<JobResult> updatePosts(final List<Post<?>> posts, final PostUpdateOperation operation) {
		final Post<?> post = posts.get(0);
		post.getUser().setName(LOGIN_USER_NAME);
		
		final List<JobResult> singletonList = Collections.singletonList(JobResult.buildSuccess(post.getResource().getIntraHash()));
		
		EasyMock.expect(this.serverLogic.updatePosts(PropertyEqualityArgumentMatcher.eq(posts, IGNORE1), PropertyEqualityArgumentMatcher.eq(operation, ""))).andReturn(singletonList);
		EasyMock.replay(this.serverLogic);
		assertEquals(convertToHashes(singletonList), convertToHashes(this.clientLogic.updatePosts(posts, operation)));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return null;
	}

	/**
	 * runs the test defined by {@link #updateUser(User, UserUpdateOperation)} with a certain argument
	 */
	@Test
	public void updateUserTest() {
		this.updateUser(ModelUtils.getUser(), UserUpdateOperation.UPDATE_ALL);
	}
	
	@Override
	public String updateUser(final User user, final UserUpdateOperation operation) {
		EasyMock.expect(this.serverLogic.createUser(PropertyEqualityArgumentMatcher.eq(user, IGNORE2))).andReturn("rVal");
		EasyMock.replay(this.serverLogic);
		assertEquals("rVal", this.clientLogic.createUser(user));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return null;
	}
	
	/**
	 * Test whether createDocument is passed through
	 */
	@Test
	public void testCreateDocument() {
		final Document doc = new Document();
		
		File tmpData = null;
		try {
			tmpData = File.createTempFile(this.getClass().getName() + ".testCreateDocument", ".txt");
			final byte[] data = "test\ndata\n".getBytes();
			
			FileUtils.writeByteArrayToFile(tmpData, data);
			
			doc.setFile(tmpData);
			doc.setFileName(tmpData.getName());
			doc.setMd5hash(HashUtils.getMD5Hash(data));
			
			this.createDocument(doc, "resHash");
		} catch (final IOException ex) {
			throw new RuntimeException(ex);
		} finally {
			if (tmpData != null) {
				tmpData.delete();
			}
		}
	}

	@Override
	public String createDocument(final Document doc, final String resourceHash) {
		EasyMock.expect(this.serverLogic.createDocument(CheckerDelegatingMatcher.check(new Checker<Document>() {
			@Override
			public boolean check(final Document obj) {
				assertEquals(doc.getFileName(), obj.getFileName());
				assertEquals(doc.getMd5hash(), obj.getMd5hash());
				byte[] sent;
				byte[] received;
				try {
					sent = FileUtils.readFileToByteArray(doc.getFile());
					received = FileUtils.readFileToByteArray(obj.getFile());
				} catch (final IOException ex) {
					throw new RuntimeException(ex);
				}
				assertEquals(Arrays.toString(sent), Arrays.toString(received));
				return true;
			}
			
		}), EasyMock.eq(resourceHash))).andReturn("rVal");
		
		EasyMock.replay(this.serverLogic);
		assertEquals("rVal", this.clientLogic.createDocument(doc, resourceHash));
		EasyMock.verify(this.serverLogic);
		return null;
	}

	@Test
	public void createConceptTest() {
		this.createConcept(ModelUtils.getTag(), GroupingEntity.USER, "testUser");
	}
	
	@Override
	public String createConcept(final Tag concept, final GroupingEntity grouping, final String groupingName) {
		EasyMock.expect(this.serverLogic.createConcept(CheckerDelegatingMatcher.check(obj -> {
			assertEquals(concept.getName(), obj.getName());
			assertNotNull(obj.getSubTags());
			assertEquals(concept.getSubTags().size(), obj.getSubTags().size());
			for (int i = 0; i < concept.getSubTags().size(); ++i) {
				final Tag origSubTag = concept.getSubTags().get(i);
				final Tag foundSubTag = obj.getSubTags().get(i);
				assertEquals(origSubTag.getName(), foundSubTag.getName());
				assertNotNull(foundSubTag.getSuperTags());
				assertEquals(origSubTag.getSuperTags().size(), foundSubTag.getSuperTags().size());
				for (int x = 0; x < origSubTag.getSuperTags().size(); ++x) {
					assertEquals(origSubTag.getSuperTags().get(x).getName(), foundSubTag.getSuperTags().get(x).getName());
				}
			}
			assertNotNull(obj.getSuperTags());
			assertEquals(concept.getSuperTags().size(), obj.getSuperTags().size());
			for (int i = 0; i < concept.getSuperTags().size(); ++i) {
				final Tag origSuperTag = concept.getSuperTags().get(i);
				final Tag foundSuperTag = obj.getSuperTags().get(i);
				assertEquals(origSuperTag.getName(), foundSuperTag.getName());
				assertNotNull(foundSuperTag.getSubTags());
				assertEquals(origSuperTag.getSubTags().size(), foundSuperTag.getSubTags().size());
				for (int x = 0; x < origSuperTag.getSubTags().size(); ++x) {
					assertEquals(origSuperTag.getSubTags().get(x).getName(), foundSuperTag.getSubTags().get(x).getName());
				}
			}
			return true;
		}), EasyMock.eq(grouping), EasyMock.eq(groupingName))).andReturn(concept.getName());;
		EasyMock.replay(this.serverLogic);
		assertEquals(concept.getName(), this.clientLogic.createConcept(concept, grouping, groupingName));
		EasyMock.verify(this.serverLogic);
		return concept.getName();
	}

	/**
	 * runs the test defined by {@link #getUsers(Class, GroupingEntity, String, List, String, SortKey, UserRelation, String, int, int)} with certain arguments
	 * (in order to retrieve all users)
	 */
	@Test
	public void getAllUsersTest() {
		//getUsers(1,56);
		this.getUsers(null, GroupingEntity.ALL, null, null, null, null, null, null, 1, 56);
	}

	/**
	 * runs the test defined by {@link #getUsers(Class, GroupingEntity, String, List, String, SortKey, UserRelation, String, int, int)} with certain arguments
	 * (in order to retrieve group members)
	 */
	@Test
	public void getGroupMembersTest() {
		this.getUsers(null, GroupingEntity.GROUP, "grpX", null, null, null, null, null, 1, 56);
	}
	
	@Override
	public List<User> getUsers(final Class<? extends org.bibsonomy.model.Resource> resourceType, final GroupingEntity grouping, final String groupingName, final List<String> tags, final String hash, final SortKey sortKey, final UserRelation relation, final String search, final int start, final int end) {
		final List<User> expected = new ArrayList<User>(2);
		expected.add(ModelUtils.getUser());
		expected.get(0).setName("Nr1");
		expected.add(ModelUtils.getUser());
		expected.get(1).setName("Nr2");
		EasyMock.expect(this.serverLogic.getUsers(resourceType, grouping, groupingName, tags, hash, sortKey, relation, search, start, end)).andReturn(expected);
		EasyMock.replay(this.serverLogic);
		final List<User> returned = this.clientLogic.getUsers(resourceType, grouping, groupingName, tags, hash, sortKey, relation, search, start, end);
		CommonModelUtils.assertPropertyEquality(expected, returned, 5, Pattern.compile(       ".*\\.(" + COMMON_USER_PROPERTIES + "|activationCode|reminderPassword|openID|ldapId|remoteUserIds)"));
		EasyMock.verify(this.serverLogic);
		assertLogin();
		return returned;
	}
}