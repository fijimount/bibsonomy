/**
 * BibSonomy-Rest-Client - The REST-client.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.rest.client.queries.get;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bibsonomy.common.SortCriteria;
import org.bibsonomy.common.enums.GroupingEntity;
import org.bibsonomy.common.enums.SortKey;
import org.bibsonomy.common.enums.SortOrder;
import org.bibsonomy.common.exceptions.InternServerException;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.util.data.NoDataAccessor;
import org.bibsonomy.rest.client.AbstractQuery;
import org.bibsonomy.rest.exceptions.BadRequestOrResponseException;
import org.bibsonomy.rest.exceptions.ErrorPerformingRequestException;

/**
 * Use this Class to receive an ordered list of all posts.
 * 
 * @author Manuel Bork <manuel.bork@uni-kassel.de>
 */
public final class GetPostsQuery extends AbstractQuery<List<Post<? extends Resource>>> {
	private static final Log log = LogFactory.getLog(GetPostsQuery.class);

	private final int start;
	private final int end;
	private List<SortCriteria> sortCriteria;
	private String search;
	private Class<? extends Resource> resourceType;
	private List<String> tags;
	private GroupingEntity grouping = GroupingEntity.ALL;
	private String groupingValue;
	private String resourceHash;
	private String userName;

	/**
	 * Gets bibsonomy's posts list.
	 */
	public GetPostsQuery() {
		this(0, 19);
	}

	/**
	 * Gets bibsonomy's posts list.
	 * 
	 * @param start
	 *            start of the list
	 * @param end
	 *            end of the list
	 */
	public GetPostsQuery(int start, int end) {
		if (start < 0) {
			start = 0;
		}
		if (end < start) {
			end = start;
		}

		this.start = start;
		this.end = end;
	}

	/**
	 * Set the grouping used for this query. If {@link GroupingEntity#ALL} is
	 * chosen, the groupingValue isn't evaluated (-> it can be null or empty).
	 * 
	 * @param grouping
	 *            the grouping to use
	 * @param groupingValue
	 *            the value for the chosen grouping; for example the username if
	 *            grouping is {@link GroupingEntity#USER}
	 * @throws IllegalArgumentException
	 *             if grouping is != {@link GroupingEntity#ALL} and
	 *             groupingValue is null or empty
	 */
	public void setGrouping(final GroupingEntity grouping, final String groupingValue) throws IllegalArgumentException {
		if (grouping == GroupingEntity.ALL) {
			this.grouping = grouping;
			return;
		}
		if (!present(groupingValue)) {
			throw new IllegalArgumentException("no grouping value given");
		}

		this.grouping = grouping;
		this.groupingValue = groupingValue;
	}

	/**
	 * set the resource type of the resources of the posts.
	 * 
	 * @param type
	 *            the type to set
	 */
	public void setResourceType(final Class<? extends Resource> type) {
		this.resourceType = type;
	}

	/**
	 * @param resourceHash
	 *            The resourceHash to set.
	 */
	public void setResourceHash(final String resourceHash) {
		this.resourceHash = resourceHash;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(final List<String> tags) {
		this.tags = tags;
	}

	/**
	 * @param sortCriteria
	 * 				the sort criteriums to set
	 */
	public void setSortCriteriums(final List<SortCriteria> sortCriteria) {
		this.sortCriteria = sortCriteria;
	}

	/**
	 * @param search
	 *            the search string to set
	 */
	public void setSearch(final String search) {
		this.search = search;
	}

	@Override
	public List<Post<? extends Resource>> getResultInternal() throws BadRequestOrResponseException, IllegalStateException {
		try {
			return this.getRenderer().parsePostList(this.downloadedDocument, NoDataAccessor.getInstance());
		} catch (final InternServerException ex) {
			throw new BadRequestOrResponseException(ex);
		}
	}

	@Override
	protected void doExecute() throws ErrorPerformingRequestException {
		if (GroupingEntity.CLIPBOARD.equals(this.grouping)) {
			final String clipboardUrl = this.getUrlRenderer().createHrefForClipboard(this.userName, null);
			this.downloadedDocument = performGetRequest(clipboardUrl);
			return;
		}
		
		final String url = this.getUrlRenderer().createHrefForPosts(this.grouping, this.groupingValue, this.resourceType, this.tags, this.resourceHash, this.search, this.sortCriteria, this.start, this.end);
		if (log.isDebugEnabled()) {
			log.debug("GetPostsQuery doExecute() called - URL: " + url);
		}
		this.downloadedDocument = performGetRequest(url);
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(final String userName) {
		this.userName = userName;
	}
}