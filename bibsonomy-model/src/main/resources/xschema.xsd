<?xml version="1.0" encoding="UTF-8"?>
<!--


     BibSonomy-Model - Java- and JAXB-Model.

     Copyright (C) 2006 - 2013 Knowledge & Data Engineering Group,
                               University of Kassel, Germany
                               http://www.kde.cs.uni-kassel.de/

     This program is free software; you can redistribute it and/or
     modify it under the terms of the GNU Lesser General Public License
     as published by the Free Software Foundation; either version 2
     of the License, or (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU Lesser General Public License for more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program; if not, write to the Free Software
     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

-->

<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">

	<!--
		root element
	-->
	<xsd:element name="bibsonomy" type="bibsonomyXML" />

	<!--
		root element type
	-->
	<xsd:complexType name="bibsonomyXML">
		<xsd:sequence>
			<xsd:choice>
				<!-- container -->
				<xsd:element name="users" type="UsersType" />
				<xsd:element name="groups" type="GroupsType" />
				<xsd:element name="tags" type="TagsType" />
				<xsd:element name="posts" type="PostsType" />
				<xsd:element name="persons" type="PersonsType" />
				<xsd:element name="projects" type="ProjectsType" />
				<xsd:element name="resourcePersonRelations" type="ResourcePersonRelationsType" />
				<xsd:element name="groupMemberships" type="GroupMembershipsType" />
				<!-- single elements -->
				<xsd:element name="user" type="UserType" />
				<xsd:element name="group" type="GroupType" />
				<xsd:element name="post" type="PostType" />
				<xsd:element name="tag" type="TagType" />
				<xsd:element name="bookmark" type="BookmarkType" />
				<xsd:element name="bibtex" type="BibtexType" />
				<xsd:element name="document" type="DocumentType" />
				<xsd:element name="person" type="PersonType" />
                <xsd:element name="resourcePersonRelation" type="ResourcePersonRelationType" />
				<xsd:element name="project" type="ProjectType" />
				<xsd:element name="cris_link" type="CRISLinkTypeType" />
				<xsd:element name="personMatch" type="PersonMatchType" />
				<xsd:element name="additionalKey" type="AdditionalKeyType" />
				<!-- error -->
				<xsd:element name="error" type="xsd:string" />
				<!-- hashes to return when creating / updating resources -->
				<xsd:element name="resourcehash" type="hashType" />
				<!-- user-/group-ids to return when creating / updating users / groups -->
				<xsd:element name="userid" type="xsd:string" />
				<xsd:element name="groupid" type="xsd:string" />
				<xsd:element name="personid" type="xsd:string" />
				<xsd:element name="projectid" type="xsd:string" />
				<xsd:element name="crislinkid" type="xsd:string" />
				<xsd:element name="resourcePersonRelationId" type="xsd:string" />
				<!-- uri to return (i.e. on uploading documents) -->
				<xsd:element name="uri" type="xsd:anyURI" />
				<!-- gold standard references -->
				<xsd:element name="references" type="ReferencesType" />
				<xsd:element name="syncPosts" type="SyncPostsType" />
				<xsd:element name="syncData" type="SyncDataType" />
			</xsd:choice>

		</xsd:sequence>
		<!-- status flag: contains "ok" when request was successful, "fail" otherwise -->
		<xsd:attribute name="stat" type="statType" />
	</xsd:complexType>

	<!--
		this type encapsulates a list of users
	-->
	<xsd:complexType name="UsersType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="user" type="UserType" />
		</xsd:sequence>
		<xsd:attribute name="start" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="end" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="next" type="xsd:anyURI" />
	</xsd:complexType>

	<!--
		a user
	-->
	<xsd:complexType name="UserType">
		<xsd:sequence>
			<xsd:element name="groups" type="GroupsType" minOccurs="0" >
				<xsd:annotation>
					<xsd:documentation>
						The groups, the user is a member of, excluding
						the standard groups 'public', 'private', and
						'friends'.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="remoteUserId" minOccurs="0" maxOccurs="unbounded" type="RemoteUserIdType" />
		</xsd:sequence>
		<xsd:attribute name="name" type="xsd:string" use="required" />
		<xsd:attribute name="realname" type="xsd:string" />
		<xsd:attribute name="email" type="xsd:string" />
		<xsd:attribute name="homepage" type="xsd:string" />
		<xsd:attribute name="password" type="xsd:string" />
		<xsd:attribute name="spammer" type="xsd:boolean" />
		<xsd:attribute name="prediction" type="xsd:positiveInteger" />
		<xsd:attribute name="algorithm" type="xsd:string" />
		<xsd:attribute name="toClassify" type="xsd:positiveInteger" />
		<xsd:attribute name="classifierMode" type="xsd:string" />
		<xsd:attribute name="registrationLog" type="xsd:string" />

		<!-- link to details page -->
		<xsd:attribute name="href" type="xsd:anyURI" />
		<xsd:attribute name="confidence" type="xsd:double" />
	</xsd:complexType>


	<xsd:complexType name="RemoteUserIdType">
		<xsd:attribute name="userId" type="xsd:string" />
		<xsd:attribute name="identityProvider" type="xsd:string" />
	</xsd:complexType>

	<!--
		this type encapsulates a list of groups
	-->
	<xsd:complexType name="GroupsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="group" type="GroupType" />
		</xsd:sequence>
		<xsd:attribute name="start" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="end" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="next" type="xsd:anyURI" />
	</xsd:complexType>

	<!--
		a group
	-->
	<xsd:simpleType name="lboolean">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="true"/>
			<xsd:enumeration value="false"/>
			<xsd:enumeration value=""/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="GroupRequestType">
		<xsd:sequence>
			<xsd:element name="requestedUser" type="UserType" />
		</xsd:sequence>
		<xsd:attribute type="xsd:string" name="reason" />
		<xsd:attribute type="xsd:dateTime" name="submissionDate" />
	</xsd:complexType>

	<xsd:complexType name="GroupType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="user" type="UserType" />
			<xsd:element name="parent" type="GroupType" minOccurs="0" />
			<xsd:element name="groupRequest" type="GroupRequestType" minOccurs="0" />
		</xsd:sequence>
		<xsd:attribute name="name" type="xsd:string" use="required" />
		<xsd:attribute name="realname" type="xsd:string" />
		<xsd:attribute name="homepage" type="xsd:anyURI" />
		<xsd:attribute name="description" type="xsd:string" />
		<!-- link to details page -->
		<xsd:attribute name="href" type="xsd:anyURI" />
		<xsd:attribute name="organization" type="lboolean" />
		<xsd:attribute name="internalId" type="xsd:string" />
		<xsd:attribute name="allowJoin" type="xsd:boolean" />
	</xsd:complexType>

	<!--
		this type encapsulated a list of tags
	-->
	<xsd:complexType name="TagsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="tag" type="TagType" />
		</xsd:sequence>
		<xsd:attribute name="start" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="end" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="next" type="xsd:anyURI" />
	</xsd:complexType>

	<!--
		a tag
	-->
	<xsd:complexType name="TagType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="subTags" type="TagsType" />
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="superTags" type="TagsType" />
		</xsd:sequence>
		<xsd:attribute name="name" type="xsd:string" use="required" />
		<xsd:attribute name="globalcount" type="positiveIntegerIncludingZero" />
		<xsd:attribute name="usercount" type="positiveIntegerIncludingZero" />
		<!-- link to details page -->
		<xsd:attribute name="href" type="xsd:anyURI" />
	</xsd:complexType>

	<!--
		this type encapsulated a list of posts
	-->
	<xsd:complexType name="PostsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="post" type="PostType" />
		</xsd:sequence>
		<xsd:attribute name="start" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="end" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="next" type="xsd:anyURI" />
	</xsd:complexType>

	<!--
		a post
	-->
	<xsd:complexType name="PostType">
		<xsd:complexContent>
			<xsd:extension base="LinkableType">
				<xsd:sequence>
					<xsd:element name="user" type="UserType" />
					<xsd:element maxOccurs="unbounded" minOccurs="0" name="group" type="GroupType" />
					<!-- Note: a post must have at least one tag -->
					<xsd:element maxOccurs="unbounded" name="tag" type="TagType" />
					<xsd:element minOccurs="0" name="documents" type="DocumentsType" />
					<xsd:choice>
						<xsd:element name="bookmark" type="BookmarkType" />
						<xsd:element name="bibtex" type="BibtexType" />
						<xsd:element name="publicationFileUpload" type="UploadDataType" />
						<xsd:element name="goldStandardPublication" type="GoldStandardPublicationType" />
					</xsd:choice>
				</xsd:sequence>
				<xsd:attribute name="description" type="xsd:string" />
				<xsd:attribute name="postingdate" type="xsd:dateTime" />
				<xsd:attribute name="changedate" type="xsd:dateTime" />
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<!--
		the final Document entry 
	 -->
	<xsd:complexType name="DocumentType">
		<xsd:attribute name="filename" type="xsd:string" />
		<xsd:attribute name="md5hash" type="xsd:string" />
		<xsd:attribute name="href" type="xsd:anyURI" />
	</xsd:complexType>
	
	<xsd:complexType name="UploadDataType">
		<xsd:attribute name="multipartName" use="required" type="xsd:string" />
	</xsd:complexType>

	<!--
    	a main name
	-->
	<xsd:complexType name="PersonNameType">
		<xsd:attribute name="firstName" type="xsd:string" />
		<xsd:attribute name="lastName" type="xsd:string" />
	</xsd:complexType>

	<!--
    	a person
	-->

	<xsd:complexType name="PersonsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="person" type="PersonType" />
		</xsd:sequence>
		<xsd:attribute name="start" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="end" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="next" type="xsd:anyURI" />
	</xsd:complexType>

	<xsd:simpleType name="GenderType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="M" />
			<xsd:enumeration value="F" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="PersonType">
		<xsd:complexContent>
			<xsd:extension base="LinkableType">
				<xsd:sequence>
					<xsd:element name="mainName" type="PersonNameType" />
					<xsd:element name="names" type="PersonNameType" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="user" type="UserType" minOccurs="0" />
					<xsd:element name="additionalKey" type="AdditionalKeyType" maxOccurs="unbounded"/>
				</xsd:sequence>
				<xsd:attribute name="personId" type="xsd:string" />
				<xsd:attribute name="gender" type="GenderType" />
				<xsd:attribute name="homepage" type="xsd:anyURI" />
				<xsd:attribute name="email" type="xsd:string" />
				<xsd:attribute name="college" type="xsd:string" />
				<xsd:attribute name="academicDegree" type="xsd:string" />
				<xsd:attribute name="orcid" type="xsd:string" />
				<xsd:attribute name="researcherid" type="xsd:string" />

			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	
	<!-- 
		list of Documents
	 -->
	<xsd:complexType name="DocumentsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" name="document" type="DocumentType" />
		</xsd:sequence>
	</xsd:complexType>


	<!--
		a URL for a publication
	 -->
	<xsd:complexType name="ExtraUrlType">
		<xsd:attribute name="title" type="xsd:string" />
		<xsd:attribute name="href" type="xsd:anyURI" />
		<xsd:attribute name="date" type="xsd:dateTime" />
	</xsd:complexType>
	
	<!-- 
		list of extra URLs to be shown in the xml 
	 -->
	<xsd:complexType name="ExtraUrlsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="url" type="ExtraUrlType" />
		</xsd:sequence>
	</xsd:complexType>
	

	<!--
		a bookmark
	-->
	<xsd:complexType name="BookmarkType">
		<xsd:attribute name="title" type="xsd:string" use="required" />
		<xsd:attribute name="url" type="xsd:anyURI" use="required" />

		<!-- hash value identifying this resource -->
		<xsd:attribute name="interhash" type="xsd:string" />
		<xsd:attribute name="intrahash" type="xsd:string" />
		<!-- link to all posts of this bookmark -->
		<xsd:attribute name="href" type="xsd:anyURI" />
	</xsd:complexType>

	<!--
		AbstractPublicationType extracted to be able to deserialize publication
		in JSON format; seems that JAXB has some problems with inheritance
		(else GoldStandardPublicationType would extend BibtexType)
		-->
	<xsd:complexType name="AbstractPublicationType">
		<!-- extra URLs -->
		<xsd:sequence>
		  <xsd:element minOccurs="0" maxOccurs="1" name="extraurls" type="ExtraUrlsType" />
		</xsd:sequence>
		<xsd:attribute name="title" type="xsd:string" use="required" />
		<xsd:attribute name="bibtexKey" type="xsd:string" />
		<xsd:attribute name="bKey" type="xsd:string" />
		<xsd:attribute name="misc" type="xsd:string" />
		<xsd:attribute name="bibtexAbstract" type="xsd:string" />
		<xsd:attribute name="entrytype" type="xsd:string" />
		<xsd:attribute name="address" type="xsd:string" />
		<xsd:attribute name="annote" type="xsd:string" />
		<xsd:attribute name="author" type="xsd:string" />
		<xsd:attribute name="booktitle" type="xsd:string" />
		<xsd:attribute name="chapter" type="xsd:string" />
		<xsd:attribute name="crossref" type="xsd:string" />
		<xsd:attribute name="edition" type="xsd:string" />
		<xsd:attribute name="editor" type="xsd:string" />
		<xsd:attribute name="howpublished" type="xsd:string" />
		<xsd:attribute name="institution" type="xsd:string" />
		<xsd:attribute name="organization" type="xsd:string" />
		<xsd:attribute name="journal" type="xsd:string" />
		<xsd:attribute name="note" type="xsd:string" />
		<xsd:attribute name="number" type="xsd:string" />
		<xsd:attribute name="pages" type="xsd:string" />
		<xsd:attribute name="publisher" type="xsd:string" />
		<xsd:attribute name="school" type="xsd:string" />
		<xsd:attribute name="series" type="xsd:string" />
		<xsd:attribute name="volume" type="xsd:string" />
		<xsd:attribute name="day" type="xsd:string" />
		<xsd:attribute name="month" type="xsd:string" />
		<xsd:attribute name="year" type="xsd:string" />
		<xsd:attribute name="type" type="xsd:string" />
		<xsd:attribute name="url" type="xsd:string" />
		<xsd:attribute name="privnote" type="xsd:string" />

		<!-- hash value identifying this resource -->
		<xsd:attribute name="intrahash" type="xsd:string" />
		<xsd:attribute name="interhash" type="xsd:string" />
		<!-- link to all posts of this publication -->
		<xsd:attribute name="href" type="xsd:anyURI" />
	</xsd:complexType>
	
	<!--
		a publication
		TODO: add a publication type with full backwards compatibility
	-->
	<xsd:complexType name="BibtexType">
		<xsd:complexContent>
			<xsd:extension base="AbstractPublicationType">
				<!-- see comment for abstract publication type -->
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	
	
	<!--
		this type encapsulated a list of references for a gold standard
	-->
	<xsd:complexType name="ReferencesType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="reference" type="ReferenceType" />
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="ReferenceType">
		<xsd:attribute name="interhash" type="xsd:string" />
		<xsd:attribute name="href" type="xsd:anyURI" />
	</xsd:complexType>
	
	<!-- 
		these publications are part of the collection, …
	 -->
	<xsd:complexType name="PublicationsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="publication" type="PublicationType" />
		</xsd:sequence>
	</xsd:complexType>
	
	<xsd:complexType name="PublicationType">
		<xsd:complexContent>
			<xsd:extension base="ReferenceType">
				<!-- nothing to extend -->
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	
	<!-- 
		this type connects the incollection to its collection, …
	 -->
	<xsd:complexType name="PublishedInType">
		<xsd:complexContent>
			<xsd:extension base="ReferenceType">
				<!-- nothing to extend -->
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	
	<xsd:complexType name="GoldStandardPublicationType">
		<xsd:complexContent>
			<xsd:extension base="AbstractPublicationType">
				<xsd:sequence>
					<xsd:element maxOccurs="1" minOccurs="1" name="references" type="ReferencesType" />
					<xsd:element name="publishedIn" type="PublishedInType" />
					<xsd:element maxOccurs="1" minOccurs="0" name="publications" type="PublicationsType" />
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	
	<!--
		helper type, as xsd:positiveInteger alone does not include zero
	-->	
	<xsd:simpleType name="positiveIntegerIncludingZero">
		<xsd:restriction base="xsd:integer">
			<xsd:minInclusive value="0" />
		</xsd:restriction>
	</xsd:simpleType>

	<!--
		type for resource hashes
	-->	
	<xsd:simpleType name="hashType">
		<xsd:restriction base="xsd:string">
			<xsd:length value="32" />
			<xsd:pattern value="([a-z]|[0-9])*" />
		</xsd:restriction>
	</xsd:simpleType>

	<!--
		type for status message
	-->	
	<xsd:simpleType name="statType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="ok" />
			<xsd:enumeration value="fail" />
		</xsd:restriction>
	</xsd:simpleType>

	<!-- 
		TODO: comment
	-->
	<xsd:complexType name="SyncPostType">
		<xsd:sequence>
			<xsd:element name="post" type="PostType" />
		</xsd:sequence>
		
		<xsd:attribute name="createDate" type="xsd:dateTime" />
		<xsd:attribute name="changeDate" type="xsd:dateTime" />
		<xsd:attribute name="hash" type="xsd:string" />
		<xsd:attribute name="action" type="xsd:string" />
	</xsd:complexType>

	<!-- 
		TODO: comment
	-->
	<xsd:complexType name="SyncDataType">
		<xsd:sequence>
			<xsd:element name="resourceType" type="xsd:string" />
			<xsd:element name="lastSyncDate" type="xsd:dateTime" />
			<xsd:element name="synchronizationStatus" type="xsd:string" />
			<xsd:element name="info" type="xsd:string" />
		</xsd:sequence>
		<xsd:attribute name="service" type="xsd:anyURI" />
	</xsd:complexType>
	
	<!-- 
		TODO: comment
	-->
	<xsd:complexType name="SyncPostsType">
		<xsd:sequence>
			<xsd:element name="syncPost" type="SyncPostType" maxOccurs="unbounded" minOccurs="0" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="ResourceLinkType">
		<xsd:attribute name="interHash" type="hashType" />
		<xsd:attribute name="intraHash" type="hashType" />
	</xsd:complexType>

	<xsd:simpleType name="RelationType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="doctor_vater" />
			<xsd:enumeration value="first_reviewer" />
			<xsd:enumeration value="reviewer" />
			<xsd:enumeration value="advisor" />
			<xsd:enumeration value="author" />
			<xsd:enumeration value="editor" />
			<xsd:enumeration value="other" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="ResourcePersonRelationsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="resourcePersonRelation" type="ResourcePersonRelationType" />
		</xsd:sequence>
		<xsd:attribute name="start" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="end" type="positiveIntegerIncludingZero" use="required" />
		<xsd:attribute name="next" type="xsd:anyURI" />
	</xsd:complexType>

    <xsd:complexType name="ResourcePersonRelationType">
        <xsd:sequence>
            <xsd:element name="person" type="PersonType" />
            <xsd:element name="resource" type="ResourceLinkType" />
			<xsd:element name="relationType" type="RelationType" />
			<xsd:element name="personIndex" type="xsd:integer" />
        </xsd:sequence>
    </xsd:complexType>

	<xsd:simpleType name="GroupRoleType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="user"/>
			<xsd:enumeration value="moderator"/>
			<xsd:enumeration value="administrator"/>
			<xsd:enumeration value="dummy"/>
			<xsd:enumeration value="invited"/>
			<xsd:enumeration value="requested"/>
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="GroupMembershipType">
		<xsd:sequence>
			<xsd:element name="user" type="UserType"/>
			<xsd:element name="joinDate" type="xsd:dateTime"/>
		</xsd:sequence>
		<xsd:attribute name="userSharedDocuments" type="xsd:boolean"/>
		<xsd:attribute name="groupRole" type="GroupRoleType"/>
	</xsd:complexType>

	<xsd:complexType name="GroupMembershipsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="groupMembership" type="GroupMembershipType"/>
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="ProjectType">
		<xsd:complexContent>
			<xsd:extension base="LinkableType">
				<xsd:sequence>
					<xsd:element name="subProjects" type="ProjectType" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="crisLinks" type="CRISLinkTypeType" minOccurs="0" maxOccurs="unbounded" />
					<xsd:element name="parentProject" type="ProjectType" minOccurs="0" />
				</xsd:sequence>
				<xsd:attribute name="externalId" type="xsd:string" />
				<xsd:attribute name="internalId" type="xsd:string" />
				<xsd:attribute name="title" type="xsd:string" />
				<xsd:attribute name="subTitle" type="xsd:string" />
				<xsd:attribute name="homepage" type="xsd:anyURI" />
				<xsd:attribute name="description" type="xsd:string" />
				<xsd:attribute name="type" type="xsd:string" />
				<xsd:attribute name="budget" type="xsd:float" />
				<xsd:attribute name="startDate" type="xsd:dateTime" />
				<xsd:attribute name="endDate" type="xsd:dateTime" />
				<xsd:attribute name="sponsor" type="xsd:string" />
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>

	<xsd:complexType name="ProjectsType">
		<xsd:sequence>
			<xsd:element maxOccurs="unbounded" minOccurs="0" name="project" type="ProjectType" />
		</xsd:sequence>
	</xsd:complexType>

	<xsd:complexType name="LinkableType" abstract="true" />

	<xsd:simpleType name="ProjectPersonLinkTypeType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="manager" />
			<xsd:enumeration value="member" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:simpleType name="CRISLinkDataSourceType">
		<xsd:restriction base="xsd:string">
			<xsd:enumeration value="user" />
			<xsd:enumeration value="system" />
		</xsd:restriction>
	</xsd:simpleType>

	<xsd:complexType name="CRISLinkTypeType">
		<xsd:sequence>
			<xsd:element name="dataSource" type="CRISLinkDataSourceType" />
			<xsd:element name="source" type="LinkableType" />
			<xsd:element name="target" type="LinkableType" />
			<xsd:element name="linkType" type="ProjectPersonLinkTypeType" />
		</xsd:sequence>
		<xsd:attribute name="startDate" type="xsd:dateTime" />
		<xsd:attribute name="endDate" type="xsd:dateTime" />
	</xsd:complexType>

	<xsd:complexType name="PersonMatchType">
		<xsd:sequence>
			<xsd:element name="person1" type="PersonType" />
			<xsd:element name="person2" type="PersonType" />
			<xsd:element name="person1Posts" type="PostType" minOccurs="0" maxOccurs="unbounded" />
			<xsd:element name="person2Posts" type="PostType" minOccurs="0" maxOccurs="unbounded" />
			<xsd:element name="userDenies" type="xsd:string" minOccurs="0" maxOccurs="unbounded" />
		</xsd:sequence>
		<xsd:attribute name="state" type="xsd:int" />
		<xsd:attribute name="matchId" type="xsd:int" />
	</xsd:complexType>

	<xsd:complexType name="AdditionalKeyType">
		<xsd:attribute name="keyName" type="xsd:string" />
		<xsd:attribute name="keyValue" type="xsd:string" />
	</xsd:complexType>
</xsd:schema>