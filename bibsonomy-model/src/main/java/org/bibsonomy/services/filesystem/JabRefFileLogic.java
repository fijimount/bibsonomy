/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.services.filesystem;

import java.util.Collection;

import org.bibsonomy.common.enums.LayoutPart;
import org.bibsonomy.model.Document;
import org.bibsonomy.model.util.file.UploadedFile;

/**
 * @author dzo
 */
public interface JabRefFileLogic {
	
	/**
	 * The file extension of layout filter file names.
	 */
	public static final String LAYOUT_FILE_EXTENSION = "layout";

	/**
	 * 
	 * @param username
	 * @param file
	 * @param layoutPart
	 * @return the document representing the jabref layout file
	 * @throws Exception
	 */
	public Document writeJabRefLayout(String username, UploadedFile file, LayoutPart layoutPart) throws Exception;
	
	/**
	 * @param hash
	 * @return <code>true</code> if file was deleted
	 */
	public boolean deleteJabRefLayout(final String hash);
	
	/**
	 * 
	 * @param file
	 * @return <code>true</code> iff the file is valid (currently only checks the
	 * file extension)
	 */
	public boolean validJabRefLayoutFile(final UploadedFile file);
	
	/**
	 * @return a set of all allowed file extensions for jabref layout files
	 */
	public Collection<String> allowedJabRefFileExtensions();
}