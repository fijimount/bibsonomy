package org.bibsonomy.model.enums;

/**
 * enum class to
 *
 * @author dzo
 */
public enum PersonOrder {

	/** order by rank (e.g. from the full text search) */
	RANK,

	/** order by the last name of the main name */
	MAIN_NAME_LAST_NAME;
}
