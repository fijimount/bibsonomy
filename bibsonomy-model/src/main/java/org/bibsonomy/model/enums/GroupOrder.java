package org.bibsonomy.model.enums;

/**
 * the order of the group
 * @author dzo
 */
public enum GroupOrder {
	/** sort by the group name */
	GROUP_NAME,

	/** sort by the group realname */
	GROUP_REALNAME,

	/** sort by the rank of the fulltext search */
	RANK
}
