/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model.util;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.bibsonomy.common.exceptions.UnsupportedResourceTypeException;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.factories.ResourceFactory;

/**
 * Static methods to handle Resources.
 * 
 * @author rja
 */
public class ResourceUtils {
	
	private static final Map<String, Class<? extends Resource>> byStringMap = new HashMap<String, Class<? extends Resource>>();
	private static final Map<Class<? extends Resource>, String> toStringMap = new HashMap<Class<? extends Resource>, String>();
	static {
		byStringMap.put("BOOKMARK", Bookmark.class);
		byStringMap.put("BIBTEX", BibTex.class);
		byStringMap.put("ALL", Resource.class);
		// TODO: shouldn't there be goldstandard-Resources too? - I dont want to break stuff relying on this
		for (final Map.Entry<String, Class<? extends Resource>> entry : byStringMap.entrySet()) {
			toStringMap.put(entry.getValue(), entry.getKey());
		}
	}

	/**
	 * @param clazz
	 * @return string
	 * @deprecated  please use {@link ResourceFactory#getResourceName(Class)}
	 */
	@Deprecated
	public static String toString(final Class<? extends Resource> clazz) {
		final String rVal = toStringMap.get(clazz);
		if (rVal == null) {
			throw new UnsupportedResourceTypeException();
		}
		return rVal;
	}
	
	/**
	 * 
	 * @param requiredType
	 * @return list with required resource types.
	 */
	@SuppressWarnings("unchecked")
	public static Class<? extends Resource>[] getResourceTypesByClass(final Class<? extends Resource> requiredType) {
		if (Resource.class.equals(requiredType)) {
			return new Class[]{Bookmark.class, BibTex.class};
		}
		return new Class[]{requiredType};
	}
	
	@SuppressWarnings("unchecked")
	@Deprecated // use ResourceFactory instead
	public static Class<? extends Resource> getResourceClassBySimpleName(String resourceName) {
		if (resourceName != null) {
			Class<? extends Resource> rVal = byStringMap.get(resourceName.toUpperCase());
			if (rVal != null) {
				return rVal;
			}
			try {
				Class<?> cls = Class.forName(BibTex.class.getPackage().getName() + "." + resourceName);
				if (Resource.class.isAssignableFrom(cls)) {
					return (Class<? extends Resource>) cls;
				}
			} catch (ClassNotFoundException e) {
			}
		}
		throw new NoSuchElementException(resourceName);
	}
}
