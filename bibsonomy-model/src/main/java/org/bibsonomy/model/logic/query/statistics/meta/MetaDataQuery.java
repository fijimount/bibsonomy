package org.bibsonomy.model.logic.query.statistics.meta;

/**
 * any kind of metadata with the result value
 *
 * @author dzo
 * @param <R> the type of the metadata
 */
public interface MetaDataQuery<R> {
	// noop
}
