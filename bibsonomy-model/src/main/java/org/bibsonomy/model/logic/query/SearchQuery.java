package org.bibsonomy.model.logic.query;

/**
 * Makes a query searchable.
 * @author ada
 */
public interface SearchQuery {

    /**
     * Returns the search string.
     *
     * @return the search string.
     */
    String getSearch();

}
