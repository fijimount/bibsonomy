/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.bibsonomy.common.enums.ProfilePrivlevel;
import org.bibsonomy.common.enums.TagCloudSort;
import org.bibsonomy.common.enums.TagCloudStyle;
import org.bibsonomy.model.enums.PersonPostsStyle;
import org.bibsonomy.model.user.settings.FavouriteLayout;
import org.bibsonomy.model.user.settings.LayoutSettings;

/**
 * Holds settings for a user.
 * 
 */
public class UserSettings implements Serializable {
	private static final long serialVersionUID = 501200873739971813L;

	/**
	 * the profile privacy level
	 */
	private ProfilePrivlevel profilePrivlevel = ProfilePrivlevel.PRIVATE;
	
	/**
	 * TODO: use {@link TagCloudStyle} as type
	 * tagbox style; 0 = cloud, 1 = list
	 */
	private int tagboxStyle = 0;

	/**
	 * TODO: use {@link TagCloudSort} as type
	 * sorting of tag box; 0 = alph, 1 = freq
	 */
	private int tagboxSort = 0;

	/**
	 * minimum frequency for tags to be displayed in tag box
	 */
	private int tagboxMinfreq = 0;

	/**
	 * top x posts shown in the tag box
	 */
	private int tagboxMaxCount = 50;
	
	/**
	 * TODO: change to boolean
	 * Show the tooltips for tags in the tag cloud? 0 = don't show, 1 = show 
	 */
	private int tagboxTooltip = 0;

	/**
	 * number of list items per page; how many posts to show in post lists
	 */
	private int listItemcount = 20;
	
	/**
	 * the layouts to be shown on publications and citations
	 * Reihenfolge ist egal. Wird vor jedem speichern in der Datenbank sortiert.
	 */
	private List<FavouriteLayout> favouriteLayouts = new LinkedList<FavouriteLayout>(Arrays.asList(new FavouriteLayout("SIMPLE", "BIBTEX"), new FavouriteLayout("SIMPLE", "ENDNOTE"), new FavouriteLayout("JABREF", "APA_HTML"),new FavouriteLayout("JABREF", "CHICAGO"),new FavouriteLayout("JABREF", "DIN1505"),new FavouriteLayout("JABREF", "HARVARDHTML"), new FavouriteLayout("JABREF", "MSOFFICEXML")));
		
	private boolean showBookmark = true;
	
	// TODO: rename to showPublication
	private boolean showBibtex = true;
	
	private LayoutSettings layoutSettings = new LayoutSettings();
	
	/**
	 * the default language for i18n
	 */
	private String defaultLanguage;

	/**
	 * style for person's page posts
	 */
	private PersonPostsStyle personPostsStyle = PersonPostsStyle.GOLDSTANDARD;

	/**
	 * layout for person's page posts
	 */
	private String personPostsLayout = "";

	/**
	 * The timeZone the user lives in. Used for rendering posts in the HTML 
	 * output. 
	 * FIXME: let user choose on the /settings page. FIXME: then we must store
	 * UTC times in the database!
	 * 
	 * FIXME: what to do with non-logged in users? They must have a valid
	 * time zone, too! Otherwise, we will get NPEs
	 */
	private final TimeZone timeZone = TimeZone.getDefault();
	
	/**
	 * TODO: change type to boolean
	 * How much data about the user behavior (clicking, etc.) is logged.
	 * 
	 * 0 = yes (log clicks to external pages)
	 * 1 = no  (don't log clicks to external pages)
	 */
	private int logLevel;

	/**
	 * Shall the web interface ask the user before it really deletes something?
	 */
	private boolean confirmDelete = true;
	
	/**
	 * User wants maxCount (true) or maxFreq (false)
	 */
	private boolean isMaxCount = true;
	

	/**
	 * @return tagboxStyle
	 */
	public int getTagboxStyle() {
		return this.tagboxStyle;
	}

	/**
	 * @param tagboxStyle
	 */
	public void setTagboxStyle(final int tagboxStyle) {
		this.tagboxStyle = tagboxStyle;
	}

	/**
	 * @return tagboxSort
	 */
	public int getTagboxSort() {
		return this.tagboxSort;
	}

	/**
	 * @param tagboxSort
	 */
	public void setTagboxSort(final int tagboxSort) {
		this.tagboxSort = tagboxSort;
	}

	/**
	 * @return tagboxMinfreq
	 */
	public int getTagboxMinfreq() {
		return this.tagboxMinfreq;
	}

	/**
	 * @param tagboxMinfreq
	 */
	public void setTagboxMinfreq(final int tagboxMinfreq) {
		this.tagboxMinfreq = tagboxMinfreq;
	}

	/**
	 * @param tagboxMaxCount
	 */
	public void setTagboxMaxCount(final int tagboxMaxCount) {
		this.tagboxMaxCount = tagboxMaxCount;
	}
	
	/**
	 * @return tagboxMaxCount
	 */
	public int getTagboxMaxCount() {
		return tagboxMaxCount;
	}

	/**
	 * @return isMaxCount
	 */
	public boolean getIsMaxCount() {
		return isMaxCount;
	}

	/**
	 * @param isMaxCount
	 */
	public void setIsMaxCount(final boolean isMaxCount) {
		this.isMaxCount = isMaxCount;
	}

	/**
	 * @return tagboxTooltip
	 */
	public int getTagboxTooltip() {
		return this.tagboxTooltip;
	}

	/**
	 * @param tagboxTooltip
	 */
	public void setTagboxTooltip(final int tagboxTooltip) {
		this.tagboxTooltip = tagboxTooltip;
	}

	/**
	 * @return listItemcount
	 */
	public int getListItemcount() {
		return this.listItemcount;
	}

	/**
	 * @param listItemcount
	 */
	public void setListItemcount(final int listItemcount) {
		this.listItemcount = listItemcount;
	}

	/**
	 * @return the default language
	 */
	public String getDefaultLanguage() {
		return this.defaultLanguage;
	}

	/**
	 * @param defaultLanguage the default language
	 */
	public void setDefaultLanguage(final String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	/**
	 * @return style for person's page posts
	 */
	public PersonPostsStyle getPersonPostsStyle() {
		return personPostsStyle;
	}

	/**
	 * @param personPostsStyle style for person's page posts
	 */
	public void setPersonPostsStyle(PersonPostsStyle personPostsStyle) {
		this.personPostsStyle = personPostsStyle;
	}

	/**
	 * @return layout for person's page posts
	 */
	public String getPersonPostsLayout() {
		return personPostsLayout;
	}


	/**
	 * @param personPostsLayout layout for person's page posts
	 */
	public void setPersonPostsLayout(String personPostsLayout) {
		this.personPostsLayout = personPostsLayout;
	}

	/**
	 * @param logLevel the logLevel to set
	 */
	public void setLogLevel(final int logLevel) {
		this.logLevel = logLevel;
	}
	
	/**
	 * @return the logLevel
	 */
	public int getLogLevel() {
		return this.logLevel;
	}

	/**
	 * gets currents confirmation status
	 * @return boolean 
	 */
	public boolean isConfirmDelete() {
		return this.confirmDelete;
	}

	/**
	 * sets the current confirmations status
	 * @param confirmDelete
	 */
	public void setConfirmDelete(final boolean confirmDelete) {
		this.confirmDelete = confirmDelete;
	}	
	
	/**
	 * retrieves the current confirmation status
	 * @return boolean
	 */
	public boolean getConfirmDelete() {
		return this.confirmDelete;
	}

	/**
	 * @param profilePrivlevel the profilePrivlevel to set
	 */
	public void setProfilePrivlevel(final ProfilePrivlevel profilePrivlevel) {
		this.profilePrivlevel = profilePrivlevel;
	}

	/**
	 * @return the profilePrivlevel
	 */
	public ProfilePrivlevel getProfilePrivlevel() {
		return profilePrivlevel;
	}
	/**
	 * @return the showBookmark
	 */
	public boolean isShowBookmark() {
		return this.showBookmark;
	}

	/**
	 * @param showBookmark the showBookmark to set
	 */
	public void setShowBookmark(final boolean showBookmark) {
		this.showBookmark = showBookmark;
	}

	/**
	 * @return the showPublication
	 */
	public boolean isShowBibtex() {
		return this.showBibtex;
	}

	/**
	 * @param showPublication the showPublication to set
	 */
	public void setShowBibtex(final boolean showPublication) {
		this.showBibtex = showPublication;
	}

	/**
	 * @return the timeZone
	 */
	public TimeZone getTimeZone() {
		return this.timeZone;
	}

	/**
	 * @return the layoutSettings
	 */
	public LayoutSettings getLayoutSettings() {
		return this.layoutSettings;
	}

	/**
	 * @param layoutSettings the layoutSettings to set
	 */
	public void setLayoutSettings(final LayoutSettings layoutSettings) {
		this.layoutSettings = layoutSettings;
	}

	/**
	 * @return the favourite_layouts
	 */
	public List<FavouriteLayout> getFavouriteLayouts() {
		return this.favouriteLayouts;
	}

	/**
	 * @param favouriteLayouts the favourite_layouts to set
	 */
	public void setFavouriteLayouts(final List<FavouriteLayout> favouriteLayouts) {
		this.favouriteLayouts = favouriteLayouts;
	}

}