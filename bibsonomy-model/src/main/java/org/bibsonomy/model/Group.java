/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model;

import java.io.Serializable;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.bibsonomy.common.enums.GroupID;
import org.bibsonomy.common.enums.GroupLevelPermission;
import org.bibsonomy.common.enums.Privlevel;
import org.bibsonomy.model.cris.Linkable;

/**
 * A group groups users.
 */
public class Group implements Linkable, Serializable {
	private static final long serialVersionUID = -4364391580208670647L;

	/** The internal id of this group. */
	private int groupId = GroupID.INVALID.getId();

	/**
	 * This group's name.
	 */
	private String name;

	/**
	 * The real (long) name of the group.
	 */
	private String realname;
	
	/**
	 * The homepage of the group.
	 */
	private URL homepage;
	
	/**
	 * A short text describing this group.
	 */
	private String description;

	/**
	 * These are the {@link Post}s of this group.
	 */
	private List<Post<? extends Resource>> posts;

	/**
	 * The permissions on group Level of this group
	 */
	private Set<GroupLevelPermission> groupLevelPermissions;

	/**
	 * The privacy level of this group.
	 */
	private Privlevel privlevel;
	
	/**
	 * If <code>true</code>, other group members can access documents
	 * attached to BibTeX posts, if the post is viewable for the group or
	 * public.
	 */
	private boolean sharedDocuments;
	
	/**
	 * If <code>true</code>, other users can send join requests to the group
	 */
	private boolean allowJoin;
	
	/**
	 * If you add a tagset to a group and a user marks this group as 
	 * 'relevent for' a post, then the user has to tag one entry of this set
	 * to his post. A tagset has the following form: Map<SetName,Tags>.
	 */
	private List<TagSet> tagSets;
	
	/** stores setting regarding publication reporting */
	private GroupPublicationReportingSettings publicationReportingSettings;

	// (ada) Why does group have a reference to group request!?
	/** stores information regarding the group request */
	private GroupRequest groupRequest;
	
	private List<GroupMembership> memberships;
	private List<GroupMembership> pendingMemberships;

	/**
	 * The parent group.
	 */
	private Group parent;

	/** a list of all subgroups. */
	private List<Group> subgroups;

	/** flag that signals if this group is an organization. */
	private boolean organization;

	/** the id of the group in an external source (e.g. the database of the university) */
	private String internalId;

	/**
	 * default constructor
	 */
	public Group() {
		// noop
	}

	/**
	 * constructor
	 * 
	 * @param name
	 */
	public Group(final String name) {
		this();
		this.setName(name);
	}

	/**
	 * constructor
	 * 
	 * @param groupId
	 */
	public Group(final GroupID groupId) {
		this(groupId.getId());
		this.setName(groupId.name().toLowerCase());
	}

	/**
	 * constructor
	 * 
	 * @param groupid
	 */
	public Group(final int groupid) {
		this.groupId = groupid;
		this.privlevel = Privlevel.MEMBERS;
		this.sharedDocuments = false;
		this.allowJoin = false;
	}

	/**
	 * Gets this groups parent group if it is set.
	 *
	 * @return the parent group if set, <code>null</code> otherwise.
	 */
	public Group getParent() {
		return parent;
	}

	/**
	 * Sets this groups parent group.
	 *
	 * @param parent the parent group.
	 */
	public void setParent(Group parent) {
		this.parent = parent;
	}

	/**
	 * @return groupId
	 */
	public int getGroupId() {
		return this.groupId;
	}

	/**
	 * @param groupId
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return posts
	 */
	public List<Post<? extends Resource>> getPosts() {
		if (this.posts == null) {
			this.posts = new LinkedList<>();
		}
		return this.posts;
	}

	/**
	 * @param posts
	 */
	public void setPosts(List<Post<? extends Resource>> posts) {
		this.posts = posts;
	}

	/**
	 * @return privlevel
	 */
	public Privlevel getPrivlevel() {
		return this.privlevel;
	}

	/**
	 * @param privlevel
	 */
	public void setPrivlevel(Privlevel privlevel) {
		this.privlevel = privlevel;
	}

	/**
	 * If <code>true</code>, other group members can access documents
	 * attached to BibTeX posts, if the post is viewable for the group or
	 * public.
	 * 
	 * @return The truth value regarding shared documents for this group.
	 */
	public boolean isSharedDocuments() {
		return this.sharedDocuments;
	}

	/**
	 * @param sharedDocuments
	 */
	public void setSharedDocuments(boolean sharedDocuments) {
		this.sharedDocuments = sharedDocuments;
	}
	
	/**
	 * If <code>true</code>, other users can send join requests to the group
	 * 
	 * @return The truth value regarding the permission to receive join requests
	 */
	public boolean isAllowJoin() {
		return this.allowJoin;
	}

	/**
	 * @param allowJoin
	 */
	public void setAllowJoin(boolean allowJoin) {
		this.allowJoin = allowJoin;
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof Group)) {
			return false;
		}
		return equals((Group) obj);
	}
	
	/**
	 * @return The tag sets associated with this group. See {@link #setTagSets(List)}.
	 */
	public List<TagSet> getTagSets() {
		return this.tagSets;
	}

	/** Sets the tag sets for this group.
	 * Tag sets allow group admins to specify pre-defined tags which
	 * the users can/should use when marking a post as "relevant for" this group. 
	 * 
	 * @param tagSets
	 */
	public void setTagSets(List<TagSet> tagSets) {
		this.tagSets = tagSets;
	}

	/** Get the real (long) name of this group.
	 * 
	 * @return The real (long) name of this group.
	 */
	public String getRealname() {
		return this.realname;
	}

	/** Set the real (long) name of this group.
	 * @param realname
	 */
	public void setRealname(String realname) {
		this.realname = realname;
	}

	/**
	 * @return The homepage of this group
	 */
	public URL getHomepage() {
		return this.homepage;
	}

	/** Set the homepage of this group.
	 * @param homepage
	 */
	public void setHomepage(URL homepage) {
		this.homepage = homepage;
	}

	/**
	 * @return the publicationReportingSettings
	 */
	public GroupPublicationReportingSettings getPublicationReportingSettings() {
		return this.publicationReportingSettings;
	}

	/**
	 * @param publicationReportingSettings the publicationReportingSettings to set
	 */
	public void setPublicationReportingSettings(GroupPublicationReportingSettings publicationReportingSettings) {
		this.publicationReportingSettings = publicationReportingSettings;
	}

	/**
	 * Gets all direct subgroups for this group.
	 *
	 * @return a list of all subgroups for this group.
	 */
	public List<Group> getSubgroups() {
		return subgroups;
	}


	/**
	 * Sets the subgroups for this group.
	 *
	 * @param subgroups a list with subgroups (groups that have this object as a parent).
	 */
	public void setSubgroups(List<Group> subgroups) {
		this.subgroups = subgroups;
	}

	/**
	 * signals whether the group should be treated as an organization.
	 *
	 * @return <code>true</code> iff this group is an organization
	 */
	public boolean isOrganization() {
		return organization;
	}

	/**
	 * Sets the organization flag.
	 *
	 * @param organization <code>true</code> iff this group is an organization
	 */
	public void setOrganization(boolean organization) {
		this.organization = organization;
	}

	/**
	 * @return the internalId
	 */
	public String getInternalId() {
		return internalId;
	}

	/**
	 * @param internalId the internalId to set
	 */
	public void setInternalId(String internalId) {
		this.internalId = internalId;
	}

	@Override
	public String getLinkableId() {
		return this.name;
	}

	@Override
	public Integer getId() {
		return this.groupId;
	}

	/**
	 * Compares two groups. Two groups are equal, if their groupId is equal.
	 * 
	 * @param other
	 * @return <code>true</code> if the two groups are equal.
	 */
	public boolean equals(final Group other) {
		if (this.groupId != GroupID.INVALID.getId() && other.groupId != GroupID.INVALID.getId()) {
			/*
			 * both groups have IDs set --> compare them by id
			 */
			final boolean groupIdEquals = GroupID.equalsIgnoreSpam(this.groupId, other.groupId);
			if (this.name != null && other.name != null) {
				final boolean nameEquals = this.name.equalsIgnoreCase(other.name);
				/*
				 * since both have also names set ... we should include the names in the comparison!
				 */
				if (( groupIdEquals && !nameEquals) ||
				    (!groupIdEquals &&  nameEquals)) {
					/*
					 * IDs do not match with names --> exception! 
					 */
					throw new RuntimeException("The names and the IDs of the given groups " + this + " and " + other + " do not match.");
				}
				/*
				 * here we know: 
				 * either both name and id are equal, or neither name and id are equal
				 * -> we need to return only the comparison value of the ids 
				 */
			}
			/*
			 * if one of the groups has a name given and the other not, they're incomparable
			 * (otherwise constructing a consistent hashcode() is impossible!
			 */
			if ((this.name != null && other.name == null) || 
			    (this.name == null && other.name != null)) {
				throw new RuntimeException("The given groups " + this + " and " + other + " are incomparable, because one of them has both name and ID set, the other not.");
			}
			return groupIdEquals;
		}
		/*
		 * at least one of the groups has no ID set --> check their name
		 */
		if (this.name != null && other.name != null) {
			return this.name.equalsIgnoreCase(other.name);
		}
		throw new RuntimeException("The given groups " + this + " and " + other + " are incomparable.");
	}

	/** 
	 * Returns a string representation of a group in the form <code>name(groupId)</code>. 
	 *  
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name + "(" + this.groupId + ")";
	}

	@Override
	public int hashCode() {
		if (this.name != null) return this.name.toLowerCase().hashCode();
		return groupId;
	}
	
	/**
	 * @return the groupRequest
	 */
	public GroupRequest getGroupRequest() {
		return this.groupRequest;
	}

	/**
	 * @param groupRequest the groupRequest to set
	 */
	public void setGroupRequest(GroupRequest groupRequest) {
		this.groupRequest = groupRequest;
	}
	
	/**
	 * @return the memberships
	 */
	public List<GroupMembership> getMemberships() {
		if (this.memberships == null) {
			this.memberships = new LinkedList<>();
		}
		return this.memberships;
	}

	/**
	 * @param memberships the memberships to set
	 */
	public void setMemberships(List<GroupMembership> memberships) {
		this.memberships = memberships;
	}

	public List<GroupMembership> getPendingMemberships() {
		if (this.pendingMemberships == null)
			this.pendingMemberships = new LinkedList<>();
		
		return pendingMemberships;
	}

	public void setPendingMemberships(List<GroupMembership> pendingMemberships) {
		this.pendingMemberships = pendingMemberships;
	}
	
	// TODO: move to utils class, remove all dependencies
	public GroupMembership getGroupMembershipForUser(String username) {
		for (GroupMembership g : this.getMemberships()) {
			if (g.getUser().getName().equals(username)) {
				return g;
			}
		}
		// look in pending memberships
		for (GroupMembership g : this.getPendingMemberships()) {
			if (g.getUser().getName().equals(username)) {
				return g;
			}
		}
		
		return null;
	}

	/**
	 * @return the group level permissions of this group
	 */
	public Set<GroupLevelPermission> getGroupLevelPermissions() {
		if (this.groupLevelPermissions == null) {
			this.groupLevelPermissions = new HashSet<>();
		}
		return this.groupLevelPermissions;
	}
	
	/**
	 * @param groupLevelPermissions the groupLevelPermissions to set
	 */
	public void setGroupLevelPermissions(Set<GroupLevelPermission> groupLevelPermissions) {
		this.groupLevelPermissions = groupLevelPermissions;
	}

	/**
	 * @param groupLevelPermission
	 */
	public void addGroupLevelPermission(GroupLevelPermission groupLevelPermission) {
		this.getGroupLevelPermissions().add(groupLevelPermission);
	}
}