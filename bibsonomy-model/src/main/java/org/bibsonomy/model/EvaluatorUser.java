/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model;

import org.bibsonomy.common.enums.Role;

/**
 * This class defines a user. An unknown user has an empty (<code>null</code>) name.
 * 
 */
public class EvaluatorUser extends User {
	private static final long serialVersionUID = 222500173040042092L;

	/**
	 * The (nick-)name of this user. Is <code>null</code> if the user is not logged in (unknown). 
	 */
	private String evaluator;
	
	private String evalDate;

	/**
	 * Constructor
	 */
	public EvaluatorUser() {
		this(null);
	}
	
	/**
	 * Constructor
	 * 
	 * @param name
	 */
	public EvaluatorUser(final String name) {
		super.setName(name); 
		super.setClipboard(new Clipboard());
		super.setSettings(new UserSettings());
		super.setRole(Role.ADMIN); // TODO: check, if this has any bad implications!
	}

	/**
	 * @return the evaluator
	 */
	public String getEvaluator() {
		return this.evaluator;
	}

	/**
	 * @param evaluator the evaluator to set
	 */
	public void setEvaluator(String evaluator) {
		this.evaluator = evaluator;
	}

	/**
	 * @return the evalDate
	 */
	public String getEvalDate() {
		return this.evalDate;
	}

	/**
	 * @param evalDate the evalDate to set
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate;
	}
}