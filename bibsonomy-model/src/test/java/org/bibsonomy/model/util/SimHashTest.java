/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model.util;

import static org.junit.Assert.assertEquals;

import org.bibsonomy.common.enums.HashID;
import org.bibsonomy.model.BibTex;
import org.bibsonomy.model.util.PersonNameParser.PersonListParserException;
import org.bibsonomy.util.StringUtils;
import org.junit.Test;

/**
 * Testcase for the SimHash class
 * 
 * @author Dominik Benz
 * @author Jens Illig
 * @author Christian Schenk
 */
public class SimHashTest {

	/**
	 * tests getSimHash
	 */
	@Test
	public void getSimHash() {
		final BibTex bibTex = new BibTex();		
		assertEquals("a127fd1f86e4ab650f2216f09992afa4", SimHash.getSimHash(bibTex, HashID.getSimHash(0)));
		assertEquals("23b58def11b45727d3351702515f86af", SimHash.getSimHash(bibTex, HashID.getSimHash(1)));
		assertEquals("7bb0edd98f22430a03b67f853e83c2ca", SimHash.getSimHash(bibTex, HashID.getSimHash(2)));
		assertEquals("", SimHash.getSimHash(bibTex, HashID.getSimHash(3)));
	}
	
	
	/**
	 * Ensure that an empty year and a NULL year don't change the hashes. 
	 */
	@Test
	public void getSimHash2() {
		final BibTex bibTex = new BibTex();
		bibTex.setYear(null);
		assertEquals("a127fd1f86e4ab650f2216f09992afa4", SimHash.getSimHash(bibTex, HashID.getSimHash(0)));
		assertEquals("23b58def11b45727d3351702515f86af", SimHash.getSimHash(bibTex, HashID.getSimHash(1)));
		assertEquals("7bb0edd98f22430a03b67f853e83c2ca", SimHash.getSimHash(bibTex, HashID.getSimHash(2)));
		assertEquals("", SimHash.getSimHash(bibTex, HashID.getSimHash(3)));
		bibTex.setYear("");
		assertEquals("a127fd1f86e4ab650f2216f09992afa4", SimHash.getSimHash(bibTex, HashID.getSimHash(0)));
		assertEquals("23b58def11b45727d3351702515f86af", SimHash.getSimHash(bibTex, HashID.getSimHash(1)));
		assertEquals("7bb0edd98f22430a03b67f853e83c2ca", SimHash.getSimHash(bibTex, HashID.getSimHash(2)));
		assertEquals("", SimHash.getSimHash(bibTex, HashID.getSimHash(3)));
		/*
		 * A hypen "-" should also not change the year.  
		 */
		bibTex.setYear("-");
		assertEquals("a127fd1f86e4ab650f2216f09992afa4", SimHash.getSimHash(bibTex, HashID.getSimHash(0)));
		assertEquals("23b58def11b45727d3351702515f86af", SimHash.getSimHash(bibTex, HashID.getSimHash(1)));
		assertEquals("7bb0edd98f22430a03b67f853e83c2ca", SimHash.getSimHash(bibTex, HashID.getSimHash(2)));
		assertEquals("", SimHash.getSimHash(bibTex, HashID.getSimHash(3)));
		
	}
	
	
	/**
	 * some tests to check author normalization
	 * 
	 * it only makes sense to test this with simHash1 (interhash), because normalization
	 * is not applied for intra-hash computation
	 * @throws PersonListParserException 
	 */
	@Test
	public void testAuthorNormalization() throws PersonListParserException {
		BibTex bib = new BibTex();
		bib.setAuthor(PersonNameUtils.discoverPersonNames("b and A"));
		final String interHash = SimHash.getSimHash(bib, HashID.getSimHash(1));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("B and A"));
		assertEquals(interHash, SimHash.getSimHash(bib, HashID.getSimHash(1)));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("a and b"));
		assertEquals(interHash, SimHash.getSimHash(bib, HashID.getSimHash(1)));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("a and a and b and b and a and B and A and B and a"));
		assertEquals(interHash, SimHash.getSimHash(bib, HashID.getSimHash(1)));
		
		bib.setAuthor(PersonNameUtils.discoverPersonNames("John Paul and Bridget Jones"));
		final String interHash2 = SimHash.getSimHash(bib, HashID.getSimHash(1));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("JoHN pAUl and BrIDgeT JOneS"));
		assertEquals(interHash2, SimHash.getSimHash(bib, HashID.getSimHash(1)));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("J PAUL and B Jones"));
		assertEquals(interHash2, SimHash.getSimHash(bib, HashID.getSimHash(1)));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("PAUL, J and jones, B"));
		assertEquals(interHash2, SimHash.getSimHash(bib, HashID.getSimHash(1)));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("JoHN pAUl and JOneS, brIDgeT"));
		assertEquals(interHash2, SimHash.getSimHash(bib, HashID.getSimHash(1)));
		
		bib.setAuthor(PersonNameUtils.discoverPersonNames("John and John, Paul"));
		final String interHash3 = SimHash.getSimHash(bib, HashID.getSimHash(1));
		bib.setAuthor(PersonNameUtils.discoverPersonNames("JoHN and Paul jOhN"));
		assertEquals(interHash3, SimHash.getSimHash(bib, HashID.getSimHash(1)));
	}
	
	/**
	 * We had a lot of posts with empty author AND editor fields. They caused
	 * problems on several occasions (e.g., model checking in the API). Thus, we
	 * wanted to enter author names that made (somehow) sense such that the fields
	 * were not empty AND most importantly, the hashes did not change.
	 * 
	 * We decided to set the author field in the database to "?", since this does 
	 * not change the hashes, as documented here.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testEmptyAuthor() throws Exception {
		final BibTex pub = new BibTex();
		pub.setTitle("A paper without author name");
		pub.setYear("2011");
		pub.setEntrytype("inbook");
		pub.setBooktitle("Anonymous Publications");
		pub.setJournal("");
		pub.setVolume("42");
		pub.setBibtexKey("key");
		
		pub.setNumber("3");
		/*
		 * old - really "empty" name in the database
		 */
		pub.setAuthor(PersonNameUtils.discoverPersonNames(""));
		
		final String simHash0Empty = SimHash.getSimHash0(pub);
		final String simHash1Empty = SimHash.getSimHash1(pub);
		final String simHash2Empty = SimHash.getSimHash2(pub);
		
		/*
		 * "new" - not so empty name in the database
		 */
		final String emptyPersonName = "?";
		pub.setAuthor(PersonNameUtils.discoverPersonNames(emptyPersonName));
		
		final String simHash0QM = SimHash.getSimHash0(pub);
		final String simHash1QM = SimHash.getSimHash1(pub);
		final String simHash2QM = SimHash.getSimHash2(pub);

		/*
		 * assert that hashes do not change
		 */
		assertEquals(simHash0Empty, simHash0QM);
		assertEquals(simHash1Empty, simHash1QM);
		assertEquals(simHash2Empty, simHash2QM);
		/*
		 * additionally ensure, that the name is correctly serialized back into
		 * the database
		 */
		assertEquals(emptyPersonName, PersonNameUtils.serializePersonNames(pub.getAuthor()));
		
		/*
		 * and last but not least: the ModelValidation utils must accept the name
		 */
		
		ModelValidationUtils.checkPublication(pub);
		
	}
	
	/**
	 * tests person normalization for simhash 2
	 * @throws PersonListParserException 
	 */
	@Test
	public void testPersonNormalizationForSimhash2() throws PersonListParserException {
		assertEquals("Bundesamt für Sicherheit in der Informationstechnik", StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(PersonNameUtils.serializePersonNames(PersonNameUtils.discoverPersonNames(" {Bundesamt für Sicherheit in der Informationstechnik}"))));
	}
	
}