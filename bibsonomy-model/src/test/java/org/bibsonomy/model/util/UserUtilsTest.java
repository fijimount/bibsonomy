/**
 * BibSonomy-Model - Java- and JAXB-Model.
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.model.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bibsonomy.common.enums.Role;
import org.bibsonomy.model.User;
import org.bibsonomy.testutil.TestUtils;
import org.junit.Test;

/**
 * Testcase for the UserUtils class
 * 
 * @author Christian Schenk
 */
public class UserUtilsTest {

	/**
	 * tests generateApiKey
	 */
	@Test
	public void generateApiKey() {
		assertEquals(32, UserUtils.generateApiKey().length());

		/*
		 * generate some keys and make sure that they're all different
		 */
		final Set<String> keys = new HashSet<String>();
		// supporting 2^16 users with different keys should be enough for now
		final int NUMBER_OF_KEYS = (int) Math.pow(2, 16);
		for (int i = 0; i < NUMBER_OF_KEYS; i++) {
			final int oldSize = keys.size();
			keys.add(UserUtils.generateApiKey());
			if (oldSize + 1 != keys.size()) {
				fail("There's a duplicate API key");
			}
		}
	}

	/**
	 * tests generateActivationKey
	 */
	@Test
	public void generateActivationKey() {
		final User user = new User();
		user.setApiKey(UserUtils.generateApiKey());
		user.setIPAddress("0.0.0.0");
		user.setName("testuser");
		assertEquals(32, UserUtils.generateActivationCode(user).length());
		
		/*
		 * generate some keys and make sure that they're all different
		 */
		final Set<String> codes = new HashSet<String>();
		// supporting 2^16 users with different keys should be enough for now
		final int NUMBER_OF_KEYS = (int) Math.pow(2, 16);
		for (int i = 0; i < NUMBER_OF_KEYS; i++) {
			final int oldSize = codes.size();
			codes.add(UserUtils.generateActivationCode(user));
			if (oldSize + 1 != codes.size()) {
				fail("There's a duplicate Activation key");
			}
		}
	}

	/**
	 * tests setGroupsByGroupIDs
	 */
	@Test
	public void setGroupsByGroupIDs() {
		final User user = new User();
		assertEquals(0, user.getGroups().size());
		UserUtils.setGroupsByGroupIDs(user, Arrays.asList(1, 2, 3));
		assertEquals(3, user.getGroups().size());
	}

	/**
	 * tests getListOfGroupIDs
	 */
	@Test
	public void getListOfGroupIDs() {
		final User user = new User();
		UserUtils.setGroupsByGroupIDs(user, Arrays.asList(1, 2, 3));
		assertEquals(3, user.getGroups().size());
		final List<Integer> groups = UserUtils.getListOfGroupIDs(user);
		assertTrue(groups.contains(1));
		assertTrue(groups.contains(2));
		assertTrue(groups.contains(3));
		assertFalse(groups.contains(23));
		assertFalse(groups.contains(42));

		// invalid user object returns an empty list
		assertNotNull(UserUtils.getListOfGroupIDs(null));
		// every user is in the "public" group
		assertEquals(1, UserUtils.getListOfGroupIDs(null).size());
	}

	/**
	 * tests {@link UserUtils#isValidMailAddress(String)}
	 */
	@Test
	public void testIsValidMailAddress() {
		assertTrue(UserUtils.isValidMailAddress("testuser1@bibsonomy.org"));
		assertFalse(UserUtils.isValidMailAddress("bib@bib@bib.com"));
		assertFalse(UserUtils.isValidMailAddress("aa bb@ccc.com"));
		assertFalse(UserUtils.isValidMailAddress("test@bbcom"));
		assertFalse(UserUtils.isValidMailAddress("     "));
		assertFalse(UserUtils.isValidMailAddress("asdasdasdasd"));
		assertTrue(UserUtils.isValidMailAddress("a@b.com"));
		assertFalse(UserUtils.isValidMailAddress("a.b@ccom"));
	}
	
	/**
	 * tests {@link UserUtils#isValidHomePage(java.net.URL)}
	 */
	@Test
	public void testIsValidHomePage() {
		assertTrue(UserUtils.isValidHomePage(null));
		assertTrue(UserUtils.isValidHomePage(TestUtils.createURL("http://bibsonomy.org")));
		assertTrue(UserUtils.isValidHomePage(TestUtils.createURL("https://bibsonomy.org")));
		assertFalse(UserUtils.isValidHomePage(TestUtils.createURL("ftp://bibsonomy.org")));
	}
	
	/**
		 * tests for {@link UserUtils#isExistingUser(User)}
		 * @throws Exception
		 */
	@Test
	public void testIsExistingUser() {
		final User user = new User();
		assertFalse(UserUtils.isExistingUser(user));
		user.setRole(Role.DELETED);
		user.setName("testuser1");
		assertFalse(UserUtils.isExistingUser(user));
		user.setName("");
		assertFalse(UserUtils.isExistingUser(user));
		
		user.setName("testuser2");
		user.setRole(Role.DEFAULT);
		
		assertTrue(UserUtils.isExistingUser(user));
		
		assertFalse(UserUtils.isExistingUser(null));
	}

	@Test
	public void testGetNiceUserName() {
		User user = new User("testuser");
		assertEquals("testuser", UserUtils.getNiceUserName(user, false));
		assertEquals("@testuser", UserUtils.getNiceUserName(user, true));

		user.setRealname("William T. Riker");
		assertEquals("William T. Riker", UserUtils.getNiceUserName(user, true));
	}
}