/**
 * BibSonomy-Database-Common - Helper classes for database interaction
 *
 * Copyright (C) 2006 - 2016 Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               http://www.kde.cs.uni-kassel.de/
 *                           Data Mining and Information Retrieval Group,
 *                               University of Würzburg, Germany
 *                               http://www.is.informatik.uni-wuerzburg.de/en/dmir/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               http://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.database.common;

import static org.bibsonomy.util.ValidationUtils.present;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/** 
 * This is the superclass for all classes that are implementing methods to
 * retrieve data from a database. It provides methods for the interaction with
 * the database, i.e. a lot of convenience methods that return the
 * <em>right</em> results, e.g. not just an Object but a User object or not
 * just a list of Objects but a list of bookmarks. This way a lot of unchecked
 * casting remains in this class and isn't scattered all over the code.
 * 
 * @author Jens Illig
 * @author Christian Schenk
 */
public abstract class AbstractDatabaseManager {

	/**
	 * @param integer
	 * @return the int representation of the provided integer (0 iff null)
	 */
	protected static int saveConvertToint(final Integer integer) {
		return present(integer) ? integer.intValue() : 0;
	}

	/**
	 * Can be used to start a query that retrieves a list of objects of a certain type.
	 * PLEASE NOTE: this methods never returns null, only an empty list if the queryForList returns null
	 */
	protected <T> List<T> queryForList(final String query, final Object param, final Class<T> type, final DBSession session) {
		@SuppressWarnings("unchecked")
		final List<T> list = (List<T>) session.queryForList(query, param);
		return list != null ? list : new LinkedList<>();
	}
	
	/**
	 * short form of queryForList without Type argument
	 * PLEASE NOTE: this methods never returns null, only an empty list if the queryForList returns null
	 */
	protected List<?> queryForList(final String query, final Object param, final DBSession session) {
		return queryForList(query, param, Object.class, session);
	}

	/**
	 * Can be used to start a query that retrieves a single object like a tag or
	 * bookmark but also an int or boolean.<br/>
	 * 
	 * In this case we break the rule to create one method for every return
	 * type, because with a single object it doesn't result in an unchecked
	 * cast.
	 */
	@SuppressWarnings("unchecked")
	protected <T> T queryForObject(final String query, final Object param, final Class<T> type, final DBSession session) {
		return (T) session.queryForObject(query, param);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> T queryForObject(final String query, final Class<T> type, final DBSession session) {
		return (T) session.queryForObject(query, null);
	}

	/**
	 * 
	 * @param query
	 * @param param
	 * @param session
	 * @return the query result
	 */
	protected Object queryForObject(final String query, final Object param, final DBSession session) {
		return this.queryForObject(query, param, Object.class, session);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param query
	 * @param param
	 * @param store
	 * @param session
	 * @return the query result
	 */
	@SuppressWarnings("unchecked")
	protected <T> T queryForObject(final String query, final Object param, final T store, final DBSession session) {
		return (T) session.queryForObject(query, param, store);
	}
	
	@SuppressWarnings("unchecked")
	protected <K, V> Map<K, V> queryForMap(final String query, final Object param, final String key, final String value, final DBSession session) {
	    return (Map<K, V>) session.queryForMap(query, param, key, value);
	}
	
	@SuppressWarnings("unchecked")
	protected <K,V> Map<K,V> queryForMap(final String query, final Object param, final String key, final DBSession session) {
	    final Map<K,V> map = (Map<K,V>) session.queryForMap(query, param, key);
		return map != null ? map : new HashMap<K, V>();
	}
	
	/**
	 * Inserts an object into the database.
	 */
	protected Object insert(final String query, final Object param, final DBSession session) {
		return session.insert(query, param);
	}

	/**
	 * Updates an object in the database.
	 */
	protected void update(final String query, final Object param, final DBSession session) {
		session.update(query, param);
	}

	/**
	 * Deletes an object from the database.
	 */
	protected void delete(final String query, final Object param, final DBSession session) {
		session.delete(query, param);
	}
}